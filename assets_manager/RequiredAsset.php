<?php
namespace app_masukptn\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_masukptn/assets';
    
    public $css = [];
    
    public $js = [];
    
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}