if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            periodeKota: {
                lembar_kode_peserta_dipakai: '',
            },
            pic: {
                jumlah_pendaftar: 0,
                harga_satuan_pendaftar: 0,
                jumlah_volunteer_hari_acara: 0,
                fee_satuan_volunteer: 0,
                picPemasukans: [],
                picPengeluarans: [],
            },
        },
        methods: {
            addPicPemasukan: function() {
                this.pic.picPemasukans.push({
                    id: null,
                    id_pic: null,
                    tanggal: null,
                    keperluan: null,
                    nominal: 0,
                    banyaknya: 0,
                    berasal_dari: null,
                });
            },
            removePicPemasukan: function(i, isNew) {
                if (this.pic.picPemasukans[i].id == null)
                    this.pic.picPemasukans.splice(i, 1);
                else
                    this.pic.picPemasukans[i].id*=-1;
            },
            addPicPengeluaran: function() {
                this.pic.picPengeluarans.push({
                    id: null,
                    id_pic: null,
                    tanggal: null,
                    keperluan: null,
                    nominal: 0,
                    banyaknya: 0,
                    catatan: null,
                });
            },
            removePicPengeluaran: function(i, isNew) {
                if (this.pic.picPengeluarans[i].id == null)
                    this.pic.picPengeluarans.splice(i, 1);
                else
                    this.pic.picPengeluarans[i].id*=-1;
            },
        },
        computed: {
            totalPemasukan: function() {
                totalPemasukan = 0;
                for (var i in this.pic.picPemasukans) {
                    if (!(this.pic.picPemasukans[i].id < 0)) {
                        totalPemasukan += this.pic.picPemasukans[i].nominal * this.pic.picPemasukans[i].banyaknya;
                    }
                }
                return totalPemasukan;
            },
            totalPemasukanPendaftar: function() {
                return this.pic.jumlah_pendaftar * this.pic.harga_satuan_pendaftar;
            },
            totalPemasukanAkhir: function() {
                return this.totalPemasukan + this.totalPemasukanPendaftar;
            },
            totalPengeluaran: function() {
                totalPengeluaran = 0;
                for (var i in this.pic.picPengeluarans) {
                    if (!(this.pic.picPengeluarans[i].id < 0)) {
                        totalPengeluaran += this.pic.picPengeluarans[i].nominal * this.pic.picPengeluarans[i].banyaknya;
                    }
                }
                return totalPengeluaran;
            },
            totalPengeluaranVolunteer: function() {
                return this.pic.jumlah_volunteer_hari_acara * this.pic.fee_satuan_volunteer;
            },
            totalPengeluaranAkhir: function() {
                return this.totalPengeluaran + this.totalPengeluaranVolunteer;
            },
            sisa: function() {
                return this.totalPemasukanAkhir - this.totalPengeluaranAkhir;
            },
        },
        /*destroyed: function () {
            console.log('asdf');
            this.changeHarga();
            for (var i=0; i < this.pic.picPemasukans.length; i++) {
                this.changeHargaTambahan(i);
            }
        }*/
    });
}