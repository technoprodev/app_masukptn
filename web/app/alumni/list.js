$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        $.fn.dataTableExt.oStdClasses.sLength = 'dataTables_length form-wrapper';
        $.fn.dataTableExt.oStdClasses.sLengthSelect = 'form-dropdown rounded-xs';
        var el = $('.datatables-alumni');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                dom: '<"clearfix margin-bottom-30"l><"clearfix margin-bottom-30 scroll-xx"rt><"clearfix"ip>',
                lengthMenu: [[16, 32, 64, 128, -1], ['16 entries', '32 entries', '64 entries', '128 entries', 'All entries']],
                ajax: {
                    url: fn.urlTo('xswzaq/datatables-alumni'),
                    type: 'POST',
                },
                columns: [
                    {
                        data: 'id',
                        className: 'padding-0',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                    '<img src="' + fn.urlTo() + '/upload/alumni-poto/' + data + '/' + row.poto + '" style="width: 100%; height: auto; max-width: 300px;">' +
                                '</div>' +
                                '<div class="margin-top-5"></div>' +
                                '<div>' +
                                    '<span class="bg-orange rounded-lg padding-x-15 padding-y-5 fs-18 fw-bold">' + row.nama + '</span>' +
                                '</div>' +
                                '<div class="margin-top-10"></div>' +
                                '<div>' +
                                    '<span class="bg-azure rounded-md padding-x-10 fs-14 fw-bold">' + row.universitas + '</span>' +
                                '</div>' +
                                '<div>' +
                                    '<span class="text-azure rounded-md padding-x-15 fs-14">' + row.jurusan + '</span>' +
                                '</div>' +
                                '<div class="margin-top-5"></div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
                createdRow: function ( row, data, index ) {
                    $(row).addClass('box-3 margin-bottom-30');
                },
                drawCallback: function(settings) {
                    $('.datatables-alumni thead').remove();
                    technoart.footerFixed();
                },
                language: {
                    lengthMenu : '_MENU_',
                    search: '',
                    searchPlaceholder: 'search all here...',
                    buttons: {
                        copyTitle: 'Title',
                        copyKeys: 'copy keys',
                        copySuccess: {
                            _: '%d rows copied',
                            1: '1 row copied'
                        }
                    },
                    emptyTable: 'No data available'
                }
            }));
            $(table.table().body()).addClass('box box-gutter box-break-md-6 box-equal text-center');

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});