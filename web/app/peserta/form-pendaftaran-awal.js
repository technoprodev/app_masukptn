if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            peserta: {
                id_provinces: '',
                id_regencies: '',
            },
            provinces: [],
            regencies: [],
        },
        methods: {
            onProvinceChange(){
                this.$http.get(fn.urlTo('peserta/get-list-kota', {idProvinces: this.peserta.id_provinces})).then(function(response) {
                        this.regencies = response.body;
                        // console.log(response);
                    }, function(response) {
                        // console.log(response);
                    }
                );
            },
        },
        computed: {
        },
        /*destroyed: function () {
        }*/
    });
}