$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-peserta-misi');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('peserta/datatables-peserta-misi'),
                    type: 'POST',
                },
                dom: '<"clearfix margin-bottom-30"lf><"clearfix margin-bottom-30 scroll-x"rt><"clearfix"ip>',
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            status_konfirmasi = '';
                            if (row.status_konfirmasi == 'Belum Ikut' || row.status_konfirmasi == 'Ditolak') {
                                status_konfirmasi = '<a href="' + fn.urlTo('peserta/peserta-misi-form', {id: data}) + '" modal-md="" modal-title="Ikuti Misi"><i class="fa fa-pencil bg-light-cyan padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            } else {
                                status_konfirmasi = '<a href="' + fn.urlTo('peserta/peserta-misi', {id: data}) + '" modal-md="" modal-title="Detail Misi"><i class="fa fa-eye bg-lighter padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            }
                            console.log(status_konfirmasi);
                                
                            return '<div class="fs-14">' +
                                status_konfirmasi +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'm.judul',
                            display: 'judul',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'pm.status_konfirmasi',
                            display: 'status_konfirmasi',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'm.dari_tanggal',
                            display: 'dari_tanggal',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'm.sampai_tanggal',
                            display: 'sampai_tanggal',
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                if (index == 3) {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') select').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                } else {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                }
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});