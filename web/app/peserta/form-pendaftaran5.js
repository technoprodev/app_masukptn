if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            transaksi: {
                pesertas: [],
            },
            periodeJenises: [],
            provinces: [],
            regencies: [],
        },
        methods: {
            addPeserta: function() {
                this.transaksi.pesertas.push({
                    id: null,
                    id_peserta: null,
                    username: null,
                    password: null,
                    username_teman: null,
                    email: null,
                    nama: null,
                    email: null,
                    handphone: null,
                    id_periode_jenis: '',
                    harga: 0,
                    harga_asli: 0,
                    id_periode_kota: '',
                    jenis_kelamin: '',
                    sekolah: '',
                    id_provinces: '',
                    id_kota: '',
                    alamat: '',
                    facebook: '',
                    twitter: '',
                    instagram: '',
                    line: '',
                    whatsapp: '',
                });
            },
            removePeserta: function(i, isNew) {
                if (this.transaksi.pesertas[i].id == null)
                    this.transaksi.pesertas.splice(i, 1);
                else
                    this.transaksi.pesertas[i].id*=-1;
            },
            changeHarga: function(key) {
                this.transaksi.pesertas[key].harga = (function (search, array, length){
                    if (search == '') {
                        return 0;
                    }
                    for (var i=0; i < array.length; i++) {
                        if (array[i].id === parseInt(search)) {
                            return array[i]['harga_' + length + '_tiket'];
                        }
                    }
                })(this.transaksi.pesertas[key].id_periode_jenis, this.periodeJenises, this.transaksi.pesertas.length);

                this.transaksi.pesertas[key].harga_asli = (function (search, array){
                    if (search == '') {
                        return 0;
                    }
                    for (var i=0; i < array.length; i++) {
                        if (array[i].id === parseInt(search)) {
                            return array[i]['harga_1_tiket'];
                        }
                    }
                })(this.transaksi.pesertas[key].id_periode_jenis, this.periodeJenises);
            },
            onProvinceChange(key){
                this.$http.get(fn.urlTo('peserta/get-list-kota', {idProvinces: this.transaksi.pesertas[key].id_provinces})).then(function(response) {
                        this.regencies = response.body;
                        // console.log(response);
                    }, function(response) {
                        // console.log(response);
                    }
                );
                this.transaksi.pesertas[key].id_kota = '';
            },
        },
        computed: {
            totalTiket: function() {
                totalTiket = 0;
                for (var i in this.transaksi.pesertas) {
                    if (!(this.transaksi.pesertas[i].id < 0)) {
                        totalTiket++;
                    }
                }
                return totalTiket;
            },
            tagihan: function() {
                tagihan = 0;
                for (var i in this.transaksi.pesertas) {
                    if (!(this.transaksi.pesertas[i].id < 0)) {
                        tagihan += this.transaksi.pesertas[i].harga;
                    }
                }
                return tagihan;
            },
            tagihanAsli: function() {
                tagihanAsli = 0;
                for (var i in this.transaksi.pesertas) {
                    if (!(this.transaksi.pesertas[i].id < 0)) {
                        tagihanAsli += this.transaksi.pesertas[i].harga_asli;
                    }
                }
                return tagihanAsli;
            },
        },
    });
}