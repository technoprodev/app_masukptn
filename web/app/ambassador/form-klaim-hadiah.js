if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            virtual_poin: 0,
            virtual_poin_klaim: 0,
            hadiahs: [],
            hadiahsHelper: [],
            hadiahsPoinHelper: [],
            peserta: {
                virtual_hadiah: [],
            },
        },
        methods: {
            pilih: function(value) {
                this.peserta.virtual_hadiah.push(value);
                this.peserta.virtual_poin_klaim += parseInt(this.hadiahsPoinHelper[value]);
            },
            hapus: function(i) {
                this.peserta.virtual_poin_klaim -= parseInt(this.hadiahsPoinHelper[this.peserta.virtual_hadiah[i]]);
                this.peserta.virtual_hadiah.splice(i, 1);
            },
        },
        computed: {
        },
    });
}