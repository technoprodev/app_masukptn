$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-volunteer');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('xswzaq/datatables-volunteer'),
                    type: 'POST',
                    /*data: function (d) {
                        console.log(d);
                        return $.extend(d, {});
                    },*/
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '<div class="fs-14">' +
                                '<a href="' + fn.urlTo('xswzaq/volunteer', {id: data}) + '" modal-md="" modal-title="Detail PIC"><i class="fa fa-eye bg-lighter padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                '<a href="' + fn.urlTo('xswzaq/form-volunteer', {id: data}) + '"><i class="fa fa-pencil bg-light-cyan padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.id_periode',
                            display: 'id_periode',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'v.nama',
                            display: 'volunteer',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'pk.nama',
                            display: 'kota',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama',
                            display: 'pic',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'v.email',
                            display: 'email',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'v.handphone',
                            display: 'handphone',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'v.ukuran_kaos',
                            display: 'ukuran_kaos',
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});