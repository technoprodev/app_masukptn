if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            peserta: {
                id_periode_jenis: '',
                harga: 0,
                harga_asli: 0,
                periode_penjualan: 0,
                pesertaTambahans: [],
            },
            periodeJenises: [],
        },
        methods: {
            addPesertaTambahan: function() {
                this.peserta.pesertaTambahans.push({
                    id: null,
                    id_peserta: null,
                    nama: null,
                    email: null,
                    handphone: null,
                    id_periode_jenis: '',
                    harga: 0,
                    harga_asli: 0,
                    periode_penjualan: 0,
                    id_periode_kota: '',
                    facebook: '',
                    twitter: '',
                    instagram: '',
                    line: '',
                    whatsapp: '',
                });
            },
            removePesertaTambahan: function(i, isNew) {
                if (this.peserta.pesertaTambahans[i].id == null)
                    this.peserta.pesertaTambahans.splice(i, 1);
                else
                    this.peserta.pesertaTambahans[i].id*=-1;
            },
            changeHarga: function() {
                this.peserta.harga = (function (search, array, length){
                    if (search == '') {
                        return 0;
                    }
                    for (var i=0; i < array.length; i++) {
                        if (array[i].id === parseInt(search)) {
                            return array[i]['harga_' + length + '_tiket'];
                        }
                    }
                })(this.peserta.id_periode_jenis, this.periodeJenises, this.peserta.pesertaTambahans.length + 1);

                this.peserta.harga_asli = (function (search, array, length){
                    if (search == '') {
                        return 0;
                    }
                    for (var i=0; i < array.length; i++) {
                        if (array[i].id === parseInt(search)) {
                            return array[i]['harga_1_tiket'];
                        }
                    }
                })(this.peserta.id_periode_jenis, this.periodeJenises);
            },
            changeHargaTambahan: function(key) {
                this.peserta.pesertaTambahans[key].harga = (function (search, array, length){
                    if (search == '') {
                        return 0;
                    }
                    for (var i=0; i < array.length; i++) {
                        if (array[i].id === parseInt(search)) {
                            return array[i]['harga_' + length + '_tiket'];
                        }
                    }
                })(this.peserta.pesertaTambahans[key].id_periode_jenis, this.periodeJenises, this.peserta.pesertaTambahans.length + 1);

                this.peserta.pesertaTambahans[key].harga_asli = (function (search, array){
                    if (search == '') {
                        return 0;
                    }
                    for (var i=0; i < array.length; i++) {
                        if (array[i].id === parseInt(search)) {
                            return array[i]['harga_1_tiket'];
                        }
                    }
                })(this.peserta.pesertaTambahans[key].id_periode_jenis, this.periodeJenises);
            },
        },
        computed: {
            totalTiket: function() {
                totalTiket = 1;
                for (var i in this.peserta.pesertaTambahans) {
                    if (!(this.peserta.pesertaTambahans[i].id < 0)) {
                        totalTiket++;
                    }
                }
                return totalTiket;
            },
            tagihan: function() {
                tagihan = this.peserta.harga;
                for (var i in this.peserta.pesertaTambahans) {
                    if (!(this.peserta.pesertaTambahans[i].id < 0)) {
                        tagihan += this.peserta.pesertaTambahans[i].harga;
                    }
                }
                return tagihan;
            },
            tagihanAsli: function() {
                tagihanAsli = this.peserta.harga_asli;
                for (var i in this.peserta.pesertaTambahans) {
                    if (!(this.peserta.pesertaTambahans[i].id < 0)) {
                        tagihanAsli += this.peserta.pesertaTambahans[i].harga_asli;
                    }
                }
                return tagihanAsli;
            },
        },
        /*destroyed: function () {
            console.log('asdf');
            this.changeHarga();
            for (var i=0; i < this.peserta.pesertaTambahans.length; i++) {
                this.changeHargaTambahan(i);
            }
        }*/
    });
}