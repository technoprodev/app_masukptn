$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('xswzaq/datatables-referral-agent'),
                    type: 'POST',
                    /*data: function (d) {
                        console.log(d);
                        return $.extend(d, {});
                    },*/
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            status = '';
                            if (row.status == 'Sedang Aktif') {
                                status = '<a href="' + fn.urlTo('xswzaq/nonaktifkan-referral-agent', {id: data}) + '" data-confirm="Apakah Anda yakin menonaktifkan referral agent ini ?" data-method="post"><i class="fa fa-toggle-on bg-light-azure padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            } else {
                                status = '<a href="' + fn.urlTo('xswzaq/aktifkan-referral-agent', {id: data}) + '" data-confirm="Apakah Anda yakin mengaktifkan referral agent ini ?" data-method="post"><i class="fa fa-toggle-off bg-light-red padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            }
                                
                            return '<div class="fs-14">' +
                                '<a href="' + fn.urlTo('xswzaq/referral-agent', {id: data}) + '" modal-md="" modal-title="Detail Referral Agent"><i class="fa fa-eye bg-lighter padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                '<a href="' + fn.urlTo('xswzaq/form-referral-agent', {id: data}) + '"><i class="fa fa-pencil bg-light-cyan padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                status +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.id_periode',
                            display: 'id_periode',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.nama',
                            display: 'nama',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'rap.count_peserta',
                            display: 'jml_peserta',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.kode',
                            display: 'kode',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.handphone',
                            display: 'handphone',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.fee',
                            display: 'fee',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.program',
                            display: 'program',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.sekolah',
                            display: 'sekolah',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.name',
                            display: 'lokasi',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.email',
                            display: 'email',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.nomor_identitas',
                            display: 'nomor_identitas',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.alamat_lengkap',
                            display: 'alamat_lengkap',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'ra.catatan',
                            display: 'catatan',
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                if (false && index == 3) {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') select').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                } else {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                }
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});