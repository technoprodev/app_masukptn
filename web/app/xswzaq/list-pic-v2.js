$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('xswzaq/datatables-pic'),
                    type: 'POST',
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            status = '';
                            if (row.status == 'Sedang Aktif') {
                                status = '<a href="' + fn.urlTo('xswzaq/nonaktifkan-pic', {id: data}) + '" data-confirm="Apakah Anda yakin menonaktifkan pic ini ?" data-method="post"><i class="fa fa-toggle-on bg-light-azure padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            } else {
                                status = '<a href="' + fn.urlTo('xswzaq/aktifkan-pic', {id: data}) + '" data-confirm="Apakah Anda yakin mengaktifkan pic ini ?" data-method="post"><i class="fa fa-toggle-off bg-light-red padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            }
                                
                            return '<div class="fs-14">' +
                                '<a href="' + fn.urlTo('xswzaq/pic', {id: data}) + '" modal-md="" modal-title="Detail PIC"><i class="fa fa-eye bg-lighter padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                '<a href="' + fn.urlTo('xswzaq/pic-laporan-keuangan', {id: data}) + '" target="_blank"><i class="fa fa-money bg-light-orange padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                '<a href="' + fn.urlTo('xswzaq/form-pic', {id: data}) + '"><i class="fa fa-pencil bg-light-cyan padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                status +
                                '<a href="' + fn.urlTo('xswzaq/hapus-pic', {id: data}) + '" data-confirm="Apakah Anda yakin menghapus pic ini dan seluruh volunteernya SELAMANYA ?" data-method="post"><i class="fa fa-trash bg-light-red padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>'
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.id_periode',
                            display: 'id_periode',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'pk.nama',
                            display: 'kota',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'pp.count_volunteer',
                            display: 'jml_volunteer',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.kode',
                            display: 'kode',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.username',
                            display: 'username',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama',
                            display: 'nama',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.email',
                            display: 'email',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.handphone',
                            display: 'handphone',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.whatsapp',
                            display: 'whatsapp',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nomor_rekening',
                            display: 'nomor_rekening',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama_bank',
                            display: 'nama_bank',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.atas_nama',
                            display: 'atas_nama',
                        },
                        defaultContent: '&nbsp;',
                    },
                    /*{
                        data: {
                            _: 'p.alamat_pengiriman',
                            display: 'alamat_pengiriman',
                        },
                        defaultContent: '&nbsp;',
                    },*/
                    {
                        data: {
                            _: 'p.ukuran_kaos',
                            display: 'ukuran_kaos',
                        },
                        defaultContent: '&nbsp;',
                    },
                    /*{
                        data: {
                            _: 'p.rangkuman_kegiatan',
                            display: 'rangkuman_kegiatan',
                        },
                        defaultContent: '&nbsp;',
                    },*/
                    /*{
                        data: {
                            _: 'p.link_foto',
                            display: 'link_foto',
                        },
                        defaultContent: '&nbsp;',
                    },*/
                    /*{
                        data: {
                            _: 'p.laporan_keuangan',
                            display: 'laporan_keuangan',
                        },
                        defaultContent: '&nbsp;',
                    },*/
                    {
                        data: {
                            _: 'p.alamat_pengiriman',
                            display: 'alamat_pengiriman',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama_penerima',
                            display: 'nama_penerima',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.telpon_penerima',
                            display: 'telpon_penerima',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.acara_lain_diluar_tryout',
                            display: 'acara_lain_diluar_tryout',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.laporan_keuangan_sudah_final',
                            display: 'laporan_keuangan_sudah_final',
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                if (index == 3) {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') select').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                } else {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                }
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});