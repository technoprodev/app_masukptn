$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-alumni');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('xswzaq/datatables-alumni'),
                    type: 'POST',
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            /*status = '';
                            if (row.status == 'Sedang Aktif') {
                                status = '<a href="' + fn.urlTo('xswzaq/nonaktifkan-alumni', {id: data}) + '" data-confirm="Apakah Anda yakin menonaktifkan alumni ini ?" data-method="post"><i class="fa fa-toggle-on bg-light-azure padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            } else {
                                status = '<a href="' + fn.urlTo('xswzaq/aktifkan-alumni', {id: data}) + '" data-confirm="Apakah Anda yakin mengaktifkan alumni ini ?" data-method="post"><i class="fa fa-toggle-off bg-light-red padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>';
                            }*/
                                
                            return '<div class="fs-14">' +
                                '<a href="' + fn.urlTo('xswzaq/detail-alumni', {id: data}) + '" modal-md="" modal-title="Detail Alumni"><i class="fa fa-eye bg-lighter padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                '<a href="' + fn.urlTo('xswzaq/update-alumni', {id: data}) + '"><i class="fa fa-pencil bg-light-cyan padding-x-10 padding-y-5 rounded-md margin-right-5"></i></a>' +
                                // status +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'nama',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="fw-bold">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'universitas',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'jurusan',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: 'angkatan',
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});