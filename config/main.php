<?php
$params = [
    'app.name' => 'MasukPTNid',
    'app.description' => 'Tryout Nasional MasukPTNid SBMPTN 2020',
    'app.owner' => 'MasukPTNid',
    'app.author' => 'MasukPTNid',
    'user.passwordResetTokenExpire' => 3600,
    'email.noreply' => 'noreply@masukptn.id',
    'emailName.noreply' => 'MasukPTNid',
    'enable.permission-checking' => true,
    'enable.permission-init' => false,
];

$config = [
    'id' => 'app_masukptn',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_masukptn\controllers',
    'bootstrap' => [
        'log',
        function() {
            Yii::setAlias('@download-transaksi-bukti_pembayaran', Yii::$app->getRequest()->getBaseUrl() . '/upload/transaksi-bukti_pembayaran');
            Yii::setAlias('@upload-transaksi-bukti_pembayaran', dirname(dirname(__DIR__)) . '/app_masukptn/web/upload/transaksi-bukti_pembayaran');
            Yii::setAlias('@download-peserta-poto', Yii::$app->getRequest()->getBaseUrl() . '/upload/peserta-poto');
            Yii::setAlias('@upload-peserta-poto', dirname(dirname(__DIR__)) . '/app_masukptn/web/upload/peserta-poto');
            Yii::setAlias('@download-pic-laporan_keuangan', Yii::$app->getRequest()->getBaseUrl() . '/upload/pic-laporan_keuangan');
            Yii::setAlias('@upload-pic-laporan_keuangan', dirname(dirname(__DIR__)) . '/app_masukptn/web/upload/pic-laporan_keuangan');
            Yii::setAlias('@download-pic-file_khusus', Yii::$app->getRequest()->getBaseUrl() . '/upload/pic-file_khusus');
            Yii::setAlias('@upload-pic-file_khusus', dirname(dirname(__DIR__)) . '/app_masukptn/web/upload/pic-file_khusus');
            Yii::setAlias('@download-pic_umum-file_umum', Yii::$app->getRequest()->getBaseUrl() . '/upload/pic_umum-file_umum');
            Yii::setAlias('@upload-pic_umum-file_umum', dirname(dirname(__DIR__)) . '/app_masukptn/web/upload/pic_umum-file_umum');
            Yii::setAlias('@download-promo-picture', Yii::$app->getRequest()->getBaseUrl() . '/upload/promo-picture');
            Yii::setAlias('@upload-promo-picture', dirname(dirname(__DIR__)) . '/app_masukptn/web/upload/promo-picture');
            Yii::setAlias('@download-alumni-poto', Yii::$app->getRequest()->getBaseUrl() . '/upload/alumni-poto');
            Yii::setAlias('@upload-alumni-poto', dirname(dirname(__DIR__)) . '/app_masukptn/web/upload/alumni-poto');
            Yii::setAlias('@download-peserta_misi-poto', Yii::$app->getRequest()->getBaseUrl() . '/upload/peserta_misi-poto');
            Yii::setAlias('@upload-peserta_misi-poto', dirname(dirname(__DIR__)) . '/app_masukptn/web/upload/peserta_misi-poto');
        },
    ],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_masukptn',
        ],
        'user' => [
            'identityClass' => 'app_tryout\models\Peserta',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_masukptn', 'httpOnly' => true],
        ],
        'userAdmin' => [
            'class'=>'yii\web\User',
            'idParam'=>'userAdmin',
            'identityClass' => 'app_tryout\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_masukptn-userAdmin', 'httpOnly' => true],
        ],
        'userReferral' => [
            'class'=>'yii\web\User',
            'idParam'=>'userReferral',
            'identityClass' => 'app_tryout\models\ReferralAgent',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_masukptn-userReferral', 'httpOnly' => true],
        ],
        'userPic' => [
            'class'=>'yii\web\User',
            'idParam'=>'userPic',
            'identityClass' => 'app_tryout\models\Pic',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_masukptn-userPic', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_masukptn',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;