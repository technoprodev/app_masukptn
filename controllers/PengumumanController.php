<?php
namespace app_masukptn\controllers;

use Yii;
use app_tryout\models\Peserta;
use technosmart\yii\web\Controller;
use yii\helpers\ArrayHelper;

class PengumumanController extends Controller
{
    public function actionIndex($kode = null, $email = null)
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['peserta/login']);


        $model['peserta'] = Peserta::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $model['transaksi'] = $model['peserta']->transaksi;

        return $this->render('index', [
            'model' => $model,
            'title' => 'Pengumuman',
        ]);
    }
}