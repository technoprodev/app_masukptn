<?php
namespace app_masukptn\controllers;

use Yii;
use app_tryout\models\Transaksi;
use app_tryout\models\Peserta;
use app_tryout\models\Periode;
use app_tryout\models\PeriodeJenis;
use app_tryout\models\PeriodeKota;
use app_tryout\models\Periode3Tokped;
use app_tryout\models\LoginPeserta;
use technosmart\yii\web\Controller;
use yii\helpers\ArrayHelper;

class GeneralController extends Controller
{
    public function actionKota()
	{
		return $this->render('kota', [
            'title' => 'Lokasi Ujian',
        ]);
	}
}