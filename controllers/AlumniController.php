<?php
namespace app_masukptn\controllers;

use Yii;
use technosmart\yii\web\Controller;
use yii\helpers\ArrayHelper;

class AlumniController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index', [
            'title' => 'Alumni',
        ]);
    }
}