<?php
namespace app_masukptn\controllers;

use Yii;
use technosmart\yii\web\Controller;
use technosmart\models\User;
use technosmart\models\Login;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['logout'], true, ['@'], ['POST']],
            ]),
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionRedirectSlash($url = '')
    {
        $newUrl = rtrim($url, '/');
        if ($newUrl) $newUrl = '/' . $newUrl;
        return $this->redirect(Yii::$app->getRequest()->getBaseUrl() . $newUrl, 301);
    }
    
    //
    
    public function actionIndex()
    {
        return $this->render('index', [
            'title' => 'Beranda',
        ]);
    }

    public function actionLogin()
    {
        return $this->redirect(['index']);
    }

    public function actionLogout()
    {
        return $this->redirect(['index']);
    }
}