<?php
namespace app_masukptn\controllers;

use Yii;
use app_tryout\models\Peserta;
use app_tryout\models\PesertaTambahan;
use app_tryout\models\Periode;
use app_tryout\models\PeriodeJenis;
use app_tryout\models\PeriodeKota;
use app_tryout\models\Sumber;
use app_tryout\models\Jurusan;
use technosmart\yii\web\Controller;
use yii\helpers\ArrayHelper;

class PengumumanMptn21Apr19Controller extends Controller
{
    /*public static $permissions = [
        'create'
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'view', 'create'], 'create'],
            ]),
        ];
    }*/

    protected function findModelPesertaByKodeEmail($kode, $email)
    {
        if (($model['peserta'] = Peserta::find()->where(['kode' => $kode, 'email' => $email, 'status_aktif' => 'Aktif', 'status_bayar' => 'Sudah Bayar', 'id_periode' => '2'])->one()) !== null) {
            return $model['peserta'];
        } else if (($model['peserta'] = PesertaTambahan::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null && $model['peserta']->peserta->status_aktif == 'Aktif' && $model['peserta']->peserta->status_bayar == 'Sudah Bayar' && $model['peserta']->peserta->id_periode == '2') {
            return $model['peserta'];
        } else {
            $model['peserta'] = new Peserta();
            $model['peserta']->kode = $kode;
            $model['peserta']->email = $email;
            $model['peserta']->validate(['kode', 'email']);
            if (!$model['peserta']->hasErrors()) {
                if ((Peserta::find()->where(['kode' => $kode, 'email' => $email])->andWhere(['<>', 'id_periode', '1'])->one()) !== null ||
                    (PesertaTambahan::find()->join('INNER JOIN', 'peserta p', 'p.id = peserta_tambahan.id_peserta')->where(['peserta_tambahan.kode' => $kode, 'peserta_tambahan.email' => $email])->andWhere(['<>', 'p.id_periode', '1'])->one()) !== null
                ) {
                    $model['peserta']->addErrors([
                        'kode' => 'Kamu bukan peserta part 1 sehingga tidak diperbolehkan mengakses fitur ini.',
                    ]);
                } else if ((Peserta::find()->where(['kode' => $kode, 'email' => $email, 'status_aktif' => 'Nonaktif'])->one()) !== null ||
                    (PesertaTambahan::find()->join('INNER JOIN', 'peserta p', 'p.id = peserta_tambahan.id_peserta')->where(['peserta_tambahan.kode' => $kode, 'peserta_tambahan.email' => $email, 'p.status_aktif' => 'Nonaktif'])->one()) !== null
                ) {
                    $model['peserta']->addErrors([
                        'kode' => 'Peserta telah dinonaktifkan oleh Admin.',
                    ]);
                } else if ((Peserta::find()->where(['kode' => $kode, 'email' => $email, 'status_bayar' => 'Belum Bayar'])->one()) !== null ||
                    (PesertaTambahan::find()->join('INNER JOIN', 'peserta p', 'p.id = peserta_tambahan.id_peserta')->where(['peserta_tambahan.kode' => $kode, 'peserta_tambahan.email' => $email, 'p.status_bayar' => 'Belum Bayar'])->one()) !== null
                ) {
                    $model['peserta']->addErrors([
                        'kode' => 'Kamu tidak terdaftar sebagai peserta karena belum melakukan pembayaran.',
                    ]);
                } else {
                    $notFound = false;
                    if ((Peserta::find()->where(['kode' => $kode])->one()) == null && (PesertaTambahan::find()->where(['kode' => $kode])->one()) == null) {
                        $model['peserta']->addErrors([
                            'kode' => 'Kode peserta tidak ditemukan di periode sekarang yang sedang aktif',
                        ]);
                        $notFound = true;
                    }
                    if ((Peserta::find()->where(['email' => $email])->one()) == null && (PesertaTambahan::find()->where(['email' => $email])->one()) == null) {
                        $model['peserta']->addErrors([
                            'email' => 'Email peserta tidak ditemukan di periode sekarang yang sedang aktif',
                        ]);
                        $notFound = true;
                    }
                    if (!$notFound) {
                        $model['peserta']->addErrors([
                            'kode' => 'Kombinasi kode dan email tidak ditemukan',
                            'email' => 'Kombinasi email dan kode tidak ditemukan',
                        ]);
                    }
                }
            }
            /*if ($post) {
                $model['peserta']->load($post);
            }*/
            return $model['peserta'];
        }
    }

    public function actionIndex($kode = null, $email = null)
    {
        $error = true;

        $model['peserta'] = new Peserta();
        $periode = Periode::getPeriodeAktif();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if (isset($post['Peserta']['kode']) && isset($post['Peserta']['email'])) {
                $model['peserta'] = $this->findModelPesertaByKodeEmail($post['Peserta']['kode'], $post['Peserta']['email']);

                $error = false;
            } else if ($kode || $email) {
                $model['peserta'] = $this->findModelPesertaByKodeEmail($kode, $email);

                if (!$model['peserta']->isNewRecord) {
                    Yii::$app->params['logout'] = true;
                    Yii::$app->params['kode'] = $kode;
                    Yii::$app->params['email'] = $email;
                }

                if ($model['peserta']->hasErrors()) {
                    if (array_key_exists('kode', $model['peserta']->errors) && $model['peserta']->errors['kode'][0] == 'Peserta telah dinonaktifkan oleh Admin.')
                        Yii::$app->session->setFlash('error', 'Peserta telah dinonaktifkan oleh Admin karena terlalu lama tidak melakukan pembayaran. Harap mendaftar kembali dan segera lakukan pembayaran untuk mengamankan kursi kamu di tryout MasukPTNid.');
                    else
                        Yii::$app->session->setFlash('error', 'Data peserta tidak ditemukan.');
                }

                $model['peserta']->load($post);

                $transaction = Peserta::getDb()->beginTransaction();

                try {
                    $model['peserta']->status_survey_pengumuman = 'Sudah Isi';
                    if (!$model['peserta']->save()) {
                        throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                    }

                    $error = false;

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Terima kasih telah mengisi survey. Berikut Pengumuman soal MasukPTNid.</div>');
                } catch (\Throwable $e) {
                    $error = true;
                    $transaction->rollBack();
                    if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
                }
            }
        } else {
            if ($kode || $email) {
                $model['peserta'] = $this->findModelPesertaByKodeEmail($kode, $email);

                if (!$model['peserta']->isNewRecord) {
                    Yii::$app->params['logout'] = true;
                    Yii::$app->params['kode'] = $kode;
                    Yii::$app->params['email'] = $email;
                }

                if ($model['peserta']->hasErrors()) {
                    if (array_key_exists('kode', $model['peserta']->errors) && $model['peserta']->errors['kode'][0] == 'Peserta telah dinonaktifkan oleh Admin.')
                        Yii::$app->session->setFlash('error', 'Peserta telah dinonaktifkan oleh Admin karena terlalu lama tidak melakukan pembayaran. Harap mendaftar kembali dan segera lakukan pembayaran untuk mengamankan kursi kamu di tryout MasukPTNid.');
                    else
                        Yii::$app->session->setFlash('error', 'Data peserta tidak ditemukan.');
                }
            } else {
                $newSearch = true;
            }
        }

        if ($error)
            return $this->render('//pengumuman/form-index', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'newSearch' => isset($newSearch) ? $newSearch : false,
                'title' => 'Pengumuman',
            ]);
        else
            return $this->redirect(['index', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email]);
    }
}