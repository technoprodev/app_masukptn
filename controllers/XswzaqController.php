<?php
namespace app_masukptn\controllers;

use Yii;
use app_tryout\models\Transaksi;
use app_tryout\models\Peserta;
use app_tryout\models\Periode;
use app_tryout\models\PeriodeKota;
use app_tryout\models\PeriodeJenis;
use app_tryout\models\Alumni;
use app_tryout\models\Promo;
use app_tryout\models\Sumber;
use app_tryout\models\Jurusan;
use app_tryout\models\Pic;
use app_tryout\models\PicUmum;
use app_tryout\models\Volunteer;
use app_tryout\models\ReferralAgent;
use app_tryout\models\Grafik;
use app_tryout\models\LoginAdmin;
use technosmart\yii\web\Controller;
use yii\helpers\ArrayHelper;

class XswzaqController extends Controller
{
    public $layout = 'xswzaq';

    public static $permissions = [
        'dashboard', 'peserta', 'setting', 'pic', 'referral'
    ];

    public function disabledbehaviors()
    {
        return [
            'access' => $this->access([
                [['login'], true],
                [[
                    'datatables-statistik',
                    'index',
                    'omset',
                ], 'dashboard'],
                [[
                    'datatables-peserta',
                    'peserta',
                    'konfirmasi',
                    'detail-peserta',
                    'update-peserta',
                    'tiket-baru',
                    'aktifkan-peserta',
                    'nonaktifkan-peserta',
                    'aktifkan-duta',
                    'nonaktifkan-duta',
                    'resend-pendaftaran',
                    'resend-konfirmasi',
                    'resend-diterima',
                    'resend-ditolak',
                ], 'peserta'],
                [[
                    'setting',
                    'datatables-periode-kota',
                    'update-periode-kota',
                    'create-periode-kota',
                    'aktifkan-periode-kota',
                    'nonaktifkan-periode-kota',
                    'detail-periode-kota',
                    'get-list-kota',
                    'datatables-periode-jenis',
                    'update-periode-jenis',
                    'create-periode-jenis',
                    'aktifkan-periode-jenis',
                    'nonaktifkan-periode-jenis',
                    'detail-periode-jenis',
                ], 'setting'],
                [[
                    'pic',
                ], 'pic'],
                [[
                    'referral-agent',
                ], 'referral'],
            ], null, 'userAdmin'),
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if ($action->id != 'login') {
            if (Yii::$app->userAdmin->isGuest) return false;
        }

        return true;
    }

    protected function findModelPeserta($id)
    {
        if (($model = Peserta::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPesertaTambahan($id)
    {
        if (($model = PesertaTambahan::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPesertaAktifkan($id)
    {
        if (($model = Peserta::find()->where(['id' => $id, 'status_aktif' => 'Nonaktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPesertaNonaktifkan($id)
    {
        if (($model = Peserta::find()->where(['id' => $id])->one()) !== null && $model->transaksi->status_aktif == 'Aktif') {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        }
    }

    protected function findModelDutaAktifkan($id)
    {
        if (($model = Peserta::find()->where(['id' => $id, 'status_duta' => 'Tidak Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data duta tidak ditemukan.');
        }
    }

    protected function findModelDutaNonaktifkan($id)
    {
        if (($model = Peserta::find()->where(['id' => $id, 'status_duta' => 'Sudah Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data duta tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKota($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKotaAktifkan($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id, 'status' => 'Tidak Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKotaNonaktifkan($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id, 'status' => 'Sedang Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenis($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisAktifkan($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id, 'status' => 'Tidak Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisNonaktifkan($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id, 'status' => 'Sedang Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisByPeriode($id)
    {
        if (($model = PeriodeJenis::find()->where(['id_periode' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Kode kota tidak ditemukan.');
        }
    }

    protected function findModelAlumni($id)
    {
        if (($model = Alumni::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Alumni tidak ditemukan.');
        }
    }

    protected function findModelPromo($id)
    {
        if (($model = Promo::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPromoAktifkan($id)
    {
        if (($model = Promo::find()->where(['id' => $id, 'status' => 'Tidak Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data kota tidak ditemukan.');
        }
    }

    protected function findModelPromoNonaktifkan($id)
    {
        if (($model = Promo::find()->where(['id' => $id, 'status' => 'Sedang Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data kota tidak ditemukan.');
        }
    }

    protected function findModelPeriode($id)
    {
        if (($model = Periode::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Periode tidak ditemukan.');
        }
    }

    protected function findModelPic($id)
    {
        if (($model = Pic::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data pic tidak ditemukan.');
        }
    }

    protected function findModelPicAktifkan($id)
    {
        if (($model = Pic::find()->where(['id' => $id, 'status' => 'Tidak Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data pic tidak ditemukan.');
        }
    }

    protected function findModelPicNonaktifkan($id)
    {
        if (($model = Pic::find()->where(['id' => $id, 'status' => 'Sedang Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data pic tidak ditemukan.');
        }
    }

    protected function findModelVolunteer($id)
    {
        if (($model = Volunteer::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data volunteer tidak ditemukan.');
        }
    }

    protected function findModelReferralAgent($id)
    {
        if (($model = ReferralAgent::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data referral agent tidak ditemukan.');
        }
    }

    protected function findModelReferralAgentAktifkan($id)
    {
        if (($model = ReferralAgent::find()->where(['id' => $id, 'status' => 'Tidak Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data referral agent tidak ditemukan.');
        }
    }

    protected function findModelReferralAgentNonaktifkan($id)
    {
        if (($model = ReferralAgent::find()->where(['id' => $id, 'status' => 'Sedang Aktif'])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data referral agent tidak ditemukan.');
        }
    }

    //////////////

    public function actionLogin()
    {
        if (!Yii::$app->userAdmin->isGuest) {
            if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
                return $this->redirect(['xswzaq/index']);
            if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['operator']))
                return $this->redirect(['xswzaq/peserta']);
        }

        $model['login'] = new LoginAdmin(['scenario' => 'using-login']);
        
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
                return $this->redirect(['xswzaq/index']);
            if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['operator']))
                return $this->redirect(['xswzaq/peserta']);
            Yii::$app->userAdmin->logout();
            Yii::$app->session->setFlash('success', 'Your role is not exists in system yet, please try login using another account.');
            return $this->redirect(['site/index']);
        } else {
            $this->layout = 'auth';
            return $this->render('login', [
                'model' => $model,
                'title' => 'Login',
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->userAdmin->logout();
        return $this->goHome();
    }

    //////////////

    public function getAll()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'count(distinct t.id) as total_transaksi',
                'count(distinct case when t.status_bayar = "Belum Bayar" then t.id end) as transaksi_belum_bayar',
                'count(distinct case when t.status_bayar = "Dalam Proses Konfirmasi" then t.id end) as transaksi_dalam_proses_konfirmasi',
                'count(distinct case when t.status_bayar = "Sudah Bayar" then t.id end) as transaksi_sudah_bayar',
                'count(distinct case when t.status_bayar = "Ditolak" then t.id end) as transaksi_ditolak',

                'count(p.id) as total_tiket',
                'count(case when t.status_bayar = "Belum Bayar" then p.id end) as tiket_belum_bayar',
                'count(case when t.status_bayar = "Dalam Proses Konfirmasi" then p.id end) as tiket_dalam_proses_konfirmasi',
                'count(case when t.status_bayar = "Sudah Bayar" then p.id end) as tiket_sudah_bayar',
                'count(case when t.status_bayar = "Ditolak" then p.id end) as tiket_ditolak',

                'count(case when pj.nama = "Saintek (IPA)" and t.status_bayar = "Sudah Bayar" then p.id end) as tiket_saintek_ipa',
                'count(case when pj.nama = "Soshum (IPS)" and t.status_bayar = "Sudah Bayar" then p.id end) as tiket_soshum_ips',
                
                'count(case when t.status_bayar = "Sudah Bayar" and p.harga = 0 then p.id end) as total_tiket_gratis',

                '(0) as duta_transaksi',
                '(0) as duta_tiket',
                '(0) as referral_tiket',
                // 'count(distinct case when status_duta = "Sudah Aktif" then t.id end) as duta_transaksi',
                // 'count(case when id_duta IS NOT NULL and t.status_bayar = "Sudah Bayar" then p.id end) + count(distinct case when id_duta IS NOT NULL and t.status_bayar = "Sudah Bayar" then t.id end) as duta_tiket',
                // 'count(case when kode_referral_agent IS NOT NULL and t.status_bayar = "Sudah Bayar" then p.id end) + count(distinct case when kode_referral_agent IS NOT NULL and t.status_bayar = "Sudah Bayar" then t.id end) as referral_tiket',
            ])
            ->from('transaksi t')
            ->join('JOIN', 'peserta p', 'p.id_transaksi = t.id AND t.id_periode = :idPeriode AND t.status_aktif = "Aktif"')
            ->join('RIGHT JOIN', 'periode_kota pk', 'pk.id = p.id_periode_kota')
            ->join('LEFT JOIN', 'periode_jenis pj', 'pj.id = p.id_periode_jenis')
            ->where([
                'pk.id_periode' => Periode::getPeriodeDashboard()->id,
            ])
            ->addParams([':idPeriode' => Periode::getPeriodeDashboard()->id])
            ;
        return $query->one();
    }

    public function getAllDetail()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'pk.nama as kota',

                'count(distinct t.id) as total_transaksi',
                'count(distinct case when t.status_bayar = "Belum Bayar" then t.id end) as transaksi_belum_bayar',
                'count(distinct case when t.status_bayar = "Dalam Proses Konfirmasi" then t.id end) as transaksi_dalam_proses_konfirmasi',
                'count(distinct case when t.status_bayar = "Sudah Bayar" then t.id end) as transaksi_sudah_bayar',
                'count(distinct case when t.status_bayar = "Ditolak" then t.id end) as transaksi_ditolak',

                'count(p.id) as total_tiket',
                'count(case when t.status_bayar = "Belum Bayar" then p.id end) as tiket_belum_bayar',
                'count(case when t.status_bayar = "Dalam Proses Konfirmasi" then p.id end) as tiket_dalam_proses_konfirmasi',
                'count(case when t.status_bayar = "Sudah Bayar" then p.id end) as tiket_sudah_bayar',
                'count(case when t.status_bayar = "Ditolak" then p.id end) as tiket_ditolak',

                'count(case when pj.nama = "Saintek (IPA)" and t.status_bayar = "Sudah Bayar" then p.id end) as tiket_saintek_ipa',
                'count(case when pj.nama = "Soshum (IPS)" and t.status_bayar = "Sudah Bayar" then p.id end) as tiket_soshum_ips',
                
                'count(case when t.status_bayar = "Sudah Bayar" and p.harga = 0 then p.id end) as total_tiket_gratis',

                '(0) as duta_transaksi',
                '(0) as duta_tiket',
                '(0) as referral_tiket',

                'ROUND(count(case when t.status_bayar = "Sudah Bayar" then p.id end) / pk.kuota * 100, 2) as tiket_sudah_bayar_persentase',
                'FORMAT(pk.kuota, 0) as kuota',
            ])
            ->from('transaksi t')
            ->join('JOIN', 'peserta p', 'p.id_transaksi = t.id AND t.id_periode = :idPeriode AND t.status_aktif = "Aktif"')
            ->join('RIGHT JOIN', 'periode_kota pk', 'pk.id = p.id_periode_kota')
            ->join('LEFT JOIN', 'periode_jenis pj', 'pj.id = p.id_periode_jenis')
            ->where([
                'pk.id_periode' => Periode::getPeriodeDashboard()->id,
            ])
            ->groupBy(['pk.nama', 'pk.kuota'])
            ->addParams([':idPeriode' => Periode::getPeriodeDashboard()->id])
        ;
        return $query;
    }

    public function getLineAnakKonfirmasi()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(t.confirm_request_at) AS year',
                'MONTH(t.confirm_request_at) AS month',
                'DAY(t.confirm_request_at) AS day',
                'count(p.id) as total_tiket',
            ])
            ->from('transaksi t')
            ->join('LEFT JOIN', 'peserta p', 'p.id_transaksi = t.id')
            ->groupBy([
                'DAY(t.confirm_request_at)',
                'MONTH(t.confirm_request_at)',
                'YEAR(t.confirm_request_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                't.id_periode' => Periode::getPeriodeDashboard()->id,
                't.status_bayar' => 'Sudah Bayar',
                't.status_aktif' => 'Aktif',
            ])
        ;

        return $query;
    }

    public function getLineAnakDikonfirmasi()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(t.confirm_at) AS year',
                'MONTH(t.confirm_at) AS month',
                'DAY(t.confirm_at) AS day',
                'count(p.id) as total_tiket',
            ])
            ->from('transaksi t')
            ->join('LEFT JOIN', 'peserta p', 'p.id_transaksi = t.id')
            ->groupBy([
                'DAY(t.confirm_at)',
                'MONTH(t.confirm_at)',
                'YEAR(t.confirm_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                't.id_periode' => Periode::getPeriodeDashboard()->id,
                't.status_bayar' => 'Sudah Bayar',
                't.status_aktif' => 'Aktif',
            ])
        ;

        return $query;
    }

    public function getLinePesertaDaftar()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(t.created_at) AS year',
                'MONTH(t.created_at) AS month',
                'DAY(t.created_at) AS day',
                'count(p.id) as total_tiket',
            ])
            ->from('transaksi t')
            ->join('LEFT JOIN', 'peserta p', 'p.id_transaksi = t.id')
            ->groupBy([
                'DAY(t.created_at)',
                'MONTH(t.created_at)',
                'YEAR(t.created_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                't.id_periode' => Periode::getPeriodeDashboard()->id,
                // 't.status_bayar' => 'Sudah Bayar',
                // 't.status_aktif' => 'Aktif',
            ])
        ;

        return $query;
    }

    public function getLineKonfirmasiSaintek()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(t.confirm_request_at) AS year',
                'MONTH(t.confirm_request_at) AS month',
                'DAY(t.confirm_request_at) AS day',
                'count(p.id) as total_tiket',
            ])
            ->from('transaksi t')
            ->join('LEFT JOIN', 'peserta p', 'p.id_transaksi = t.id')
            ->join('LEFT JOIN', 'periode_jenis pjp', 'pjp.id = p.id_periode_jenis')
            ->groupBy([
                'DAY(t.confirm_request_at)',
                'MONTH(t.confirm_request_at)',
                'YEAR(t.confirm_request_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                't.id_periode' => Periode::getPeriodeDashboard()->id,
                't.status_bayar' => 'Sudah Bayar',
                't.status_aktif' => 'Aktif',
                'pjp.nama' => 'Saintek (IPA)',
            ])
        ;

        return $query;
    }

    public function getLineKonfirmasiSoshum()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(t.confirm_request_at) AS year',
                'MONTH(t.confirm_request_at) AS month',
                'DAY(t.confirm_request_at) AS day',
                'count(p.id) as total_tiket',
            ])
            ->from('transaksi t')
            ->join('LEFT JOIN', 'peserta p', 'p.id_transaksi = t.id')
            ->join('LEFT JOIN', 'periode_jenis pjp', 'pjp.id = p.id_periode_jenis')
            ->groupBy([
                'DAY(t.confirm_request_at)',
                'MONTH(t.confirm_request_at)',
                'YEAR(t.confirm_request_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                't.id_periode' => Periode::getPeriodeDashboard()->id,
                't.status_bayar' => 'Sudah Bayar',
                't.status_aktif' => 'Aktif',
                'pjp.nama' => 'Soshum (IPS)',
            ])
        ;

        return $query;
    }

    public function getLinePesertaReferral()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(t.created_at) AS year',
                'MONTH(t.created_at) AS month',
                'DAY(t.created_at) AS day',
                'count(p.id) as total_tiket',
            ])
            ->from('transaksi t')
            ->join('LEFT JOIN', 'peserta p', 'p.id_transaksi = t.id')
            ->groupBy([
                'DAY(t.created_at)',
                'MONTH(t.created_at)',
                'YEAR(t.created_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                't.id_periode' => Periode::getPeriodeDashboard()->id,
                't.status_bayar' => 'Sudah Bayar',
                't.status_aktif' => 'Aktif',
            ])
            ->andWhere('t.kode_referral_agent IS NOT NULL')
        ;

        return $query;
    }

    public function getLinePesertaTiketDashboard()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'YEAR(t.created_at) AS year',
                'MONTH(t.created_at) AS month',
                'DAY(t.created_at) AS day',
                'count(p.id) as total_tiket',
            ])
            ->from('transaksi t')
            ->join('LEFT JOIN', 'peserta p', 'p.id_transaksi = t.id')
            ->groupBy([
                'DAY(t.created_at)',
                'MONTH(t.created_at)',
                'YEAR(t.created_at)',
            ])
            ->orderBy([
                'year' => SORT_ASC,
                'month' => SORT_ASC,
                'day' => SORT_ASC,
            ])
            ->where([
                't.id_periode' => Periode::getPeriodeDashboard()->id,
                't.status_bayar' => 'Sudah Bayar',
                't.status_aktif' => 'Aktif',
            ])
            ->andWhere('t.pendaftaran_melalui = "Dashboard - Tiket Baru"')
        ;

        return $query;
    }

    public function actionDatatablesStatistik()
    {
        $query = $this->getAllDetail();

        return $this->datatables($query, $post = Yii::$app->request->post(), Peserta::getDb());
    }

    public function actionIndex()
    {
        if (!isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
            throw new \yii\web\HttpException(403, 'Akun kamu dilarang ngakses halaman ini, bambang.');

        // kotak2
        $model['all'] = $this->getAll();

        // pie
        $query = $this->getAllDetail();
        $model['pie'] = $query->all();
        $model['label_tiket_belum_bayar_per_kota'] = $model['data_tiket_belum_bayar_per_kota'] = [];
        $model['label_tiket_sudah_bayar_per_kota'] = $model['data_tiket_sudah_bayar_per_kota'] = [];
        $border = ['#663333', '#664d33', '#666633', '#4d6633', '#336633', '#33664d', '#336666', '#334d66', '#333366', '#4d3366', '#663366', '#66334d'];
        $background = ['#e6b8b8', '#e6cfb8', '#e6e6b8', '#cfe6b8', '#b8e6b8', '#b8e6cf', '#b8e6e6', '#b8cfe6', '#b8b8e6', '#cfb8e6', '#e6b8e6', '#e6b8cf'];
        foreach ($model['pie'] as $key => $value) {
            $model['label_tiket_belum_bayar_per_kota'][] = $value['kota'];
            $model['data_tiket_belum_bayar_per_kota'][] = $value['tiket_belum_bayar'];
            $model['background_tiket_belum_bayar_per_kota'][] = $background[$key % 12];
            $model['border_tiket_belum_bayar_per_kota'][] = $border[$key % 12];
            
            $model['label_tiket_sudah_bayar_per_kota'][] = $value['kota'];
            $model['data_tiket_sudah_bayar_per_kota'][] = $value['tiket_sudah_bayar'];
            $model['background_tiket_sudah_bayar_per_kota'][] = $background[$key % 12];
            $model['border_tiket_sudah_bayar_per_kota'][] = $border[$key % 12];
        }

        // anak konfirmasi
        $query = $this->getLineAnakKonfirmasi();
        $model['anak_konfirmasi'] = $query->all();
        $model['label_anak_konfirmasi'] = $model['data_anak_konfirmasi'] = [];
        foreach ($model['anak_konfirmasi'] as $key => $value) {
            $model['label_anak_konfirmasi'][] = $value['day'] . '/' . $value['month'];
            $model['data_anak_konfirmasi'][] = $value['total_tiket'];

            if (isset($model['anak_konfirmasi'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['anak_konfirmasi'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_anak_konfirmasi'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_anak_konfirmasi'][] = 0;
                }
            }
        }

        // anak dikonfirmasi
        $query = $this->getLineAnakDikonfirmasi();
        $model['anak_dikonfirmasi'] = $query->all();
        $model['label_anak_dikonfirmasi'] = $model['data_anak_dikonfirmasi'] = [];
        foreach ($model['anak_dikonfirmasi'] as $key => $value) {
            $model['label_anak_dikonfirmasi'][] = $value['day'] . '/' . $value['month'];
            $model['data_anak_dikonfirmasi'][] = $value['total_tiket'];

            if (isset($model['anak_dikonfirmasi'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['anak_dikonfirmasi'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_anak_dikonfirmasi'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_anak_dikonfirmasi'][] = 0;
                }
            }
        }

        // peserta daftar
        $query = $this->getLinePesertaDaftar();
        $model['peserta_daftar'] = $query->all();
        $model['label_peserta_daftar'] = $model['data_peserta_daftar'] = [];
        foreach ($model['peserta_daftar'] as $key => $value) {
            $model['label_peserta_daftar'][] = $value['day'] . '/' . $value['month'];
            $model['data_peserta_daftar'][] = $value['total_tiket'];

            if (isset($model['peserta_daftar'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['peserta_daftar'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_peserta_daftar'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_peserta_daftar'][] = 0;
                }
            }
        }

        // anak konfirmasi saintek
        $query = $this->getLineKonfirmasiSaintek();
        $model['konfirmasi_saintek'] = $query->all();
        $model['label_konfirmasi_saintek'] = $model['data_konfirmasi_saintek'] = [];
        foreach ($model['konfirmasi_saintek'] as $key => $value) {
            $model['label_konfirmasi_saintek'][] = $value['day'] . '/' . $value['month'];
            $model['data_konfirmasi_saintek'][] = $value['total_tiket'];

            if (isset($model['konfirmasi_saintek'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['konfirmasi_saintek'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_konfirmasi_saintek'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_konfirmasi_saintek'][] = 0;
                }
            }
        }

        // anak konfirmasi soshum
        $query = $this->getLineKonfirmasiSoshum();
        $model['konfirmasi_soshum'] = $query->all();
        $model['label_konfirmasi_soshum'] = $model['data_konfirmasi_soshum'] = [];
        foreach ($model['konfirmasi_soshum'] as $key => $value) {
            $model['label_konfirmasi_soshum'][] = $value['day'] . '/' . $value['month'];
            $model['data_konfirmasi_soshum'][] = $value['total_tiket'];

            if (isset($model['konfirmasi_soshum'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['konfirmasi_soshum'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_konfirmasi_soshum'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_konfirmasi_soshum'][] = 0;
                }
            }
        }

        // peserta referral
        $query = $this->getLinePesertaReferral();
        $model['peserta_referral'] = $query->all();
        $model['label_peserta_referral'] = $model['data_peserta_referral'] = [];
        foreach ($model['peserta_referral'] as $key => $value) {
            $model['label_peserta_referral'][] = $value['day'] . '/' . $value['month'];
            $model['data_peserta_referral'][] = $value['total_tiket'];

            if (isset($model['peserta_referral'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['peserta_referral'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_peserta_referral'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_peserta_referral'][] = 0;
                }
            }
        }

        // peserta tiket dashboard
        $query = $this->getLinePesertaTiketDashboard();
        $model['peserta_tiket_dashboard'] = $query->all();
        $model['label_peserta_tiket_dashboard'] = $model['data_peserta_tiket_dashboard'] = [];
        foreach ($model['peserta_tiket_dashboard'] as $key => $value) {
            $model['label_peserta_tiket_dashboard'][] = $value['day'] . '/' . $value['month'];
            $model['data_peserta_tiket_dashboard'][] = $value['total_tiket'];

            if (isset($model['peserta_tiket_dashboard'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['peserta_tiket_dashboard'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_peserta_tiket_dashboard'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_peserta_tiket_dashboard'][] = 0;
                }
            }
        }

        return $this->render('index', [
            'model' => $model,
            'title' => 'Dashboard',
        ]);
    }

    public function actionGrafik($kota = 'all', $periode = 'seminggu')
    {
        if (!isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
            throw new \yii\web\HttpException(403, 'Akun kamu dilarang ngakses halaman ini, bambang.');

        $model['grafik'] = new Grafik();
        $model['grafik']->kota = $kota;
        $model['grafik']->periode = $periode;

        $query = $this->getLineAnakKonfirmasi();
        if ($model['grafik']->kota && $model['grafik']->kota != 'all') {
            $query->andWhere([
                'p.id_periode_kota' => $model['grafik']->kota,
            ]);
        }
        if ($model['grafik']->periode == 'seminggu') {
            $query->andWhere([
                'between', 'confirm_request_at', date('Y-m-d', strtotime('-1 week')), date('Y-m-d')
            ]);
        } else if ($model['grafik']->periode == 'sebulan') {
            $query->andWhere([
                'between', 'confirm_request_at', date('Y-m-d', strtotime('-1 month')), date('Y-m-d')
            ]);
        }
        $model['anak_konfirmasi'] = $query->all();
        $model['label_anak_konfirmasi'] = $model['data_anak_konfirmasi'] = [];
        foreach ($model['anak_konfirmasi'] as $key => $value) {
            $model['label_anak_konfirmasi'][] = $value['day'] . '/' . $value['month'];
            $model['data_anak_konfirmasi'][] = $value['total_tiket'];

            if (isset($model['anak_konfirmasi'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['anak_konfirmasi'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_anak_konfirmasi'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_anak_konfirmasi'][] = 0;
                }
            }
        }

        $query = $this->getLineAnakDikonfirmasi();
        if ($model['grafik']->kota && $model['grafik']->kota != 'all') {
            $query->andWhere([
                'p.id_periode_kota' => $model['grafik']->kota,
            ]);
        }
        if ($model['grafik']->periode == 'seminggu') {
            $query->andWhere([
                'between', 'confirm_at', date('Y-m-d', strtotime('-1 week')), date('Y-m-d')
            ]);
        } else if ($model['grafik']->periode == 'sebulan') {
            $query->andWhere([
                'between', 'confirm_at', date('Y-m-d', strtotime('-1 month')), date('Y-m-d')
            ]);
        }
        $model['anak_dikonfirmasi'] = $query->all();
        $model['label_anak_dikonfirmasi'] = $model['data_anak_dikonfirmasi'] = [];
        foreach ($model['anak_dikonfirmasi'] as $key => $value) {
            $model['label_anak_dikonfirmasi'][] = $value['day'] . '/' . $value['month'];
            $model['data_anak_dikonfirmasi'][] = $value['total_tiket'];

            if (isset($model['anak_dikonfirmasi'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['anak_dikonfirmasi'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_anak_dikonfirmasi'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_anak_dikonfirmasi'][] = 0;
                }
            }
        }

        $query = $this->getLinePesertaDaftar();
        if ($model['grafik']->kota && $model['grafik']->kota != 'all') {
            $query->andWhere([
                'p.id_periode_kota' => $model['grafik']->kota,
            ]);
        }
        if ($model['grafik']->periode == 'seminggu') {
            $query->andWhere([
                'between', 'created_at', date('Y-m-d', strtotime('-1 week')), date('Y-m-d')
            ]);
        } else if ($model['grafik']->periode == 'sebulan') {
            $query->andWhere([
                'between', 'created_at', date('Y-m-d', strtotime('-1 month')), date('Y-m-d')
            ]);
        }
        $model['peserta_daftar'] = $query->all();
        $model['label_peserta_daftar'] = $model['data_peserta_daftar'] = [];
        foreach ($model['peserta_daftar'] as $key => $value) {
            $model['label_peserta_daftar'][] = $value['day'] . '/' . $value['month'];
            $model['data_peserta_daftar'][] = $value['total_tiket'];

            if (isset($model['peserta_daftar'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['peserta_daftar'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_peserta_daftar'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_peserta_daftar'][] = 0;
                }
            }
        }

        $query = $this->getLinePesertaReferral();
        if ($model['grafik']->kota && $model['grafik']->kota != 'all') {
            $query->andWhere([
                'p.id_periode_kota' => $model['grafik']->kota,
            ]);
        }
        if ($model['grafik']->periode == 'seminggu') {
            $query->andWhere([
                'between', 'created_at', date('Y-m-d', strtotime('-1 week')), date('Y-m-d')
            ]);
        } else if ($model['grafik']->periode == 'sebulan') {
            $query->andWhere([
                'between', 'created_at', date('Y-m-d', strtotime('-1 month')), date('Y-m-d')
            ]);
        }
        $model['peserta_referral'] = $query->all();
        $model['label_peserta_referral'] = $model['data_peserta_referral'] = [];
        foreach ($model['peserta_referral'] as $key => $value) {
            $model['label_peserta_referral'][] = $value['day'] . '/' . $value['month'];
            $model['data_peserta_referral'][] = $value['total_tiket'];

            if (isset($model['peserta_referral'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['peserta_referral'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_peserta_referral'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_peserta_referral'][] = 0;
                }
            }
        }

        $query = $this->getLinePesertaTiketDashboard();
        if ($model['grafik']->kota && $model['grafik']->kota != 'all') {
            $query->andWhere([
                'p.id_periode_kota' => $model['grafik']->kota,
            ]);
        }
        if ($model['grafik']->periode == 'seminggu') {
            $query->andWhere([
                'between', 'created_at', date('Y-m-d', strtotime('-1 week')), date('Y-m-d')
            ]);
        } else if ($model['grafik']->periode == 'sebulan') {
            $query->andWhere([
                'between', 'created_at', date('Y-m-d', strtotime('-1 month')), date('Y-m-d')
            ]);
        }
        $model['peserta_tiket_dashboard'] = $query->all();
        $model['label_peserta_tiket_dashboard'] = $model['data_peserta_tiket_dashboard'] = [];
        foreach ($model['peserta_tiket_dashboard'] as $key => $value) {
            $model['label_peserta_tiket_dashboard'][] = $value['day'] . '/' . $value['month'];
            $model['data_peserta_tiket_dashboard'][] = $value['total_tiket'];

            if (isset($model['peserta_tiket_dashboard'][$key+1]) && $value['day'] && $value['month']) {
                $nextValue = $model['peserta_tiket_dashboard'][$key+1];
                $begin = new \DateTime($value['year'] . '-' . $value['month'] . '-' . $value['day'] . '+1 days');
                $end = new \DateTime($nextValue['year'] . '-' . $nextValue['month'] . '-' . $nextValue['day']);

                $range = \DateInterval::createFromDateString('1 day');
                $allInterval = new \DatePeriod($begin, $range, $end);

                foreach ($allInterval as $date) {
                    $model['label_peserta_tiket_dashboard'][] = $date->format('d') . '/' . $date->format('m');
                    $model['data_peserta_tiket_dashboard'][] = 0;
                }
            }
        }

        return $this->render('grafik', [
            'model' => $model,
            'title' => 'Grafik',
        ]);
    }

    public function actionOmset()
    {
        if (!isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
            throw new \yii\web\HttpException(403, 'Akun kamu dilarang ngakses halaman ini, bambang.');

        $query = new \yii\db\Query();
        $omset = $query
            ->select([
                'sum(tagihan) AS omset',
            ])
            ->from('transaksi t')
            ->join('JOIN', 'periode p', 'p.id = t.id_periode AND p.id = ' . Periode::getPeriodeDashboard()->id)
            ->where(['status_aktif' => 'Aktif', 'status_bayar' => 'Sudah Bayar'])
            ->scalar()
        ;

        return $this->render('omset', [
            'omset' => $omset,
            'title' => 'Omset',
        ]);
    }

    //////////////

    public function actionDatatablesPeserta()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama',
                'p.kode',
                'p.email',
                'p.handphone',
                'FORMAT(t.tagihan, 2) AS tagihan',
                't.jumlah_tiket',
                'pk.nama AS lokasi',
                'r.name AS request',
                't.status_bayar',
                't.status_bayar AS status_duta',
                't.status_aktif',
                'DATE_FORMAT(t.created_at, "%d %M %Y, %h:%i") AS created_at',
            ])
            ->from('peserta p')
            ->join('JOIN', 'transaksi t', 't.id = p.id_transaksi AND t.id_periode = ' . Periode::getPeriodeDashboard()->id)
            ->join('LEFT JOIN', 'periode_jenis pj', 'p.id_periode_jenis = pj.id')
            ->join('LEFT JOIN', 'periode_kota pk', 'p.id_periode_kota = pk.id')
            ->join('LEFT JOIN', 'regencies r', 'r.id = p.id_kota')
            ->where([
                'status_aktif' => 'Aktif',
            ])
        ;

        return $this->datatables($query, $post = Yii::$app->request->post(), Peserta::getDb());
    }

    public function actionPeserta()
    {
        $model['all'] = $this->getAll();

        $model['peserta'] = Peserta::find()
            ->select([
                'p.id',
                'p.kode',
                'p.nama',
                't.tagihan',
                't.tanggal_pembayaran',
            ])
            ->from('peserta p')
            ->join('JOIN', 'transaksi t', 't.id = p.id_transaksi AND t.id_periode = ' . Periode::getPeriodeDashboard()->id)
            ->where([
                'status_aktif' => 'Aktif',
                't.status_bayar' => 'Dalam Proses Konfirmasi',
            ])
            ->asArray()
            ->all();
        foreach ($model['peserta'] as $key => $peserta) {
            $pos = strpos($peserta['nama'], ' ');
            $firstLetter = strtoupper($peserta['nama'][0]);
            $secondLetter = $pos && isset($peserta['nama'][$pos + 1]) ? strtoupper($peserta['nama'][$pos + 1]) : '';
            $model['peserta'][$key]['alias'] = ($firstLetter . $secondLetter);

            if ($firstLetter >= 'A' && $firstLetter <= 'C') {
                $model['peserta'][$key]['color'] = 'azure';
            } else if ($firstLetter >= 'D' && $firstLetter <= 'F') {
                $model['peserta'][$key]['color'] = 'spring';
            } else if ($firstLetter >= 'G' && $firstLetter <= 'H') {
                $model['peserta'][$key]['color'] = 'red';
            } else if ($firstLetter >= 'I' && $firstLetter <= 'J') {
                $model['peserta'][$key]['color'] = 'yellow';
            } else if ($firstLetter >= 'K' && $firstLetter <= 'L') {
                $model['peserta'][$key]['color'] = 'orange';
            } else if ($firstLetter >= 'M' && $firstLetter <= 'N') {
                $model['peserta'][$key]['color'] = 'chartreuse';
            } else if ($firstLetter >= 'O' && $firstLetter <= 'P') {
                $model['peserta'][$key]['color'] = 'green';
            } else if ($firstLetter >= 'Q' && $firstLetter <= 'R') {
                $model['peserta'][$key]['color'] = 'cyan';
            } else if ($firstLetter >= 'S' && $firstLetter <= 'T') {
                $model['peserta'][$key]['color'] = 'blue';
            } else if ($firstLetter >= 'U' && $firstLetter <= 'V') {
                $model['peserta'][$key]['color'] = 'violet';
            } else if ($firstLetter >= 'W' && $firstLetter <= 'X') {
                $model['peserta'][$key]['color'] = 'magenta';
            } else if ($firstLetter >= 'Y' && $firstLetter <= 'Z') {
                $model['peserta'][$key]['color'] = 'rose';
            }
        }

        return $this->render('peserta', [
            'model' => $model,
            'title' => 'Kelola Peserta',
        ]);
    }

    public function actionKonfirmasi($id)
    {
        $error = true;

        $model['transaksi'] = Transaksi::find()
            ->join('JOIN', 'peserta p', 'transaksi.id = p.id_transaksi')
            ->where(['p.id' => $id, 'transaksi.status_bayar' => 'Dalam Proses Konfirmasi'])
            ->one();
        if (!$model['transaksi']) throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        $model['peserta'] = Peserta::find()->where(['id' => $id])->one();
        if (!$model['peserta']) throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        $periode = Periode::getPeriodeDashboard();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['transaksi']->confirm_at = new \yii\db\Expression("now()");
                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Peserta gagal dikonfirmasi.');
                }

                $error = false;
                $transaction->commit();

                if ($model['transaksi']->status_bayar == 'Sudah Bayar') {
                    Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Peserta berhasil dikonfirmasi.</div>');
                    if (YII_ENV == 'prod') {
                        \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                            ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                            ->setSubject('Tiket Tryout Nasional ' . Yii::$app->params['app.name'] . ' 2019 Milikmu Siap Dicetak')->send();
                    } else if (YII_ENV == 'dev') {
                        /*return $this->render('/email/email-konfirmasi-diterima', [
                            'model' => $model,
                            'idPeriode' => $periode->id,
                            'title' => 'Konfirmasi Diterima',
                        ]);*/
                        \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                            ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                            ->setSubject('Tiket Tryout Nasional MasukPTNid 2019 Milikmu Siap Dicetak')->send();
                    }
                } else if ($model['transaksi']->status_bayar == 'Ditolak') {
                    Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Pembayaran berhasil ditolak.</div>');
                    if (YII_ENV == 'prod') {
                        \Yii::$app->mail->compose('email/email-konfirmasi-ditolak', ['model' => $model])
                            ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                            ->setSubject('Konfirmasi Pembayaran Ditolak/Bermasalah')->send();
                    } else if (YII_ENV == 'dev') {
                        /*return $this->render('/email/email-konfirmasi-ditolak', [
                            'model' => $model,
                            'idPeriode' => $periode->id,
                            'title' => 'Konfirmasi Ditolak',
                        ]);*/
                        \Yii::$app->mail->compose('email/email-konfirmasi-ditolak', ['model' => $model])
                            ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                            ->setSubject('Konfirmasi Pembayaran Ditolak/Bermasalah')->send();
                    }
                }
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            $model['transaksi']->catatan = '';
        }

        if ($error)
            return $this->render('form-konfirmasi', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Konfirmasi Pembayaran ' . $model['peserta']->kode,
            ]);
        else
            return $this->redirect(['peserta']);
    }

    public function actionDetailPeserta($id)
    {
        $error = true;

        $model['peserta'] = Peserta::find()->where(['id' => $id])->one();
        if (!$model['peserta']) throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        $periode = Periode::getPeriodeDashboard();
        
        if ($error)
            return $this->render('detail-peserta', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Detail Peserta',
            ]);
        else
            return $this->redirect(['xswzaq/index']);
    }

    public function actionUpdatePeserta($id)
    {
        $error = true;

        $gatePeserta = Peserta::find()->where(['id' => $id])->one();
        if (!$gatePeserta) throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        $model['transaksi'] = $gatePeserta->transaksi;
        $periode = Periode::getPeriodeDashboard();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi']->load($post);
            if (isset($post['Peserta'])) {
                foreach ($post['Peserta'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $peserta = Peserta::find()->where(['id' => $value['id']])->one();
                        $peserta->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $peserta = Peserta::find()->where(['id' => ($value['id']*-1)])->one();
                        $peserta->isDeleted = true;
                    } else {
                        $peserta = new Peserta();
                        $peserta->setAttributes($value);
                    }
                    $model['peserta'][] = $peserta;
                }
            }

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $model['transaksi']->tagihan = 0;
                
                $error = false;

                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $peserta->id_transaksi = $model['transaksi']->id;
                            $peserta->nama = ucwords(strtolower($peserta->nama));

                            // $peserta->kode = $periode->kode . $peserta->id_kota . $this->sequence('peserta-kode');

                            if (!$peserta->validate()) {
                                $error = true;
                            }

                            /*if ($peserta->id_periode_jenis && $peserta->id_periode_kota) {
                                $periodeJenis = $this->findModelPeriodeJenis($peserta->id_periode_jenis);
                                $peserta->harga = $periodeJenis->$variableJumlahTiket;
                                $peserta->periode_penjualan = $periodeJenis->periode_penjualan;
                                $model['transaksi']->tagihan += (int)$peserta->harga;
                            } else {
                                $error = true;
                                if (!$peserta->id_periode_jenis) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_jenis' => 'Jenis tiket harus dipilih.',
                                    ]);
                                }
                                if (!$peserta->id_periode_kota) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_kota' => 'Lokasi tryout harus dipilih.',
                                    ]);
                                }
                            }*/

                            $model['transaksi']->tagihan += (int)$peserta->harga;

                            /*if (!$peserta->id_kota) {
                                $error = true;
                                $model['peserta'][$key]->addErrors([
                                    'id_kota' => 'Domisili belum dipilih.',
                                ]);
                            }

                            $count = count($model['peserta']);
                            for ($i = $key + 1; $i < $count; $i++) { 
                                if ($peserta->email == $model['peserta'][$i]->email) {
                                    $model['peserta'][$i]->addErrors([
                                        'email' => 'Email tidak boleh ada yang sama.',
                                    ]);
                                    $error = true;
                                }
                            }*/
                        }
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                    }
                
                    foreach ($model['peserta'] as $key => $peserta) {
                        if ($peserta->isDeleted) {
                            if (!$peserta->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$peserta->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data peserta berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['transaksi']->pesertas as $key => $peserta)
                $model['peserta'][] = $peserta;
        }

        if ($error)
            return $this->render('form-update-peserta', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Form Update Peserta',
            ]);
        else
            return $this->redirect(['peserta']);
    }

    public function actionTiketBaru()
    {
        $error = true;

        $model['transaksi'] = new Transaksi();
        $periode = Periode::getPeriodeDashboard();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi']->load($post);
            if (isset($post['Peserta'])) {
                foreach ($post['Peserta'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $peserta = Peserta::find()->where(['id' => $value['id']])->one();
                        $peserta->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $peserta = Peserta::find()->where(['id' => ($value['id']*-1)])->one();
                        $peserta->isDeleted = true;
                    } else {
                        $peserta = new Peserta();
                        $peserta->setAttributes($value);
                    }
                    $model['peserta'][] = $peserta;
                }
            }

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['transaksi']->id_periode = $periode->id;
                $model['transaksi']->status_bayar = 'Sudah Bayar';

                $jumlahTiket = 0;
                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $jumlahTiket++;
                        }
                    }
                }
                $variableJumlahTiket = 'harga_' . $jumlahTiket . '_tiket';
                
                $model['transaksi']->jumlah_tiket = $jumlahTiket;
                $model['transaksi']->tagihan = 0;
                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $model['transaksi']->tagihan = 0;
                
                $error = false;

                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $peserta->id_transaksi = $model['transaksi']->id;
                            $peserta->nama = ucwords(strtolower($peserta->nama));

                            $peserta->kode = $periode->kode . $peserta->id_kota . $this->sequence('peserta-kode');

                            if (!$peserta->validate()) {
                                $error = true;
                            }

                            if ($peserta->id_periode_jenis && $peserta->id_periode_kota) {
                                $periodeJenis = $this->findModelPeriodeJenis($peserta->id_periode_jenis);
                                // $peserta->harga = $periodeJenis->$variableJumlahTiket;
                                $peserta->periode_penjualan = $periodeJenis->periode_penjualan;
                                // $model['transaksi']->tagihan += (int)$peserta->harga;
                            } else {
                                $error = true;
                                if (!$peserta->id_periode_jenis) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_jenis' => 'Jenis tiket harus dipilih.',
                                    ]);
                                }
                                if (!$peserta->id_periode_kota) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_kota' => 'Lokasi tryout harus dipilih.',
                                    ]);
                                }
                            }

                            $model['transaksi']->tagihan += (int)$peserta->harga;

                            if (!$peserta->id_kota) {
                                $error = true;
                                $model['peserta'][$key]->addErrors([
                                    'id_kota' => 'Domisili belum dipilih.',
                                ]);
                            }

                            $count = count($model['peserta']);
                            for ($i = $key + 1; $i < $count; $i++) { 
                                if ($peserta->email == $model['peserta'][$i]->email) {
                                    $model['peserta'][$i]->addErrors([
                                        'email' => 'Email tidak boleh ada yang sama.',
                                    ]);
                                    $error = true;
                                }
                            }
                        }
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                    }
                
                    foreach ($model['peserta'] as $key => $peserta) {
                        if ($peserta->isDeleted) {
                            if (!$peserta->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$peserta->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data peserta berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['transaksi']->pesertas as $key => $peserta)
                $model['peserta'][] = $peserta;
        }

        if ($error)
            return $this->render('form-tiket-baru', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Form Tiket Baru',
            ]);
        else
            return $this->redirect(['xswzaq/peserta']);
    }

    public function actionAktifkanPeserta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelPesertaAktifkan($id);
        $periode = Periode::getPeriodeDashboard();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['peserta']->status_aktif = 'Aktif';
                if (!$model['peserta']->save()) {
                    throw new \yii\web\HttpException(400, 'Peserta gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Peserta berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['peserta']);
    }

    public function actionNonaktifkanPeserta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelPesertaNonaktifkan($id);
        $model['transaksi'] = $model['peserta']->transaksi;
        $periode = Periode::getPeriodeDashboard();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['transaksi']->status_aktif = 'Nonaktif';
                $model['transaksi']->nonaktif_at = new \yii\db\Expression('now()');
                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Peserta gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Peserta berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['peserta']);
    }

    public function actionAktifkanDuta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelDutaAktifkan($id);
        $periode = Periode::getPeriodeDashboard();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['peserta']->status_duta = 'Sudah Aktif';
                if (!$model['peserta']->save()) {
                    throw new \yii\web\HttpException(400, 'Duta gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Duta berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['peserta']);
    }

    public function actionNonaktifkanDuta($id)
    {
        $error = true;

        $model['peserta'] = $this->findModelDutaNonaktifkan($id);
        $periode = Periode::getPeriodeDashboard();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['peserta']->status_duta = 'Tidak Aktif';
                // $model['peserta']->nonaktif_at = new \yii\db\Expression('now()');
                if (!$model['peserta']->save()) {
                    throw new \yii\web\HttpException(400, 'Duta gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Duta berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['peserta']);
    }

    public function actionResendPendaftaran($id)
    {
        $model['peserta'] = Peserta::find()->where(['id' => $id])->one();
        if (!$model['peserta']) throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        $periode = Periode::getPeriodeDashboard();
        
        Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Email pendaftaran diterima berhasil dikirim ulang.</div>');
        if (YII_ENV == 'prod') {
            \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                ->setSubject('Username dan Password MasukPTNid')->send();
        } else if (YII_ENV == 'dev') {
            /*return $this->render('/email/email-pendaftaran', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Username dan Password MasukPTNid',
            ]);*/
            \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                ->setSubject('Username dan Password MasukPTNid')->send();
        }

        return $this->redirect(['peserta']);
    }

    public function actionResendPendaftaranBeli($id)
    {
        $model['peserta'] = Peserta::find()->where(['id' => $id])->one();
        if (!$model['peserta']) throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        $model['transaksi'] = $model['peserta']->transaksi;
        $periode = Periode::getPeriodeDashboard();
        
        Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Email pilih kota tryout berhasil dikirim ulang.</div>');
        if (YII_ENV == 'prod') {
            \Yii::$app->mail->compose('email/email-pendaftaran-beli', ['model' => $model])
                ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                ->setSubject('Segera Lakukan Pembayaran')->send();
        } else if (YII_ENV == 'dev') {
            /*return $this->render('/email/email-pendaftaran-beli', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Segera Lakukan Pembayaran',
            ]);*/
            \Yii::$app->mail->compose('email/email-pendaftaran-beli', ['model' => $model])
                ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                ->setSubject('Segera Lakukan Pembayaran')->send();
        }

        return $this->redirect(['peserta']);
    }

    public function actionResendKonfirmasi($id)
    {
        $model['peserta'] = Peserta::find()->where(['id' => $id])->one();
        if (!$model['peserta']) throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        $model['transaksi'] = $model['peserta']->transaksi;
        $periode = Periode::getPeriodeDashboard();
        
        Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Email dalam proses konfirmasi berhasil dikirim ulang.</div>');
        if (YII_ENV == 'prod') {
            \Yii::$app->mail->compose('email/email-konfirmasi', ['model' => $model])
                ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                ->setSubject('Konfirmasi Pembayaran Sedang Diproses')->send();
        } else if (YII_ENV == 'dev') {
            /*return $this->render('/email/email-konfirmasi', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'konfirmasi',
            ]);*/
            \Yii::$app->mail->compose('email/email-konfirmasi', ['model' => $model])
                ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                ->setSubject('Konfirmasi Pembayaran Sedang Diproses')->send();
        }

        return $this->redirect(['peserta']);
    }

    public function actionResendDiterima($id)
    {
        $model['peserta'] = Peserta::find()->where(['id' => $id])->one();
        if (!$model['peserta']) throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        $model['transaksi'] = $model['peserta']->transaksi;
        $periode = Periode::getPeriodeDashboard();
        
        Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Email konfirmasi diterima berhasil dikirim ulang.</div>');
        if (YII_ENV == 'prod') {
            \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                ->setSubject('Pembayaran Sudah Diterima')->send();
        } else if (YII_ENV == 'dev') {
            /*return $this->render('/email/email-konfirmasi-diterima', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Konfirmasi Diterima',
            ]);*/
            \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                ->setSubject('Pembayaran Sudah Diterima')->send();
        }

        return $this->redirect(['peserta']);
    }

    public function actionResendDitolak($id)
    {
        $model['peserta'] = Peserta::find()->where(['id' => $id])->one();
        if (!$model['peserta']) throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        $model['transaksi'] = $model['peserta']->transaksi;
        $periode = Periode::getPeriodeDashboard();

        Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Email konfirmasi ditolak berhasil dikirim ulang.</div>');
        if (YII_ENV == 'prod') {
            \Yii::$app->mail->compose('email/email-konfirmasi-ditolak', ['model' => $model])
                ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                ->setSubject('Konfirmasi Pembayaran Ditolak/Bermasalah')->send();
        } else if (YII_ENV == 'dev') {
            /*return $this->render('/email/email-konfirmasi-ditolak', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Konfirmasi Ditolak',
            ]);*/
            \Yii::$app->mail->compose('email/email-konfirmasi-ditolak', ['model' => $model])
                ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                ->setSubject('Konfirmasi Pembayaran Ditolak/Bermasalah')->send();
        }

        return $this->redirect(['peserta']);
    }

    //////////////

    public function actionSetting()
    {
        if (!isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
            throw new \yii\web\HttpException(403, 'Akun kamu dilarang ngakses halaman ini, bambang.');

        return $this->render('setting', [
            // 'model' => $model,
            'title' => 'Setting',
        ]);
    }

    public function actionDatatablesPeriodeKota()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'pk.id',
                'pk.id_periode',
                'r.name as kota',
                'pk.kode',
                'pk.nama',
                'pk.status',
                'pk.alamat',
                'pk.kuota',
            ])
            ->from('periode_kota pk')
            ->join('INNER JOIN', 'regencies r', 'r.id = pk.id_regencies')
            ->where(['id_periode' => (Periode::getPeriodeDashboard()->id)])
        ;


        return $this->datatables($query, $post = Yii::$app->request->post(), PeriodeKota::getDb());
    }

    public function actionUpdatePeriodeKota($id)
    {
        $error = true;

        $model['periode_kota'] = $this->findModelPeriodeKota($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_kota']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if ($model['periode_kota']->isNewRecord) {
                    $model['periode_kota']->id_periode = (Periode::getPeriodeDashboard())->id;
                }
                if (!$model['periode_kota']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data kota berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode_kota', [
                'model' => $model,
                'title' => 'Form Update Kota',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionCreatePeriodeKota()
    {
        $error = true;

        $model['periode_kota'] = new PeriodeKota();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_kota']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if ($model['periode_kota']->isNewRecord) {
                    $model['periode_kota']->id_periode = (Periode::getPeriodeDashboard())->id;
                }
                if (!$model['periode_kota']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data kota berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode_kota', [
                'model' => $model,
                'title' => 'Form Tambah Kota',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionAktifkanPeriodeKota($id)
    {
        $error = true;

        $model['periode_kota'] = $this->findModelPeriodeKotaAktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_kota']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['periode_kota']->status = 'Sedang Aktif';
                if (!$model['periode_kota']->save()) {
                    throw new \yii\web\HttpException(400, 'Kota gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Kota berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionNonaktifkanPeriodeKota($id)
    {
        $error = true;

        $model['periode_kota'] = $this->findModelPeriodeKotaNonaktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_kota']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['periode_kota']->status = 'Tidak Aktif';
                if (!$model['periode_kota']->save()) {
                    throw new \yii\web\HttpException(400, 'Kota gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Kota berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionDetailPeriodeKota($id)
    {
        $error = true;

        $model['periode_kota'] = $this->findModelPeriodeKota($id);
        
        if ($error)
            return $this->render('detail-periode_kota', [
                'model' => $model,
                'title' => 'Detail Kota',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionGetListKota($idProvinces = null)
    {
        $regenciesOriginal = [];
        $regenciesOriginal = array_map('ucwords', array_map('strtolower', ArrayHelper::map(\technosmart\modules\location\models\Regencies::find()->select(['id', 'CONCAT(id, " - ", name) as name'])->where(['province_id' => $idProvinces])->orderBy('name')->asArray()->all(), 'id', 'name')));

        $regencies = [];
        foreach ($regenciesOriginal as $key => $value) {
            $regencies[] = [
                'value' => $key,
                'text' => $value,
            ];
        }
        return $this->json($regencies);
    }

    public function actionDatatablesPeriodeJenis()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'pj.id',
                'pj.id_periode',
                'pj.nama',
                'pj.status',
                'pj.periode_penjualan',
                'FORMAT(pj.harga_1_tiket, 2) AS harga_1_tiket',
                'FORMAT(pj.harga_2_tiket, 2) AS harga_2_tiket',
                'FORMAT(pj.harga_3_tiket, 2) AS harga_3_tiket',
                'FORMAT(pj.harga_4_tiket, 2) AS harga_4_tiket',
                'FORMAT(pj.harga_5_tiket, 2) AS harga_5_tiket',
                'FORMAT(pj.harga_6_tiket, 2) AS harga_6_tiket',
                'FORMAT(pj.harga_7_tiket, 2) AS harga_7_tiket',
                'FORMAT(pj.harga_8_tiket, 2) AS harga_8_tiket',
                'FORMAT(pj.harga_9_tiket, 2) AS harga_9_tiket',
                'FORMAT(pj.harga_10_tiket, 2) AS harga_10_tiket',
            ])
            ->from('periode_jenis pj')
            ->where(['id_periode' => (Periode::getPeriodeDashboard()->id)])
        ;
        return $this->datatables($query, $post = Yii::$app->request->post(), PeriodeJenis::getDb());
    }

    public function actionUpdatePeriodeJenis($id)
    {
        $error = true;

        $model['periode_jenis'] = $this->findModelPeriodeJenis($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_jenis']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if ($model['periode_jenis']->isNewRecord) {
                    $model['periode_jenis']->id_periode = (Periode::getPeriodeDashboard())->id;
                }
                if (!$model['periode_jenis']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data jenis, periode & harga berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode_jenis', [
                'model' => $model,
                'title' => 'Form Update Jenis, Periode & Harga',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionCreatePeriodeJenis()
    {
        $error = true;

        $model['periode_jenis'] = new PeriodeJenis();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_jenis']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                if ($model['periode_jenis']->isNewRecord) {
                    $model['periode_jenis']->id_periode = (Periode::getPeriodeDashboard())->id;
                }
                if (!$model['periode_jenis']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data jenis, periode & harga berhasil ditambah.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode_jenis', [
                'model' => $model,
                'title' => 'Form Tambah Jenis, Periode & Harga',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionAktifkanPeriodeJenis($id)
    {
        $error = true;

        $model['periode_jenis'] = $this->findModelPeriodeJenisAktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_jenis']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['periode_jenis']->status = 'Sedang Aktif';
                if (!$model['periode_jenis']->save()) {
                    throw new \yii\web\HttpException(400, 'Jenis, Periode & Harga gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Jenis, Periode & Harga berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionNonaktifkanPeriodeJenis($id)
    {
        $error = true;

        $model['periode_jenis'] = $this->findModelPeriodeJenisNonaktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode_jenis']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                $model['periode_jenis']->status = 'Tidak Aktif';
                if (!$model['periode_jenis']->save()) {
                    throw new \yii\web\HttpException(400, 'Jenis, Periode & Harga gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Jenis, Periode & Harga berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionDetailPeriodeJenis($id)
    {
        $error = true;

        $model['periode_jenis'] = $this->findModelPeriodeJenis($id);
        
        if ($error)
            return $this->render('detail-periode_jenis', [
                'model' => $model,
                'title' => 'Detail Jenis, Periode & Harga',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionDatatablesAlumni()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'a.id',
                'a.nama',
                'a.universitas',
                'a.jurusan',
                'a.angkatan',
                'a.poto',
            ])
            ->from('alumni a')
            // ->where(['id_periode' => (Periode::getPeriodeDashboard()->id)])
        ;
        return $this->datatables($query, $post = Yii::$app->request->post(), Alumni::getDb());
    }

    public function actionUpdateAlumni($id)
    {
        $error = true;

        $model['alumni'] = $this->findModelAlumni($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['alumni']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                /*if ($model['alumni']->isNewRecord) {
                    $model['alumni']->id_periode = (Periode::getPeriodeDashboard())->id;
                }*/
                if (!$model['alumni']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data alumni berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-alumni', [
                'model' => $model,
                'title' => 'Form Update Alumni',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionCreateAlumni()
    {
        $error = true;

        $model['alumni'] = new Alumni();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['alumni']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                /*if ($model['alumni']->isNewRecord) {
                    $model['alumni']->id_periode = (Periode::getPeriodeDashboard())->id;
                }*/
                if (!$model['alumni']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data alumni berhasil ditambah.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-alumni', [
                'model' => $model,
                'title' => 'Form Tambah Alumni',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionDetailAlumni($id)
    {
        $error = true;

        $model['alumni'] = $this->findModelAlumni($id);
        
        if ($error)
            return $this->render('detail-alumni', [
                'model' => $model,
                'title' => 'Detail Alumni',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionDatatablesPeriode()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.kode',
                'p.status',
                'p.status_dashboard',
            ])
            ->from('periode p')
            // ->where(['id_periode' => (Periode::getPeriodeDashboard()->id)])
        ;
        return $this->datatables($query, $post = Yii::$app->request->post(), Periode::getDb());
    }

    public function actionUpdatePeriode($id)
    {
        $error = true;

        $model['periode'] = $this->findModelPeriode($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                /*if ($model['periode']->isNewRecord) {
                    $model['periode']->id_periode = (Periode::getPeriodeDashboard())->id;
                }*/
                if (!$model['periode']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data periode berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode', [
                'model' => $model,
                'title' => 'Form Update Periode',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionCreatePeriode()
    {
        $error = true;

        $model['periode'] = new Periode();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['periode']->load($post);

            $transaction = Peserta::getDb()->beginTransaction();

            try {
                /*if ($model['periode']->isNewRecord) {
                    $model['periode']->id_periode = (Periode::getPeriodeDashboard())->id;
                }*/
                if (!$model['periode']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data periode berhasil ditambah.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-periode', [
                'model' => $model,
                'title' => 'Form Tambah Periode',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionDetailPeriode($id)
    {
        $error = true;

        $model['periode'] = $this->findModelPeriode($id);
        
        if ($error)
            return $this->render('detail-periode', [
                'model' => $model,
                'title' => 'Detail Periode',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    ///

    public function actionDatatablesPromo()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.id_periode',
                'p.judul',
                'p.status',
                'p.link',
                'CONCAT(SUBSTRING(p.content, 1, 10), "...") AS content',
            ])
            ->from('promo p')
            ->where(['id_periode' => (Periode::getPeriodeDashboard()->id)])
        ;
        return $this->datatables($query, $post = Yii::$app->request->post(), Promo::getDb());
    }

    public function actionUpdatePromo($id)
    {
        $error = true;

        $model['promo'] = $this->findModelPromo($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['promo']->load($post);

            $transaction = Promo::getDb()->beginTransaction();

            try {
                if ($model['promo']->isNewRecord) {
                    $model['promo']->id_periode = (Periode::getPeriodeDashboard())->id;
                    $model['promo']->status = 'Sedang Aktif';
                }
                if (!$model['promo']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data promo berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-promo', [
                'model' => $model,
                'title' => 'Form Update Promo',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionCreatePromo()
    {
        $error = true;

        $model['promo'] = new Promo();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['promo']->load($post);

            $transaction = Promo::getDb()->beginTransaction();

            try {
                if ($model['promo']->isNewRecord) {
                    $model['promo']->id_periode = (Periode::getPeriodeDashboard())->id;
                    $model['promo']->status = 'Sedang Aktif';
                }
                if (!$model['promo']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Data promo berhasil diupdate.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-promo', [
                'model' => $model,
                'title' => 'Form Tambah Promo Baru',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    public function actionAktifkanPromo($id)
    {
        $error = true;

        $model['promo'] = $this->findModelPromoAktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['promo']->load($post);

            $transaction = Promo::getDb()->beginTransaction();

            try {
                $model['promo']->status = 'Sedang Aktif';
                if (!$model['promo']->save()) {
                    throw new \yii\web\HttpException(400, 'Jenis, Periode & Harga gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Promo berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionNonaktifkanPromo($id)
    {
        $error = true;

        $model['promo'] = $this->findModelPromoNonaktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['promo']->load($post);

            $transaction = Promo::getDb()->beginTransaction();

            try {
                $model['promo']->status = 'Tidak Aktif';
                if (!$model['promo']->save()) {
                    throw new \yii\web\HttpException(400, 'Promo gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Promo berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['setting']);
    }

    public function actionDetailPromo($id)
    {
        $error = true;

        $model['promo'] = $this->findModelPromo($id);
        
        if ($error)
            return $this->render('detail-promo', [
                'model' => $model,
                'title' => 'Detail Promo',
            ]);
        else
            return $this->redirect(['xswzaq/setting']);
    }

    ///

    public function actionDatatablesPic()
    {
        if (!isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
            throw new \yii\web\HttpException(403, 'Akun kamu dilarang ngakses halaman ini, bambang.');

        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.id_periode',
                'p.status',
                'CONCAT(pk.kode, " - ", pk.nama) AS kota',
                'p.kode',
                'pp.count_volunteer AS jml_volunteer',
                'username',
                'p.nama',
                'p.email',
                'p.handphone',
                'p.whatsapp',
                'p.nomor_rekening',
                'p.nama_bank',
                'p.atas_nama',
                'p.alamat_pengiriman',
                'p.nama_penerima',
                'p.telpon_penerima',
                'p.ukuran_kaos',
                'p.rangkuman_kegiatan',
                'p.link_foto',
                'p.laporan_keuangan',
                'p.laporan_keuangan_sudah_final',
                'p.acara_lain_diluar_tryout',
            ])
            ->from('pic p')
            ->join('LEFT JOIN', 'periode_kota pk', 'pk.id = p.id_periode_kota')
            ->join('JOIN', '(
                    select p.id, count(v.id) as count_volunteer
                    from pic p
                    LEFT JOIN volunteer v ON v.id_pic = p.id
                    group by p.id
                ) pp', 'pp.id = p.id')
            ->where(['p.id_periode' => (Periode::getPeriodeDashboard()->id)])
        ;
        return $this->datatables($query, $post = Yii::$app->request->post(), PeriodeJenis::getDb());
    }

    public function actionPic($id = null)
    {
        if (!isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
            throw new \yii\web\HttpException(403, 'Akun kamu dilarang ngakses halaman ini, bambang.');

        if (!$id) {
            $model['pic_umum'] = PicUmum::getModel();
            return $this->render('list-pic', [
                'model' => $model,
                'title' => 'Pic',
            ]);
        }

        $model['pic'] = $this->findModelPic($id);
        $model['periode_kota'] = $model['pic']->periodeKota;
        return $this->render('detail-pic', [
            'model' => $model,
            'title' => 'Detail Pic',
        ]);
    }

    public function actionPicLaporanKeuangan($id)
    {
        $model['pic'] = $this->findModelPic($id);
        $model['periode_kota'] = $model['pic']->periodeKota;

        foreach ($model['pic']->picPemasukans as $key => $picPemasukan)
            $model['pic_pemasukan'][] = $picPemasukan;

        foreach ($model['pic']->picPengeluarans as $key => $picPengeluaran)
            $model['pic_pengeluaran'][] = $picPengeluaran;
            
        return $this->render('detail-pic-laporan-keuangan', [
            'model' => $model,
            'title' => 'Laporan Keuangan Pic',
        ]);
    }

    public function actionFormPic($id = null)
    {
        $error = true;

        $model['pic'] = isset($id) ? $this->findModelPic($id) : new Pic();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pic']->load($post);
            if (isset($post['Volunteer'])) {
                foreach ($post['Volunteer'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pic = $this->findModelVolunteer($value['id']);
                        $pic->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pic = $this->findModelVolunteer(($value['id']*-1));
                        $pic->isDeleted = true;
                    } else {
                        $pic = new Volunteer();
                        $pic->setAttributes($value);
                    }
                    $model['volunteer'][] = $pic;
                }
            }

            $transaction['pic'] = Pic::getDb()->beginTransaction();

            try {
                if ($model['pic']->isNewRecord) {
                    $model['pic']->id_periode = Periode::getPeriodeDashboard()->id;
                }
                if (!$model['pic']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                if (isset($model['volunteer']) and is_array($model['volunteer'])) {
                    foreach ($model['volunteer'] as $key => $pic) {
                        if (!$pic->isDeleted && !$pic->validate()) $error = true;                        
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['volunteer'] as $key => $pic) {
                        if ($pic->isDeleted) {
                            if (!$pic->delete()) {
                                $error = true;
                            }
                        } else {
                            $pic->id_pic = $model['pic']->id;
                            if (!$pic->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $transaction['pic']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pic']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['pic']->volunteers as $key => $volunteer)
                $model['volunteer'][] = $volunteer;
        }

        if ($error)
            return $this->render('form-pic', [
                'model' => $model,
                'idPeriode' => Periode::getPeriodeDashboard()->id,
                'title' => isset($id) ? 'Update Pic' : 'Tambah Pic',
            ]);
        else
            return $this->redirect(['pic']);
    }

    public function actionFormPicUmum()
    {
        $error = true;

        $model['pic_umum'] = PicUmum::getModel();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pic_umum']->load($post);

            $transaction['pic_umum'] = PicUmum::getDb()->beginTransaction();

            try {
                if (!$model['pic_umum']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                $transaction['pic_umum']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pic_umum']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-pic-umum', [
                'model' => $model,
                'idPeriode' => Periode::getPeriodeDashboard()->id,
                'title' => 'Update Note Umum',
            ]);
        else
            return $this->redirect(['pic']);
    }

    public function actionAktifkanPic($id)
    {
        $error = true;

        $model['pic'] = $this->findModelPicAktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pic']->load($post);

            $transaction = Pic::getDb()->beginTransaction();

            try {
                $model['pic']->status = 'Sedang Aktif';
                if (!$model['pic']->save()) {
                    throw new \yii\web\HttpException(400, 'PIC gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">PIC berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['pic']);
    }

    public function actionNonaktifkanPic($id)
    {
        $error = true;

        $model['pic'] = $this->findModelPicNonaktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pic']->load($post);

            $transaction = Pic::getDb()->beginTransaction();

            try {
                $model['pic']->status = 'Tidak Aktif';
                if (!$model['pic']->save()) {
                    throw new \yii\web\HttpException(400, 'PIC gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">PIC berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['pic']);
    }

    public function actionHapusPic($id)
    {
        $error = true;

        $model['pic'] = $this->findModelPic($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pic']->load($post);

            $transaction = Pic::getDb()->beginTransaction();

            try {
                \Yii::$app->db->createCommand()->delete('volunteer', 'id_pic = ' . (int) $model['pic']->id)->execute();
                if (!$model['pic']->delete()) {
                    throw new \yii\web\HttpException(400, 'PIC gagal dihapus.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">PIC berhasil dihapus.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['pic']);
    }

    ////

    public function actionDatatablesVolunteer()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'v.id',
                'p.id_periode',
                'v.nama AS volunteer',
                'pk.nama AS kota',
                'p.nama AS pic',
                'v.email',
                'v.handphone',
                'v.ukuran_kaos',
            ])
            ->from('volunteer v')
            ->join('JOIN', 'pic p', 'p.id = v.id_pic')
            ->join('LEFT JOIN', 'periode_kota pk', 'pk.id = p.id_periode_kota')
            ->where(['p.id_periode' => (Periode::getPeriodeDashboard()->id)])
        ;
        return $this->datatables($query, $post = Yii::$app->request->post(), PeriodeJenis::getDb());
    }

    public function actionVolunteer($id = null)
    {
        if (!$id) {
            return $this->render('list-volunteer', [
                'title' => 'Volunteer',
            ]);
        }

        $model['volunteer'] = $this->findModelVolunteer($id);
        return $this->render('detail-volunteer', [
            'model' => $model,
            'title' => 'Detail Volunteer',
        ]);
    }

    public function actionFormVolunteer($id = null)
    {
        $error = true;

        $model['volunteer'] = isset($id) ? $this->findModelVolunteer($id) : new Volunteer();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['volunteer']->load($post);

            $transaction['volunteer'] = Volunteer::getDb()->beginTransaction();

            try {
                if (!$model['volunteer']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                $transaction['volunteer']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['volunteer']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-volunteer', [
                'model' => $model,
                'idPeriode' => Periode::getPeriodeDashboard()->id,
                'title' => isset($id) ? 'Update Volunteer' : 'Tambah Volunteer',
            ]);
        else
            return $this->redirect(['pic']);
    }

    ///

    public function actionDatatablesReferralAgent()
    {
        if (!isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
            throw new \yii\web\HttpException(403, 'Akun kamu dilarang ngakses halaman ini, bambang.');
            
        $query = new \yii\db\Query();
        $query
            ->select([
                'ra.id',
                'ra.id_periode',
                'ra.status',
                'ra.nama',
                'rap.count_peserta AS jml_peserta',
                'ra.kode',
                'ra.handphone',
                'FORMAT(ra.fee, 2) AS fee',
                'ra.program',
                'ra.sekolah',
                'LOWER(CONCAT(r.name, " - ", p.name)) AS lokasi',
                'ra.email',
                'ra.nomor_identitas',
                'ra.alamat_lengkap',
                'ra.catatan',
            ])
            ->from('referral_agent ra')
            ->join('LEFT JOIN', 'regencies r', 'r.id = ra.id_regencies')
            ->join('LEFT JOIN', 'provinces p', 'p.id = r.province_id')
            ->join('JOIN', '(
                    select ra.id, count(p.id) as count_peserta
                    from referral_agent ra
                    LEFT JOIN transaksi t ON t.kode_referral_agent = ra.id AND t.status_aktif = "Aktif" AND t.status_bayar = "Sudah Bayar"
                    LEFT JOIN peserta p ON p.id_transaksi = t.id
                    group by ra.id
                ) rap', 'rap.id = ra.id')
            ->where(['ra.id_periode' => Periode::getPeriodeDashboard()->id])
        ;
        return $this->datatables($query, $post = Yii::$app->request->post(), PeriodeJenis::getDb());
    }

    public function actionReferralAgent($id = null)
    {
        if (!isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster']))
            throw new \yii\web\HttpException(403, 'Akun kamu dilarang ngakses halaman ini, bambang.');
            
        if (!$id) {
            return $this->render('list-referral-agent', [
                'title' => 'Referral Agent',
            ]);
        }

        $model['referral_agent'] = $this->findModelReferralAgent($id);
        return $this->render('detail-referral-agent', [
            'model' => $model,
            'title' => 'Detail Referral Agent',
        ]);
    }

    public function actionGetListKotaReferralAgent($idProvinces = null)
    {
        $regenciesOriginal = [];
        $regenciesOriginal = array_map('ucwords', array_map('strtolower', ArrayHelper::map(\technosmart\modules\location\models\Regencies::find()->select(['id', 'CONCAT(id, " - ", name) as name'])->where(['province_id' => $idProvinces])->orderBy('name')->asArray()->all(), 'id', 'name')));

        $regencies = [];
        foreach ($regenciesOriginal as $key => $value) {
            $regencies[] = [
                'value' => $key,
                'text' => $value,
            ];
        }
        return $this->json($regencies);
    }

    public function actionFormReferralAgent($id = null)
    {
        $error = true;

        $model['referral_agent'] = isset($id) ? $this->findModelReferralAgent($id) : new ReferralAgent();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['referral_agent']->load($post);

            $transaction['referral_agent'] = ReferralAgent::getDb()->beginTransaction();

            try {
                if ($model['referral_agent']->isNewRecord) {
                    $model['referral_agent']->id_periode = Periode::getPeriodeDashboard()->id;
                    $model['referral_agent']->status = 'Sedang Aktif';
                }
                if (!$model['referral_agent']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                $transaction['referral_agent']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['referral_agent']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-referral-agent', [
                'model' => $model,
                'idPeriode' => Periode::getPeriodeDashboard()->id,
                'title' => isset($id) ? 'Update Referral Agent' : 'Tambah Referral Agent',
            ]);
        else
            return $this->redirect(['referral-agent']);
    }

    public function actionAktifkanReferralAgent($id)
    {
        $error = true;

        $model['referral_agent'] = $this->findModelReferralAgentAktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['referral_agent']->load($post);

            $transaction = ReferralAgent::getDb()->beginTransaction();

            try {
                $model['referral_agent']->status = 'Sedang Aktif';
                if (!$model['referral_agent']->save()) {
                    throw new \yii\web\HttpException(400, 'Referral Agent gagal diaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Referral Agent berhasil diaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['referral-agent']);
    }

    public function actionNonaktifkanReferralAgent($id)
    {
        $error = true;

        $model['referral_agent'] = $this->findModelReferralAgentNonaktifkan($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['referral_agent']->load($post);

            $transaction = ReferralAgent::getDb()->beginTransaction();

            try {
                $model['referral_agent']->status = 'Tidak Aktif';
                if (!$model['referral_agent']->save()) {
                    dd($model['referral_agent']->fee);
                    ddx($model['referral_agent']->errors);
                    throw new \yii\web\HttpException(400, 'Referral Agent gagal dinonaktifkan.');
                }

                $error = false;
                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Referral Agent berhasil dinonaktifkan.</div>');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        return $this->redirect(['referral-agent']);
    }
}