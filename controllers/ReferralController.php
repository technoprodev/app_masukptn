<?php
namespace app_masukptn\controllers;

use Yii;
use app_tryout\models\ReferralAgent;
use app_tryout\models\Transaksi;
use app_tryout\models\Peserta;
use app_tryout\models\Periode;
use app_tryout\models\PeriodeJenis;
use app_tryout\models\PeriodeKota;
use app_tryout\models\Periode3Tokped;
use app_tryout\models\LoginReferral;
use technosmart\yii\web\Controller;

class ReferralController extends Controller
{
    protected function findModelPeserta($id)
    {
        if (($model = Peserta::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPesertaTambahan($id)
    {
        if (($model = PesertaTambahan::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data peserta tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenis($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisByPeriode($id)
    {
        if (($model = PeriodeJenis::find()->where(['id_periode' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Kode kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKota($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Kode kota tidak ditemukan.');
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->userReferral->isGuest) return $this->redirect(['dashboard']);

        $periode = Periode::getPeriodeAktif();
        $model['login'] = new LoginReferral(['scenario' => 'using-kode']);

        
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            return $this->redirect(['dashboard']);
        } else {
            return $this->render('form-login', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Login Referral',
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->userReferral->logout();
        return $this->goHome();
    }

    public function actionDashboard()
    {
        if (Yii::$app->userReferral->isGuest) return $this->redirect(['login']);

        $model['referral_agent'] = ReferralAgent::find()->where(['id' => Yii::$app->userReferral->identity->id])->one();
        $periode = Periode::getPeriodeAktif();

        return $this->render('form-dashboard', [
            'model' => $model,
            'idPeriode' => $periode->id,
            'title' => 'Dashboard Referral',
        ]);
    }

    public function actionPendaftaran()
    {
        if (Yii::$app->userReferral->isGuest) return $this->redirect(['login']);

        $error = true;

        $model['transaksi'] = new Transaksi();

        $periode = Periode::getPeriodeAktif();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi']->load($post);
            if (isset($post['Peserta'])) {
                foreach ($post['Peserta'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $peserta = $this->findModelPeserta($value['id']);
                        $peserta->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $peserta = $this->findModelPeserta(($value['id']*-1));
                        $peserta->isDeleted = true;
                    } else {
                        $peserta = new Peserta();
                        $peserta->setAttributes($value);
                    }
                    $model['peserta'][] = $peserta;
                }
            }

            $transaction = Transaksi::getDb()->beginTransaction();

            try {
                $model['transaksi']->id_periode = $periode->id;
                $model['transaksi']->status_bayar = 'Sudah Bayar';
                $model['transaksi']->kode_referral_agent = Yii::$app->userReferral->identity->id;
                $model['transaksi']->confirm_request_at = new \yii\db\Expression("now()");
                $model['transaksi']->confirm_at = new \yii\db\Expression("now()");

                $jumlahTiket = 0;
                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $jumlahTiket++;
                        }
                    }
                }
                $variableJumlahTiket = 'harga_' . $jumlahTiket . '_tiket';
                
                $model['transaksi']->jumlah_tiket = $jumlahTiket;
                $model['transaksi']->tagihan = 0;

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $peserta->id_transaksi = $model['transaksi']->id;
                            $peserta->nama = ucwords(strtolower($peserta->nama));

                            $peserta->kode = $periode->kode . $peserta->id_kota . $this->sequence('peserta-kode');

                            if (!$peserta->validate()) {
                                $error = true;
                            }

                            if ($peserta->id_periode_jenis && $peserta->id_periode_kota) {
                                $periodeJenis = $this->findModelPeriodeJenis($peserta->id_periode_jenis);
                                // $peserta->harga = $periodeJenis->$variableJumlahTiket;
                                $peserta->periode_penjualan = $periodeJenis->periode_penjualan;
                                $model['transaksi']->tagihan += (int)$peserta->harga;
                            } else {
                                $error = true;
                                if (!$peserta->id_periode_jenis) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_jenis' => 'Jenis tiket harus dipilih.',
                                    ]);
                                }
                                if (!$peserta->id_periode_kota) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_kota' => 'Lokasi tryout harus dipilih.',
                                    ]);
                                }
                            }

                            // $model['transaksi']->tagihan += (int)$peserta->harga;

                            if (!$peserta->id_kota) {
                                $error = true;
                                $model['peserta'][$key]->addErrors([
                                    'id_kota' => 'Domisili belum dipilih.',
                                ]);
                            }

                            $count = count($model['peserta']);
                            for ($i = $key + 1; $i < $count; $i++) { 
                                if ($peserta->email == $model['peserta'][$i]->email) {
                                    $model['peserta'][$i]->addErrors([
                                        'email' => 'Email tidak boleh ada yang sama.',
                                    ]);
                                    $error = true;
                                }
                            }
                        }
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                    }
                
                    foreach ($model['peserta'] as $key => $peserta) {
                        if ($peserta->isDeleted) {
                            if (!$peserta->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$peserta->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;
                
                if (($model['transaksi'] = Transaksi::find()->where(['id' => $model['transaksi']->id])->one()) == null) {
                    throw new \yii\web\HttpException(404, 'The requested page does not exist.');
                }
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\"><b><u>Screenshot Halaman ini terlebih dahulu!</u></b><br><br>Pendaftaran berhasil dilakukan. <br><br>SIMPAN BAIK BAIK data berikut <br>Username : <b>' . $model['peserta'][0]->username . '</b><br> password <b>' . $model['peserta'][0]->password . '</b>.<br> Username dan password digunakan untuk login dan download tiket. <br><br></div>');

                $model['peserta'] = $model['peserta'][0];

                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                        ->setSubject('Tiket Tryout Nasional ' . Yii::$app->params['app.name'] . ' 2020 Milikmu Siap Dicetak')->send();
                } else if (YII_ENV == 'dev') {
                    /*return $this->render('/email/email-konfirmasi-diterima', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Konfirmasi Diterima',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                        ->setSubject('Tiket Tryout Nasional MasukPTNid 2020 Milikmu Siap Dicetak')->send();
                }

                $transaction->commit();
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['transaksi']->pesertas as $key => $peserta)
                $model['peserta'][] = $peserta;
        }

        if ($error)
            return $this->render('form-pendaftaran', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Pendaftaran',
            ]);
        else
            return $this->redirect(['referral/dashboard']);
    }
}