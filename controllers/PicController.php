<?php
namespace app_masukptn\controllers;

use Yii;
use app_tryout\models\PicUmum;
use app_tryout\models\Pic;
use app_tryout\models\PicPemasukan;
use app_tryout\models\PicPengeluaran;
use app_tryout\models\Volunteer;
use app_tryout\models\Periode;
use app_tryout\models\LoginPic;
use technosmart\yii\web\Controller;

class PicController extends Controller
{
    /*public static $permissions = [
        'create'
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'view', 'create'], 'create'],
            ]),
        ];
    }*/

    protected function findModelPicByKodeUsernameDownload($kode, $username)
    {
        if (($model['pic'] = Pic::find()->where(['kode' => $kode, 'username' => $username])->one()) !== null) {
            return $model['pic'];
        } else {
            $model['pic'] = new Pic();
            $model['pic']->addErrors([
                'kode' => 'Kombinasi kode dan username tidak ditemukan',
                'username' => 'Kombinasi username dan kode tidak ditemukan',
            ]);
            return $model['pic'];
        }
    }

    protected function findModelPic($id)
    {
        if (($model = Pic::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data pic tidak ditemukan.');
        }
    }

    protected function findModelPicPemasukan($id)
    {
        if (($model = PicPemasukan::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data pemasukan tidak ditemukan.');
        }
    }

    protected function findModelPicPengeluaran($id)
    {
        if (($model = PicPengeluaran::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data pengeluaran tidak ditemukan.');
        }
    }

    protected function findModelVolunteer($id)
    {
        if (($model = Volunteer::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data volunteer tidak ditemukan.');
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->userPic->isGuest) return $this->redirect(['dashboard']);

        $periode = Periode::getPeriodeAktif();
        $model['login'] = new LoginPic(['scenario' => 'using-kode']);
        
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            return $this->redirect(['dashboard']);
        } else {
            return $this->render('form-login', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Login PIC',
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->userPic->logout();
        return $this->goHome();
    }

    public function actionDashboard()
    {
        if (Yii::$app->userPic->isGuest) return $this->redirect(['login']);

        $model['pic'] = Pic::find()->where(['id' => Yii::$app->userPic->identity->id])->one();
        $periode = Periode::getPeriodeAktif();

        $query = new \yii\db\Query();
        $query
            ->select([
                'p.sekolah as sekolah',
                'count(case when t.status_bayar = "Sudah Bayar" then p.id end) as tiket_sudah_bayar',
                'count(case when pj.nama = "Saintek (IPA)" and t.status_bayar = "Sudah Bayar" then p.id end) as tiket_saintek_ipa',
                'count(case when pj.nama = "Soshum (IPS)" and t.status_bayar = "Sudah Bayar" then p.id end) as tiket_soshum_ips',
            ])
            ->from('transaksi t')
            ->join('JOIN', 'peserta p', 'p.id_transaksi = t.id AND t.status_aktif = "Aktif"')
            ->join('RIGHT JOIN', 'periode_kota pk', 'pk.id = p.id_periode_kota AND pk.id = ' . $model['pic']->id_periode_kota)
            ->join('LEFT JOIN', 'periode_jenis pj', 'pj.id = p.id_periode_jenis')
            ->groupBy(['p.sekolah'])
            ;
        $model['tiket'] = $query->all();

        $model['pic_umum'] = PicUmum::getModel();

        return $this->render('form-dashboard', [
            'model' => $model,
            'idPeriode' => $periode->id,
            'title' => 'Dashboard Pic',
        ]);
    }

    public function actionFormPic($f)
    {
        if (Yii::$app->userPic->isGuest) return $this->redirect(['login']);

        $error = true;

        $model['pic'] = Pic::find()->where(['id' => Yii::$app->userPic->identity->id])->one();

        $model['periode_kota'] = $model['pic']->periodeKota;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pic']->load($post);
            $model['periode_kota']->load($post);
            if (isset($post['Volunteer'])) {
                foreach ($post['Volunteer'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pic = $this->findModelVolunteer($value['id']);
                        $pic->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pic = $this->findModelVolunteer(($value['id']*-1));
                        $pic->isDeleted = true;
                    } else {
                        $pic = new Volunteer();
                        $pic->setAttributes($value);
                    }
                    $model['volunteer'][] = $pic;
                }
            }
            if (isset($post['PicPemasukan'])) {
                foreach ($post['PicPemasukan'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pic = $this->findModelPicPemasukan($value['id']);
                        $pic->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pic = $this->findModelPicPemasukan(($value['id']*-1));
                        $pic->isDeleted = true;
                    } else {
                        $pic = new PicPemasukan();
                        $pic->setAttributes($value);
                    }
                    $model['pic_pemasukan'][] = $pic;
                }
            }
            if (isset($post['PicPengeluaran'])) {
                foreach ($post['PicPengeluaran'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $pic = $this->findModelPicPengeluaran($value['id']);
                        $pic->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $pic = $this->findModelPicPengeluaran(($value['id']*-1));
                        $pic->isDeleted = true;
                    } else {
                        $pic = new PicPengeluaran();
                        $pic->setAttributes($value);
                    }
                    $model['pic_pengeluaran'][] = $pic;
                }
            }

            $transaction['pic'] = Pic::getDb()->beginTransaction();

            try {
                if ($model['pic']->isNewRecord) {
                    $model['pic']->id_periode = Periode::getPeriodeAktif()->id;
                }
                if (!$model['pic']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                if (!$model['periode_kota']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                if (isset($model['volunteer']) and is_array($model['volunteer'])) {
                    foreach ($model['volunteer'] as $key => $pic) {
                        $pic->id_pic = $model['pic']->id;
                        if (!$pic->isDeleted && !$pic->validate()) $error = true;                        
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['volunteer'] as $key => $pic) {
                        if ($pic->isDeleted) {
                            if (!$pic->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pic->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if (isset($model['pic_pemasukan']) and is_array($model['pic_pemasukan'])) {
                    foreach ($model['pic_pemasukan'] as $key => $pic) {
                        $pic->id_pic = $model['pic']->id;
                        if (!$pic->isDeleted && !$pic->validate()) $error = true;                        
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['pic_pemasukan'] as $key => $pic) {
                        if ($pic->isDeleted) {
                            if (!$pic->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pic->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if (isset($model['pic_pengeluaran']) and is_array($model['pic_pengeluaran'])) {
                    foreach ($model['pic_pengeluaran'] as $key => $pic) {
                        $pic->id_pic = $model['pic']->id;
                        if (!$pic->isDeleted && !$pic->validate()) $error = true;                        
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['pic_pengeluaran'] as $key => $pic) {
                        if ($pic->isDeleted) {
                            if (!$pic->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$pic->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $transaction['pic']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pic']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['pic']->volunteers as $key => $volunteer)
                $model['volunteer'][] = $volunteer;

            foreach ($model['pic']->picPemasukans as $key => $picPemasukan)
                $model['pic_pemasukan'][] = $picPemasukan;

            foreach ($model['pic']->picPengeluarans as $key => $picPengeluaran)
                $model['pic_pengeluaran'][] = $picPengeluaran;
        }

        if ($error)
            return $this->render('form-pic', [
                'model' => $model,
                'idPeriode' => Periode::getPeriodeAktif()->id,
                'f' => $f,
                'title' => (function($form){
                    if ($form == 'biodata') {
                        return 'Edit Biodata';
                    }/* else if ($form == 'rangkuman_kegiatan') {
                        return 'Isi Rangkuman Kegiatan';
                    } else if ($form == 'link_foto') {
                        return 'Isi Link Foto';
                    } else if ($form == 'laporan_keuangan') {
                        return 'Upload Laporan Keuangan';
                    }*/ else if ($form == 'laporan_kegiatan') {
                        return 'Laporan Kegiatan';
                    } else if ($form == 'laporan_keuangan') {
                        return 'Laporan Keuangan';
                    } else if ($form == 'acara_lain_diluar_tryout') {
                        return 'Acara Lain Diluar Tryout';
                    }
                })($f),
            ]);
        else
            return $this->redirect(['dashboard']);
    }

    public function actionFormVolunteer($id = null)
    {
        if (Yii::$app->userPic->isGuest) return $this->redirect(['login']);

        $error = true;

        $model['pic'] = Pic::find()->where(['id' => Yii::$app->userPic->identity->id])->one();
        $model['volunteer'] = isset($id) ? $this->findModelVolunteer($id) : new Volunteer();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['volunteer']->load($post);

            $transaction = Volunteer::getDb()->beginTransaction();

            try {
                $model['volunteer']->id_pic = $model['pic']->id;

                if (!$model['volunteer']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $transaction->commit();
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div>Data volunteer berhasil disimpan.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-volunteer', [
                'model' => $model,
                'idPeriode' => Periode::getPeriodeAktif()->id,
                'title' => (isset($id) ? 'Update' : 'Pendaftaran') . ' Volunteer oleh PIC',
            ]);
        else
            return $this->redirect(['dashboard']);
    }

    public function actionHadiahPic()
    {
        if (Yii::$app->userPic->isGuest) return $this->redirect(['login']);

        $model['pic'] = Pic::find()->where(['id' => Yii::$app->userPic->identity->id])->one();
        return $this->render('hadiah-pic', [
            'model' => $model,
            'title' => 'Hadiah Pic',
        ]);
    }

    public function actionSertifikatPic()
    {
        // throw new \yii\web\HttpException(404, 'Hanya bisa diakses setelah hari H');
        if (Yii::$app->userPic->isGuest) return $this->redirect(['login']);

        $model['pic'] = Pic::find()->where(['id' => Yii::$app->userPic->identity->id])->one();

        // $title = 'Sertifikat ' . Yii::$app->params['app.name'] . ' PIC ' . $model['peserta']->nama;
        // return $this->render('download-kartu-ujian1', ['model' => $model, 'title' => $title]);
        $this->layout = 'download';
        $title = 'Sertifikat ' . Yii::$app->params['app.name'] . ' PIC ' . $model['pic']->nama;

        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->render('//peserta/download-sertifikat', ['model' => $model, 'title' => $title]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream($title . '.pdf');
        exit;
    }

    public function actionSertifikatVolunteer($id)
    {
        // throw new \yii\web\HttpException(404, 'Hanya bisa diakses setelah hari H');
        if (Yii::$app->userPic->isGuest) return $this->redirect(['login']);

        $model['pic'] = Pic::find()->where(['id' => Yii::$app->userPic->identity->id])->one();
        $model['volunteer'] = isset($id) ? $this->findModelVolunteer($id) : new Volunteer();

        // $title = 'Sertifikat ' . Yii::$app->params['app.name'] . ' PIC ' . $model['peserta']->nama;
        // return $this->render('download-kartu-ujian1', ['model' => $model, 'title' => $title]);
        $this->layout = 'download';
        $title = 'Sertifikat ' . Yii::$app->params['app.name'] . ' Volunteer ' . $model['volunteer']->nama;

        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->render('//peserta/download-sertifikat', ['model' => $model, 'title' => $title]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream($title . '.pdf');
        exit;
    }
}