<?php
namespace app_masukptn\controllers;

use Yii;
use app_tryout\models\Peserta;
use app_tryout\models\Periode;
use technosmart\yii\web\Controller;

class EmailController extends Controller
{
	public function actionPendaftaran()
	{
        $periode = Periode::getPeriodeAktif();
        $pesertas1 = Peserta::find()->where(['id_periode' => $periode->id, 'status_bayar' => 'Belum Bayar', 'status_aktif' => 'Aktif', 'resend_pendaftaran_1' => 0, 'resend_pendaftaran_2' => 0])
            ->andWhere('DATE(CREATED_AT) <= CURDATE() - INTERVAL 2 DAY')
            ->asArray()
            ->all();
        $pesertas2 = Peserta::find()->where(['id_periode' => $periode->id, 'status_bayar' => 'Belum Bayar', 'status_aktif' => 'Aktif', 'resend_pendaftaran_1' => 1, 'resend_pendaftaran_2' => 0])
            ->andWhere('resend_pendaftaran_1_date IS NOT NULL')
            ->andWhere('DATE(resend_pendaftaran_1_date) <= CURDATE() - INTERVAL 3 DAY')
            // ->andWhere(['<>', 'resend_pendaftaran_1_date', new \yii\db\Expression('DATE(NOW())')])
            ->asArray()
            ->all();
        // ddx($pesertas1, $pesertas2);

        if (ob_get_level() == 0) ob_start();

        foreach ($pesertas1 as $key => $peserta) {
            $model['peserta'] = Peserta::find()->where(['id' => $peserta['id']])->one();

            try {
                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                } else if (YII_ENV == 'dev') {
                    /*return $this->render('/email/email-pendaftaran', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Pendaftaran',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                }
                $model['peserta']->resend_pendaftaran_1 = 1;
                $model['peserta']->resend_pendaftaran_1_date = date('Y-m-d');
                if (!$model['peserta']->save()) {
                    throw new \yii\web\HttpException(400, 'Peserta gagal diupdate.');
                }
                echo '2 hr blm byr - (' . $model['peserta']->kode . ' - ' . $model['peserta']->nama . ' - ' . $model['peserta']->email . ') - berhasil<br>';
            } catch (\Throwable $e) {
                echo '2 hr blm byr - (' . $model['peserta']->kode . ' - ' . $model['peserta']->nama . ' - ' . $model['peserta']->email . ') - gagal<br>';
            }
            ob_flush();
            flush();
        }
        foreach ($pesertas2 as $key => $peserta) {
            $model['peserta'] = Peserta::find()->where(['id' => $peserta['id']])->one();

            try {
                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                } else if (YII_ENV == 'dev') {
                    /*return $this->render('/email/email-pendaftaran', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Pendaftaran',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                }
                $model['peserta']->resend_pendaftaran_2 = 1;
                $model['peserta']->resend_pendaftaran_2_date = date('Y-m-d');
                if (!$model['peserta']->save()) {
                    throw new \yii\web\HttpException(400, 'Peserta gagal diupdate.');
                }
                echo '5 hr blm byr - (' . $model['peserta']->kode . ' - ' . $model['peserta']->nama . ' - ' . $model['peserta']->email . ') - berhasil<br>';
            } catch (\Throwable $e) {
                echo '5 hr blm byr - (' . $model['peserta']->kode . ' - ' . $model['peserta']->nama . ' - ' . $model['peserta']->email . ') - gagal<br>';
            }
            ob_flush();
            flush();
        }
        echo 'selesai';
        ob_end_flush();
        exit;
    }
}
