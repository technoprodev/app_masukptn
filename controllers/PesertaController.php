<?php
namespace app_masukptn\controllers;

use Yii;
use app_tryout\models\Transaksi;
use app_tryout\models\Peserta;
use app_tryout\models\Periode;
use app_tryout\models\PeriodeJenis;
use app_tryout\models\PeriodeKota;
use app_tryout\models\Periode3Tokped;
use app_tryout\models\LoginPeserta;
use app_tryout\models\Misi;
use app_tryout\models\PesertaMisi;
use technosmart\yii\web\Controller;
use yii\helpers\ArrayHelper;

class PesertaController extends Controller
{
    /*public static $permissions = [
        'create'
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'view', 'create'], 'create'],
            ]),
        ];
    }*/

    protected function findModelTransaksi($id)
    {
        if (($model = Transaksi::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data transaksi tidak ditemukan.');
        }
    }

    protected function findModelPeserta($id)
    {
        if (($model = Peserta::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Data transaksi tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenis($id)
    {
        if (($model = PeriodeJenis::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Jenis tiket tidak ditemukan.');
        }
    }

    protected function findModelPeriodeJenisByPeriode($id)
    {
        if (($model = PeriodeJenis::find()->where(['id_periode' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Jenis tiket tidak ditemukan.');
        }
    }

    protected function findModelPeriodeKota($id)
    {
        if (($model = PeriodeKota::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Kota tidak ditemukan.');
        }
    }

    protected function findModelPeriodeRegency($id)
    {
        if (($model = \technosmart\modules\location\models\Regencies::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'Kode kota tidak ditemukan.');
        }
    }

    public function actionPendaftaran()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('error', 'Untuk melakukan pembelian tiket baru, harap logout terlebih dahulu.');
            return $this->redirect(['dashboard']);
        }

        $error = true;

        $model['transaksi'] = new Transaksi();

        $periode = Periode::getPeriodeAktif();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi']->load($post);
            if (isset($post['Peserta'])) {
                foreach ($post['Peserta'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $peserta = $this->findModelPeserta($value['id']);
                        $peserta->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $peserta = $this->findModelPeserta(($value['id']*-1));
                        $peserta->isDeleted = true;
                    } else {
                        $peserta = new Peserta();
                        $peserta->setAttributes($value);
                    }
                    $model['peserta'][] = $peserta;
                }
            }

            $transaction = Transaksi::getDb()->beginTransaction();

            try {
                $model['transaksi']->id_periode = $periode->id;
                $model['transaksi']->status_bayar = 'Hanya Daftar';

                $jumlahTiket = 0;
                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $jumlahTiket++;
                        }
                    }
                }
                $variableJumlahTiket = 'harga_' . $jumlahTiket . '_tiket';
                
                $model['transaksi']->jumlah_tiket = $jumlahTiket;
                $model['transaksi']->tagihan = 0;

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $peserta->id_transaksi = $model['transaksi']->id;
                            $peserta->nama = ucwords(strtolower($peserta->nama));

                            $peserta->kode = $periode->kode . $peserta->id_kota . $this->sequence('peserta-kode');

                            if (!$peserta->validate()) {
                                $error = true;
                            }

                            /*if ($peserta->id_periode_jenis && $peserta->id_periode_kota) {
                                $periodeJenis = $this->findModelPeriodeJenis($peserta->id_periode_jenis);
                                $peserta->harga = $periodeJenis->$variableJumlahTiket;
                                $peserta->periode_penjualan = $periodeJenis->periode_penjualan;
                                $model['transaksi']->tagihan += (int)$peserta->harga;
                            } else {
                                $error = true;
                                if (!$peserta->id_periode_jenis) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_jenis' => 'Jenis tiket harus dipilih.',
                                    ]);
                                }
                                if (!$peserta->id_periode_kota) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_kota' => 'Lokasi tryout harus dipilih.',
                                    ]);
                                }
                            }*/

                            $model['transaksi']->tagihan += (int)$peserta->harga;

                            if (!$peserta->id_kota) {
                                $error = true;
                                $model['peserta'][$key]->addErrors([
                                    'id_kota' => 'Domisili belum dipilih.',
                                ]);
                            }

                            $count = count($model['peserta']);
                            for ($i = $key + 1; $i < $count; $i++) { 
                                if ($peserta->email == $model['peserta'][$i]->email) {
                                    $model['peserta'][$i]->addErrors([
                                        'email' => 'Email tidak boleh ada yang sama.',
                                    ]);
                                    $error = true;
                                }
                            }
                        }
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                    }
                
                    foreach ($model['peserta'] as $key => $peserta) {
                        if ($peserta->isDeleted) {
                            if (!$peserta->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$peserta->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $model['peserta'] = $model['peserta'][0];

                $model['transaksi'] = $this->findModelTransaksi($model['transaksi']->id);
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\"><b><u>Screenshot Halaman ini terlebih dahulu!</u></b><br><br>Pendaftaran berhasil dilakukan. <br><br>SIMPAN BAIK BAIK data berikut <br>Username : <b>' . $model['peserta']->username . '</b><br> password <b>' . $model['peserta']->password . '</b>.<br> Username dan password digunakan untuk login dan download tiket. <br><br></div>');

                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                        ->setSubject('Username dan Password MasukPTNid')->send();
                } else if (YII_ENV == 'dev') {
                    /*return $this->render('/email/email-pendaftaran', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Pendaftaran',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-pendaftaran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                        ->setSubject('Username dan Password MasukPTNid')->send();
                }
                
                $transaction->commit();

                $model['login'] = new LoginPeserta(['scenario' => 'using-username']);
                $model['login']->username = $model['peserta']->username;
                $model['login']->password = $model['peserta']->password;
                $model['login']->login();
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            foreach ($model['transaksi']->pesertas as $key => $peserta)
                $model['peserta'][] = $peserta;
        }

        if ($error)
            return $this->render('form-pendaftaran', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Pendaftaran',
            ]);
        else
            return $this->redirect(['peserta/login']);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->redirect(['dashboard']);

        $periode = Periode::getPeriodeAktif();
        $model['login'] = new LoginPeserta(['scenario' => 'using-username']);
        
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            return $this->redirect(['dashboard']);
        } else {
            return $this->render('form-login', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Login',
            ]);
        }
    }

    public function actionDashboard()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['login']);

        $model['peserta'] = Peserta::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $model['transaksi'] = $model['peserta']->transaksi;
        $periode = Periode::getPeriodeAktif();
        
        if (!isset($post))
            return $this->render('form-dashboard', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Halaman Peserta',
            ]);
        else
            return $this->redirect(['login', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionPendaftaranBeli()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['login']);

        $error = true;

        $peserta = Peserta::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $model['transaksi'] = $peserta->transaksi;

        $periode = Periode::getPeriodeAktif();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi']->load($post);
            if (isset($post['Peserta'])) {
                foreach ($post['Peserta'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $peserta = $this->findModelPeserta($value['id']);
                        $peserta->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $peserta = $this->findModelPeserta(($value['id']*-1));
                        $peserta->isDeleted = true;
                    } else {
                        $peserta = new Peserta();
                        $peserta->setAttributes($value);
                    }
                    $model['peserta'][] = $peserta;
                }
            }

            $transaction = Transaksi::getDb()->beginTransaction();

            try {
                $model['transaksi']->id_periode = $periode->id;
                $model['transaksi']->status_bayar = 'Belum Bayar';

                $jumlahTiket = 0;
                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $jumlahTiket++;
                        }
                    }
                }
                $variableJumlahTiket = 'harga_' . $jumlahTiket . '_tiket';
                
                $model['transaksi']->jumlah_tiket = $jumlahTiket;
                $model['transaksi']->tagihan = 0;

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $peserta->id_transaksi = $model['transaksi']->id;
                            $peserta->nama = ucwords(strtolower($peserta->nama));

                            // $peserta->kode = $periode->kode . $peserta->id_kota . $this->sequence('peserta-kode');

                            if (!$peserta->validate()) {
                                $error = true;
                            }

                            if ($peserta->id_periode_jenis && $peserta->id_periode_kota) {
                                $periodeJenis = $this->findModelPeriodeJenis($peserta->id_periode_jenis);
                                $peserta->harga = $periodeJenis->$variableJumlahTiket;
                                $peserta->periode_penjualan = $periodeJenis->periode_penjualan;
                                $model['transaksi']->tagihan += (int)$peserta->harga;
                            } else {
                                $error = true;
                                if (!$peserta->id_periode_jenis) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_jenis' => 'Jenis tiket harus dipilih.',
                                    ]);
                                }
                                if (!$peserta->id_periode_kota) {
                                    $model['peserta'][$key]->addErrors([
                                        'id_periode_kota' => 'Lokasi tryout harus dipilih.',
                                    ]);
                                }
                            }

                            // $model['transaksi']->tagihan += (int)$peserta->harga;
                            
                            /*if (!$peserta->id_kota) {
                                $error = true;
                                $model['peserta'][$key]->addErrors([
                                    'id_kota' => 'Domisili belum dipilih.',
                                ]);
                            }*/

                            $count = count($model['peserta']);
                            for ($i = $key + 1; $i < $count; $i++) { 
                                if ($peserta->email == $model['peserta'][$i]->email) {
                                    $model['peserta'][$i]->addErrors([
                                        'email' => 'Email tidak boleh ada yang sama.',
                                    ]);
                                    $error = true;
                                }
                            }
                        }
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                    } 

                    foreach ($model['peserta'] as $key => $peserta) {
                        if ($peserta->isDeleted) {
                            if (!$peserta->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$peserta->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                $model['transaksi'] = $this->findModelTransaksi($model['transaksi']->id);
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\"><b><u>Screenshot Halaman ini terlebih dahulu!</u></b><br><br>Pendaftaran berhasil dilakukan. Harap cek email untuk mendapat petunjuk pembayaran. <br><br>SIMPAN BAIK BAIK data berikut <br>Nomor Peserta : <b>' . $model['peserta'][0]->kode . '</b> dan email <b>' . $model['peserta'][0]->email . '</b>.<br> Data Nomor Peserta digunakan untuk melakukan konfirmasi pembayaran, login dan download tiket. <br><br>Nomor Peserta tambahan bisa dilihat setelah kamu login.</div>');

                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-pendaftaran-beli', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta'][0]->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                } else if (YII_ENV == 'dev') {
                    /*return $this->render('/email/email-pendaftaran-beli', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Pendaftaran',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-pendaftaran-beli', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta'][0]->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                }

                $transaction->commit();
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            $model['peserta'] = $model['transaksi']->pesertas;
        }

        if ($error)
            return $this->render('form-pendaftaran-beli', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Pilih Kota Tryout',
            ]);
        else
            return $this->redirect(['login', 'kode' => $model['peserta'][0]->kode, 'email' => $model['peserta'][0]->email]);
    }

    public function actionKonfirmasi()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['login']);

        $model['peserta'] = Peserta::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $model['transaksi'] = $model['peserta']->transaksi;
        $model['transaksi']->scenario = 'konfirmasi';
        $periode = Periode::getPeriodeAktif();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi']->load($post);

            $transaction = Transaksi::getDb()->beginTransaction();

            try {
                if ($model['transaksi']->status_bayar == 'Sudah Bayar') {
                    throw new \yii\web\HttpException(400, 'Pembayaran Anda sudah berhasil dilakukan. Anda tidak perlu melakukan konfirmasi pembayaran lagi.');
                }

                if ($model['transaksi']->status_bayar == 'Hanya Daftar') {
                    throw new \yii\web\HttpException(400, 'Anda tidak dapat melakukan konfirmasi karena belum membeli tiket. Harap membeli tiket terlebih dahulu dengan cara login dan membuka halaman dashboard peserta.');
                }
                $model['transaksi']->confirm_request_at = new \yii\db\Expression("now()");
                $model['transaksi']->pembayaran_atas_nama = strtoupper($model['transaksi']->pembayaran_atas_nama);

                $kodeNotFound = false;
                $model['periode3_tokped'] = Periode3Tokped::find()->where(['kode' => $model['transaksi']->pembayaran_atas_nama])->one();
                if (!$model['periode3_tokped']) {
                    /*$model['transaksi']->addErrors([
                        'pembayaran_atas_nama' => 'Kode ini tidak valid.',
                    ]);*/
                    $kodeNotFound = true;
                } else if ($model['periode3_tokped']->id_transaksi && $model['periode3_tokped']->id_transaksi != $model['transaksi']->id) {
                    $pemakai = Peserta::find()->where(['id_transaksi' => $model['periode3_tokped']->id_transaksi])->one();
                    $model['transaksi']->addErrors([
                        'pembayaran_atas_nama' => 'Kamu tidak dapat melakukan konfirmasi karena kode tokopedia tersebut sudah pernah dipakai oleh peserta dengan kode ' . $pemakai->kode,
                    ]);
                    $kodeNotFound = true;
                    throw new \yii\web\HttpException(400, 'Kamu tidak dapat melakukan konfirmasi karena kode tokopedia tersebut sudah pernah dipakai.');
                }

                if ($kodeNotFound) {
                    $model['transaksi']->status_bayar = 'Dalam Proses Konfirmasi';

                    if (!$model['transaksi']->save()) {
                        throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                    }

                    $error = false;
                    Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/sand-clock.png\" width=\"70px;\"></div><div class=\"text-center\">Konfirmasi pembayaran berhasil dilakukan. Kamu akan mendapat notifikasi melalui email setelah admin melakukan verifikasi.</div>');

                    if (YII_ENV == 'prod') {
                        \Yii::$app->mail->compose('email/email-konfirmasi', ['model' => $model])
                            ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                            ->setSubject('Konfirmasi Pembayaran Sedang Diproses')->send();
                    } else if (YII_ENV == 'dev') {
                        /*return $this->render('/email/email-konfirmasi', [
                            'model' => $model,
                            'idPeriode' => $periode->id,
                            'title' => 'konfirmasi',
                        ]);*/
                        \Yii::$app->mail->compose('email/email-konfirmasi', ['model' => $model])
                            ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                            ->setSubject('Konfirmasi Pembayaran Sedang Diproses')->send();
                    }
                } else {
                    $model['periode3_tokped']->id_transaksi = $model['transaksi']->id;

                    if (!$model['periode3_tokped']->save()) {
                        throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                    }

                    $model['transaksi']->status_bayar = 'Sudah Bayar';
                    if ($model['periode3_tokped']->harga !== null) {
                        $model['transaksi']->tagihan = 0;
                        foreach ($model['transaksi']->pesertas as $key => $peserta) {
                            $peserta->harga = $model['periode3_tokped']->harga;
                            if (!$peserta->save()) {
                                throw new \yii\web\HttpException(400, 'Peserta gagal dikonfirmasi.');
                            }
                            $model['transaksi']->tagihan += $peserta->harga;
                        }
                    }
                    $model['transaksi']->confirm_at = new \yii\db\Expression("now()");
                    if (!$model['transaksi']->save()) {
                        throw new \yii\web\HttpException(400, 'Peserta gagal dikonfirmasi.');
                    }

                    $error = false;
                    Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/sand-clock.png\" width=\"70px;\"></div><div class=\"text-center\">Terima kasih, pembayaran kamu sudah kami terima.</div>');

                    if (YII_ENV == 'prod') {
                        \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                            ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta']->email)
                            ->setSubject('Tiket Tryout Nasional ' . Yii::$app->params['app.name'] . ' 2020 Milikmu Siap Dicetak')->send();
                    } else if (YII_ENV == 'dev') {
                        /*return $this->render('/email/email-konfirmasi-diterima', [
                            'model' => $model,
                            'idPeriode' => $periode->id,
                            'title' => 'Konfirmasi Diterima',
                        ]);*/
                        \Yii::$app->mail->compose('email/email-konfirmasi-diterima', ['model' => $model])
                            ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta']->email)
                            ->setSubject('Tiket Tryout Nasional MasukPTNid 2020 Milikmu Siap Dicetak')->send();
                    }
                }

                $transaction->commit();
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            $model['transaksi']->id_periode_metode_pembayaran = null;
            $model['transaksi']->tanggal_pembayaran = null;
            $model['transaksi']->pembayaran_atas_nama = null;
            $model['transaksi']->bukti_pembayaran = null;
            $model['transaksi']->catatan_pembayaran = null;
        }
        
        if (!isset($post))
            return $this->render('form-konfirmasi', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Konfirmasi Pembayaran',
            ]);
        else
            return $this->redirect(['login', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email]);
    }

    public function actionPhoto()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['login']);

        $error = true;

        $periode = Periode::getPeriodeAktif();
        $model['periode_jenis'] = $this->findModelPeriodeJenisByPeriode($periode->id);

        $peserta = Peserta::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $model['transaksi'] = $peserta->transaksi;
        $model['peserta'] = $model['transaksi']->pesertas;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi']->load($post);
            $model['peserta'] = null;
            if (isset($post['Peserta'])) {
                foreach ($post['Peserta'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $peserta = $this->findModelPeserta($value['id']);
                        $peserta->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $peserta = $this->findModelPeserta(($value['id']*-1));
                        $peserta->isDeleted = true;
                    } else {
                        $peserta = new Peserta();
                        $peserta->setAttributes($value);
                    }
                    $model['peserta'][] = $peserta;
                }
            }

            $transaction = Transaksi::getDb()->beginTransaction();

            try {
                $model['transaksi']->id_periode = $periode->id;
                $model['transaksi']->status_bayar = 'Belum Bayar';

                $jumlahTiket = 0;
                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $jumlahTiket++;
                        }
                    }
                }
                $variableJumlahTiket = 'harga_' . $jumlahTiket . '_tiket';
                
                $model['transaksi']->jumlah_tiket = $jumlahTiket;
                $model['transaksi']->tagihan = 0;

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                if (isset($model['peserta']) and is_array($model['peserta'])) {
                    foreach ($model['peserta'] as $key => $peserta) {
                        if (!$peserta->isDeleted) {
                            $peserta->id_transaksi = $model['transaksi']->id;
                            $peserta->nama = ucwords(strtolower($peserta->nama));

                            if (!$peserta->validate()) {
                                $error = true;
                            }

                            if ($peserta->id_periode_jenis && $peserta->id_periode_kota) {
                                $periodeJenis = $this->findModelPeriodeJenis($peserta->id_periode_jenis);
                                $peserta->harga = $periodeJenis->$variableJumlahTiket;
                                $peserta->periode_penjualan = $periodeJenis->periode_penjualan;
                                $model['transaksi']->tagihan += (int)$peserta->harga;
                            } else {
                                $error = true;
                                if (!$peserta->id_periode_jenis) {
                                    $model['peserta'][$i]->addErrors([
                                        'id_periode_jenis' => 'Jenis tiket harus dipilih.',
                                    ]);
                                }
                                if (!$peserta->id_periode_kota) {
                                    $model['peserta'][$i]->addErrors([
                                        'id_periode_kota' => 'Lokasi tryout harus dipilih.',
                                    ]);
                                }
                            }

                            $count = count($model['peserta']);
                            for ($i = $key + 1; $i < $count; $i++) { 
                                if ($peserta->email == $model['peserta'][$i]->email) {
                                    $model['peserta'][$i]->addErrors([
                                        'email' => 'Email tidak boleh ada yang sama.',
                                    ]);
                                    $error = true;
                                }
                            }
                        }
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                    } 

                    foreach ($model['peserta'] as $key => $peserta) {
                        if ($peserta->isDeleted) {
                            if (!$peserta->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$peserta->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                if (!$model['transaksi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
                }

                $error = false;

                // ddx($model);
                $transaction->commit();
                $model['transaksi'] = $this->findModelTransaksi($model['transaksi']->id);
                Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\"><b><u>Screenshot Halaman ini terlebih dahulu!</u></b><br><br>Pendaftaran berhasil dilakukan. Harap cek email untuk mendapat petunjuk pembayaran. <br><br>SIMPAN BAIK BAIK data berikut <br>Nomor Peserta : <b>' . $model['peserta'][0]->kode . '</b> dan email <b>' . $model['peserta'][0]->email . '</b>.<br> Data Nomor Peserta digunakan untuk melakukan konfirmasi pembayaran, login dan download tiket. <br><br>Nomor Peserta tambahan bisa dilihat setelah kamu login.</div>');

                if (YII_ENV == 'prod') {
                    \Yii::$app->mail->compose('email/email-pendaftaran-beli', ['model' => $model])
                        ->setFrom([Yii::$app->params['email.noreply'] => Yii::$app->params['emailName.noreply']])->setTo($model['peserta'][0]->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                } else if (YII_ENV == 'dev') {
                    /*return $this->render('/email/email-pendaftaran-beli', [
                        'model' => $model,
                        'idPeriode' => $periode->id,
                        'title' => 'Pendaftaran',
                    ]);*/
                    \Yii::$app->mail->compose('email/email-pendaftaran-beli', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo($model['peserta'][0]->email)
                        ->setSubject('Segera Lakukan Pembayaran')->send();
                }
            } catch (\Throwable $e) {
                $error = true;
                $transaction->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-pendaftaran-beli', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'title' => 'Pilih Kota Tryout',
            ]);
        else
            return $this->redirect(['login', 'kode' => $model['peserta'][0]->kode, 'email' => $model['peserta'][0]->email]);
    }

    public function actionDownloadKartuUjian($kode, $email)
    {
        $error = true;

        $model['peserta'] = Peserta::find()
            ->join('INNER JOIN', 'transaksi t', 't.id = peserta.id_transaksi')
            ->where([
                'kode' => $kode,
                'email' => $email,
                't.id_periode' => (Periode::getPeriodeAktif())->id,
                't.status_bayar' => 'Sudah Bayar',
            ])
            ->one();

        if (!$model['peserta']) Yii::$app->session->setFlash('error', 'Download kartu ujian gagal.');
        else $error = false;

        if ($error)
            return $this->redirect(['site/index']);
        else {
            // $title = 'E-Tiket ' . Yii::$app->params['app.name'] . ' #' . $model['peserta']->kode;
            // return $this->render('download-periode3-kartu-ujian', ['model' => $model, 'title' => $title]);
            $this->layout = 'download';
            $title = 'E-Tiket ' . Yii::$app->params['app.name'] . ' #' . $model['peserta']->kode;

            $dompdf = new \Dompdf\Dompdf();
            $dompdf->loadHtml($this->render('download-periode3-kartu-ujian', ['model' => $model, 'title' => $title]));
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $dompdf->stream($title . '.pdf');
            exit;
        }
    }

    public function actionDownloadSertifikat($kode, $email)
    {
        $error = true;

        $model['peserta'] = Peserta::find()
            ->join('INNER JOIN', 'transaksi t', 't.id = peserta.id_transaksi')
            ->where([
                'kode' => $kode,
                'email' => $email,
                't.id_periode' => (Periode::getPeriodeAktif())->id,
                't.status_bayar' => 'Sudah Bayar',
            ])
            ->one();

        if (!$model['peserta']) Yii::$app->session->setFlash('error', 'Download kartu ujian gagal.');
        else $error = false;

        if ($error)
            return $this->redirect(['site/index']);
        else {
            // $title = 'E-Tiket ' . Yii::$app->params['app.name'] . ' #' . $model['peserta']->kode;
            // return $this->render('download-kartu-ujian1', ['model' => $model, 'title' => $title]);
            $this->layout = 'download';
            $title = 'Sertifikat ' . Yii::$app->params['app.name'] . ' #' . $model['peserta']->kode;

            $dompdf = new \Dompdf\Dompdf();
            $dompdf->loadHtml($this->render('download-sertifikat', ['model' => $model, 'title' => $title]));
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $dompdf->stream($title . '.pdf');
            exit;
        }
    }

    public function actionDetailProsedur()
    {
        return $this->render('detail-prosedur', [
            'title' => 'Prosedur Pendaftaran',
        ]);
    }

    public function actionGetListKota($idProvinces = null)
    {
        $regenciesOriginal = [];
        $regenciesOriginal = array_map('ucwords', array_map('strtolower', ArrayHelper::map(\technosmart\modules\location\models\Regencies::find()->select(['id', 'name'])->where(['province_id' => $idProvinces])->orderBy('name')->asArray()->all(), 'id', 'name')));

        $regencies = [];
        foreach ($regenciesOriginal as $key => $value) {
            $regencies[] = [
                'value' => $key,
                'text' => $value,
            ];
        }
        return $this->json($regencies);
    }

    public function actionDetailKota($id)
    {
        $model['periode_kota'] = $this->findModelPeriodeKota($id);

        return $this->render('detail-kota', [
            'model' => $model,
            'title' => 'Detail Lokasi',
        ]);
    }

    public function actionGantiLokasi()
    {
        return $this->render('ganti-lokasi', [
            'title' => 'Ganti Lokasi',
        ]);
    }

    ///

    public function actionDownloadNilai()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['login']);

        $model['peserta'] = Peserta::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $model['transaksi'] = $model['peserta']->transaksi;
        $periode = Periode::getPeriodeAktif();

        $error = true;

        if ($model['transaksi']->status_bayar != 'Sudah Bayar' || $model['transaksi']->id_periode != (Periode::getPeriodeAktif())->id) Yii::$app->session->setFlash('error', 'Download nilai gagal.');
        else $error = false;

        if (($model['nilai'] = \app_tryout\models\Periode3Pengumuman::find()->where(['kode' => $model['peserta']->kode])->one()) == null) {
            $error = true;
            // pesan dibawah harus 1 line ya gar, ngga boleh dienter
            Yii::$app->session->setFlash('error', 'Data Nilai Tryout mu tidak ditemukan. Jika nama atau nomor transaksi kamu tidak ditemukan, ada 3 kemungkinan<br><br>1. Kamu tidak ikut tryout dan tidak mengumpulkan LJK<br>2. LJK yg kamu kumpulkan tidak ter-scan secara sempurna.<br>3. Kamu mengisi nomor transaksi yg berbeda di ljk dengan yg terdaftar di sistem.<br><br>hubungi admin LINE official kami di @masukptnid (pakai @ dan id), untuk penjelasan lebih detail.');
        }

        if ($error)
            return $this->redirect(['site/index']);
        else {
            // $title = 'E-Tiket ' . Yii::$app->params['app.name'] . ' #' . $model['transaksi']->kode;
            // return $this->render('download-kartu-ujian1', ['model' => $model, 'title' => $title]);
            $this->layout = 'download';
            $title = 'Nilai ' . Yii::$app->params['app.name'] . ' #' . $model['peserta']->kode;

            $dompdf = new \Dompdf\Dompdf();
            $dompdf->loadHtml($this->render('download-periode3-nilai-' . strtolower($model['nilai']->jurusan), ['model' => $model, 'title' => $title]));
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $dompdf->stream($title . '.pdf');
            exit;
        }
    }

    ///

    public function actionDatatablesPesertaMisi()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['login']);

        $query = new \yii\db\Query();
        $query
            ->select([
                'm.id',
                'm.judul',
                'IFNULL(pm.status_konfirmasi, "Belum Ikut") AS status_konfirmasi',
                'm.dari_tanggal',
                'm.sampai_tanggal',
                'm.status_aktif',
            ])
            ->from('misi m')
            ->join('LEFT JOIN', 'peserta_misi pm', 'm.id = pm.id_misi AND pm.id_peserta = ' . Yii::$app->user->identity->id)
            ->where(['m.status_aktif' => ['Sedang Aktif', 'Tidak Aktif']])
        ;
        return $this->datatables($query, $post = Yii::$app->request->post(), Misi::getDb());
    }

    public function actionPesertaMisi($id = null)
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['login']);

        if (!$id) {
            return $this->render('list-peserta-misi', [
                'title' => 'Misi Peserta',
            ]);
        }

        if (($model['misi'] = Misi::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
        if (($model['peserta_misi'] = PesertaMisi::find()->where(['id_misi' => $id])->one()) == null) {
            $model['peserta_misi'] = new PesertaMisi();
        }
        return $this->render('detail-peserta-misi', [
            'model' => $model,
            'title' => 'Detail Misi',
        ]);
    }

    public function actionPesertaMisiForm($id)
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect(['login']);

        $error = true;

        if (($model['misi'] = Misi::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
        if (($model['peserta_misi'] = PesertaMisi::find()->where(['id_misi' => $id])->one()) == null) {
            $model['peserta_misi'] = new PesertaMisi();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['peserta_misi']->load($post);

            $transaction['peserta_misi'] = PesertaMisi::getDb()->beginTransaction();

            try {
                if ($model['peserta_misi']->isNewRecord) {
                    $model['peserta_misi']->id_misi = $id;
                    $model['peserta_misi']->id_peserta = Yii::$app->user->identity->id;
                }
                $model['peserta_misi']->status_konfirmasi = 'Diajukan';
                if (!$model['peserta_misi']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                $transaction['peserta_misi']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['peserta_misi']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-peserta-misi', [
                'model' => $model,
                'idPeriode' => Periode::getPeriodeDashboard()->id,
                'title' => isset($id) ? 'Ikuti Misi' : 'Ikuti Misi',
            ]);
        else
            return $this->redirect(['peserta-misi']);
    }

    //

    /*public function aactionPromo($kode = null, $email = null)
    {
        $error = true;

        $model['transaksi'] = new Transaksi();
        $periode = Periode::getPeriodeAktif();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaksi'] = $this->findModelTransaksiByKodeEmail($post['Transaksi']['kode'], $post['Transaksi']['email'], null, false);

            $error = false;
        } else {
            if ($kode || $email) {
                $model['transaksi'] = $this->findModelTransaksiByKodeEmail($kode, $email, null, false);

                if (!$model['transaksi']->isNewRecord) {
                    Yii::$app->params['logout'] = true;
                    Yii::$app->params['kode'] = $kode;
                    Yii::$app->params['email'] = $email;
                }

                if ($model['transaksi']->hasErrors()) {
                    if (array_key_exists('kode', $model['transaksi']->errors) && $model['transaksi']->errors['kode'][0] == 'Transaksi telah dinonaktifkan oleh Admin.')
                        Yii::$app->session->setFlash('error', 'Peserta telah dinonaktifkan oleh Admin karena terlalu lama tidak melakukan pembayaran. Harap mendaftar kembali dan segera lakukan pembayaran untuk mengamankan kursi kamu di tryout MasukPTNid.');
                    else
                        Yii::$app->session->setFlash('error', 'Data transaksi tidak ditemukan.');
                }
            } else {
                $newSearch = true;
            }
        }

        if ($error)
            return $this->render('form-promo', [
                'model' => $model,
                'idPeriode' => $periode->id,
                'newSearch' => isset($newSearch) ? $newSearch : false,
                'title' => 'Promo',
            ]);
        else
            return $this->redirect(['promo', 'kode' => $model['transaksi']->kode, 'email' => $model['transaksi']->email]);
    }

    public function aactionDetailPromo($promo)
    {
        return $this->render('detail-promo', [
            'promo' => $promo,
            'title' => 'Promo ' . $promo,
        ]);
    }

    public function aactionDownloadNilaiPart2($kode, $email)
    {
        $error = true;

        $model['transaksi'] = $this->findModelTransaksiByKodeEmailDownloadNilaiPart2($kode, $email);
        
        if ($model['transaksi']->hasErrors())
            Yii::$app->session->setFlash('error', 'Download sertifikat gagal.');
        else
            $error = false;

        if (($model['nilai'] = \app_tryout\models\Part2Pengumuman::find()->where(['kode' => $kode])->one()) == null) {
            $error = true;
            // pesan dibawah harus 1 line ya gar, ngga boleh dienter
            Yii::$app->session->setFlash('error', 'Data Nilai Tryout mu tidak ditemukan. Jika nama atau nomor transaksi kamu tidak ditemukan, ada 3 kemungkinan<br><br>1. Kamu tidak ikut tryout dan tidak mengumpulkan LJK<br>2. LJK yg kamu kumpulkan tidak ter-scan secara sempurna.<br>3. Kamu mengisi nomor transaksi yg berbeda di ljk dengan yg terdaftar di sistem.<br><br>hubungi admin LINE official kami di @masukptnid (pakai @ dan id), untuk penjelasan lebih detail.');
        }

        if ($error)
            return $this->redirect(['site/index']);
        else {
            // $title = 'E-Tiket ' . Yii::$app->params['app.name'] . ' #' . $model['transaksi']->kode;
            // return $this->render('download-kartu-ujian1', ['model' => $model, 'title' => $title]);
            $this->layout = 'download';
            $title = 'Nilai ' . Yii::$app->params['app.name'] . ' #' . $model['transaksi']->kode;

            $dompdf = new \Dompdf\Dompdf();
            $dompdf->loadHtml($this->render('download-part2-nilai-' . strtolower($model['nilai']->jurusan), ['model' => $model, 'title' => $title]));
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $dompdf->stream($title . '.pdf');
            exit;
        }
    }

    public function aactionAktifkanDuta($kode, $email)
    {
        $error = true;

        $model['transaksi'] = $this->findModelTransaksiByKodeEmail($kode, $email, null, false);
        $periode = Periode::getPeriodeAktif();
        
        $post = Yii::$app->request->post();

        $transaction = Transaksi::getDb()->beginTransaction();

        try {
            if ($model['transaksi']->hasErrors()) {
                if (array_key_exists('kode', $model['transaksi']->errors) && $model['transaksi']->errors['kode'][0] == 'Transaksi telah dinonaktifkan oleh Admin.')
                    throw new \yii\web\HttpException(400, 'Peserta telah dinonaktifkan oleh Admin karena terlalu lama tidak melakukan pembayaran. Harap mendaftar kembali dan segera lakukan pembayaran untuk mengamankan kursi kamu di tryout MasukPTNid.');
                else
                    throw new \yii\web\HttpException(400, 'Data transaksi tidak ditemukan.');
            }

            $model['transaksi']->status_duta = 'Sudah Aktif';
            // ddx($model['transaksi'], $model['transaksi']->validate());
            if (!$model['transaksi']->save()) {
                throw new \yii\web\HttpException(400, 'Data tidak dapat disimpan karena terjadi kesalahan pada pengisian form. Mohon perbaiki kesalahan tersebut sebelum submit form.');
            }

            $error = false;
            $transaction->commit();
            Yii::$app->session->setFlash('success', '<div class=\"text-center margin-bottom-15\"><img src=\"' . Yii::$app->getRequest()->getBaseUrl() . '/img/check.png\" width=\"70px;\"></div><div class=\"text-center\">Selamat! Anda telah berhasil menjadi ambassador. Kumpulkan poin ambassador sebanyak-banyaknya untuk mendapat berbagai macam hadiah menarik. Klik link <a href=\"' . Yii::$app->urlManager->createUrl('ambassador/index') . '\">ini</a> untuk menambahkan poin Ambassador Anda.</div>');

        } catch (\Throwable $e) {
            $error = true;
            $transaction->rollBack();
            if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
        }

        return $this->redirect(['transaksi/login', 'kode' => $kode, 'email' => $email]);
    }

    public function aactionDetailPopup($kode = null, $email = null, $d)
    {
        $error = true;

        $model['transaksi'] = new Transaksi();
        $periode = Periode::getPeriodeAktif();
        
        if ($kode && $email) {
            $model['transaksi'] = $this->findModelTransaksiByKodeEmail($kode, $email, null, false);

            if (!$model['transaksi']->isNewRecord) {
                Yii::$app->params['logout'] = true;
                Yii::$app->params['kode'] = $kode;
                Yii::$app->params['email'] = $email;
            }
        }

        return $this->render('detail-popup', [
            'model' => $model,
            'd' => $d,
            'title' => (function($detail){
                if ($detail == 'menginap') {
                    return 'Diskon Voucher Menginap';
                } else if ($detail == 'ptngo') {
                    return 'Premium Member Aplikasi Belajar SBMPTN (PTN GO)';
                } else if ($detail == 'pantau-nilai') {
                    return 'Early Access Aplikasi Pantau Nilai UTBK';
                } else if ($detail == 'certificate') {
                    return 'E-Certificate';
                } else if ($detail == 'beasiswa-online') {
                    return 'Gratis Kompetisi Beasiswa Online';
                }
            })($d),
        ]);
    }

    public function aactionAnytest($kode, $email)
    {
        if (($model['transaksi'] = Transaksi::find()->where(['kode' => $kode, 'email' => $email, 'status_aktif' => 'Aktif', 'status_bayar' => 'Sudah Bayar', 'id_periode' => 2])->one()) !== null) {
            return $this->json([
                'status' => 200,
                'data' => [
                    'kode' => $model['transaksi']->kode,
                    'email' => $model['transaksi']->email,
                    'nama' => $model['transaksi']->nama,
                    'handphone' => $model['transaksi']->handphone,
                ],
            ]);
        } else if (($model['transaksi'] = Peserta::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null && $model['transaksi']->transaksi->status_aktif == 'Aktif' && $model['transaksi']->transaksi->status_bayar == 'Sudah Bayar' && $model['transaksi']->transaksi->id_periode == 2) {
            return $this->json([
                'status' => 200,
                'data' => [
                    'kode' => $model['transaksi']->kode,
                    'email' => $model['transaksi']->email,
                    'nama' => $model['transaksi']->nama,
                    'handphone' => $model['transaksi']->handphone,
                ],
            ]);
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'data tidak ditemukan',
            ]);
        }
    }

    protected function findModelTransaksiByKodeEmail($kode, $email, $post = null, $allPeriode = true)
    {
        $param = [];
        if (!$allPeriode) {
            $param = ['id_periode' => (Periode::getPeriodeAktif())->id];
        }
        if (($model['transaksi'] = Transaksi::find()->where(array_merge(['kode' => $kode, 'email' => $email, 'status_aktif' => 'Aktif'], $param))->one()) !== null) {
            if ($post) {
                $model['transaksi']->load($post);
            }
            return $model['transaksi'];
        } else if (($model['transaksi'] = Peserta::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null && $model['transaksi']->transaksi->status_aktif == 'Aktif' && ($allPeriode || $model['transaksi']->transaksi->id_periode == (Periode::getPeriodeAktif())->id)) {
            $model['transaksi'] = $model['transaksi']->transaksi;
            if ($post) {
                unset($post['Transaksi']['kode']);
                unset($post['Transaksi']['email']);
                $model['transaksi']->load($post);
            }
            return $model['transaksi'];
        } else {
            $model['transaksi'] = new Transaksi();
            $model['transaksi']->kode = $kode;
            $model['transaksi']->email = $email;
            $model['transaksi']->validate(['kode', 'email']);
            if (!$model['transaksi']->hasErrors()) {
                if (!$allPeriode && ((Transaksi::find()->where(['kode' => $kode, 'email' => $email])->andWhere(['<>', 'id_periode', (Periode::getPeriodeAktif())->id])->one()) !== null ||
                    (Peserta::find()->join('INNER JOIN', 'transaksi p', 'p.id = peserta.id_peserta')->where(['peserta.kode' => $kode, 'peserta.email' => $email])->andWhere(['<>', 'p.id_periode', (Periode::getPeriodeAktif())->id])->one()) !== null)
                ) {
                    $model['transaksi']->addErrors([
                        'kode' => 'Kamu merupakan transaksi part 1 sehingga tidak diperbolehkan mengakses fitur ini. Harap mendaftar ulang di part 2 yang sedang aktif sekarang.',
                    ]);
                } else if ((Transaksi::find()->where(['kode' => $kode, 'email' => $email, 'status_aktif' => 'Nonaktif'])->one()) !== null ||
                    (Peserta::find()->join('INNER JOIN', 'transaksi p', 'p.id = peserta.id_peserta')->where(['peserta.kode' => $kode, 'peserta.email' => $email, 'p.status_aktif' => 'Nonaktif'])->one()) !== null
                ) {
                    $model['transaksi']->addErrors([
                        'kode' => 'Transaksi telah dinonaktifkan oleh Admin.',
                    ]);
                } else {
                    $notFound = false;
                    if ((Transaksi::find()->where(['kode' => $kode])->one()) == null && (Peserta::find()->where(['kode' => $kode])->one()) == null) {
                        $model['transaksi']->addErrors([
                            'kode' => 'Kode transaksi tidak ditemukan di periode sekarang yang sedang aktif',
                        ]);
                        $notFound = true;
                    }
                    if ((Transaksi::find()->where(['email' => $email])->one()) == null && (Peserta::find()->where(['email' => $email])->one()) == null) {
                        $model['transaksi']->addErrors([
                            'email' => 'Email transaksi tidak ditemukan di periode sekarang yang sedang aktif',
                        ]);
                        $notFound = true;
                    }
                    if (!$notFound) {
                        $model['transaksi']->addErrors([
                            'kode' => 'Kombinasi kode dan email tidak ditemukan',
                            'email' => 'Kombinasi email dan kode tidak ditemukan',
                        ]);
                    }
                }
            }
            if ($post) {
                $model['transaksi']->load($post);
            }
            return $model['transaksi'];
        }
    }

    protected function findModelTransaksiByKodeEmailDownloadNilaiPart2($kode, $email)
    {
        if (($model['transaksi'] = Transaksi::find()->where(['kode' => $kode, 'email' => $email, 'status_bayar' => 'Sudah Bayar'])->one()) !== null && $model['transaksi']->id_periode == 2) {
            return $model['transaksi'];
        } else if (($model['transaksi'] = Peserta::find()->where(['kode' => $kode, 'email' => $email])->one()) !== null && $model['transaksi']->transaksi->status_bayar == 'Sudah Bayar' && $model['transaksi']->transaksi->id_periode == 2) {
            return $model['transaksi'];
        } else {
            $model['transaksi'] = new Transaksi();
            $model['transaksi']->addErrors([
                'kode' => 'Kombinasi kode dan email tidak ditemukan',
                'email' => 'Kombinasi email dan kode tidak ditemukan',
            ]);
            return $model['transaksi'];
        }
    }*/
} 