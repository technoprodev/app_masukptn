<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

?>
<?php if (!Yii::$app->request->isAjax) : ?>
<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-40 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

        <table class="table table-no-line">
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">1</span></td>
                <td class="padding-left-10">Ambassador adalah peserta yang telah mendaftarkan dirinya menjadi peserta MasukPTNid 2019 dan telah meng-klik tombol "Aktifkan Ambassador" pada halaman login peserta.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">2</span></td>
                <td class="padding-left-10">Ambassador bertugas untuk mempublikasikan kegiatan MasukPTNid - Tryout Nasional SBMPTN Test Center 2019 baik secara online maupun offline</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">3</span></td>
                <td class="padding-left-10">Ambassador mempunyai keuntungan untuk mendapatkan hadiah-hadiah untuk setiap peserta yang mendaftarkan dirinya via link Ambassador dirinya sendiri.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">4</span></td>
                <td class="padding-left-10">Untuk mendapatkan hadiah-hadiah, tiap Ambassador dapat mendaftarkan peserta lainnya melalui link <a href="<?= Yii::$app->urlManager->createUrl(['ambassador/pendaftaran', 'kode' => $model['duta']->kode, 'email' => $model['duta']->email]) ?>">Ini</a>, atau klik tombol <a href="<?= Yii::$app->urlManager->createUrl(['ambassador/pendaftaran', 'kode' => $model['duta']->kode, 'email' => $model['duta']->email]) ?>">Tambah Poin</a> di halaman Ambassador masing-masing.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">5</span></td>
                <td class="padding-left-10 fw-bold">Peserta yang didaftarkan Ambassador hanya bisa untuk pembelian tiket 1 orang dalam 1 transaksi.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">6</span></td>
                <td class="padding-left-10 fw-bold">Peserta yang didaftarkan Ambassador tidak bisa untuk pembelian tiket 2 orang / lebih dalam 1 transaksi.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">7</span></td>
                <td class="padding-left-10 fw-bold">Tiap 1 peserta yang didaftarkan dan sudah melakukan pembayaran, Ambassador akan memperolah 15 Poin Ambassador yang bisa ditukarkan dengan hadiah - hadiah.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">8</span></td>
                <td class="padding-left-10">Hadiah - hadiah yang bisa ditukarkan dapat dilihat di link <a href="<?= Yii::$app->urlManager->createUrl(['ambassador/hadiah-ambassador', 'kode' => $model['duta']->kode, 'email' => $model['duta']->email]) ?>">ini</a>, atau klik tombol <a href="<?= Yii::$app->urlManager->createUrl(['ambassador/hadiah-ambassador', 'kode' => $model['duta']->kode, 'email' => $model['duta']->email]) ?>"></a>Hadiah Ambassador di halaman Ambassador masing-masing.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">9</span></td>
                <td class="padding-left-10">Waktu penukaran hadiah - hadiah akan diumumkan oleh panitia maksimal H-7 acara pada Instagram dan Official Account LINE @masukptnid.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">10</span></td>
                <td class="padding-left-10">Penerimaan hadiah - hadiah akan diberikan oleh panitia MAKSIMAL H+14 acara.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">11</span></td>
                <td class="padding-left-10">Ambassador dilarang keras mempublikasikan berita HOAX dan hal-hal yang mengandung SARA.</td>
            </tr>
            <tr>
                <td class="padding-x-0"><span class="bg-azure border-azure inline-block text-center" style="width:20px;height:20px;">12</span></td>
                <td class="padding-left-10">Ambassador dapat dengan bebas berkreasi dan melakukan improvisasi dalam mendapatkan peserta untuk mendaftar via Ambassador.<br>Contoh: Share postingan di Instagram masukptnid, story ajakan mendaftar via Ambassador, share timeline line @masukptnid, cetak poster acara secara offline, memasang poster acara di mading sekolah, mengajak anggota di grup kelas yang dimiliku untuk mendaftar, dan lain sebagainya.</td>
            </tr>
        </table>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>

</div>
<?php endif; ?>
