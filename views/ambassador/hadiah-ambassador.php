<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

?>
<?php if (!Yii::$app->request->isAjax) : ?>
<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-40 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

    <div class="">
        <ul class="list-unstyled">
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">10 poin</span> Undian Smartphone Android </li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">20 poin</span> Pulsa senilai Rp 10.000</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">40 poin</span> Topi Alumni OSIS Indonesia</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">50 poin</span> Voucher Belanja</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">60 poin</span> Pulsa 50rb</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">70 poin</span> 220 Diamond Mobile Legends</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">80 poin</span> Flashdisk Alumni OSIS Indonesia 8GB</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">100 poin</span> Kaos dari campusstyle.id</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">110 poin</span> Uang Tunai Rp 100.000</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">150 poin</span> Power Bank 10.000 mah</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">200 poin</span> PIN Pembayaran SBMPTN 2019</li>
			<li class="margin-bottom-10"><span class="bg-azure border-azure inline-block text-center" style="width:60px;height:20px;">1500 poin</span> Smartphone Android senilai 2jt rupiah</li>
		</ul>
    </div>

    <p><b>1 tiket yang kamu beli = 15 poin</b></p>
    <p>waktu penukaran poin harap melihat syarat dan ketentuan poin 9</p>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>

</div>
<?php endif; ?>
