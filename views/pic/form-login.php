<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$errorMessage = '';
$errorVue = false;
if ($model['login']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['login'], ['class' => '']);
}

/*if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-6">
                <?= $form->field($model['login'], 'kode', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['login'], 'kode', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Kode']); ?>
                    <?= Html::activeTextInput($model['login'], 'kode', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['login'], 'kode', ['class' => 'form-info']); ?>
                <?= $form->field($model['login'], 'kode')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['login'], 'username', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['login'], 'username', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Username']); ?>
                    <?= Html::activeTextInput($model['login'], 'username', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['login'], 'username', ['class' => 'form-info']); ?>
                <?= $form->field($model['login'], 'username')->end(); ?>
            </div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Masuk/Login', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>

</div>