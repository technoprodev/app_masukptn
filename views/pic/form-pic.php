<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/pic/form-pic.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\SummernoteAsset::register($this);

//
$picPemasukans = [];
if (isset($model['pic_pemasukan']))
    foreach ($model['pic_pemasukan'] as $key => $picPemasukan)
        $picPemasukans[] = $picPemasukan->attributes;

$picPengeluarans = [];
if (isset($model['pic_pengeluaran']))
    foreach ($model['pic_pengeluaran'] as $key => $picPengeluaran)
        $picPengeluarans[] = $picPengeluaran->attributes;

$this->registerJs(
    'vm.$data.periodeKota.lembar_kode_peserta_dipakai = "' . $model['periode_kota']->lembar_kode_peserta_dipakai . '";' .
    'vm.$data.pic.jumlah_pendaftar = parseInt(' . json_encode($model['pic']->jumlah_pendaftar) . ');' .
    'vm.$data.pic.harga_satuan_pendaftar = parseInt(' . json_encode($model['pic']->harga_satuan_pendaftar) . ');' .
    'vm.$data.pic.picPemasukans = vm.$data.pic.picPemasukans.concat(' . json_encode($picPemasukans) . ');' .
    'vm.$data.pic.jumlah_volunteer_hari_acara = parseInt(' . json_encode($model['pic']->jumlah_volunteer_hari_acara) . ');' .
    'vm.$data.pic.fee_satuan_volunteer = parseInt(' . json_encode($model['pic']->fee_satuan_volunteer) . ');' .
    'vm.$data.pic.picPengeluarans = vm.$data.pic.picPengeluarans.concat(' . json_encode($picPengeluarans) . ');' .
    ($picPemasukans ? '' : 'vm.addPicPemasukan();') .
    ($picPengeluarans ? '' : 'vm.addPicPengeluaran();') .
    '',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['pic']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['pic'], ['class' => '']);
}

if (isset($model['pic_pemasukan'])) foreach ($model['pic_pemasukan'] as $key => $picPemasukan) {
    if ($picPemasukan->hasErrors()) {
        $errorMessage .= Html::errorSummary($picPemasukan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pic_pengeluaran'])) foreach ($model['pic_pengeluaran'] as $key => $picPengeluaran) {
    if ($picPengeluaran->hasErrors()) {
        $errorMessage .= Html::errorSummary($picPengeluaran, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  box-shadow: 0 0 10px rgba(51, 118, 184, 0.3);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: <?php ($f == 'laporan_keuangan') ? '900' : '600' ?>px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <br>
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">
            <?php if ($f == 'biodata') : ?>
                Edit biodata kamu pada formulir dibawah ini
            <?php elseif (false && $f == 'rangkuman_kegiatan') : ?>
                Isi rangkuman kegiatan pada formulir dibawah ini
            <?php elseif (false && $f == 'link_foto') : ?>
                Paste link foto pada formulir dibawah ini
            <?php elseif (false && $f == 'laporan_keuangan') : ?>
                Upload laporan keuangan pada formulir dibawah ini
            <?php elseif ($f == 'laporan_kegiatan') : ?>
                Laporan Kegiatan
            <?php elseif ($f == 'laporan_keuangan') : ?>
                Laporan Keuangan
            <?php elseif ($f == 'laporan_pengiriman') : ?>
                Pastikan kamu memakai jasa pengiriman yang 1 hari sampai, contoh: Travel, KALOG, atau Ekspedisi lainnya yang esok hari sampai
            <?php elseif ($f == 'acara_lain_diluar_tryout') : ?>
                Harap diisi jika ada acara lain yang PIC adakan diluar tryout ini, agar tim pusat bisa mengklarifikasi jika ada peserta yang bertanya
            <?php endif; ?>
        </span>
        
        <?php if ($f == 'laporan_pengiriman') : ?>
            <br>
            <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
            <br>

            <span class="bg-lightest rounded-md border-light-red padding-x-20 padding-y-10 inline-block">
                Kirim kembali paket ke alamat: <br> <b>Jl Gajah Lumantung no.35, Kec. Bandung Wetan, Kel. Tamansari, Kota Bandung, Jawa Barat 40117 <br> (Purnawarman atas, belakangnya Unpas, pagar hitam, pintu biru) <br> Penerima: Berna AOI <br> No. Telp: 0812-2250-063</b>
            </span>
            <br>
        <?php endif; ?>

        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="box box-break-sm box-gutter box-equal">
            <?php if ($f == 'biodata') : ?>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'nama', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nama lengkap PIC']); ?>
                        <?= Html::activeTextInput($model['pic'], 'nama', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'nama', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'nama')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'email', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Email']); ?>
                        <?= Html::activeTextInput($model['pic'], 'email', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'email', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'email')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'handphone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'handphone', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Handphone']); ?>
                        <?= Html::activeTextInput($model['pic'], 'handphone', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'handphone', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'handphone')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'whatsapp', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'whatsapp', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Whatsapp']); ?>
                        <?= Html::activeTextInput($model['pic'], 'whatsapp', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'whatsapp', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'whatsapp')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'nomor_rekening', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'nomor_rekening', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nomor Rekening']); ?>
                        <?= Html::activeTextInput($model['pic'], 'nomor_rekening', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'nomor_rekening', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'nomor_rekening')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'nama_bank', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'nama_bank', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nama Bank']); ?>
                        <?= Html::activeTextInput($model['pic'], 'nama_bank', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'nama_bank', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'nama_bank')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'atas_nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'atas_nama', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Atas Nama']); ?>
                        <?= Html::activeTextInput($model['pic'], 'atas_nama', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'atas_nama', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'atas_nama')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'alamat_pengiriman', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'alamat_pengiriman', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Alamat Pengiriman']); ?>
                        <?= Html::activeTextArea($model['pic'], 'alamat_pengiriman', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'alamat_pengiriman', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'alamat_pengiriman')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'nama_penerima', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'nama_penerima', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nama Penerima Kiriman Logistik']); ?>
                        <?= Html::activeTextInput($model['pic'], 'nama_penerima', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'nama_penerima', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'nama_penerima')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'telpon_penerima', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'telpon_penerima', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'No. Telp Penerima Kiriman Logistik']); ?>
                        <?= Html::activeTextInput($model['pic'], 'telpon_penerima', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'telpon_penerima', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'telpon_penerima')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'ukuran_kaos', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'ukuran_kaos', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Ukuran kaos']); ?>
                        <?= Html::activeTextInput($model['pic'], 'ukuran_kaos', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'ukuran_kaos', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'ukuran_kaos')->end(); ?>
                </div>
            <?php elseif (false && $f == 'rangkuman_kegiatan') : ?>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'rangkuman_kegiatan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'rangkuman_kegiatan', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Rangkuman Kegiatan']); ?>
                        <?= Html::activeTextArea($model['pic'], 'rangkuman_kegiatan', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'rangkuman_kegiatan', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'rangkuman_kegiatan')->end(); ?>
                </div>
            <?php elseif (false && $f == 'link_foto') : ?>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'link_foto', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'link_foto', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Link Foto']); ?>
                        <?= Html::activeTextInput($model['pic'], 'link_foto', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'link_foto', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'link_foto')->end(); ?>
                </div>
            <?php elseif (false && $f == 'laporan_keuangan') : ?>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'laporan_keuangan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'laporan_keuangan', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Laporan Keuangan']); ?>
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <a href="#" class="input-group-addon btn btn-default square fileinput-exists" data-dismiss="fileinput"><i class="fa fa-close"></i></a>
                            <div class="form-text rounded-xs">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                <span class="fileinput-filename"><a href="<?= $model['pic']->virtual_laporan_keuangan_download ?>"><?= $model['pic']->laporan_keuangan ?></a></span>
                            </div>
                            <span class="input-group-addon btn btn-default square btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                <?= Html::activeFileInput($model['pic'], 'virtual_laporan_keuangan_upload'); ?>
                            </span>
                        </div>
                        <?= Html::error($model['pic'], 'laporan_keuangan', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'laporan_keuangan')->end(); ?>
                </div>
            <?php elseif ($f == 'laporan_pengiriman') : ?>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'nama_jasa_pengiriman', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'nama_jasa_pengiriman', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nama Jasa Pengiriman']); ?>
                        <?= Html::activeTextInput($model['pic'], 'nama_jasa_pengiriman', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'nama_jasa_pengiriman', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'nama_jasa_pengiriman')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'nomor_resi', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'nomor_resi', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nomor resi / sejenis']); ?>
                        <?= Html::activeTextInput($model['pic'], 'nomor_resi', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'nomor_resi', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'nomor_resi')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_pengiriman', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_pengiriman', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Catatan Pengiriman']); ?>
                        <?= Html::activeTextArea($model['pic'], 'catatan_pengiriman', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'catatan_pengiriman', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'catatan_pengiriman')->end(); ?>
                </div>
            <?php elseif ($f == 'laporan_kegiatan') : ?>
                <div class="box-6">
                    <?= $form->field($model['periode_kota'], 'total_saintek_yang_datang', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['periode_kota'], 'total_saintek_yang_datang', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextInput($model['periode_kota'], 'total_saintek_yang_datang', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['periode_kota'], 'total_saintek_yang_datang', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'total_saintek_yang_datang')->end(); ?>
                    <?= $form->field($model['periode_kota'], 'saintek', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeRadioList($model['periode_kota'], 'saintek', $model['periode_kota']->getEnum('saintek'), ['class' => 'form-radio form-radio-inline margin-top-min-10', 'unselect' => null,
                            'item' => function($index, $label, $name, $checked, $value){
                                $checked = $checked ? 'checked' : '';
                                $disabled = in_array($value, ['', '']) ? 'disabled' : '';
                                return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                            }]); ?>
                        <?= Html::error($model['periode_kota'], 'saintek', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'saintek')->end(); ?>
                    <?= $form->field($model['periode_kota'], 'catatan_saintek', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeTextArea($model['periode_kota'], 'catatan_saintek', ['class' => 'form-textarea rounded-xs margin-top-min-10', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'placeholder' => 'catatan']); ?>
                        <?= Html::error($model['periode_kota'], 'catatan_saintek', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'catatan_saintek')->end(); ?>
                </div>
                <div class="box-6">
                    <?= $form->field($model['periode_kota'], 'total_soshum_yang_datang', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['periode_kota'], 'total_soshum_yang_datang', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextInput($model['periode_kota'], 'total_soshum_yang_datang', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['periode_kota'], 'total_soshum_yang_datang', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'total_soshum_yang_datang')->end(); ?>
                    <?= $form->field($model['periode_kota'], 'soshum', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeRadioList($model['periode_kota'], 'soshum', $model['periode_kota']->getEnum('soshum'), ['class' => 'form-radio form-radio-inline margin-top-min-10', 'unselect' => null,
                            'item' => function($index, $label, $name, $checked, $value){
                                $checked = $checked ? 'checked' : '';
                                $disabled = in_array($value, ['', '']) ? 'disabled' : '';
                                return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                            }]); ?>
                        <?= Html::error($model['periode_kota'], 'soshum', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'soshum')->end(); ?>
                    <?= $form->field($model['periode_kota'], 'catatan_soshum', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeTextArea($model['periode_kota'], 'catatan_soshum', ['class' => 'form-textarea rounded-xs margin-top-min-10', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'placeholder' => 'catatan']); ?>
                        <?= Html::error($model['periode_kota'], 'catatan_soshum', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'catatan_soshum')->end(); ?>
                </div>
                <div class="box-12 margin-top-15">
                </div>
                <div class="box-12">
                    <?= $form->field($model['periode_kota'], 'jumlah_volunteer_hari_h', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['periode_kota'], 'jumlah_volunteer_hari_h', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Jumlah volunteer yang datang hari H']); ?>
                        <?= Html::activeTextInput($model['periode_kota'], 'jumlah_volunteer_hari_h', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['periode_kota'], 'jumlah_volunteer_hari_h', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'jumlah_volunteer_hari_h')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['periode_kota'], 'lembar_kode_peserta_dipakai', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['periode_kota'], 'lembar_kode_peserta_dipakai', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Apakah lembar no.unik peserta  dipakai?']); ?>
                        <?= Html::activeRadioList($model['periode_kota'], 'lembar_kode_peserta_dipakai', $model['periode_kota']->getEnum('lembar_kode_peserta_dipakai'), ['class' => 'form-radio form-radio-inline', 'unselect' => null,
                            'item' => function($index, $label, $name, $checked, $value){
                                $checked = $checked ? 'checked' : '';
                                $disabled = in_array($value, ['', '']) ? 'disabled' : '';
                                return "<label><input type='radio' name='$name' value='$value' $checked $disabled v-model='periodeKota.lembar_kode_peserta_dipakai'><i></i>$label</label>";
                            }]); ?>
                        <?= Html::error($model['periode_kota'], 'lembar_kode_peserta_dipakai', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'lembar_kode_peserta_dipakai')->end(); ?>
                </div>
                <div class="box-12" v-if="periodeKota.lembar_kode_peserta_dipakai == 'Ya'">
                    <?= $form->field($model['periode_kota'], 'dimana_lembar_kode_peserta_kamu', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['periode_kota'], 'dimana_lembar_kode_peserta_kamu', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Ada dimana lembar no.unik peserta kamu?']); ?>
                        <?= Html::activeTextInput($model['periode_kota'], 'dimana_lembar_kode_peserta_kamu', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['periode_kota'], 'dimana_lembar_kode_peserta_kamu', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'dimana_lembar_kode_peserta_kamu')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['periode_kota'], 'kejadian_aneh_hari_h', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['periode_kota'], 'kejadian_aneh_hari_h', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Apakah ada kejadian aneh pada hari acara?']); ?>
                        <?= Html::activeTextArea($model['periode_kota'], 'kejadian_aneh_hari_h', ['class' => 'form-textarea summernote-simple rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['periode_kota'], 'kejadian_aneh_hari_h', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'kejadian_aneh_hari_h')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['periode_kota'], 'cerita_curhat_kritik_saran', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['periode_kota'], 'cerita_curhat_kritik_saran', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Silahkan isi jika ingin curhat / bercerita / kritik / saran']); ?>
                        <?= Html::activeTextArea($model['periode_kota'], 'cerita_curhat_kritik_saran', ['class' => 'form-textarea summernote-simple rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['periode_kota'], 'cerita_curhat_kritik_saran', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'cerita_curhat_kritik_saran')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['periode_kota'], 'laporan_kegiatan_sudah_final', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['periode_kota'], 'laporan_kegiatan_sudah_final', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Pilih \'Ya\' jika sudah tidak ada perubahan (sudah final) <br> Pilih \'Belum\' jika masih ada perubahan (belum final)']); ?>
                        <?= Html::activeRadioList($model['periode_kota'], 'laporan_kegiatan_sudah_final', $model['periode_kota']->getEnum('laporan_kegiatan_sudah_final'), ['class' => 'form-radio', 'unselect' => null,
                            'item' => function($index, $label, $name, $checked, $value){
                                $checked = $checked ? 'checked' : '';
                                $disabled = in_array($value, ['']) ? 'disabled' : '';
                                return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                            }]); ?>
                        <?= Html::error($model['periode_kota'], 'laporan_kegiatan_sudah_final', ['class' => 'form-info']); ?>
                    <?= $form->field($model['periode_kota'], 'laporan_kegiatan_sudah_final')->end(); ?>
                </div>
            <?php elseif ($f == 'laporan_keuangan') : ?>
                <div class="box-12"><div class="margin-top-30"></div><h1 class="text-center">Pemasukan</h1></div>

                <?php if (isset($model['pic_pemasukan'])) foreach ($model['pic_pemasukan'] as $key => $value): ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]tanggal", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]tanggal")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]keperluan", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]keperluan")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]nominal", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]nominal")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]banyaknya", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]banyaknya")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]berasal_dari", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]berasal_dari")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]catatan", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]catatan")->end(); ?>
                <?php endforeach; ?>

                <div class="box-2 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Tanggal</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Keperluan</label>
                </div>
                <div class="box-1 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Nominal</label>
                </div>
                <div class="box-1 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Jumlah</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Berasal Dari ?</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Catatan</label>
                </div>
                <div class="box-1 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Total</label>
                </div>
                <div class="box-1 hidden-sm-less">
                </div>
                <template v-if="typeof pic.picPemasukans == 'object'">
                    <template v-for="(value, key, index) in pic.picPemasukans">
                        <div class="box-12" v-show="!(value.id < 0)">
                            <div class="box box-break-sm box-gutter box-equal">
                                <input type="hidden" v-bind:id="'picpemasukan-' + key + '-id'" v-bind:name="'PicPemasukan[' + key + '][id]'" type="text" v-model="pic.picPemasukans[key].id">
                                <div class="box-2 padding-right-0 m-padding-x-15 visible-sm-less text-center">
                                    <div class="margin-top-15"></div>
                                    <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Pemasukan {{ (key+1) }}</span>
                                    <div class="margin-top-15"></div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Tanggal</label>
                                    <div v-bind:class="'form-wrapper field-picpemasukan-' + key + '-tanggal'">
                                        <input v-bind:id="'picpemasukan-' + key + '-tanggal'" v-bind:name="'PicPemasukan[' + key + '][tanggal]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPemasukans[key].tanggal" placeholder="cth: 30/12/2019">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Keperluan</label>
                                    <div v-bind:class="'form-wrapper field-picpemasukan-' + key + '-keperluan'">
                                        <input v-bind:id="'picpemasukan-' + key + '-keperluan'" v-bind:name="'PicPemasukan[' + key + '][keperluan]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPemasukans[key].keperluan">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-1 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Nominal</label>
                                    <div v-bind:class="'form-wrapper field-picpemasukan-' + key + '-nominal'">
                                        <input v-bind:id="'picpemasukan-' + key + '-nominal'" v-bind:name="'PicPemasukan[' + key + '][nominal]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPemasukans[key].nominal">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-1 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Jumlah</label>
                                    <div v-bind:class="'form-wrapper field-picpemasukan-' + key + '-banyaknya'">
                                        <input v-bind:id="'picpemasukan-' + key + '-banyaknya'" v-bind:name="'PicPemasukan[' + key + '][banyaknya]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPemasukans[key].banyaknya">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Berasal Dari ?</label>
                                    <div v-bind:class="'form-wrapper field-picpemasukan-' + key + '-berasal_dari'">
                                        <select v-bind:id="'picpemasukan-' + key + '-berasal_dari'" v-bind:name="'PicPemasukan[' + key + '][berasal_dari]'" class="form-dropdown rounded-xs" style="background: #d6ebed;" v-model="pic.picPemasukans[key].berasal_dari">
                                            <option value="Ega">Ega</option>
                                            <option value="Tegar">Tegar</option>
                                            <option value="Yudha">Yudha</option>
                                            <option value="Rizki">Rizki</option>
                                            <option value="Dara">Dara</option>
                                            <option value="Rusti">Rusti</option>
                                            <option value="Gerald">Gerald</option>
                                            <option value="Gesta">Gesta</option>
                                            <option value="Lainnya">Lainnya</option>
                                        </select>
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Catatan</label>
                                    <div v-bind:class="'form-wrapper field-picpemasukan-' + key + '-catatan'">
                                        <input v-bind:id="'picpemasukan-' + key + '-catatan'" v-bind:name="'PicPemasukan[' + key + '][catatan]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPemasukans[key].catatan">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-1 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Total</label>
                                    <div class="form-text rounded-xs" style="background: #d6ebed;">{{ (parseInt(pic.picPemasukans[key].nominal) * parseInt(pic.picPemasukans[key].banyaknya)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                                </div>
                                <div class="box-1">
                                    <div v-on:click="removePicPemasukan(key)" class="form-text rounded-xs padding-x-0 text-center hover-pointer m-margin-y-15" style="border-color: #f2bebe; background: #f4e7e7;"><i class="fa fa-trash"></i></div>
                                </div>
                            </div>
                        </div>
                    </template>
                </template>
                <div class="box-10 padding-right-0 m-padding-x-15">
                    <a v-on:click="addPicPemasukan" class="button button-block rounded-xs border-light-azure text-azure m-margin-y-30">Tambah Pemasukan</a>
                </div>
                <div class="box-2">
                    <div class="form-text rounded-xs padding-x-0 text-center" style="background-color: #d6ebed;">Rp {{ totalPemasukan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="fw-bold text-uppercase text-gray margin-top-5 text-center">Total Keseluruhan</div>
                </div>
                <div class="box-12">
                    <hr>
                </div>
                <div class="box-4">
                    <?= $form->field($model['pic'], 'jumlah_pendaftar', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'jumlah_pendaftar', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Jumlah Pendaftar Hari Acara']); ?>
                        <?= Html::activeTextInput($model['pic'], 'jumlah_pendaftar', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'v-model' => 'pic.jumlah_pendaftar']); ?>
                        <?= Html::error($model['pic'], 'jumlah_pendaftar', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'jumlah_pendaftar')->end(); ?>
                </div>
                <div class="box-4">
                    <?= $form->field($model['pic'], 'harga_satuan_pendaftar', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'harga_satuan_pendaftar', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextInput($model['pic'], 'harga_satuan_pendaftar', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'v-model' => 'pic.harga_satuan_pendaftar']); ?>
                        <?= Html::error($model['pic'], 'harga_satuan_pendaftar', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'harga_satuan_pendaftar')->end(); ?>
                </div>
                <div class="box-4">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total</div>
                    <div class="form-text rounded-xs" style="background: #d6ebed;">Rp {{ totalPemasukanPendaftar.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_pendaftar', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_pendaftar', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextArea($model['pic'], 'catatan_pendaftar', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'catatan_pendaftar', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'catatan_pendaftar')->end(); ?>
                </div>
                <div class="box-12">
                    <hr>
                </div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total Pemasukan</div>
                    <div class="form-text rounded-xs" style="background: #d6ebed;">Rp {{ totalPemasukanAkhir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_pemasukan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_pemasukan', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextArea($model['pic'], 'catatan_pemasukan', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'catatan_pemasukan', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'catatan_pemasukan')->end(); ?>
                </div>

                <div class="box-12"><div class="margin-top-30"></div><h1 class="text-center">Pengeluaran</h1></div>
                
                <?php if (isset($model['pic_pengeluaran'])) foreach ($model['pic_pengeluaran'] as $key => $value): ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]tanggal", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]tanggal")->end(); ?>

                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]keperluan", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]keperluan")->end(); ?>

                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]nominal", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]nominal")->end(); ?>

                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]banyaknya", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]banyaknya")->end(); ?>

                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]catatan", ['options' => ['class' => ''], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]catatan")->end(); ?>
                <?php endforeach; ?>

                <div class="box-2 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Tanggal</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Keperluan</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Nominal</label>
                </div>
                <div class="box-1 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Jumlah</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Catatan</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15 hidden-sm-less">
                    <label class="form-label fw-bold text-uppercase text-gray">Total</label>
                </div>
                <div class="box-1">
                </div>
                <template v-if="typeof pic.picPengeluarans == 'object'">
                    <template v-for="(value, key, index) in pic.picPengeluarans">
                        <div class="box-12" v-show="!(value.id < 0)">
                            <div class="box box-break-sm box-gutter box-equal">
                                <input type="hidden" v-bind:id="'picpengeluaran-' + key + '-id'" v-bind:name="'PicPengeluaran[' + key + '][id]'" type="text" v-model="pic.picPengeluarans[key].id">
                                <div class="box-2 padding-right-0 m-padding-x-15 visible-sm-less text-center">
                                    <div class="margin-top-15"></div>
                                    <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Pengeluaran {{ (key+1) }}</span>
                                    <div class="margin-top-15"></div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Tanggal</label>
                                    <div v-bind:class="'form-wrapper field-picpengeluaran-' + key + '-tanggal'">
                                        <input v-bind:id="'picpengeluaran-' + key + '-tanggal'" v-bind:name="'PicPengeluaran[' + key + '][tanggal]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPengeluarans[key].tanggal" placeholder="cth: 30/12/2019">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Keperluan</label>
                                    <div v-bind:class="'form-wrapper field-picpengeluaran-' + key + '-keperluan'">
                                        <input v-bind:id="'picpengeluaran-' + key + '-keperluan'" v-bind:name="'PicPengeluaran[' + key + '][keperluan]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPengeluarans[key].keperluan">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Nominal</label>
                                    <div v-bind:class="'form-wrapper field-picpengeluaran-' + key + '-nominal'">
                                        <input v-bind:id="'picpengeluaran-' + key + '-nominal'" v-bind:name="'PicPengeluaran[' + key + '][nominal]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPengeluarans[key].nominal">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-1 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Jumlah</label>
                                    <div v-bind:class="'form-wrapper field-picpengeluaran-' + key + '-banyaknya'">
                                        <input v-bind:id="'picpengeluaran-' + key + '-banyaknya'" v-bind:name="'PicPengeluaran[' + key + '][banyaknya]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPengeluarans[key].banyaknya">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Catatan</label>
                                    <div v-bind:class="'form-wrapper field-picpengeluaran-' + key + '-catatan'">
                                        <input v-bind:id="'picpengeluaran-' + key + '-catatan'" v-bind:name="'PicPengeluaran[' + key + '][catatan]'" class="form-text rounded-xs" style="background: #d6ebed;" type="text" v-model="pic.picPengeluarans[key].catatan">
                                        <div class="form-info"></div>
                                    </div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <label class="form-label fw-bold text-uppercase text-gray visible-sm-less">Total</label>
                                    <div class="form-text rounded-xs" style="background: #d6ebed;">{{ parseInt(pic.picPengeluarans[key].nominal) * parseInt(pic.picPengeluarans[key].banyaknya) }}</div>
                                </div>
                                <div class="box-1">
                                    <div v-on:click="removePicPengeluaran(key)" class="form-text rounded-xs padding-x-0 text-center hover-pointer m-margin-y-15" style="border-color: #f2bebe; background: #f4e7e7;"><i class="fa fa-trash"></i></div>
                                </div>
                            </div>
                        </div>
                    </template>
                </template>
                <div class="box-9 padding-right-0 m-padding-x-15">
                    <a v-on:click="addPicPengeluaran" class="button button-block rounded-xs border-light-azure text-azure m-margin-y-30">Tambah Pengeluaran</a>
                </div>
                <div class="box-3">
                    <div class="form-text rounded-xs padding-x-0 text-center" style="background-color: #d6ebed;">Rp {{ totalPengeluaran.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="fw-bold text-uppercase text-gray margin-top-5 text-center">Total Keseluruhan</div>
                </div>
                <div class="box-12">
                    <hr>
                </div>
                <div class="box-4">
                    <?= $form->field($model['pic'], 'jumlah_volunteer_hari_acara', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'jumlah_volunteer_hari_acara', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextInput($model['pic'], 'jumlah_volunteer_hari_acara', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'v-model' => 'pic.jumlah_volunteer_hari_acara']); ?>
                        <?= Html::error($model['pic'], 'jumlah_volunteer_hari_acara', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'jumlah_volunteer_hari_acara')->end(); ?>
                </div>
                <div class="box-4">
                    <?= $form->field($model['pic'], 'fee_satuan_volunteer', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'fee_satuan_volunteer', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextInput($model['pic'], 'fee_satuan_volunteer', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'v-model' => 'pic.fee_satuan_volunteer']); ?>
                        <?= Html::error($model['pic'], 'fee_satuan_volunteer', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'fee_satuan_volunteer')->end(); ?>
                </div>
                <div class="box-4">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total</div>
                    <div class="form-text rounded-xs" style="background: #d6ebed;">Rp {{ totalPengeluaranVolunteer.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_volunteer', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_volunteer', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextArea($model['pic'], 'catatan_volunteer', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'catatan_volunteer', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'catatan_volunteer')->end(); ?>
                </div>
                <div class="box-12">
                    <hr>
                </div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total Pengeluaran</div>
                    <div class="form-text rounded-xs" style="background: #d6ebed;">Rp {{ totalPengeluaranAkhir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_pengeluaran', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_pengeluaran', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextArea($model['pic'], 'catatan_pengeluaran', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'catatan_pengeluaran', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'catatan_pengeluaran')->end(); ?>
                </div>

                <div class="box-12"><div class="margin-top-30"></div><h1 class="text-center">Total</h1></div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total Pemasukan</div>
                    <div class="form-text rounded-xs" style="background: #d6ebed;">Rp {{ totalPemasukanAkhir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total Pengeluaran</div>
                    <div class="form-text rounded-xs" style="background: #d6ebed;">Rp {{ totalPengeluaranAkhir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Sisa</div>
                    <div class="form-text rounded-xs" v-bind:style="{ background: '#' + (sisa >= 0 ? 'd6ebed' : 'f4e7e7') }">Rp {{ sisa.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'uang_ini_ada_dimana', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'uang_ini_ada_dimana', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextInput($model['pic'], 'uang_ini_ada_dimana', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'uang_ini_ada_dimana', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'uang_ini_ada_dimana')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_laporan_keuangan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_laporan_keuangan', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <?= Html::activeTextArea($model['pic'], 'catatan_laporan_keuangan', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'catatan_laporan_keuangan', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'catatan_laporan_keuangan')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'laporan_keuangan_sudah_final', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'laporan_keuangan_sudah_final', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Pilih \'Ya\' jika sudah tidak ada perubahan (sudah final) <br> Pilih \'Belum\' jika masih ada perubahan (belum final)']); ?>
                        <?= Html::activeRadioList($model['pic'], 'laporan_keuangan_sudah_final', $model['pic']->getEnum('laporan_keuangan_sudah_final'), ['class' => 'form-radio', 'unselect' => null,
                            'item' => function($index, $label, $name, $checked, $value){
                                $checked = $checked ? 'checked' : '';
                                $disabled = in_array($value, ['']) ? 'disabled' : '';
                                return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                            }]); ?>
                        <?= Html::error($model['pic'], 'laporan_keuangan_sudah_final', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'laporan_keuangan_sudah_final')->end(); ?>
                </div>
            <?php elseif ($f == 'acara_lain_diluar_tryout') : ?>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'acara_lain_diluar_tryout', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'acara_lain_diluar_tryout', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Acara Lain Diluar Tryout']); ?>
                        <?= Html::activeTextArea($model['pic'], 'acara_lain_diluar_tryout', ['class' => 'form-textarea rounded-xs summernote-simple', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                        <?= Html::error($model['pic'], 'acara_lain_diluar_tryout', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'acara_lain_diluar_tryout')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'acara_lain_diluar_tryout_sudah_final', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'acara_lain_diluar_tryout_sudah_final', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Pilih \'Ya\' jika sudah tidak ada perubahan (sudah final) <br> Pilih \'Belum\' jika masih ada perubahan (belum final)']); ?>
                        <?= Html::activeRadioList($model['pic'], 'acara_lain_diluar_tryout_sudah_final', $model['pic']->getEnum('acara_lain_diluar_tryout_sudah_final'), ['class' => 'form-radio', 'unselect' => null,
                            'item' => function($index, $label, $name, $checked, $value){
                                $checked = $checked ? 'checked' : '';
                                $disabled = in_array($value, ['']) ? 'disabled' : '';
                                return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                            }]); ?>
                        <?= Html::error($model['pic'], 'acara_lain_diluar_tryout_sudah_final', ['class' => 'form-info']); ?>
                    <?= $form->field($model['pic'], 'acara_lain_diluar_tryout_sudah_final')->end(); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="margin-top-30"></div>
        
        <?= Html::submitButton('Simpan', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>

</div>