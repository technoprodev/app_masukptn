<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Akses Khusus PIC</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <div class="box box-gutter box-break-sm">
        <div class="box-12">
            <h4 class="">Note Umum (Untuk semua PIC)</h4>
            <div>
                <?= $model['pic_umum']->note_umum ? $model['pic_umum']->note_umum : 'Note umum belum tersedia' ?>
            </div>

            <div class="margin-top-30"></div>
            
            <h4 class="">Note Khusus (Untuk PIC <?= $model['pic']->id_periode_kota ? $model['pic']->periodeKota->nama : '' ?>)</h4>
            <div>
                <?= $model['pic']->note_khusus ? $model['pic']->note_khusus : 'Note khusus belum tersedia' ?>
            </div>

            <div class="margin-top-30"></div>

            <h4 class="">File Khusus</h4>
            <div>
                <?= $model['pic']->file_khusus ? 'Nama file: ' . $model['pic']->file_khusus : 'File khusus belum tersedia' ?>
            </div>
            <div class="margin-top-5"></div>
            <div>
                <?php if ($model['pic']->file_khusus) : ?>
                    <a href="<?= $model['pic']->virtual_file_khusus_download ?>" class="button button-block button-sm border-azure text-azure">Download File Khusus</a>
                <?php endif; ?>
            </div>

            <div class="margin-top-30"></div>
        </div>
        <div class="box-7">
            <h4 class="">Biodata PIC</h4>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Kota :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->id_periode_kota ? $model['pic']->periodeKota->nama : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Kode :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->kode ? $model['pic']->kode : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Username :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->username ? $model['pic']->username : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Status :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->status ? $model['pic']->status : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Nama Lengkap PIC :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->nama ? $model['pic']->nama : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Email :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->email ? $model['pic']->email : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Handphone :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->handphone ? $model['pic']->handphone : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Whatsapp :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->whatsapp ? $model['pic']->whatsapp : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Nomor Rekening :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->nomor_rekening ? $model['pic']->nomor_rekening : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Nama Bank :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->nama_bank ? $model['pic']->nama_bank : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Atas Nama Pemilik Rekening :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->atas_nama ? $model['pic']->atas_nama : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Alamat Pengiriman Logistik Tryout :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->alamat_pengiriman ? $model['pic']->alamat_pengiriman : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Nama Penerima Kiriman Logistik :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->nama_penerima ? $model['pic']->nama_penerima : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">No. Telp Penerima Kiriman Logistik :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->telpon_penerima ? $model['pic']->telpon_penerima : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Ukuran Kaos PIC :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->ukuran_kaos ? $model['pic']->ukuran_kaos : '(kosong)' ?></div>
            </div>

            <!-- <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Rangkuman Kegiatan :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->rangkuman_kegiatan ? $model['pic']->rangkuman_kegiatan : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Link Foto :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->link_foto ? $model['pic']->link_foto : '(kosong)' ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Laporan Keuangan :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $model['pic']->laporan_keuangan ? $model['pic']->laporan_keuangan : '(kosong)' ?></div>
            </div> -->

            <div class="margin-top-20"></div>

            <div>
                <?= Html::a('Edit Biodata', ['form-pic', 'f' => 'biodata'], ['class' => 'button button-block button-md border-azure text-azure']) ?>
            </div>

            <div class="margin-top-5"></div>

            <div>
                <?= Html::a('Download Sertifikat PIC', ['sertifikat-pic', 'f' => 'biodata'], ['class' => 'button button-block button-md border-azure text-azure']) ?>
            </div>

            <div class="margin-top-30"></div>
        </div>
        <div class="box-5">
            <!-- <h4 class="">File Umum</h4>
            <div>
                <?= $model['pic_umum']->file_umum ? 'Nama file: ' . $model['pic_umum']->file_umum : 'File umum belum tersedia' ?>
            </div>
            <div class="margin-top-5"></div>
            <div>
                <?php if ($model['pic_umum']->file_umum) : ?>
                    <a href="<?= $model['pic_umum']->virtual_file_umum_download ?>" class="button button-block button-sm border-azure text-azure">Download File Umum</a>
                <?php endif; ?>
            </div> -->

            <!-- <h4 class="">Rangkuman Kegiatan</h4>
            <div>
                <?= $model['pic']->rangkuman_kegiatan ? $model['pic']->rangkuman_kegiatan : 'Rangkuman kegiatan belum diisi' ?>
            </div>
            <div class="margin-top-5"></div>
            <div>
                <?= Html::a('Edit Rangkuman Kegiatan', ['form-pic', 'f' => 'rangkuman_kegiatan'], ['class' => 'button button-block button-sm border-azure text-azure']) ?>
            </div>

            <div class="margin-top-30"></div> -->

            <h4 class="">Acara Lain Diluar Tryout</h4>
            <div>
                Klik tombol dibawah jika kamu mengadakan acara lain diluar yang pusat adakan, agar tidak ada miss communication ke peserta dan penyedia venue
            </div>
            <div class="margin-top-5"></div>
            <div>
                <?= Html::a('Edit Acara Lain', ['form-pic', 'f' => 'acara_lain_diluar_tryout'], ['class' => 'button button-block button-sm border-azure text-azure']) ?>
            </div>

            <div class="margin-top-30"></div>

            <h4 class="">Laporan Pengiriman</h4>
            <div>
                Isi laporan ini setelah kamu melakukan pengiriman
            </div>
            <div class="margin-top-5"></div>
            <div class="fw-bold text-red">
                Spanduk / Banner dan Soal Sisa dilarang keras dikirim balik ke pusat
            </div>
            <div class="margin-top-5"></div>
            <div>
                <?= Html::a('Edit Laporan Pengiriman', ['form-pic', 'f' => 'laporan_pengiriman'], ['class' => 'button button-block button-sm border-azure text-azure']) ?>
            </div>

            <div class="margin-top-30"></div>

            <h4 class="">Laporan Kegiatan</h4>
            <div>
                <?= 'Isi setelah acara tryout selesai' ?>
            </div>
            <div class="margin-top-5"></div>
            <div>
                <?= Html::a('Edit Laporan Kegiatan', ['form-pic', 'f' => 'laporan_kegiatan'], ['class' => 'button button-block button-sm border-azure text-azure']) ?>
            </div>

            <div class="margin-top-30"></div>

            <!-- <h4 class="">Link Foto</h4>
            <div>
                <?= $model['pic']->link_foto ? $model['pic']->link_foto : 'Link foto belum diisi' ?>
            </div>
            <div class="margin-top-5"></div>
            <div>
                <?= Html::a('Edit Link Foto', ['form-pic', 'f' => 'link_foto'], ['class' => 'button button-block button-sm border-azure text-azure']) ?>
            </div>

            <div class="margin-top-30"></div> -->

            <h4 class="">Laporan Keuangan</h4>
            <!-- <div>
                <?= $model['pic']->laporan_keuangan ? 'Nama file: ' . $model['pic']->laporan_keuangan : 'Laporan Keuangan belum diupload' ?>
            </div>
                <div class="margin-top-5"></div>
                <div>
                    <?= Html::a('Upload Laporan Keuangan', ['form-pic', 'f' => 'laporan_keuangan'], ['class' => 'button button-block button-sm border-azure text-azure']) ?>
                </div>
            <?php if ($model['pic']->laporan_keuangan) : ?>
            <div class="margin-top-5"></div>
            <div>
                <a href="<?= $model['pic']->virtual_laporan_keuangan_download ?>" class="button button-block button-sm border-azure text-azure">Download Laporan Keuangan</a>
            </div>
            <?php endif; ?> -->
            <div>
                <?= 'Lembar budgeting pemasukan dan pengeluaran' ?>
            </div>
            <div class="margin-top-5"></div>
            <div>
                <?= Html::a('Edit Laporan Keuangan', ['form-pic', 'f' => 'laporan_keuangan'], ['class' => 'button button-block button-sm border-azure text-azure']) ?>
            </div>

            <!-- <h4 class="">Total Volunteer</h4>
            <div>
                <?= count($model['pic']->volunteers) ?>
            </div> -->

            <div class="margin-top-30"></div>
        </div>

        <div class="box-12">
            <h4 class="">Total Peserta Sudah Bayar</h4>

            <?php
                $tiketSaintekIpa = $tiketSoshumIps = $tiketSudahBayar = 0;
                
                foreach ($model['tiket'] as $key => $tiket) {
                    $tiketSaintekIpa += ($tiket['tiket_saintek_ipa'] ? (int)$tiket['tiket_saintek_ipa'] : 0);
                    $tiketSoshumIps += ($tiket['tiket_soshum_ips'] ? (int)$tiket['tiket_soshum_ips'] : 0);
                    $tiketSudahBayar += ($tiket['tiket_sudah_bayar'] ? (int)$tiket['tiket_sudah_bayar'] : 0);
                }
            ?>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Peserta Saintek :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $tiketSaintekIpa ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Peserta Soshum :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $tiketSoshumIps ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 text-left text-gray">Total Peserta :</div>
                <div class="box-9 m-padding-x-0 text-dark"><?= $tiketSudahBayar ?></div>
            </div>

            <div class="margin-top-30"></div>
        </div>

        <div class="box-12">
            <h4 class="">Tambah Volunteer</h4>

            <div class="clearfix">
                <?= Html::a('Tambah Volunteer', ['form-volunteer'], ['class' => 'button button-md border-azure bg-azure button-block']) ?>
            </div>
            
            <div class="margin-top-30"></div>
        </div>

        <div class="box-12">
            <h4 class="">Daftar Volunteer</h4>

            <?php if ($model['pic']->volunteers) : ?>        
                <div class="scroll-x">
                    <table class="table margin-0">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Volunteer</th>
                                <th>Email</th>
                                <th>Handphone</th>
                                <th>Ukuran Kaos</th>
                                <th>Uraian Pekerjaan</th>
                                <th>Sertifikat Volunteer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($model['pic']->volunteers as $key => $volunteer) : ?>
                                <tr>
                                    <td class="padding-x-0">
                                        <?= Html::a('<i class="fa fa-pencil bg-light-cyan padding-x-10 padding-y-5 rounded-md"></i>', ['form-volunteer', 'id' => $volunteer->id], ['class' => '']) ?>
                                    </td>
                                    <td>
                                        <div class="fs-14 text-dark"><?= $volunteer->nama ?></div>
                                    </td>
                                    <td>
                                        <div class="fs-14 text-dark"><?= $volunteer->email ?></div>
                                    </td>
                                    <td>
                                        <div class="fs-14 text-dark"><?= $volunteer->handphone ?></div>
                                    </td>
                                    <td>
                                        <div class="fs-14 text-dark"><?= $volunteer->ukuran_kaos ?></div>
                                    </td>
                                    <td>
                                        <div class="fs-14 text-dark"><?= $volunteer->uraian_pekerjaan ? substr($volunteer->uraian_pekerjaan, 0, 10) . '...' : 'belum diisi' ?></div>
                                    </td>
                                    <td>
                                        <?= Html::a('Download Sertifikat', ['sertifikat-volunteer', 'id' => $volunteer->id], ['class' => '']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php else : ?>
                <div class="text-red">
                    Belum ada daftar volunteer
                </div>
            <?php endif; ?>

            <div class="margin-top-30"></div>
        </div>

        <div class="box-12">
            <h4 class="">Asal Sekolah Peserta</h4>

            <div class="scroll-x">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sekolah</th>
                            <th>Penjualan Saintek</th>
                            <th>Penjualan Soshum</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $tiketSaintekIpa = $tiketSoshumIps = $tiketSudahBayar = 0;
                        ?>
                        <?php foreach ($model['tiket'] as $key => $tiket) : ?>
                            <?php
                                $tiketSaintekIpa += ($tiket['tiket_saintek_ipa'] ? (int)$tiket['tiket_saintek_ipa'] : 0);
                                $tiketSoshumIps += ($tiket['tiket_soshum_ips'] ? (int)$tiket['tiket_soshum_ips'] : 0);
                                $tiketSudahBayar += ($tiket['tiket_sudah_bayar'] ? (int)$tiket['tiket_sudah_bayar'] : 0);
                            ?>
                            <tr>
                                <td><?= $tiket['sekolah'] ? $tiket['sekolah'] : 'kosong' ?></td>
                                <td><?= $tiket['tiket_saintek_ipa'] ? $tiket['tiket_saintek_ipa'] : 0 ?></td>
                                <td><?= $tiket['tiket_soshum_ips'] ? $tiket['tiket_soshum_ips'] : 0 ?></td>
                                <td><?= $tiket['tiket_sudah_bayar'] ? $tiket['tiket_sudah_bayar'] : 0 ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="fw-bold">
                            <td>Total</td>
                            <td><?= $tiketSaintekIpa ?></td>
                            <td><?= $tiketSoshumIps ?></td>
                            <td><?= $tiketSudahBayar ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="margin-top-30"></div>
        </div>
    </div>

    </div>
</div>

</div>