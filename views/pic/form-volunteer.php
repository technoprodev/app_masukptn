<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/ambassador/form-pendaftaran.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

//
$errorMessage = '';
$errorVue = false;
if ($model['volunteer']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['volunteer'], ['class' => '']);
}
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  box-shadow: 0 0 10px rgba(51, 118, 184, 0.3);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Lakukan pendaftaran volunteer pada formulir dibawah ini</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-12">
                <?= $form->field($model['volunteer'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['volunteer'], 'nama', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nama lengkap volunteer']); ?>
                    <?= Html::activeTextInput($model['volunteer'], 'nama', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['volunteer'], 'nama', ['class' => 'form-info']); ?>
                <?= $form->field($model['volunteer'], 'nama')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['volunteer'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['volunteer'], 'email', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Email']); ?>
                    <?= Html::activeTextInput($model['volunteer'], 'email', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['volunteer'], 'email', ['class' => 'form-info']); ?>
                <?= $form->field($model['volunteer'], 'email')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['volunteer'], 'handphone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['volunteer'], 'handphone', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Handphone']); ?>
                    <?= Html::activeTextInput($model['volunteer'], 'handphone', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['volunteer'], 'handphone', ['class' => 'form-info']); ?>
                <?= $form->field($model['volunteer'], 'handphone')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['volunteer'], 'ukuran_kaos', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['volunteer'], 'ukuran_kaos', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Ukuran kaos']); ?>
                    <?= Html::activeTextInput($model['volunteer'], 'ukuran_kaos', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['volunteer'], 'ukuran_kaos', ['class' => 'form-info']); ?>
                <?= $form->field($model['volunteer'], 'ukuran_kaos')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['volunteer'], 'uraian_pekerjaan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['volunteer'], 'uraian_pekerjaan', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'kesibukan sekarang']); ?>
                    <?= Html::activeTextArea($model['volunteer'], 'uraian_pekerjaan', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['volunteer'], 'uraian_pekerjaan', ['class' => 'form-info']); ?>
                <?= $form->field($model['volunteer'], 'uraian_pekerjaan')->end(); ?>
            </div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>

</div>