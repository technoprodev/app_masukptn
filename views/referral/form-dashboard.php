<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$jumlahTiket = 0;

foreach ($model['referral_agent']->transaksis as $key => $transaksi) {
    if ($transaksi->status_bayar == 'Sudah Bayar' && $transaksi->status_aktif == 'Aktif') {
        foreach ($transaksi->pesertas as $key => $peserta) {
            $jumlahTiket++;
        }
    }
}
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Akses Khusus Referral</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <div class="box box-gutter box-break-sm">
        <div class="box-12">
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Nama :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->nama ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Kode :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->kode ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Handphone :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->handphone ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Program :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->program ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Sekolah :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->sekolah ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Kota :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->regencies ? $model['referral_agent']->regencies->name : '(kosong)' ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Provinsi :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->regencies ? $model['referral_agent']->regencies->province->name : '(kosong)' ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Email :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->email ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Nomor Identitas :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->nomor_identitas ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Alamat Lengkap :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->alamat_lengkap ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Catatan :</div>
                <div class="box-10 m-padding-x-0 text-dark"><span class="underline"><?= $model['referral_agent']->catatan ?></span></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Total tiket :</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $jumlahTiket ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-left text-gray">Angka Hasil :</div>
                <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format((int)$jumlahTiket * (int)$model['referral_agent']->fee) ?></div>
            </div>
        </div>
    </div>

    <div class="margin-bottom-15"></div>

    <?php if ($model['referral_agent']->transaksis) : ?>        
    <div class="scroll-x">
    <table class="table margin-0">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Kontak</th>
                <th>Jenis</th>
                <th>Lokasi</th>
                <th>Status</th>
                <th>E-Tiket</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model['referral_agent']->transaksis as $key => $transaksi) : ?>
                <?php if (/*$transaksi->status_bayar == 'Sudah Bayar' && */$transaksi->status_aktif == 'Aktif') foreach ($transaksi->pesertas as $key => $peserta) : ?>
                    <tr>
                        <td>
                            <div class="fs-14 text-dark"><?= $peserta->nama ?></div>
                            <div>
                                <span class="text-gray">kode: </span>
                                <span class="text-dark"><?= $peserta->kode ?></span>
                            </div>
                        </td>
                        <td>
                            <div>
                                <i class="fa fa-envelope margin-right-5 text-gray" style="width: 10px;"></i>
                                <span class="text-dark"><?= $peserta->email ?></span>
                            </div>
                            <div>
                                <i class="fa fa-phone margin-right-5 text-gray" style="width: 10px;"></i>
                                <span class="text-dark"><?= $peserta->handphone ?></span>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-dark"><?= $peserta->id_periode_jenis ? $peserta->periodeJenis->nama : '(belum dipilih)' ?></div>
                            <div>
                                <span class="text-gray">Rp </span>
                                <span class="text-dark"><?= number_format($peserta->harga, 2) ?></span>
                            </div>
                        </td>
                        <td>
                            <div>
                                <i class="fa fa-map-marker margin-right-5 text-gray"></i>
                                <span class="text-dark"><?= $peserta->id_periode_kota ? $peserta->periodeKota->nama : '(belum dipilih)' ?></span>
                            </div>
                            <div>
                                <?= $peserta->id_periode_kota ? Html::a('Lihat Detail', ['detail-kota', 'id' => $peserta->id_periode_kota], ['modal-md' => '', 'modal-title' => 'Detail Lokasi']) : '' ?>
                            </div>
                        </td>
                        <?php
                            $color = 'red';
                            if ($transaksi->status_bayar == 'Dalam Proses Konfirmasi') $color = 'orange';
                            elseif ($transaksi->status_bayar == 'Sudah Bayar') $color = 'azure';
                        ?>
                        <td class="text-<?= $color ?>">
                            <i class="fa fa-circle margin-right-2"></i>
                            <?= $transaksi->status_bayar ?>
                        </td>
                        <td>
                            <?php if ($transaksi->status_bayar == 'Sudah Bayar') : ?>
                                <?= Html::a('Download', ['/peserta/download-kartu-ujian', 'kode' => $peserta->kode, 'email' => $peserta->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                            <?php else : ?>
                                Belum tersedia
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    <?php endif; ?>

    <div class="margin-top-30"></div>

    <div class="clearfix">
        <?= Html::a('Tambah Peserta', ['pendaftaran'], ['class' => 'button button-md border-azure bg-azure button-block']) ?>
    </div>

    </div>
</div>

</div>