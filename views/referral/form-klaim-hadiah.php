<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/ambassador/form-klaim-hadiah.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$pesertaTambahans = [];
if (isset($model['peserta_tambahan']))
    foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan)
        $pesertaTambahans[] = $pesertaTambahan->attributes;

$periodeJenises = [];
if (isset($model['periode_jenis']))
    foreach ($model['periode_jenis'] as $key => $periodeJenis)
        $periodeJenises[] = $periodeJenis->attributes;

$hadiahsOriginal = [];
$hadiahsOriginal = ArrayHelper::map(\app_tryout\models\Hadiah::find()->select(['id', 'CONCAT(poin, " poin : ", nama) AS nama'])->orderBy('id')->asArray()->all(), 'id', 'nama');

$hadiahs = [];
foreach ($hadiahsOriginal as $key => $value) {
    $hadiahs[] = [
        'value' => $key,
        'text' => $value,
    ];
}

$hadiahsPoinHelper = [];
$hadiahsPoinHelper = ArrayHelper::map(\app_tryout\models\Hadiah::find()->select(['id', 'poin'])->orderBy('id')->asArray()->all(), 'id', 'poin');

$this->registerJs(
    'vm.$data.peserta.virtual_poin = ' . json_encode((int) $model['duta']->virtual_poin) . ';' .
    'vm.$data.peserta.virtual_poin_klaim = ' . json_encode((int) $model['duta']->virtual_poin_klaim) . ';' .
    'vm.$data.peserta.virtual_hadiah = ' . json_encode($model['duta']->virtual_hadiah) . ';' .
    'vm.$data.hadiahs = ' . json_encode($hadiahs) . ';' .
    'vm.$data.hadiahsHelper = ' . json_encode($hadiahsOriginal) . ';' .
    'vm.$data.hadiahsPoinHelper = ' . json_encode($hadiahsPoinHelper) . ';' .
    '',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['peserta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta'], ['class' => '']);
}

if (isset($model['peserta_tambahan'])) foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
    if ($pesertaTambahan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pesertaTambahan, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  box-shadow: 0 0 10px rgba(51, 118, 184, 0.3);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Selamat, kamu berhak menukar poin ambassador kamu dengan hadiah-hadiah dibawah ini ! <br><br> Semakin banyak poin yang kamu kumpulkan, semakin menarik hadiah yang bisa kamu dapat</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-12">
                <div class="form-wrapper">
                    <label class="form-label fw-bold text-uppercase text-gray">Pilih Hadiah</label>
                    <template v-if="typeof hadiahs == 'object'">
                        <template v-for="(value, key, index) in hadiahs">
                            <div class="margin-bottom-5">
                                <span class="bg-light-azure border-azure text-azure hover-pointer button button-xs margin-right-5" v-on:click="pilih(value.value)">pilih</span>
                                {{value.text}}
                            </div>
                        </template>
                    </template>
                </div>
            </div>

            <div class="box-12">
                <?= $form->field($model['peserta'], 'virtual_hadiah', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'virtual_hadiah', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Hadiah yang Dipilih']); ?>
                    
                    <?php Html::activeCheckboxList($model['peserta'], 'virtual_hadiah', ArrayHelper::map(\app_tryout\models\Hadiah::find()->select(['id', 'CONCAT(poin, " poin : ", nama) AS pilihan'])->indexBy('id')->asArray()->all(), 'id', 'pilihan'), ['class' => 'form-checkbox', 'unselect' => null,
                        'item' => function($index, $label, $name, $checked, $value){
                            $checked = $checked ? 'checked' : '';
                            $disabled = in_array($value, []) ? 'disabled' : '';
                            return "<label class=''><input type='checkbox' name='$name' value='$value' $checked $disabled v-model='peserta.virtual_hadiah'><i></i>$label</label>";
                        },
                    ]); ?>

                    <template v-if="typeof peserta.virtual_hadiah == 'object'">
                        <template v-for="(value, key, index) in peserta.virtual_hadiah">
                            <div class="margin-bottom-5">
                                {{hadiahsHelper[value]}}
                                <span class="bg-light-red border-red text-red hover-pointer button button-xs margin-left-5" v-on:click="hapus(key)">hapus pilihan</span>
                            </div>
                            <div style="display: none;">
                                <input type='checkbox' id="peserta-virtual_hadiah" name="Peserta[virtual_hadiah][]" v-bind:value="value" v-model="peserta.virtual_hadiah"></label>
                            </div>
                        </template>
                        <template v-if="peserta.virtual_hadiah.length == 0">
                            <div>Belum ada hadiah yang dipilih</div>
                            <div style="display: none;">
                                <input type='hidden' id="peserta-virtual_hadiah" name="Peserta[virtual_hadiah]" value="" v-model="peserta.virtual_hadiah"></label>
                            </div>
                        </template>
                    </template>

                    <?= Html::error($model['peserta'], 'virtual_hadiah', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'virtual_hadiah')->end(); ?>
            </div>

            <div class="box-12">
                <h5>Total Poin : {{peserta.virtual_poin}}</h5>
                <h5>
                    Total Hadiah yang Diklaim : {{peserta.virtual_poin_klaim}}
                    <span class="text-red" v-if="peserta.virtual_poin < peserta.virtual_poin_klaim">(TIDAK MENCUKUPI)</span>
                    <span class="text-azure" v-else="">(SISA {{peserta.virtual_poin - peserta.virtual_poin_klaim}} POIN)</span>
                </h5>
            </div>
        </div>

        <hr class="border-light-azure border-top margin-top-50">

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure', 'data-confirm' => '<b>Apakah kamu yakin klaim hadiah ambassador sekarang ?</b> <br><br> <span class="text-red">Kamu hanya dapat melakukan klaim hadiah 1 (satu) kali saja.</span>']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>

</div>