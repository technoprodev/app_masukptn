<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/alumni/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="margin-top-60"></div>
<div class="container">
	<div class="text-center">
        <div class="fs-40 m-fs-30 fw-bold text-orange">Alumni Sukses MasukPTNid</div>
    </div>
    <div class="margin-top-60 m-margin-top-15"></div>
    <!-- <div class="box box-space-md box-gutter box-break-md-6 box-equal">
        <div class="box-3 text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/alumni/Jessica.png" style="width: 100%; height: auto; max-width: 300px;">
        </div>
        <div class="box-3 text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/alumni/Leandra.png" style="width: 100%; height: auto; max-width: 300px;">
        </div>
        <div class="box-3 text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/alumni/Maulana.png" style="width: 100%; height: auto; max-width: 300px;">
        </div>
        <div class="box-3 text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/alumni/Widiyana.png" style="width: 100%; height: auto; max-width: 300px;">
        </div>
    </div>
    <div class="margin-top-30 hidden-md-less"></div>
    <div class="box box-space-md box-gutter box-break-md-6 box-equal">
        <div class="box-3 text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/alumni/Willy.png" style="width: 100%; height: auto; max-width: 300px;">
        </div>
    </div>

    <div class="margin-y-60 m-margin-y-30">
        <hr class="border-top border-light-azure">
    </div> -->

    <table class="datatables-alumni table table-no-line block">
    </table>
</div>