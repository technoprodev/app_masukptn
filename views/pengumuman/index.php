<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-gray bg-lightest rounded-sm" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">
    
    <h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

    <div class="fs-14 m-fs-13 text-gray text-center">
        <span class="">
            Selamat telah menyelesaikan ujian. Berikut adalah pengumuman dari ujian yang telah kamu ikuti
        </span>
        <hr class="border border-top margin-y-15">
    </div>

    <div class="margin-top-30"></div>

    <div>
        <center><iframe src="https://drive.google.com/file/d/1tzioPnkbnHrosAbEssJK_JGtBapEhMQ1/preview" width="100%" height="600"></iframe></center>
    </div>

    </div>
</div>

<div class="margin-top-50"></div>

</div>