<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/pic/form-pic.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

//
$picPemasukans = [];
if (isset($model['pic_pemasukan']))
    foreach ($model['pic_pemasukan'] as $key => $picPemasukan)
        $picPemasukans[] = $picPemasukan->attributes;

$picPengeluarans = [];
if (isset($model['pic_pengeluaran']))
    foreach ($model['pic_pengeluaran'] as $key => $picPengeluaran)
        $picPengeluarans[] = $picPengeluaran->attributes;

$this->registerJs(
    'vm.$data.periodeKota.lembar_kode_peserta_dipakai = "' . $model['periode_kota']->lembar_kode_peserta_dipakai . '";' .
    'vm.$data.pic.jumlah_pendaftar = parseInt(' . json_encode($model['pic']->jumlah_pendaftar) . ');' .
    'vm.$data.pic.harga_satuan_pendaftar = parseInt(' . json_encode($model['pic']->harga_satuan_pendaftar) . ');' .
    'vm.$data.pic.picPemasukans = vm.$data.pic.picPemasukans.concat(' . json_encode($picPemasukans) . ');' .
    'vm.$data.pic.jumlah_volunteer_hari_acara = parseInt(' . json_encode($model['pic']->jumlah_volunteer_hari_acara) . ');' .
    'vm.$data.pic.fee_satuan_volunteer = parseInt(' . json_encode($model['pic']->fee_satuan_volunteer) . ');' .
    'vm.$data.pic.picPengeluarans = vm.$data.pic.picPengeluarans.concat(' . json_encode($picPengeluarans) . ');' .
    ($picPemasukans ? '' : 'vm.addPicPemasukan();') .
    ($picPengeluarans ? '' : 'vm.addPicPengeluaran();') .
    '',
    3
);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Laporan Keuangan PIC <?= str_replace('<br>', '-', $model['periode_kota']->nama) ?>
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 1200px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div class="box box-break-sm box-gutter box-equal">
                <div class="box-12 text-center">
                    <?= $form->field($model['pic'], 'laporan_keuangan_sudah_final', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'laporan_keuangan_sudah_final', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Status']); ?>
                        <div class="fs-16 margin-bottom-5 <?= $model['pic']->laporan_keuangan_sudah_final == 'Sudah' ? 'text-azure' : 'text-red' ?>"><?= $model['pic']->laporan_keuangan_sudah_final ?> Final</div>
                    <?= $form->field($model['pic'], 'laporan_keuangan_sudah_final')->end(); ?>
                </div>

                <div class="box-12"><div class="margin-top-30"></div><h1 class="text-center">Pemasukan</h1></div>

                <?php if (isset($model['pic_pemasukan'])) foreach ($model['pic_pemasukan'] as $key => $value): ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]tanggal", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]tanggal")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]keperluan", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]keperluan")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]nominal", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]nominal")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]banyaknya", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]banyaknya")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]berasal_dari", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]berasal_dari")->end(); ?>

                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]catatan", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pemasukan'][$key], "[$key]catatan")->end(); ?>
                <?php endforeach; ?>

                <div class="box-2 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Tanggal</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Keperluan</label>
                </div>
                <div class="box-1 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Nominal</label>
                </div>
                <div class="box-1 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Jumlah</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Berasal Dari ?</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Catatan</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total</div>
                </div>
                <template v-if="typeof pic.picPemasukans == 'object'">
                    <template v-for="(value, key, index) in pic.picPemasukans">
                        <div class="box-12" v-show="!(value.id < 0)">
                            <div class="box box-break-sm box-gutter box-equal">
                                <input type="hidden" v-bind:id="'picpemasukan-' + key + '-id'" v-bind:name="'PicPemasukan[' + key + '][id]'" type="text" v-model="pic.picPemasukans[key].id">
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPemasukans[key].tanggal }}</div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPemasukans[key].keperluan }}</div>
                                </div>
                                <div class="box-1 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPemasukans[key].nominal }}</div>
                                </div>
                                <div class="box-1 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPemasukans[key].banyaknya }}</div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPemasukans[key].berasal_dari }}</div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPemasukans[key].catatan }}</div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ (parseInt(pic.picPemasukans[key].nominal) * parseInt(pic.picPemasukans[key].banyaknya)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                                </div>
                            </div>
                        </div>
                    </template>
                </template>
                <div class="box-10 padding-right-0 m-padding-x-15">
                    <div class="fw-bold text-uppercase text-gray text-right">Total :</div>
                </div>
                <div class="box-2">
                    <div class="fs-14 margin-bottom-5">Rp {{ totalPemasukan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                </div>
                <div class="box-12">
                    <hr>
                </div>
                <div class="box-4">
                    <?= $form->field($model['pic'], 'jumlah_pendaftar', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'jumlah_pendaftar', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => 'Jumlah Pendaftar Hari Acara']); ?>
                        <div class="fs-14 margin-bottom-5">{{ pic.jumlah_pendaftar }}</div>
                    <?= $form->field($model['pic'], 'jumlah_pendaftar')->end(); ?>
                </div>
                <div class="box-4">
                    <?= $form->field($model['pic'], 'harga_satuan_pendaftar', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'harga_satuan_pendaftar', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <div class="fs-14 margin-bottom-5">{{ pic.harga_satuan_pendaftar }}</div>
                    <?= $form->field($model['pic'], 'harga_satuan_pendaftar')->end(); ?>
                </div>
                <div class="box-4">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total</div>
                    <div class="fs-14 margin-bottom-5">{{ totalPemasukanPendaftar.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_pendaftar', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_pendaftar', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <div class="fs-14 margin-bottom-5"><?= $model['pic']->catatan_pendaftar ? $model['pic']->catatan_pendaftar : '-' ?></div>
                    <?= $form->field($model['pic'], 'catatan_pendaftar')->end(); ?>
                </div>
                <div class="box-12">
                    <hr>
                </div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total Pemasukan</div>
                    <div class="fs-14 margin-bottom-5">Rp {{ totalPemasukanAkhir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_pemasukan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_pemasukan', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <div class="fs-14 margin-bottom-5"><?= $model['pic']->catatan_pemasukan ? $model['pic']->catatan_pemasukan : '-' ?></div>
                    <?= $form->field($model['pic'], 'catatan_pemasukan')->end(); ?>
                </div>

                <div class="box-12"><div class="margin-top-30"></div><h1 class="text-center">Pengeluaran</h1></div>
                
                <?php if (isset($model['pic_pengeluaran'])) foreach ($model['pic_pengeluaran'] as $key => $value): ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]tanggal", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]tanggal")->end(); ?>

                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]keperluan", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]keperluan")->end(); ?>

                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]nominal", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]nominal")->end(); ?>

                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]banyaknya", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]banyaknya")->end(); ?>

                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]catatan", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= $form->field($model['pic_pengeluaran'][$key], "[$key]catatan")->end(); ?>
                <?php endforeach; ?>

                <div class="box-2 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Tanggal</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Keperluan</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Nominal</label>
                </div>
                <div class="box-1 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Jumlah</label>
                </div>
                <div class="box-2 padding-right-0 m-padding-x-15">
                    <label class="form-label fw-bold text-uppercase text-gray">Catatan</label>
                </div>
                <div class="box-3 padding-right-0 m-padding-x-15">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total</div>
                </div>
                <template v-if="typeof pic.picPengeluarans == 'object'">
                    <template v-for="(value, key, index) in pic.picPengeluarans">
                        <div class="box-12" v-show="!(value.id < 0)">
                            <div class="box box-break-sm box-gutter box-equal">
                                <input type="hidden" v-bind:id="'picpengeluaran-' + key + '-id'" v-bind:name="'PicPengeluaran[' + key + '][id]'" type="text" v-model="pic.picPengeluarans[key].id">
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPengeluarans[key].tanggal }}</div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPengeluarans[key].keperluan }}</div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPengeluarans[key].nominal }}</div>
                                </div>
                                <div class="box-1 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPengeluarans[key].banyaknya }}</div>
                                </div>
                                <div class="box-2 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ pic.picPengeluarans[key].catatan }}</div>
                                </div>
                                <div class="box-3 padding-right-0 m-padding-x-15">
                                    <div class="fs-14 margin-bottom-5">{{ parseInt(pic.picPengeluarans[key].nominal) * parseInt(pic.picPengeluarans[key].banyaknya) }}</div>
                                </div>
                            </div>
                        </div>
                    </template>
                </template>
                <div class="box-9 padding-right-0 m-padding-x-15">
                    <div class="fw-bold text-uppercase text-gray text-right">Total :</div>
                </div>
                <div class="box-3">
                    <div class="fs-14 margin-bottom-5">Rp {{ totalPengeluaran.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                </div>
                <div class="box-12">
                    <hr>
                </div>
                <div class="box-4">
                    <?= $form->field($model['pic'], 'jumlah_volunteer_hari_acara', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'jumlah_volunteer_hari_acara', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <div class="fs-14 margin-bottom-5">{{ pic.jumlah_volunteer_hari_acara }}</div>
                    <?= $form->field($model['pic'], 'jumlah_volunteer_hari_acara')->end(); ?>
                </div>
                <div class="box-4">
                    <?= $form->field($model['pic'], 'fee_satuan_volunteer', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'fee_satuan_volunteer', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <div class="fs-14 margin-bottom-5">{{ pic.fee_satuan_volunteer }}</div>
                    <?= $form->field($model['pic'], 'fee_satuan_volunteer')->end(); ?>
                </div>
                <div class="box-4">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total</div>
                    <div class="fs-14 margin-bottom-5">Rp {{ totalPengeluaranVolunteer.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_volunteer', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_volunteer', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <div class="fs-14 margin-bottom-5"><?= $model['pic']->catatan_volunteer ? $model['pic']->catatan_volunteer : '-' ?></div>
                    <?= $form->field($model['pic'], 'catatan_volunteer')->end(); ?>
                </div>
                <div class="box-12">
                    <hr>
                </div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total Pengeluaran</div>
                    <div class="fs-14 margin-bottom-5">Rp {{ totalPengeluaranAkhir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_pengeluaran', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_pengeluaran', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <div class="fs-14 margin-bottom-5"><?= $model['pic']->catatan_pengeluaran ? $model['pic']->catatan_pengeluaran : '-' ?></div>
                    <?= $form->field($model['pic'], 'catatan_pengeluaran')->end(); ?>
                </div>

                <div class="box-12"><div class="margin-top-30"></div><h1 class="text-center">Total</h1></div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total Pemasukan</div>
                    <div class="fs-14 margin-bottom-5">Rp {{ totalPemasukanAkhir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Total Pengeluaran</div>
                    <div class="fs-14 margin-bottom-5">Rp {{ totalPengeluaranAkhir.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <div class="fw-bold text-uppercase text-gray margin-bottom-5">Sisa</div>
                    <div class="fs-14 margin-bottom-5" v-bind:style="{ color: '#' + (sisa >= 0 ? '3376b8' : 'b83333') }">Rp {{ sisa.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') }}</div>
                    <div class="margin-top-15"></div>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'uang_ini_ada_dimana', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'uang_ini_ada_dimana', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <div class="fs-14 margin-bottom-5"><?= $model['pic']->uang_ini_ada_dimana ? $model['pic']->uang_ini_ada_dimana : '-' ?></div>
                    <?= $form->field($model['pic'], 'uang_ini_ada_dimana')->end(); ?>
                </div>
                <div class="box-12">
                    <?= $form->field($model['pic'], 'catatan_laporan_keuangan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeLabel($model['pic'], 'catatan_laporan_keuangan', ['class' => 'form-label fw-bold text-uppercase text-gray']); ?>
                        <div class="fs-14 margin-bottom-5"><?= $model['pic']->catatan_laporan_keuangan ? $model['pic']->catatan_laporan_keuangan : '-' ?></div>
                    <?= $form->field($model['pic'], 'catatan_laporan_keuangan')->end(); ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>
<?php endif; ?>