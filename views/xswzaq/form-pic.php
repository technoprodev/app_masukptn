<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\SummernoteAsset::register($this);

//
$errorMessage = '';
if ($model['pic']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['pic'], ['class' => '']);
}
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola data PIC dan volunteernya pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model['pic'], 'id_periode_kota', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'id_periode_kota', ['class' => 'form-label fw-bold', 'label' =>'Lokasi PIC']); ?>
            <?= Html::activeDropDownList($model['pic'], 'id_periode_kota', ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->select(['periode_kota.id', 'CONCAT(periode_kota.kode, " - ", periode_kota.nama) AS nama'])->where(['periode_kota.id_periode' => $idPeriode])->indexBy('id')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih lokasi', 'class' => 'form-dropdown rounded-xs']); ?>
            <?= Html::error($model['pic'], 'id_periode_kota', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'id_periode_kota')->end(); ?>

        <?= $form->field($model['pic'], 'kode', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'kode', ['class' => 'form-label fw-bold', 'label' => 'Kode (bebas, untuk login)']); ?>
            <?= Html::activeTextInput($model['pic'], 'kode', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'kode', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'kode')->end(); ?>

        <?= $form->field($model['pic'], 'username', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'username', ['class' => 'form-label fw-bold', 'label' => 'Username (bebas, untuk login)']); ?>
            <?= Html::activeTextInput($model['pic'], 'username', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'username', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'username')->end(); ?>

        <?= $form->field($model['pic'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'nama', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['pic'], 'nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'nama', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'nama')->end(); ?>

        <?= $form->field($model['pic'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'email', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['pic'], 'email', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'email', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'email')->end(); ?>

        <?= $form->field($model['pic'], 'handphone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'handphone', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['pic'], 'handphone', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'handphone', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'handphone')->end(); ?>

        <?= $form->field($model['pic'], 'whatsapp', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'whatsapp', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['pic'], 'whatsapp', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'whatsapp', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'whatsapp')->end(); ?>

        <?= $form->field($model['pic'], 'nomor_rekening', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'nomor_rekening', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['pic'], 'nomor_rekening', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'nomor_rekening', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'nomor_rekening')->end(); ?>

        <?= $form->field($model['pic'], 'nama_bank', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'nama_bank', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['pic'], 'nama_bank', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'nama_bank', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'nama_bank')->end(); ?>

        <?= $form->field($model['pic'], 'atas_nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'atas_nama', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['pic'], 'atas_nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'atas_nama', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'atas_nama')->end(); ?>

        <?= $form->field($model['pic'], 'alamat_pengiriman', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'alamat_pengiriman', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextArea($model['pic'], 'alamat_pengiriman', ['class' => 'form-textarea rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'alamat_pengiriman', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'alamat_pengiriman')->end(); ?>

        <?= $form->field($model['pic'], 'ukuran_kaos', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'ukuran_kaos', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['pic'], 'ukuran_kaos', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'ukuran_kaos', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'ukuran_kaos')->end(); ?>

        <!-- <?= $form->field($model['pic'], 'rangkuman_kegiatan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'rangkuman_kegiatan', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextArea($model['pic'], 'rangkuman_kegiatan', ['class' => 'form-textarea rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'rangkuman_kegiatan', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'rangkuman_kegiatan')->end(); ?> -->

        <!-- <?= $form->field($model['pic'], 'link_foto', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'link_foto', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['pic'], 'link_foto', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'link_foto', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'link_foto')->end(); ?> -->

        <!-- <?= $form->field($model['pic'], 'laporan_keuangan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'laporan_keuangan', ['class' => 'form-label fw-bold']); ?>
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default square fileinput-exists" data-dismiss="fileinput"><i class="fa fa-close"></i></a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['pic']->virtual_laporan_keuangan_download ?>"><?= $model['pic']->laporan_keuangan ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default square btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['pic'], 'virtual_laporan_keuangan_upload'); ?>
                </span>
            </div>
            <?= Html::error($model['pic'], 'laporan_keuangan', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'laporan_keuangan')->end(); ?> -->

        <?= $form->field($model['pic'], 'note_khusus', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'note_khusus', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextArea($model['pic'], 'note_khusus', ['class' => 'form-textarea rounded-xs summernote-default', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'note_khusus', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'note_khusus')->end(); ?>

        <?= $form->field($model['pic'], 'file_khusus', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'file_khusus', ['class' => 'form-label fw-bold']); ?>
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default square fileinput-exists" data-dismiss="fileinput"><i class="fa fa-close"></i></a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['pic']->virtual_file_khusus_download ?>"><?= $model['pic']->file_khusus ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default square btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['pic'], 'virtual_file_khusus_upload'); ?>
                </span>
            </div>
            <?= Html::error($model['pic'], 'file_khusus', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'file_khusus')->end(); ?>

        <?= $form->field($model['pic'], 'acara_lain_diluar_tryout', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic'], 'acara_lain_diluar_tryout', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextArea($model['pic'], 'acara_lain_diluar_tryout', ['class' => 'form-textarea rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['pic'], 'acara_lain_diluar_tryout', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic'], 'acara_lain_diluar_tryout')->end(); ?>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>