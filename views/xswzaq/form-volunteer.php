<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$errorMessage = '';
if ($model['volunteer']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['volunteer'], ['class' => '']);
}
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola data volunteer pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model['volunteer'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['volunteer'], 'nama', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['volunteer'], 'nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['volunteer'], 'nama', ['class' => 'form-info']); ?>
        <?= $form->field($model['volunteer'], 'nama')->end(); ?>
        
        <?= $form->field($model['volunteer'], 'id_pic', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['volunteer'], 'id_pic', ['class' => 'form-label fw-bold', 'label' =>'PIC']); ?>
            <?= Html::activeDropDownList($model['volunteer'], 'id_pic', ArrayHelper::map(\app_tryout\models\Pic::find()->select(['pic.id', 'CONCAT(pic.nama, " - ", pk.nama) AS nama'])->join('JOIN', 'periode_kota pk', 'pk.id = pic.id_periode_kota')->where(['pic.id_periode' => $idPeriode, 'pic.status' => 'Sedang Aktif'])->indexBy('id')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih pic', 'class' => 'form-dropdown rounded-xs']); ?>
            <?= Html::error($model['volunteer'], 'id_pic', ['class' => 'form-info']); ?>
        <?= $form->field($model['volunteer'], 'id_pic')->end(); ?>

        <?= $form->field($model['volunteer'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['volunteer'], 'email', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['volunteer'], 'email', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['volunteer'], 'email', ['class' => 'form-info']); ?>
        <?= $form->field($model['volunteer'], 'email')->end(); ?>

        <?= $form->field($model['volunteer'], 'handphone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['volunteer'], 'handphone', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['volunteer'], 'handphone', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['volunteer'], 'handphone', ['class' => 'form-info']); ?>
        <?= $form->field($model['volunteer'], 'handphone')->end(); ?>

        <?= $form->field($model['volunteer'], 'ukuran_kaos', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['volunteer'], 'ukuran_kaos', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['volunteer'], 'ukuran_kaos', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['volunteer'], 'ukuran_kaos', ['class' => 'form-info']); ?>
        <?= $form->field($model['volunteer'], 'ukuran_kaos')->end(); ?>

        <?= $form->field($model['volunteer'], 'uraian_pekerjaan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['volunteer'], 'uraian_pekerjaan', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextArea($model['volunteer'], 'uraian_pekerjaan', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['volunteer'], 'uraian_pekerjaan', ['class' => 'form-info']); ?>
        <?= $form->field($model['volunteer'], 'uraian_pekerjaan')->end(); ?>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>