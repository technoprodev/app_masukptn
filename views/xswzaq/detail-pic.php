<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Detail PIC
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#1" data-toggle="tab">Biodata PIC</a></li>
            <li role="presentation" class="activ3"><a href="#2" data-toggle="tab">Acara Lain Diluar Tryout</a></li>
            <li role="presentation" class="activ3"><a href="#3" data-toggle="tab">Laporan Pengiriman</a></li>
            <li role="presentation" class="activ3"><a href="#4" data-toggle="tab">Laporan Kegiatan</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane padding-y-15 active" id="1">
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Kab/Kota</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->id_periode_kota ? $model['pic']->periodeKota->nama : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Kode</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->kode ? $model['pic']->kode : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Username</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->username ? $model['pic']->username : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nama</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->nama ? $model['pic']->nama : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Email</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->email ? $model['pic']->email : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Handphone</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->handphone ? $model['pic']->handphone : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Whatsapp</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->whatsapp ? $model['pic']->whatsapp : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nomor Rekening</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->nomor_rekening ? $model['pic']->nomor_rekening : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nama Bank</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->nama_bank ? $model['pic']->nama_bank : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Atas Nama</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->atas_nama ? $model['pic']->atas_nama : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Alamat Pengiriman</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->alamat_pengiriman ? $model['pic']->alamat_pengiriman : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Ukuran Kaos</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->ukuran_kaos ? $model['pic']->ukuran_kaos : '(kosong)' ?></div>
                </div>

                <!-- <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Rangkuman Kegiatan</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->rangkuman_kegiatan ? $model['pic']->rangkuman_kegiatan : '(kosong)' ?></div>
                </div> -->
                
                <!-- <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Link Foto</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->link_foto ? $model['pic']->link_foto : '(kosong)' ?></div>
                </div> -->

                <!-- <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Laporan Keuangan</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->laporan_keuangan ? '<a href="' . $model['pic']->virtual_laporan_keuangan_download . '" target="_blank" rel="noopener noreferrer">' . $model['pic']->laporan_keuangan . '</a>' : '(kosong)' ?></div>
                </div> -->
                
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Note Khusus</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->note_khusus ? $model['pic']->note_khusus : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">File Khusus</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->file_khusus ? '<a href="' . $model['pic']->virtual_file_khusus_download . '" target="_blank" rel="noopener noreferrer">' . $model['pic']->file_khusus . '</a>' : '(kosong)' ?></div>
                </div>
            </div>
            <div class="tab-pane padding-y-15 activ3" id="2">
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Acara Lain Diluar Tryout</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->acara_lain_diluar_tryout ? $model['pic']->acara_lain_diluar_tryout : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Sudah Final ?</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->acara_lain_diluar_tryout_sudah_final ? $model['pic']->acara_lain_diluar_tryout_sudah_final : '(kosong)' ?></div>
                </div>
            </div>
            <div class="tab-pane padding-y-15 activ3" id="3">
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nama Jasa Pengiriman</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->nama_jasa_pengiriman ? $model['pic']->nama_jasa_pengiriman : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nomor Resi / Sejenis</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->nomor_resi ? $model['pic']->nomor_resi : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Catatan Pengiriman</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['pic']->catatan_pengiriman ? $model['pic']->catatan_pengiriman : '(kosong)' ?></div>
                </div>
            </div>
            <div class="tab-pane padding-y-15 activ3" id="4">
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total Saintek yang Datang</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->total_saintek_yang_datang ? $model['periode_kota']->total_saintek_yang_datang : '(kosong)' ?> - (<?= $model['periode_kota']->saintek ?>)</div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Catatan</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->catatan_saintek ? $model['periode_kota']->catatan_saintek : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total Soshum yang Datang</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->total_soshum_yang_datang ? $model['periode_kota']->total_soshum_yang_datang : '(kosong)' ?> - (<?= $model['periode_kota']->soshum ?>)</div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Catatan</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->catatan_soshum ? $model['periode_kota']->catatan_soshum : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Jumlah volunteer yang datang hari H</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->jumlah_volunteer_hari_h ? $model['periode_kota']->jumlah_volunteer_hari_h : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Apakah lembar no.unik peserta dipakai?</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->lembar_kode_peserta_dipakai ? $model['periode_kota']->lembar_kode_peserta_dipakai : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Ada dimana lembar no.unik peserta?</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->dimana_lembar_kode_peserta_kamu ? $model['periode_kota']->dimana_lembar_kode_peserta_kamu : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Apakah ada kejadian aneh pada hari acara?</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->kejadian_aneh_hari_h ? $model['periode_kota']->kejadian_aneh_hari_h : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Curhat / cerita / kritik / saran</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->cerita_curhat_kritik_saran ? $model['periode_kota']->cerita_curhat_kritik_saran : '(kosong)' ?></div>
                </div>

                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Sudah Final ?</div>
                    <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_kota']->laporan_kegiatan_sudah_final ? $model['periode_kota']->laporan_kegiatan_sudah_final : '(kosong)' ?></div>
                </div>
            </div>
        </div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>
<?php endif; ?>