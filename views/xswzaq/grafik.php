<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChartAsset::register($this);

$kota = str_replace('<br>', ' - ', ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->orderBy('nama')->asArray()->all(), 'id', 'nama'));
$kota['all'] = 'Semua Kota';
$periode = ['seminggu' => 'Seminggu Kebelakang', 'sebulan' => 'Sebulan Kebelakang', 'semua' => 'Semua Waktu'];
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Grafik <?= $kota[$model['grafik']->kota] ?> <?= $periode[$model['grafik']->periode] ?>
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="box box-space-md box-gutter box-break-sm box-equal">
        <div class="box-12 padding-20 border-light-azure">
            <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app'], 'method' => 'get', 'action' => Url::to(['xswzaq/grafik'])]); ?>
                <div class="box box-break-md box-gutter">
                    <div class="box-4">
                        <?= $form->field($model['grafik'], 'kota', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                            <?= Html::activeDropDownList($model['grafik'], 'kota', $kota, ['class' => 'form-dropdown', 'name' => 'kota']); ?>
                        <?= $form->field($model['grafik'], 'kota')->end(); ?>
                    </div>
                    <div class="box-4">
                        <?= $form->field($model['grafik'], 'periode', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                            <?= Html::activeDropDownList($model['grafik'], 'periode', $periode, ['class' => 'form-dropdown', 'name' => 'periode']); ?>
                        <?= $form->field($model['grafik'], 'periode')->end(); ?>
                    </div>
                    <div class="box-4">
                        <div class="form-wrapper margin-0">
                            <button class="button button-block border-azure bg-azure" type="submit">Filter</button>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="box-12 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Anak Konfirmasi</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="anakKonfirmasi" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('anakKonfirmasi').getContext('2d');
                        var config = {
                            type: 'line',
                            data: {
                              labels: <?= json_encode($model['label_anak_konfirmasi']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_anak_konfirmasi']) ?>,
                                borderColor: '#3376b8',
                                borderWidth: 1,
                                fill: false,
                                label: '',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Tanggal'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Total Tiket'
                                        },
                                    }]
                                },
                                legend: {
                                    display: false
                                },
                            }
                        };
                        window.anakKonfirmasi = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
        <div class="box-12 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Anak Dikonfirmasi</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="anakDikonfirmasi" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('anakDikonfirmasi').getContext('2d');
                        var config = {
                            type: 'line',
                            data: {
                              labels: <?= json_encode($model['label_anak_dikonfirmasi']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_anak_dikonfirmasi']) ?>,
                                borderColor: '#3376b8',
                                borderWidth: 1,
                                fill: false,
                                label: '',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Tanggal'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Total Tiket'
                                        },
                                    }]
                                },
                                legend: {
                                    display: false
                                },
                            }
                        };
                        window.anakDikonfirmasi = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
        <div class="box-12 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Peserta Daftar</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="pesertaDaftar" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('pesertaDaftar').getContext('2d');
                        var config = {
                            type: 'line',
                            data: {
                              labels: <?= json_encode($model['label_peserta_daftar']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_peserta_daftar']) ?>,
                                borderColor: '#3376b8',
                                borderWidth: 1,
                                fill: false,
                                label: '',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Tanggal'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Total Tiket'
                                        },
                                    }]
                                },
                                legend: {
                                    display: false
                                },
                            }
                        };
                        window.pesertaDaftar = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
        <div class="box-12 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Peserta Referral</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="pesertaReferral" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('pesertaReferral').getContext('2d');
                        var config = {
                            type: 'line',
                            data: {
                              labels: <?= json_encode($model['label_peserta_referral']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_peserta_referral']) ?>,
                                borderColor: '#3376b8',
                                borderWidth: 1,
                                fill: false,
                                label: '',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Tanggal'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Total Tiket'
                                        },
                                    }]
                                },
                                legend: {
                                    display: false
                                },
                            }
                        };
                        window.pesertaReferral = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
        <div class="box-12 padding-20 border-light-azure hover-scroll-y" style="height: 500px;">
            <div class="fs-15 text-dark m-text-center">
                <span class="border-azure text-azure inline-block padding-y-5 padding-x-15 rounded-lg text-middle">Peserta Tiket Dashboard</span>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <div>
                <canvas id="pesertaTiketDashboard" height="400"></canvas>
                <script>
                    window.addEventListener('load', function() {
                        var ctx = document.getElementById('pesertaTiketDashboard').getContext('2d');
                        var config = {
                            type: 'line',
                            data: {
                              labels: <?= json_encode($model['label_peserta_tiket_dashboard']) ?>,
                              datasets: [{
                                data: <?= json_encode($model['data_peserta_tiket_dashboard']) ?>,
                                borderColor: '#3376b8',
                                borderWidth: 1,
                                fill: false,
                                label: '',
                              }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                elements: {
                                    line: {
                                        tension: 0, // disables bezier curves
                                    }
                                },
                                animation: {
                                    duration: 0, // general animation time
                                },
                                hover: {
                                    animationDuration: 0, // duration of animations when hovering an item
                                },
                                responsiveAnimationDuration: 0, // animation duration after a resize
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false,
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Tanggal'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Total Tiket'
                                        },
                                    }]
                                },
                                legend: {
                                    display: false
                                },
                            }
                        };
                        window.pesertaTiketDashboard = new Chart(ctx, config);
                    });
                </script>
            </div>
        </div>
    </div>
</div>

<div class="margin-top-50"></div>