<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Detail Jenis, Periode & Harga
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nama</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_jenis']->nama ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Periode Penjualan</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_jenis']->periode_penjualan ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['periode_jenis']->status ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 1 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_1_tiket, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 2 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_2_tiket, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 3 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_3_tiket, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 4 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_4_tiket, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 5 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_5_tiket, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 6 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_6_tiket, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 7 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_7_tiket, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 8 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_8_tiket, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 9 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_9_tiket, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga Satuan / 10 Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['periode_jenis']->harga_10_tiket, 2) ?></div>
        </div>
        
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>
<?php endif; ?>