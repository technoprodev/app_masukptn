<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/xswzaq/list-pic-v2.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/xswzaq/list-volunteer-v1.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola pic & volunteer
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">

    <div class="box box-space-md box-gutter box-break-sm box-equal">
        <div class="box-12 padding-20 border-light-azure">
            <div class="fs-15 text-dark m-text-center clearfix">
                <span class="border-azure text-azure padding-y-5 padding-x-15 rounded-lg text-middle">Daftar PIC</span>
                <a href="<?= Yii::$app->urlManager->createUrl('xswzaq/form-pic') ?>" class="button border-azure bg-azure hover-bg-lightest hover-text-azure pull-right m-pull-none m-button-block m-margin-top-30">Tambah PIC</a>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <table class="datatables table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>Part</th>
                        <th>Kota</th>
                        <th>Jml Volunteer</th>
                        <th>Kode</th>
                        <th>Username</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Handphone</th>
                        <th>Whatsapp</th>
                        <th>Nomor Rekening</th>
                        <th>Nama Bank</th>
                        <th>Atas Nama</th>
                        <!-- <th>Alamat Pengiriman</th> -->
                        <th>Ukuran Kaos</th>
                        <!-- <th>Rangkuman Kegiatan</th> -->
                        <!-- <th>Link Foto</th> -->
                        <!-- <th>Laporan Keuangan</th> -->
                        <th>Alamat Pengiriman</th>
                        <th>Nama Penerima</th>
                        <th>Telpon Penerima</th>
                        <th>Acara Lain Diluar Tryout</th>
                        <th>Laporan Keuangan Sudah Final?</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search part..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kota..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search jml volunteer..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kode..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search username..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search email..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search handphone..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search whatsapp..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor rekening..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama bank..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search atas nama..."/></th>
                        <!-- <th><input type="text" class="form-text border-none padding-0" placeholder="search alamat pengiriman..."/></th> -->
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search ukuran kaos..."/></th>
                        <!-- <th><input type="text" class="form-text border-none padding-0" placeholder="search rangkuman kegiatan..."/></th> -->
                        <!-- <th><input type="text" class="form-text border-none padding-0" placeholder="search link foto..."/></th> -->
                        <!-- <th><input type="text" class="form-text border-none padding-0" placeholder="search laporan keuangan..."/></th> -->
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search alamat pengiriman..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama penerima..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search telpon penerima..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search acara lain diluar tryout..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search laporan keuangan sudah final?..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="box-12 padding-20 border-light-azure">
            <div class="fs-15 text-dark m-text-center clearfix">
                <span class="border-azure text-azure padding-y-5 padding-x-15 rounded-lg text-middle">Note Umum<!--  & File Umum --></span>
                <a href="<?= Yii::$app->urlManager->createUrl('xswzaq/form-pic-umum') ?>" class="button border-azure bg-azure hover-bg-lightest hover-text-azure pull-right m-pull-none m-button-block m-margin-top-30">Edit Note Umum<!--  & File Umum --></a>
            </div>

            <div class="margin-top-30"></div>

            <!-- <h4 class="text-azure">Note Umum</h4> -->
            <div>
                <?= $model['pic_umum']->note_umum ? $model['pic_umum']->note_umum : 'Note umum tidak tersedia' ?>
            </div>

            <!-- <h4 class="text-azure">File Umum</h4>
            <div>
                <?= $model['pic_umum']->file_umum ? 'Nama file: ' . $model['pic_umum']->file_umum : 'File umum tidak tersedia' ?>
            </div>
            <div class="margin-top-5"></div>
            <div>
                <?php if ($model['pic_umum']->file_umum) : ?>
                    <a href="<?= $model['pic_umum']->virtual_file_umum_download ?>" class="button button-block button-sm border-azure text-azure">Download File Umum</a>
                <?php endif; ?>
            </div> -->
        </div>
        <div class="box-12 padding-20 border-light-azure">
            <div class="fs-15 text-dark m-text-center clearfix">
                <span class="border-azure text-azure padding-y-5 padding-x-15 rounded-lg text-middle">Daftar Volunteer</span>
                <a href="<?= Yii::$app->urlManager->createUrl('xswzaq/form-volunteer') ?>" class="button border-azure bg-azure hover-bg-lightest hover-text-azure pull-right m-pull-none m-button-block m-margin-top-30">Tambah Volunteer</a>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <table class="datatables-volunteer table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>Part</th>
                        <th>Volunteer</th>
                        <th>Kota</th>
                        <th>PIC</th>
                        <th>Email</th>
                        <th>Handphone</th>
                        <th>Ukuran Kaos</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search part..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search volunteer..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kota..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search pic..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search email..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search handphone..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search ukuran kaos..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>