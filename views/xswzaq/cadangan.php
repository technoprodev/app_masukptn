

    <div class="margin-top-20"></div>

    <div class="box box-space-md box-gutter box-break-sm box-equal">
        <div class="box-12 padding-20 border-lighter">
            <div class="fs-15 text-dark">Peserta Periode 1</div>
            <div class="margin-top-30"></div>
            <div class="clearfix fs-12 text-gray">
                <div class="pull-left m-pull-none padding-y-10">
                    Jenis: <span class="bg-lighter text-gray padding-x-20 padding-y-10 rounded-md margin-right-20"><i class="fa fa-circle margin-right-5"></i> All <i class="fa fa-angle-down margin-left-5"></i></span> Status: <span class="bg-lighter text-gray padding-x-20 padding-y-10 rounded-md margin-right-20"><i class="fa fa-circle margin-right-5"></i> All <i class="fa fa-angle-down margin-left-5"></i></span>                    Lokasi: <span class="bg-lighter text-gray padding-x-20 padding-y-10 rounded-md margin-right-20"><i class="fa fa-circle margin-right-5"></i> All <i class="fa fa-angle-down margin-left-5"></i></span> &nbsp;
                </div>
                <div class="pull-right m-pull-none m-margin-top-15 m-text-right">
                    <div class="form-wrapper form-inline">
                        <input class="form-text rounded-md" name="text" placeholder="Search" type="text">
                        <button class="button padding-y-5 padding-x-20 rounded-lg border-azure bg-azure hover-bg-transparent hover-text-azure">Pendaftaran manual</button>
                    </div>
                </div>
            </div>
            <div class="margin-top-30"></div>
            <table class="table table-text-middle">
                <thead>
                    <tr class="text-left text-gray fw-bold">
                        <th>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </th>
                        <th>Name <i class="fa fa-angle-down margin-left-5"></i></th>
                        <th>Jenis <i class="fa fa-angle-down margin-left-5"></i></th>
                        <th>Status <i class="fa fa-angle-down margin-left-5"></i></th>
                        <th>Lokasi <i class="fa fa-angle-down margin-left-5"></i></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-azure border-transparent">FP</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Fandy Pradana
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Fandy.Pradana@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-azure"><i class="fa fa-circle margin-right-5"></i> Sudah bayar</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-magenta border-transparent">AS</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Ara Salvina
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Ara.Salvina@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-orange"><i class="fa fa-circle margin-right-5"></i> Dalam Proses Konfirmasi</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-rose border-transparent">EM</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Enzy Morisca
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Enzy.Morisca@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-azure"><i class="fa fa-circle margin-right-5"></i> Sudah bayar</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-azure border-transparent">RN</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Riasyah Novita
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Riasyah.Novita@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-azure"><i class="fa fa-circle margin-right-5"></i> Sudah bayar</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-yellow border-transparent">KL</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Kira Levina
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Kira.Levina@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-red"><i class="fa fa-circle margin-right-5"></i> Belum Bayar</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-cyan border-transparent">RA</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Rava Alina
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Rava.Alina@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-azure"><i class="fa fa-circle margin-right-5"></i> Sudah bayar</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-magenta border-transparent">BP</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Bintan Pradika
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Bintan.Pradika@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-azure"><i class="fa fa-circle margin-right-5"></i> Sudah bayar</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-spring border-transparent">FP</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Fandy Pradana
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Fandy.Pradana@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-red"><i class="fa fa-circle margin-right-5"></i> Belum Bayar</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-violet border-transparent">AM</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Aina Milana
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Aina.Milana@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-red"><i class="fa fa-circle margin-right-5"></i> Belum Bayar</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-checkbox form-checkbox-elegant">
                                <label>
                                    <input name="checkbox-elegant" value="1" type="checkbox"><i></i>
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="box box-gutter">
                                <div class="box-3">
                                    <span class="circle-icon fs-16 bg-blue border-transparent">IS</span>
                                </div>
                                <div class="box-9">
                                    <div class="fs-14 text-grayest">
                                        Irina Salvina
                                    </div>
                                    <div class="fs-12 text-gray text-lowercase">
                                        Irina.Salvina@gmail.com
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="fs-14 text-grayest">
                                Rp 30.000
                            </div>
                            <div class="fs-12 text-gray">
                                Saintek (IPA)
                            </div>
                        </td>
                        <td>
                            <div class="text-azure"><i class="fa fa-circle margin-right-5"></i> Sudah bayar</div>
                        </td>
                        <td>
                            Bandung UPI
                        </td>
                        <td class="fs-15 text-gray">
                            <i class="fa fa-pencil bg-light-cyan padding-x-15 padding-y-5 rounded-md margin-right-10"></i>
                            <i class="fa fa-trash-o margin-right-10"></i>
                            <i class="fa fa-ellipsis-h"></i>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>