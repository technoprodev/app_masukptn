<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/peserta/form-pendaftaran5.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$pesertas = [];
if (isset($model['peserta']))
    foreach ($model['peserta'] as $key => $peserta) {
        if (!$peserta->id_periode_jenis) {
            $peserta->id_periode_jenis = '';
        }
        if (!$peserta->id_periode_kota) {
            $peserta->id_periode_kota = '';
        }
        $temp = $peserta->attributes;
        $temp['harga'] = (int)$temp['harga'];
        $temp['id_provinces'] = $peserta->id_provinces;
        $pesertas[] = $temp;
    }

$periodeJenises = [];
if (isset($model['periode_jenis']))
    foreach ($model['periode_jenis'] as $key => $periodeJenis)
        $periodeJenises[] = $periodeJenis->attributes;

$provincesOriginal = [];
$provincesOriginal = array_map('ucwords', array_map('strtolower', ArrayHelper::map(\technosmart\modules\location\models\Provinces::find()->orderBy('name')->asArray()->all(), 'id', 'name')));

$provinces = [];
foreach ($provincesOriginal as $key => $value) {
    $provinces[] = [
        'value' => $key,
        'text' => $value,
    ];
}

$this->registerJs(
    'var a = [];' .
    ($pesertas ? '' : 'vm.addPeserta();') .
    'vm.$data.transaksi.pesertas = vm.$data.transaksi.pesertas.concat(' . json_encode($pesertas) . ');' .
    'vm.$data.periodeJenises = vm.$data.periodeJenises.concat(' . json_encode($periodeJenises) . ');' .
    // 'vm.$data.peserta.id_provinces = ' . json_encode($model['peserta']->regencies ? $model['peserta']->regencies->province_id : '') . ';' .
    // 'vm.onProvinceChange();' .
    // 'vm.$data.peserta.id_kota = ' . json_encode($model['peserta']->id_kota ? $model['peserta']->id_kota : '') . ';' .
    'vm.$data.provinces = ' . json_encode($provinces) . ';' .
    '',
    3
);
$disabledPeriodeKota = ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->where(['id_periode' => $idPeriode, 'status' => 'Tidak Aktif'])->orderBy('nama')->asArray()->all(), 'id', 'nama');
foreach ($disabledPeriodeKota as $key => $value) {
    $disabledPeriodeKota[$key] = ['disabled' => true];
}

//
$errorMessage = '';
$errorVue = false;
if ($model['transaksi']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['transaksi'], ['class' => '', 'header' => 'Harap perbaiki kesalahan berikut ini']);
}

if (isset($model['peserta'])) foreach ($model['peserta'] as $key => $peserta) {
    if ($peserta->hasErrors()) {
        $errorMessage .= Html::errorSummary($peserta, ['class' => '', 'header' => 'Harap perbaiki kesalahan berikut ini']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Update data peserta pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <?php if (isset($model['peserta'])) foreach ($model['peserta'] as $key => $value): ?>
            <?php
                $this->registerJs(
                    "
                    a[$key] = vm.transaksi.pesertas[$key].id_kota;
                    vm.onProvinceChange($key);
                    vm.transaksi.pesertas[$key].id_kota = a[$key];",
                    3
                );
            ?>

            <?= $form->field($model['peserta'][$key], "[$key]username", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]username", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]password", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]password", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]username_teman", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]username_teman", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]nama", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]nama", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]email", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]email", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]handphone", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]handphone", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]id_periode_jenis", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]id_periode_jenis", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]harga", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]harga", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]id_periode_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]id_periode_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]sekolah", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]sekolah", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]id_provinces", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]id_provinces", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]id_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]id_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]alamat", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]alamat", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]facebook", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]facebook", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]twitter", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]twitter", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]instagram", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]instagram", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]line", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]line", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]whatsapp", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]whatsapp", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof transaksi.pesertas == 'object'">
            <template v-for="(value, key, index) in transaksi.pesertas">
                <div v-show="!(value.id < 0)">
                    <!-- <hr class="border-light-azure border-top margin-top-50"> -->

                    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-azure text-center" v-if="key!=0">
                      <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
                      Tiket Ke-{{key+1}}
                      <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
                    </div>
                    <div class="margin-top-15"></div>

                    <input type="hidden" v-bind:id="'peserta-' + key + '-id'" v-bind:name="'Peserta[' + key + '][id]'" type="text" v-model="transaksi.pesertas[key].id">

                    <div class="box box-break-sm box-gutter box-equal">
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-nama'">
                                <label v-bind:for="'peserta-' + key + '-nama'" class="form-label fw-bold">Nama Lengkap</label>
                                <input v-bind:id="'peserta-' + key + '-nama'" v-bind:name="'Peserta[' + key + '][nama]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].nama">
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-email'">
                                <label v-bind:for="'peserta-' + key + '-email'" class="form-label fw-bold">Email</label>
                                <div class="margin-y-5">Pastikan email kamu aktif dan benar, karena SBMPTN nanti menggunakan email aktif kamu.</div>
                                <input v-bind:id="'peserta-' + key + '-email'" v-bind:name="'Peserta[' + key + '][email]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].email">
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-username'">
                                <label v-bind:for="'peserta-' + key + '-username'" class="form-label fw-bold">Username</label>
                                <div class="margin-y-5">Ini digunakan untuk login dan donwload tiket.</div>
                                <input v-bind:id="'peserta-' + key + '-username'" v-bind:name="'Peserta[' + key + '][username]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].username">
                                <div class="form-info"></div>
                                
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-password'">
                                <label v-bind:for="'peserta-' + key + '-password'" class="form-label fw-bold">Password</label>
                                <div class="margin-y-5">Ini digunakan untuk login dan donwload tiket.</div>
                                <input v-bind:id="'peserta-' + key + '-password'" v-bind:name="'Peserta[' + key + '][password]'" class="form-text rounded-xs" type="password" v-model="transaksi.pesertas[key].password">
                                <div class="form-info"></div>
                                
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-id_periode_jenis'">
                                <label v-bind:for="'peserta-' + key + '-id_periode_jenis'" class="form-label fw-bold">Jenis Tryout</label>
                                <select v-bind:id="'peserta-' + key + '-id_periode_jenis'" v-bind:name="'Peserta[' + key + '][id_periode_jenis]'" class="form-dropdown rounded-xs" v-model="transaksi.pesertas[key].id_periode_jenis">
                                    <option value="">Pilih jenis tryout</option>
                                    <?php foreach (ArrayHelper::map(\app_tryout\models\PeriodeJenis::find()->select(['id', 'CONCAT(nama, " - ", periode_penjualan, " - ", harga_1_tiket) AS nama'])->where(['id_periode' => $idPeriode/*, 'status' => 'Sedang Aktif'*/])->indexBy('id')->asArray()->all(), 'id', 'nama') as $id => $nama) : ?>
                                        <option value="<?= $id ?>"><?= $nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-harga'">
                                <label v-bind:for="'peserta-' + key + '-harga'" class="form-label fw-bold text-uppercase text-gray">Harga</label>
                                <input v-bind:id="'peserta-' + key + '-harga'" v-bind:name="'Peserta[' + key + '][harga]'" class="form-text rounded-xs" type="text" v-model.number="transaksi.pesertas[key].harga">
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-id_periode_kota'">
                                <label v-bind:for="'peserta-' + key + '-id_periode_kota'" class="form-label fw-bold">Lokasi Tryout</label>
                                <select v-bind:id="'peserta-' + key + '-id_periode_kota'" v-bind:name="'Peserta[' + key + '][id_periode_kota]'" class="form-dropdown rounded-xs" v-model="transaksi.pesertas[key].id_periode_kota">
                                    <option value="">Pilih lokasi</option>
                                    <?php foreach (str_replace('<br>', ' - ', ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->where(['id_periode' => $idPeriode/*, 'status' => 'Sedang Aktif'*/])->orderBy('nama')->asArray()->all(), 'id', 'nama')) as $id => $nama) : ?>
                                        <option value="<?= $id ?>"><?= $nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-handphone'">
                                <label v-bind:for="'peserta-' + key + '-handphone'" class="form-label fw-bold">Nomor Handphone</label>
                                <!-- <div class="form-icon">
                                    <span class="icon-prepend padding-right-0" style="line-height: 18px">0 </span>
                                </div> -->
                                <input v-bind:id="'peserta-' + key + '-handphone'" v-bind:name="'Peserta[' + key + '][handphone]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].handphone" placeholder="08xxxxx">
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-jenis_kelamin'">
                                <label v-bind:for="'peserta-' + key + '-jenis_kelamin'" class="form-label fw-bold">Jenis Kelamin</label>
                                <select v-bind:id="'peserta-' + key + '-jenis_kelamin'" v-bind:name="'Peserta[' + key + '][jenis_kelamin]'" class="form-dropdown rounded-xs" v-model="transaksi.pesertas[key].jenis_kelamin">
                                    <option value="">Pilih jenis kelamin</option>
                                    <?php foreach (str_replace('<br>', ' - ', (new \app_tryout\models\Peserta)->getEnum('jenis_kelamin')) as $id => $nama) : ?>
                                        <option value="<?= $id ?>"><?= $nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-sekolah'">
                                <label v-bind:for="'peserta-' + key + '-sekolah'" class="form-label fw-bold">Nama Sekolah</label>
                                <textarea v-bind:id="'peserta-' + key + '-sekolah'" v-bind:name="'Peserta[' + key + '][sekolah]'" class="form-textarea rounded-xs" v-model="transaksi.pesertas[key].sekolah"></textarea>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-id_kota'">
                                <label v-bind:for="'peserta-' + key + '-id_kota'" class="form-label fw-bold">Domisili Kota</label>
                                <?php if (false) : ?>
                                    <select v-bind:id="'peserta-' + key + '-id_kota'" v-bind:name="'Peserta[' + key + '][id_kota]'" class="form-dropdown rounded-xs" v-model="transaksi.pesertas[key].id_kota">
                                        <option value="">Pilih kota</option>
                                        <?php foreach (ArrayHelper::map(
                                                \Yii::$app->db->createCommand(
                                                    "
                                                        SELECT p.name AS province, r.id, r.name FROM regencies r JOIN provinces p ON p.id = r.province_id
                                                        order by r.name
                                                    ", []
                                                )->queryAll(), 'id', 'name', 'province'
                                            ) as $province => $regencies) :
                                        ?>
                                            <optgroup label="<?= $province ?>" value="<?= $province ?>">
                                            <?php foreach ($regencies as $id => $name) :
                                            ?>
                                                <option value="<?= $id ?>"><?= $name ?></option>
                                            <?php endforeach; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    </select>
                                <?php endif; ?>

                                <select v-bind:id="'peserta-' + key + '-id_provinces'" v-bind:name="'Peserta[' + key + '][id_provinces]'" class="form-dropdown rounded-xs" aria-invalid="false" v-model="transaksi.pesertas[key].id_provinces" v-on:change="onProvinceChange(key);">
                                    <option value="">Pilih provinsi</option>
                                    <option v-for="option in provinces" v-bind:value="option.value">
                                        {{ option.text }}
                                    </option>
                                </select>

                                <div class="margin-top-15"></div>

                                <select  v-bind:id="'peserta-' + key + '-id_kota'" v-bind:name="'Peserta[' + key + '][id_kota]'" class="form-dropdown rounded-xs" aria-invalid="false" v-model="transaksi.pesertas[key].id_kota">
                                    <option value="">Pilih kota</option>
                                    <option v-for="option in regencies" v-bind:value="option.value">
                                        {{ option.text }}
                                    </option>
                                </select>

                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-alamat'">
                                <label v-bind:for="'peserta-' + key + '-alamat'" class="form-label fw-bold">Alamat</label>
                                <textarea v-bind:id="'peserta-' + key + '-alamat'" v-bind:name="'Peserta[' + key + '][alamat]'" class="form-textarea rounded-xs" v-model="transaksi.pesertas[key].alamat"></textarea>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <label class="form-label fw-bold">Social Media / Instant Messenger <span class="margin-y-5 fw-normal fs-italic">- Isi Minimal 2 dari 5.</span></label>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-facebook'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-facebook text-azure"></i>
                                    <input v-bind:id="'peserta-' + key + '-facebook'" v-bind:name="'Peserta[' + key + '][facebook]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].facebook" placeholder="facebook">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-twitter'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-twitter text-azure"></i>
                                    <input v-bind:id="'peserta-' + key + '-twitter'" v-bind:name="'Peserta[' + key + '][twitter]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].twitter" placeholder="twitter">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-instagram'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-instagram text-azure"></i>
                                    <input v-bind:id="'peserta-' + key + '-instagram'" v-bind:name="'Peserta[' + key + '][instagram]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].instagram" placeholder="instagram">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-line'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-commenting-o text-azure"></i>
                                    <input v-bind:id="'peserta-' + key + '-line'" v-bind:name="'Peserta[' + key + '][line]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].line" placeholder="line">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-6">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-whatsapp'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-whatsapp text-azure"></i>
                                    <input v-bind:id="'peserta-' + key + '-whatsapp'" v-bind:name="'Peserta[' + key + '][whatsapp]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].whatsapp" placeholder="whatsapp">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-username_teman'">
                                <label v-bind:for="'peserta-' + key + '-username_teman'" class="form-label fw-bold">Username Teman</label>
                                <input v-bind:id="'peserta-' + key + '-username_teman'" v-bind:name="'Peserta[' + key + '][username_teman]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].username_teman">
                                <div class="form-info"></div>
                            </div>
                        </div>
                    </div>

                    <div class="margin-top-15"></div>
                    
                    <div class="text-center" v-if="key!=0">
                        <a v-on:click="removePeserta(key)" class="button button-sm border-light-red bg-light-red">Hapus Tiket Ke-{{key+1}}</a>
                    </div>
                </div>
            </template>
        </template>

        <hr class="border-light-azure border-top margin-top-50">

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray"></div>
            <div class="box-10 m-padding-x-0 text-dark fw-bold">Ringkasan</div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['transaksi']->jumlah_tiket ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Tagihan</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['transaksi']->tagihan, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status Bayar</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['transaksi']->status_bayar ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status Keanggotaan</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['transaksi']->status_aktif ?></div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>