<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/xswzaq/form-referral-agent.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

//
$provincesOriginal = [];
$provincesOriginal = array_map('ucwords', array_map('strtolower', ArrayHelper::map(\technosmart\modules\location\models\Provinces::find()->orderBy('name')->asArray()->all(), 'id', 'name')));

$provinces = [];
foreach ($provincesOriginal as $key => $value) {
    $provinces[] = [
        'value' => $key,
        'text' => $value,
    ];
}
// ddx($model['referral_agent']->regencies);
$this->registerJs(
    'vm.$data.referralAgent.id_provinces = ' . ($model['referral_agent']->regencies ? json_encode($model['referral_agent']->regencies->province_id) : 'null') . ';' .
    'vm.onProvinceChange();' .
    'vm.$data.referralAgent.id_regencies = ' . json_encode($model['referral_agent']->id_regencies) . ';' .
    'vm.$data.provinces = ' . json_encode($provinces) . ';',
    3
);

//
$errorMessage = '';
if ($model['referral_agent']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['referral_agent'], ['class' => '']);
}
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola data PIC dan volunteernya pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model['referral_agent'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'nama', ['class' => 'form-label fw-bold', 'label' => 'Nama Lengkap']); ?>
            <?= Html::activeTextInput($model['referral_agent'], 'nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'nama', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'nama')->end(); ?>

        <?= $form->field($model['referral_agent'], 'kode', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'kode', ['class' => 'form-label fw-bold', 'label' => 'Kode (bebas, untuk dipakai saat login)']); ?>
            <?= Html::activeTextInput($model['referral_agent'], 'kode', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'kode', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'kode')->end(); ?>

        <?= $form->field($model['referral_agent'], 'handphone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'handphone', ['class' => 'form-label fw-bold', 'label' => 'Handphone (yang aktif, untuk dipakai saat login)']); ?>
            <?= Html::activeTextInput($model['referral_agent'], 'handphone', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'handphone', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'handphone')->end(); ?>

        <?= $form->field($model['referral_agent'], 'fee', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'fee', ['class' => 'form-label fw-bold', 'label' => 'Fee (uang yang didapat oleh agent per-1 org pendaftar)']); ?>
            <?= Html::activeTextInput($model['referral_agent'], 'fee', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'fee', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'fee')->end(); ?>

        <?= $form->field($model['referral_agent'], 'program', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'program', ['class' => 'form-label fw-bold', 'label' => 'Program (m.agent / PIC / paguyuban / lainnya)']); ?>
            <?= Html::activeTextInput($model['referral_agent'], 'program', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'program', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'program')->end(); ?>

        <?= $form->field($model['referral_agent'], 'sekolah', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'sekolah', ['class' => 'form-label fw-bold', 'label' => 'Institusi (sekolah / univ / paguyuban / PIC / lainnya)']); ?>
            <?= Html::activeTextInput($model['referral_agent'], 'sekolah', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'sekolah', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'sekolah')->end(); ?>

        <?= $form->field($model['referral_agent'], 'id_regencies', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'id_regencies', ['class' => 'form-label fw-bold', 'label' =>'kota & provinsi']); ?>
            
            <select id="referral_agent-id_provinces" name="ReferralAgent[id_provinces]" class="form-dropdown rounded-xs" aria-invalid="false" v-model="referralAgent.id_provinces" v-on:change="onProvinceChange">
                <option value="">Pilih provinsi</option>
                <option v-for="option in provinces" v-bind:value="option.value">
                    {{ option.text }}
                </option>
            </select>

            <div class="margin-top-15"></div>

            <select id="referral_agent-id_regencies" name="ReferralAgent[id_regencies]" class="form-dropdown rounded-xs" aria-invalid="false" v-model="referralAgent.id_regencies">
                <option value="">Pilih kota</option>
                <option v-for="option in regencies" v-bind:value="option.value">
                    {{ option.text }}
                </option>
            </select>

            <?= Html::error($model['referral_agent'], 'id_regencies', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'id_regencies')->end(); ?>

        <?= $form->field($model['referral_agent'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'email', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['referral_agent'], 'email', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'email', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'email')->end(); ?>

        <?= $form->field($model['referral_agent'], 'nomor_identitas', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'nomor_identitas', ['class' => 'form-label fw-bold', 'label' => '(KTP / NIK / kartu pelajar / lainnya)']); ?>
            <?= Html::activeTextInput($model['referral_agent'], 'nomor_identitas', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'nomor_identitas', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'nomor_identitas')->end(); ?>

        <?= $form->field($model['referral_agent'], 'alamat_lengkap', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'alamat_lengkap', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextArea($model['referral_agent'], 'alamat_lengkap', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'alamat_lengkap', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'alamat_lengkap')->end(); ?>

        <?= $form->field($model['referral_agent'], 'catatan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['referral_agent'], 'catatan', ['class' => 'form-label fw-bold', 'label' => 'Catatan (untuk menandai keterangan tertentu)']); ?>
            <?= Html::activeTextArea($model['referral_agent'], 'catatan', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['referral_agent'], 'catatan', ['class' => 'form-info']); ?>
        <?= $form->field($model['referral_agent'], 'catatan')->end(); ?>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>