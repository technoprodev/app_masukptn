<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/xswzaq/list-referral-agent.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola referral agent
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">

    <div class="box box-space-md box-gutter box-break-sm box-equal">
        <div class="box-12 padding-20 border-light-azure">
            <div class="fs-15 text-dark m-text-center clearfix">
                <span class="border-azure text-azure padding-y-5 padding-x-15 rounded-lg text-middle">Daftar Referral Agent</span>
                <a href="<?= Yii::$app->urlManager->createUrl('xswzaq/form-referral-agent') ?>" class="button border-azure bg-azure hover-bg-lightest hover-text-azure pull-right m-pull-none m-button-block m-margin-top-30">Tambah Referral Agent</a>
            </div>
            <div class="margin-top-30 m-margin-top-15"></div>
            <table class="datatables table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>Part</th>
                        <th>Nama</th>
                        <th>Jml Peserta</th>
                        <th>Kode</th>
                        <th>Handphone</th>
                        <th>Fee</th>
                        <th>Program</th>
                        <th>Sekolah</th>
                        <th>Lokasi</th>
                        <th>Email</th>
                        <th>Nomor Identitas</th>
                        <th>Alamat Lengkap</th>
                        <th>Catatan</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search part..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search jml peserta..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kode..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search handphone..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search fee..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search program..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search sekolah..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search lokasi..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search email..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor identitas..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search alamat lengkap..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search catatan..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>