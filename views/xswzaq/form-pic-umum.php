<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\SummernoteAsset::register($this);

//
$errorMessage = '';
if ($model['pic_umum']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['pic_umum'], ['class' => '']);
}
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola note umum <!-- dan file umum --> pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 1200px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model['pic_umum'], 'note_umum', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic_umum'], 'note_umum', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextArea($model['pic_umum'], 'note_umum', ['class' => 'form-text rounded-xs summernote-default', 'maxlength' => true]); ?>
            <?= Html::error($model['pic_umum'], 'note_umum', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic_umum'], 'note_umum')->end(); ?>

        <!-- <?= $form->field($model['pic_umum'], 'file_umum', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['pic_umum'], 'file_umum', ['class' => 'form-label fw-bold']); ?>
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default square fileinput-exists" data-dismiss="fileinput"><i class="fa fa-close"></i></a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['pic_umum']->virtual_file_umum_download ?>"><?= $model['pic_umum']->file_umum ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default square btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['pic_umum'], 'virtual_file_umum_upload'); ?>
                </span>
            </div>
            <?= Html::error($model['pic_umum'], 'file_umum', ['class' => 'form-info']); ?>
        <?= $form->field($model['pic_umum'], 'file_umum')->end(); ?> -->

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>