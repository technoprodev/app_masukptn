<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Detail peserta
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray"></div>
            <div class="box-10 m-padding-x-0 text-dark fw-bold">Ringkasan Transaksi</div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total Tiket</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->jumlah_tiket ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Tagihan</div>
            <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['peserta']->transaksi->tagihan, 2) ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status Bayar</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->status_bayar ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status Keanggotaan</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->status_aktif ?></div>
        </div>

        <?php if (isset($model['peserta']->transaksi->pesertas)) foreach ($model['peserta']->transaksi->pesertas as $key => $peserta): ?>

            <hr class="border-light-azure border-top">

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray"></div>
                <div class="box-10 m-padding-x-0 text-dark fw-bold">Peserta ke-<?= ($key+1) ?></div>
            </div>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Username</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->username ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Password</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->password ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Kode</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->kode ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nama</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->nama ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Email</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->email ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Handphone</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->handphone ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Jenis tryout</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->id_periode_jenis ? $peserta->periodeJenis->nama : 'Belum diisi' ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga</div>
                <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($peserta->harga, 2) ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Periode Penjualan</div>
                <div class="box-10 m-padding-x-0 text-dark">Rp <?= $peserta->periode_penjualan ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Lokasi</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->id_periode_kota ? $peserta->periodeKota->nama : 'Belum diisi' ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Jenis Kelamin</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->jenis_kelamin ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Sekolah</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->sekolah ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Domisili</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->id_kota ? $peserta->kota->name : 'Belum diisi' ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Alamat</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->alamat ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Facebook</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->facebook ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Twitter</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->twitter ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Instagram</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->instagram ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Line</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->line ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Whatsapp</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->whatsapp ?></div>
            </div>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Jurusan</div>
                <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->id_jurusan ? $peserta->jurasan : 'Belum diisi' ?></div>
            </div>

        <?php endforeach; ?>

        <hr class="border-light-azure border-top">

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray"></div>
            <div class="box-10 m-padding-x-0 text-dark fw-bold">Ringkasan Pembayaran</div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Metode Pembayaran</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= isset($model['peserta']->transaksi->id_periode_metode_pembayaran) ? $model['peserta']->transaksi->periodeMetodePembayaran->nama : '-' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Tanggal Pembayaran</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= isset($model['peserta']->transaksi->tanggal_pembayaran) ? $model['peserta']->transaksi->tanggal_pembayaran : '-' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Pembayaran Atas Nama</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= isset($model['peserta']->transaksi->pembayaran_atas_nama) ? $model['peserta']->transaksi->pembayaran_atas_nama : '-' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Bukti Pembayaran</div>
            <div class="box-10 m-padding-x-0 text-dark">
                <?php if (isset($model['peserta']->transaksi->virtual_bukti_pembayaran_download)) : ?>
                    <a target="_blank" rel="noopener noreferrer" href="<?= $model['peserta']->transaksi->virtual_bukti_pembayaran_download ?>" class="a-nocolor">
                        <img src="<?= $model['peserta']->transaksi->virtual_bukti_pembayaran_download ?>" width="100%;" class="padding-5 border-lighter">
                    </a>
                <?php else : ?>
                    -
                <?php endif; ?>
            </div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Catatan Admin</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->catatan ? $model['peserta']->transaksi->catatan : '-' ?></div>
        </div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>
<?php endif; ?>