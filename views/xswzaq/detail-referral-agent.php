<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Detail PIC
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Part</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->id_periode ? $model['referral_agent']->id_periode : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nama</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->nama ? $model['referral_agent']->nama : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Kode</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->kode ? $model['referral_agent']->kode : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Handphone</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->handphone ? $model['referral_agent']->handphone : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Fee</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->fee ? $model['referral_agent']->fee : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Program</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->program ? $model['referral_agent']->program : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Sekolah</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->sekolah ? $model['referral_agent']->sekolah : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Lokasi</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->id_regencies ? ucwords(strtolower(($model['referral_agent']->regencies->name . ' - ' . $model['referral_agent']->regencies->province->name))) : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Email</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->email ? $model['referral_agent']->email : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nomor Identitas</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->nomor_identitas ? $model['referral_agent']->nomor_identitas : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Alamat Lengkap</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->alamat_lengkap ? $model['referral_agent']->alamat_lengkap : '(kosong)' ?></div>
        </div>

        <div class="box box-break-sm margin-bottom-10">
            <div class="box-2 padding-x-0 text-right m-text-left text-gray">Catatan</div>
            <div class="box-10 m-padding-x-0 text-dark"><?= $model['referral_agent']->catatan ? $model['referral_agent']->catatan : '(kosong)' ?></div>
        </div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>
<?php endif; ?>