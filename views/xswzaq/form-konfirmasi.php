<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;


$errorMessage = '';
$errorVue = false;
if ($model['peserta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    a.n. <span class="text-azure"><?= $model['peserta']->nama ?></span>
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
    
        <div class="box box-break-sm box-gutter">
            <div class="box-6">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#konfirmasi" data-toggle="tab">Data Konfirmasi</a></li>
                        <li role="presentation" class="activ3"><a href="#peserta" data-toggle="tab">Data Peserta</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="konfirmasi">
                            <div class="margin-top-30"></div>
                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray"></div>
                                <div class="box-10 m-padding-x-0 text-dark fw-bold">Ringkasan Transaksi</div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total Tiket</div>
                                <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->jumlah_tiket ?></div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Tagihan</div>
                                <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['peserta']->transaksi->tagihan, 2) ?></div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status Bayar</div>
                                <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->status_bayar ?></div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status Keanggotaan</div>
                                <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->status_aktif ?></div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray"></div>
                                <div class="box-10 m-padding-x-0 text-dark fw-bold">Ringkasan Pembayaran</div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Metode Pembayaran</div>
                                <div class="box-10 m-padding-x-0 text-dark"><?= isset($model['peserta']->transaksi->id_periode_metode_pembayaran) ? $model['peserta']->transaksi->periodeMetodePembayaran->nama : '-' ?></div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Tanggal Pembayaran</div>
                                <div class="box-10 m-padding-x-0 text-dark"><?= isset($model['peserta']->transaksi->tanggal_pembayaran) ? $model['peserta']->transaksi->tanggal_pembayaran : '-' ?></div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Kode TP / Pembayaran A.N.</div>
                                <div class="box-10 m-padding-x-0 text-dark"><?= isset($model['peserta']->transaksi->pembayaran_atas_nama) ? $model['peserta']->transaksi->pembayaran_atas_nama : '-' ?></div>
                            </div>

                            <?php if(false) : ?>
                            <div class="box box-break-sm margin-bottom-15">
                                <div class="box-2 padding-x-0 text-right m-text-left fw-bold"><?= $model['peserta']->attributeLabels()['tagihan'] ?></div>
                                <div class="box-10 m-padding-x-0">
                                    <div>
                                        <span class="margin-right-2 text-gray">Rp </span>
                                        <span class="text-spring fw-bold"><?= number_format($model['peserta']->tagihan, 2) ?></span>
                                    </div>
                                    <div class="margin-top-5"></div>
                                    <div>
                                        <i class="fa fa-ticket margin-right-5 text-gray"></i>
                                        <span class="text-dark"><?= $model['peserta']->jumlah_tiket ?> tiket</span>
                                    </div>
                                </div>
                            </div>

                            <div class="box box-break-sm margin-bottom-15">
                                <div class="box-2 padding-x-0 text-right m-text-left fw-bold">Peserta</div>
                                <div class="box-10 m-padding-x-0">
                                    <div>
                                        <span class="margin-right-2 text-gray">1. </span>
                                        <span class="text-dark"><?= $model['peserta']->nama ?></span>
                                        <span class="margin-right-5"></span>
                                        <span class="text-gray">kode: </span>
                                        <span class="text-dark"><?= $model['peserta']->nama ?></span>
                                    </div>
                                    <div>
                                        <span class="margin-right-2 text-gray">Rp </span>
                                        <span class="text-spring"><?= number_format($model['peserta']->harga, 2) ?></span>
                                        <span class="margin-right-5"></span>
                                        <span class="text-gray">jenis: </span>
                                        <span class="text-dark"><?= $model['peserta']->periodeJenis->nama ?></span>
                                    </div>
                                    <?php foreach ($model['peserta']->pesertaTambahans as $key => $pesertaTambahan) : ?>
                                        <div class="margin-top-10"></div>
                                        <div>
                                            <span class="margin-right-2 text-gray"><?= $key + 2 ?>. </span>
                                            <span class="text-dark"><?= $pesertaTambahan->nama ?></span>
                                            <span class="margin-right-5"></span>
                                            <span class="text-gray">kode: </span>
                                            <span class="text-dark">1010004</span>
                                        </div>
                                        <div>
                                            <span class="margin-right-2 text-gray">Rp </span>
                                            <span class="text-spring"><?= number_format($pesertaTambahan->harga, 2) ?></span>
                                            <span class="margin-right-5"></span>
                                            <span class="text-gray">jenis: </span>
                                            <span class="text-dark"><?= $pesertaTambahan->periodeJenis->nama ?></span>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <div class="box box-break-sm margin-bottom-15">
                                <div class="box-2 padding-x-0 text-right m-text-left fw-bold">Kota</div>
                                <div class="box-10 m-padding-x-0">
                                    <span class="text-grayest"><?= isset($model['peserta']->id_periode_kota) ? $model['peserta']->periodeKota->nama : '-' ?></span>
                                </div>
                            </div>

                            <div class="box box-break-sm margin-bottom-15">
                                <div class="box-2 padding-x-0 text-right m-text-left fw-bold">Metode</div>
                                <div class="box-10 m-padding-x-0">
                                    <span class="text-grayest"><?= isset($model['peserta']->id_periode_metode_pembayaran) ? $model['peserta']->periodeMetodePembayaran->nama : '-' ?></span>
                                </div>
                            </div>

                            <div class="box box-break-sm margin-bottom-15">
                                <div class="box-2 padding-x-0 text-right m-text-left fw-bold">Tanggal</div>
                                <div class="box-10 m-padding-x-0">
                                    <span class="text-grayest"><?= isset($model['peserta']->tanggal_pembayaran) ? $model['peserta']->tanggal_pembayaran : '-' ?></span>
                                </div>
                            </div>

                            <div class="box box-break-sm margin-bottom-15">
                                <div class="box-2 padding-x-0 text-right m-text-left fw-bold">A.n.</div>
                                <div class="box-10 m-padding-x-0">
                                    <span class="text-grayest"><?= isset($model['peserta']->pembayaran_atas_nama) ? $model['peserta']->pembayaran_atas_nama : '-' ?></span>
                                </div>
                            </div>
                            <?php endif; ?>

                            <div class="box box-break-sm">
                                <div class="box-2 padding-x-0 text-right m-text-left fw-bold">Verifikasi</div>
                                <div class="box-10 m-padding-x-0">
                                    <?= $form->field($model['transaksi'], 'status_bayar', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                        <?= Html::activeRadioList($model['transaksi'], 'status_bayar', ['Sudah Bayar' => 'Sudah Bayar', 'Ditolak' => 'Ditolak'], ['class' => 'form-radio', 'unselect' => null,
                                            'item' => function($index, $label, $name, $checked, $value){
                                                $checked = $checked ? 'checked' : '';
                                                $disabled = in_array($value, []) ? 'disabled' : '';
                                                return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                                            }]); ?>
                                        <?= Html::error($model['transaksi'], 'status_bayar', ['class' => 'form-info']); ?>
                                    <?= $form->field($model['transaksi'], 'status_bayar')->end(); ?>
                                </div>
                            </div>

                            <div class="box box-break-sm">
                                <div class="box-2 padding-x-0 text-right m-text-left fw-bold margin-top-5">Catatan</div>
                                <div class="box-10 m-padding-x-0">
                                    <?= $form->field($model['transaksi'], 'catatan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                        <?= Html::activeTextArea($model['transaksi'], 'catatan', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <div class="text-gray fs-12">jika diisi, catatan akan disampaikan ke transaksi melalui email.</div>
                                        <?= Html::error($model['transaksi'], 'catatan', ['class' => 'form-info']); ?>
                                    <?= $form->field($model['transaksi'], 'catatan')->end(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane activ3" id="peserta">
                            <div class="margin-top-30"></div>
                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray"></div>
                                <div class="box-10 m-padding-x-0 text-dark fw-bold">Ringkasan</div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Total Tiket</div>
                                <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->jumlah_tiket ?></div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Tagihan</div>
                                <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($model['peserta']->transaksi->tagihan, 2) ?></div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status Bayar</div>
                                <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->status_bayar ?></div>
                            </div>

                            <div class="box box-break-sm margin-bottom-10">
                                <div class="box-2 padding-x-0 text-right m-text-left text-gray">Status Keanggotaan</div>
                                <div class="box-10 m-padding-x-0 text-dark"><?= $model['peserta']->transaksi->status_aktif ?></div>
                            </div>

                            <?php if (isset($model['peserta']->transaksi->pesertas)) foreach ($model['peserta']->transaksi->pesertas as $key => $peserta): ?>

                                <hr class="border-light-azure border-top">

                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray"></div>
                                    <div class="box-10 m-padding-x-0 text-dark fw-bold">Peserta ke-<?= ($key+1) ?></div>
                                </div>

                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Username</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->username ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Password</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->password ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Kode</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->kode ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Nama</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->nama ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Email</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->email ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Handphone</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->handphone ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Jenis tryout</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->id_periode_jenis ? $peserta->periodeJenis->nama : 'Belum diisi' ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Harga</div>
                                    <div class="box-10 m-padding-x-0 text-dark">Rp <?= number_format($peserta->harga, 2) ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Periode Penjualan</div>
                                    <div class="box-10 m-padding-x-0 text-dark">Rp <?= $peserta->periode_penjualan ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Lokasi</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->id_periode_kota ? $peserta->periodeKota->nama : 'Belum diisi' ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Jenis Kelamin</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->jenis_kelamin ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Sekolah</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->sekolah ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Domisili</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->id_kota ? $peserta->kota->name : 'Belum diisi' ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Alamat</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->alamat ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Facebook</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->facebook ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Twitter</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->twitter ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Instagram</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->instagram ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Line</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->line ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Whatsapp</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->whatsapp ?></div>
                                </div>
                                <div class="box box-break-sm margin-bottom-10">
                                    <div class="box-2 padding-x-0 text-right m-text-left text-gray">Jurusan</div>
                                    <div class="box-10 m-padding-x-0 text-dark"><?= $peserta->id_jurusan ? $peserta->jurasan : 'Belum diisi' ?></div>
                                </div>

                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                        
            </div>
            <div class="box-6">
                <div class="box box-break-sm margin-bottom-15">
                    <div class="box-2 padding-x-0 text-right m-text-left fw-bold">Bukti</div>
                    <div class="box-10 m-padding-x-0">
                        <?php if (isset($model['peserta']->transaksi->virtual_bukti_pembayaran_download)) : ?>
                            <a target="_blank" rel="noopener noreferrer" href="<?= $model['peserta']->transaksi->virtual_bukti_pembayaran_download ?>" class="a-nocolor">
                                <img src="<?= $model['peserta']->transaksi->virtual_bukti_pembayaran_download ?>" width="100%;" class="padding-5 border-lighter">
                            </a>
                        <?php else : ?>
                            -
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>
<?php if (!Yii::$app->request->isAjax) : ?>

    </div>
</div>

<div class="margin-top-50"></div>
<?php endif; ?>
