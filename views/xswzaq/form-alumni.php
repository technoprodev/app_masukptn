<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FileInputAsset::register($this);

//
$errorMessage = '';
if ($model['alumni']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['alumni'], ['class' => '']);
}
?>

<div class="margin-top-60"></div>

<h1 class="text-uppercase text-red fs-60 m-fs-40 text-center"><?= $title; ?></h1>

<div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
    Kelola data alumni pada formulir dibawah ini
    <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
</div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-red" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model['alumni'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['alumni'], 'nama', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['alumni'], 'nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['alumni'], 'nama', ['class' => 'form-info']); ?>
        <?= $form->field($model['alumni'], 'nama')->end(); ?>

        <?= $form->field($model['alumni'], 'universitas', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['alumni'], 'universitas', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['alumni'], 'universitas', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['alumni'], 'universitas', ['class' => 'form-info']); ?>
        <?= $form->field($model['alumni'], 'universitas')->end(); ?>

        <?= $form->field($model['alumni'], 'jurusan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['alumni'], 'jurusan', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['alumni'], 'jurusan', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['alumni'], 'jurusan', ['class' => 'form-info']); ?>
        <?= $form->field($model['alumni'], 'jurusan')->end(); ?>

        <?= $form->field($model['alumni'], 'angkatan', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['alumni'], 'angkatan', ['class' => 'form-label fw-bold']); ?>
            <?= Html::activeTextInput($model['alumni'], 'angkatan', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
            <?= Html::error($model['alumni'], 'angkatan', ['class' => 'form-info']); ?>
        <?= $form->field($model['alumni'], 'angkatan')->end(); ?>

        <?= $form->field($model['alumni'], 'poto', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= Html::activeLabel($model['alumni'], 'poto', ['class' => 'form-label fw-bold']); ?>
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <a href="#" class="input-group-addon btn btn-default square fileinput-exists" data-dismiss="fileinput"><i class="fa fa-close"></i></a>
                <div class="form-text rounded-xs">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"><a href="<?= $model['alumni']->virtual_poto_download ?>"><?= $model['alumni']->poto ?></a></span>
                </div>
                <span class="input-group-addon btn btn-default square btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <?= Html::activeFileInput($model['alumni'], 'virtual_poto_upload'); ?>
                </span>
            </div>
            <?= Html::error($model['alumni'], 'poto', ['class' => 'form-info']); ?>
        <?= $form->field($model['alumni'], 'poto')->end(); ?>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('<i class="fa fa-rocket margin-right-5"></i> Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>