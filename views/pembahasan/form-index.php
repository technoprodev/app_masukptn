<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/dev/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);*/

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
/*$devChildren = [];
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $devChild)
        $devChildren[] = $devChild->attributes;

$this->registerJs(
    'vm.$data.dev.virtual_category = ' . json_encode($model['peserta']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = vm.$data.dev.devChildren.concat(' . json_encode($devChildren) . ');',
    // 'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');',
    3
);*/

//
$errorMessage = '';
$errorVue = false;
if ($model['peserta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta'], ['class' => '']);
}

/*if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/

$surveyPembahasans = \app_tryout\models\SurveyPembahasan::find()->select(['id', 'pertanyaan', 'tipe_pertanyaan', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'])->where(['id_periode' => 2])->indexBy('id')->asArray()->all();
// ddx($model['peserta']);
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<?php if (!$model['peserta']->isNewRecord) : ?>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">
            <?php if ($model['peserta']->status_survey_pembahasan == 'Belum Isi') : ?>
                Kamu belum bisa melihat pembahasan karena belum mengisi survey. Harap mengisi survey terlebih dahulu sebelum bisa melihat pembahasan.
            <?php else: ?>
                Silahkan download pembahasan pada tombol dibawah
            <?php endif; ?>
        </span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <?php if (false && $model['peserta']->status_survey_pembahasan == 'Belum Isi') : ?>
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
          
            <div class="box box-break-sm box-gutter box-equal">
                <?php foreach ($surveyPembahasans as $key => $surveyPembahasan) : ?>
                    <!-- box-6 --><div class="box-12">
                        <?= $form->field($model['peserta'], "virtual_survey_pembahasan[$key]", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info'], 'enableClientValidation' => false])->begin(); ?>
                            <?= Html::activeLabel($model['peserta'], "virtual_survey_pembahasan[$key]", ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' => $surveyPembahasan['pertanyaan']]); ?>

                            <?php if ($surveyPembahasan['tipe_pertanyaan'] == 'Essay') : ?>
                                <?= Html::activeTextArea($model['peserta'], "virtual_survey_pembahasan[$key]", ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'required' => true]); ?>
                            <?php else: ?>
                                <?php
                                    $pilihans = [];
                                    if ($surveyPembahasan['a']) $pilihans[$surveyPembahasan['a']] = $surveyPembahasan['a'];
                                    if ($surveyPembahasan['b']) $pilihans[$surveyPembahasan['b']] = $surveyPembahasan['b'];
                                    if ($surveyPembahasan['c']) $pilihans[$surveyPembahasan['c']] = $surveyPembahasan['c'];
                                    if ($surveyPembahasan['d']) $pilihans[$surveyPembahasan['d']] = $surveyPembahasan['d'];
                                    if ($surveyPembahasan['e']) $pilihans[$surveyPembahasan['e']] = $surveyPembahasan['e'];
                                    if ($surveyPembahasan['f']) $pilihans[$surveyPembahasan['f']] = $surveyPembahasan['f'];
                                    if ($surveyPembahasan['g']) $pilihans[$surveyPembahasan['g']] = $surveyPembahasan['g'];
                                    if ($surveyPembahasan['h']) $pilihans[$surveyPembahasan['h']] = $surveyPembahasan['h'];
                                    if ($surveyPembahasan['i']) $pilihans[$surveyPembahasan['i']] = $surveyPembahasan['i'];
                                    if ($surveyPembahasan['j']) $pilihans[$surveyPembahasan['j']] = $surveyPembahasan['j'];
                                    if ($surveyPembahasan['k']) $pilihans[$surveyPembahasan['k']] = $surveyPembahasan['k'];
                                    if ($surveyPembahasan['l']) $pilihans[$surveyPembahasan['l']] = $surveyPembahasan['l'];
                                    if ($surveyPembahasan['m']) $pilihans[$surveyPembahasan['m']] = $surveyPembahasan['m'];
                                    if ($surveyPembahasan['n']) $pilihans[$surveyPembahasan['n']] = $surveyPembahasan['n'];
                                ?>
                                <?= Html::activeRadioList($model['peserta'], "virtual_survey_pembahasan[$key]", $pilihans, ['class' => 'form-radio', 'unselect' => null,
                                    'item' => function($index, $label, $name, $checked, $value){
                                        $checked = $checked ? 'checked' : '';
                                        $disabled = in_array($value, []) ? 'disabled' : '';
                                        return "<label><input type='radio' name='$name' value='$value' $checked $disabled required><i></i>$label</label>";
                                    },
                                ]); ?>
                            <?php endif; ?>
                            <?php if (false && !$model['peserta']->virtual_survey_pembahasan[$key]) : ?>
                                <?= Html::error($model['peserta'], "virtual_survey_pembahasan[$key]", ['class' => 'form-info']); ?>
                            <?php endif; ?>
                        <?= $form->field($model['peserta'], "virtual_survey_pembahasan[$key]")->end(); ?>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="margin-top-30"></div>
            
            <div class="form-wrapper clearfix">
                <?= Html::submitButton('Submit & Lihat Pembahasan', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
            </div>
            
        <?php ActiveForm::end(); ?>
    <?php else: ?>
        <ul class="nav nav-tabs" role="tablist">
            <?php if ($model['peserta']->periodeJenis->nama == 'Saintek (IPA)') : ?><li role="presentation" class="active"><a href="#1" data-toggle="tab">Saintek</a></li><?php endif; ?>
            <?php if ($model['peserta']->periodeJenis->nama == 'Soshum (IPS)') : ?><li role="presentation" class="active"><a href="#2" data-toggle="tab">Soshum</a></li><?php endif; ?>
            <li role="presentation" class="activ3"><a href="#3" data-toggle="tab">TPS</a></li>
        </ul>

        <div class="tab-content">
            <?php if ($model['peserta']->periodeJenis->nama == 'Saintek (IPA)') : ?>
            <div class="tab-pane active padding-y-30" id="1">
                <center><iframe src="https://drive.google.com/file/d/1EqjBAFcsYrDl0c6GAXLEAYLwsIhAogzN/preview" width="100%" height="480"></iframe></center>
                <br>
                <a href="https://drive.google.com/file/d/1EqjBAFcsYrDl0c6GAXLEAYLwsIhAogzN/" class="button button-lg button-block bg-azure border-azure hover-text-azure hover-bg-lightest">Download Pembahasan Saintek</a>
            </div>
            <?php endif; ?>
            <?php if ($model['peserta']->periodeJenis->nama == 'Soshum (IPS)') : ?>
            <div class="tab-pane active padding-y-30" id="2">
                <center><iframe src="https://drive.google.com/file/d/1h2ci1TjXMteE8wpwhvq6HDxAolJLkS-m/preview" width="100%" height="480"></iframe></center>
                <br>
                <a href="https://drive.google.com/file/d/1h2ci1TjXMteE8wpwhvq6HDxAolJLkS-m/" class="button button-lg button-block bg-azure border-azure hover-text-azure hover-bg-lightest">Download Pembahasan Soshum</a>
            </div>
            <?php endif; ?>
            <div class="tab-pane activ3 padding-y-30" id="3">
                <center><iframe src="https://drive.google.com/file/d/1dspEYBbLgGPyBOdnmLPUWnDJE8V_l4C0/preview" width="100%" height="480"></iframe></center>
                <br>
                <a href="https://drive.google.com/file/d/1dspEYBbLgGPyBOdnmLPUWnDJE8V_l4C0/" class="button button-lg button-block bg-azure border-azure hover-text-azure hover-bg-lightest">Download Pembahasan TPS</a>
            </div>
        </div>


        <!-- <h2 class="text-azure text-center">Saintek</h2>
        <center><iframe src="https://drive.google.com/file/d/1UhgSkJiJ7DWyQbYVnosN9vvcLrsKZa0E/preview" width="100%" height="480"></iframe><p></p></center>
        <a href="https://drive.google.com/file/d/1UhgSkJiJ7DWyQbYVnosN9vvcLrsKZa0E/" class="button button-lg button-block bg-azure border-azure hover-text-azure hover-bg-lightest">Download Pembahasan Saintek</a>

        <hr class="border-light-azure border-top margin-top-50">
        
        <h2 class="text-azure text-center">Soshum</h2>
        <center><iframe src="https://drive.google.com/file/d/1in0UDRyeaa1Rg7g21Ax3k-JHJNzebFDU/preview" width="100%" height="480"></iframe><p></p></center> 
        <a href="https://drive.google.com/file/d/1in0UDRyeaa1Rg7g21Ax3k-JHJNzebFDU/" class="button button-lg button-block bg-azure border-azure hover-text-azure hover-bg-lightest">Download Pembahasan Soshum</a> -->
    <?php endif; ?>

    </div>
</div>

<div class="margin-top-30"></div>

<?php elseif (!$newSearch): ?>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Hasil pencarian</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>
        
    <div class="text-red text-uppercase text-center">
        <div>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/error.png" width="50px;">
        </div>
        <div class="margin-top-15"></div>
        <div>
            <span class="fs-20 m-fs-18">Data Tidak Ditemukan</span>
        </div>
    </div>

    </div>
</div>

<div class="margin-top-30"></div>

<?php endif; ?>

<?php if ($model['peserta']->isNewRecord) : ?>
<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">
            <?php if (!$model['peserta']->isNewRecord) : ?>
                Cek ulang status pembayaran kamu pada formulir di bawah ini
            <?php elseif (!$newSearch): ?>
                Harap perbaiki kesalahan sebelum melakukan pencarian kembali
            <?php else: ?>
                Silahkan masukan Nomor Peserta dan email kamu untuk melihat pembahasan
            <?php endif; ?>
        </span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-6">
                <?= $form->field($model['peserta'], 'kode', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'kode', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nomor Peserta']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Kode pendaftaran yang Kamu dapatkan melalui email</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'kode', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'kode', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'kode')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'email', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Email']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Alamat email yang Kamu gunakan untuk mendaftar.</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'email', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'email', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'email')->end(); ?>
            </div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Masuk', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    <div class="margin-top-30"></div>

    <div class="fs-13 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Belum punya akun ?</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-15"></div>

    <?= Html::a('Daftar Sekarang', ['pendaftaran'], ['class' => 'button button-md border-azure bg-azure button-block']) ?>

    </div>
</div>
<?php endif; ?>

<div class="margin-top-50"></div>

</div>