<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-gray bg-lightest rounded-sm" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">
    
    <h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

    <div class="fs-14 m-fs-13 text-gray text-center">
        <span class="">
            Selamat telah menyelesaikan ujian. Berikut adalah pembahasan dari ujian yang telah kamu ikuti
        </span>
        <hr class="border border-top margin-y-15">
    </div>

    <div class="margin-top-30"></div>

    <div>
        <ul class="nav nav-tabs" role="tablist">
            <?php if ($model['peserta']->periodeJenis->nama == 'Saintek (IPA)') : ?><li role="presentation" class="active"><a href="#1" data-toggle="tab">Saintek</a></li><?php endif; ?>
            <?php if ($model['peserta']->periodeJenis->nama == 'Soshum (IPS)') : ?><li role="presentation" class="active"><a href="#2" data-toggle="tab">Soshum</a></li><?php endif; ?>
            <!-- <li role="presentation" class="activ3"><a href="#3" data-toggle="tab">TPS</a></li> -->
        </ul>

        <div class="tab-content">
            <?php if ($model['peserta']->periodeJenis->nama == 'Saintek (IPA)') : ?>
            <div class="tab-pane active padding-y-30" id="1">
                <center><iframe src="https://drive.google.com/file/d/1sVw18yh3gHWcYAZzw-YvJTyzG37MdAf6/preview" width="100%" height="600"></iframe></center>
            </div>
            <?php endif; ?>
            <?php if ($model['peserta']->periodeJenis->nama == 'Soshum (IPS)') : ?>
            <div class="tab-pane active padding-y-30" id="2">
                <center><iframe src="https://drive.google.com/file/d/1vfVujGYgxDTSrfotrUZQobWlzPErBP7a/preview" width="100%" height="600"></iframe></center>
            </div>
            <?php endif; ?>
            <!-- <div class="tab-pane activ3 padding-y-30" id="3">
                <center><iframe src="https://drive.google.com/file/d/1dspEYBbLgGPyBOdnmLPUWnDJE8V_l4C0/preview" width="100%" height="600"></iframe></center>
                <br>
                <a href="https://drive.google.com/file/d/1dspEYBbLgGPyBOdnmLPUWnDJE8V_l4C0/" class="button button-lg button-block bg-azure border-azure hover-text-azure hover-bg-lightest">Download Pembahasan TPS</a>
            </div> -->
        </div>
    </div>

    </div>
</div>

<div class="margin-top-50"></div>

</div>