<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/dev/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);*/

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
/*$devChildren = [];
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $devChild)
        $devChildren[] = $devChild->attributes;

$this->registerJs(
    'vm.$data.dev.virtual_category = ' . json_encode($model['peserta']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = vm.$data.dev.devChildren.concat(' . json_encode($devChildren) . ');',
    // 'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');',
    3
);*/

//
$errorMessage = '';
$errorVue = false;
if ($model['peserta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta'], ['class' => '']);
}

/*if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<?php if (!$model['peserta']->isNewRecord) : ?>

<div class="container padding-y-30">
    <div class="" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="box box-gutter box-space-md box-break-sm box-equal">
        <div class="box-6 padding-30 shadow border-azure bg-lightest rounded-sm">
            <h4 class="text-center">
                <span class="text-azure">Promo</span>
            </h4>
            <div class="text-center text-orange fs-18">
                Spesial ketua OSIS & mantan ketua OSIS, PASTI dapat MINIMAL 5 tiket gratis!!
            </div>
            <div class="margin-top-15"></div>
            <div class="text-center">
                <a modal-md="" modal-title="Promo 1" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-promo', 'promo' => 1]) ?>" class="button button-lg border-azure bg-azure hover-bg-lightest hover-text-azure">Klik Disini</a>
            </div>
        </div>
        <div class="box-6 padding-30 shadow border-azure bg-lightest rounded-sm">
            <h4 class="text-center">
                <span class="text-azure">Promo</span>
            </h4>
            <div class="text-center text-orange fs-18">
                Kamu pernah juara Olimpiade? Langsung dapat tiket gratis!
            </div>
            <div class="margin-top-15"></div>
            <div class="text-center">
                <a modal-md="" modal-title="Promo 2" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-promo', 'promo' => 2]) ?>" class="button button-lg border-azure bg-azure hover-bg-lightest hover-text-azure">Klik Disini</a>
            </div>
        </div>
        <div class="box-6 padding-30 shadow border-azure bg-lightest rounded-sm">
            <h4 class="text-center">
                <span class="text-azure">Promo</span>
            </h4>
            <div class="text-center text-orange fs-18">
                Promo spesial jika 1 angkatan sekolahmu ikut semua! Harga tiket sangat murah!
            </div>
            <div class="margin-top-15"></div>
            <div class="text-center">
                <a modal-md="" modal-title="Promo 3" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-promo', 'promo' => 3]) ?>" class="button button-lg border-azure bg-azure hover-bg-lightest hover-text-azure">Klik Disini</a>
            </div>
        </div>
        <div class="box-6 padding-30 shadow border-azure bg-lightest rounded-sm">
            <h4 class="text-center">
                <span class="text-azure">Promo</span>
            </h4>
            <div class="text-center text-orange fs-18">
                Jadilah panitia volunteer acara kami!
            </div>
            <div class="margin-top-15"></div>
            <div class="text-center">
                <a modal-md="" modal-title="Promo 4" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-promo', 'promo' => 4]) ?>" class="button button-lg border-azure bg-azure hover-bg-lightest hover-text-azure">Klik Disini</a>
            </div>
        </div>
        <div class="box-6 padding-30 shadow border-azure bg-lightest rounded-sm">
            <h4 class="text-center">
                <span class="text-azure">Promo</span>
            </h4>
            <div class="text-center text-orange fs-18">
                Jadilah perwakilan kampusmu di acara kami!
            </div>
            <div class="margin-top-15"></div>
            <div class="text-center">
                <a modal-md="" modal-title="Promo 5" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-promo', 'promo' => 5]) ?>" class="button button-lg border-azure bg-azure hover-bg-lightest hover-text-azure">Klik Disini</a>
            </div>
        </div>
        <div class="box-6 padding-30 shadow border-azure bg-lightest rounded-sm">
            <h4 class="text-center">
                <span class="text-azure">Promo</span>
            </h4>
            <div class="text-center text-orange fs-18">
                Gabung grup Whatsapp kami, dapatkan info dan promo yang hanya ada disana!
            </div>
            <div class="margin-top-15"></div>
            <div class="text-center">
                <a modal-md="" modal-title="Promo 6" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-promo', 'promo' => 6]) ?>" class="button button-lg border-azure bg-azure hover-bg-lightest hover-text-azure">Klik Disini</a>
            </div>
        </div>
        <div class="box-6 padding-30 shadow border-azure bg-lightest rounded-sm">
            <h4 class="text-center">
                <span class="text-azure">Promo</span>
            </h4>
            <div class="text-center text-orange fs-18">
                Ajukan kotamu jadi lokasi kami selanjutnya!
            </div>
            <div class="margin-top-15"></div>
            <div class="text-center">
                <a modal-md="" modal-title="Promo 7" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-promo', 'promo' => 7]) ?>" class="button button-lg border-azure bg-azure hover-bg-lightest hover-text-azure">Klik Disini</a>
            </div>
        </div>
        <div class="box-6 padding-30 shadow border-azure bg-lightest rounded-sm">
            <h4 class="text-center">
                <span class="text-azure">Promo</span>
            </h4>
            <div class="text-center text-orange fs-18">
                Selesaikan pendaftaranmu di tanggal 24-27 Okt 2018, menangkan pulsa 25.000!
            </div>
            <div class="margin-top-15"></div>
            <div class="text-center">
                <a class="button disabled button-lg border-azure bg-azure hover-bg-lightest hover-text-azure">Promo Telah Usai</a>
            </div>
        </div>
        <div class="box-6 padding-30 shadow border-azure bg-lightest rounded-sm">
            <h4 class="text-center">
                <span class="text-azure">Promo</span>
            </h4>
            <div class="text-center text-orange fs-18">
                Tebak gambar, menangkan 5 tiket gratis!!
            </div>
            <div class="margin-top-15"></div>
            <div class="text-center">
                <a class="button disabled button-lg border-azure bg-azure hover-bg-lightest hover-text-azure">Promo Telah Usai</a>
            </div>
        </div>
    </div>

    </div>
</div>

<div class="margin-top-30"></div>

<?php elseif (!$newSearch): ?>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Hasil pencarian</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>
        
    <div class="text-red text-uppercase text-center">
        <div>
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/error.png" width="50px;">
        </div>
        <div class="margin-top-15"></div>
        <div>
            <span class="fs-20 m-fs-18">Data Tidak Ditemukan</span>
        </div>
    </div>

    </div>
</div>

<div class="margin-top-30"></div>

<?php endif; ?>

<?php if ($model['peserta']->isNewRecord) : ?>
<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">
            <?php if (!$model['peserta']->isNewRecord) : ?>
                Cek ulang status pembayaran kamu pada formulir di bawah ini
            <?php elseif (!$newSearch): ?>
                Harap perbaiki kesalahan sebelum melakukan pencarian kembali
            <?php else: ?>
                Silahkan masukan Nomor Peserta dan email kamu untuk mendapat promo
            <?php endif; ?>
        </span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-6">
                <?= $form->field($model['peserta'], 'kode', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'kode', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nomor Peserta']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Kode pendaftaran yang Kamu dapatkan melalui email</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'kode', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'kode', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'kode')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['peserta'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'email', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Email']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Alamat email yang Kamu gunakan untuk mendaftar.</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'email', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'email', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'email')->end(); ?>
            </div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Masuk/Login', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    <div class="margin-top-30"></div>

    <div class="fs-13 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Belum punya akun ?</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-15"></div>

    <?= Html::a('Daftar Sekarang', ['pendaftaran'], ['class' => 'button button-md border-azure bg-azure button-block']) ?>

    </div>
</div>
<?php endif; ?>

<div class="margin-top-50"></div>

</div>