<!-- <div class="margin-y-30 padding-30 shadow" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;"> -->

<style type="text/css">@page { margin: 5px; }</style>

<div class="has-bg-img">
	<div class="bg-img" style="background-repeat: no-repeat; background-image: url('<?= $this->render('img-bg-periode3-kartu-ujian'); ?>')"></div>
	<h2 class="text-" style="margin-top:387px;margin-bottom:0px;margin-left:275px;">
		<span style="display: inline-block;"><?= strtoupper($model['peserta']->kode) ?></span>
	</h2>
	<h2 class="text-" style="margin-top:0px;margin-bottom:0px;margin-left:275px;">
		<span style="display: inline-block;"><?= strtoupper($model['peserta']->nama) ?></span>
	</h2>
	<h2 class="text-" style="margin-top:0px;margin-bottom:70px;margin-left:275px;">
		<span style="display: inline-block;"><?= strtoupper($model['peserta']->email) ?></span>
	</h2>
	<h2 class="text-" style="margin-top:0px;margin-bottom:0px;margin-left:275px;">
		<span style="display: inline-block;">MINGGU</span>
	</h2>
	<h2 class="text-" style="margin-top:0px;margin-bottom:0px;margin-left:275px;">
		<span style="display: inline-block;">12 JANUARI 2020</span>
	</h2>
	<h2 class="text-" style="margin-top:0px;margin-bottom:50px;margin-left:275px;">
		<span style="display: inline-block;">07.00 - SELESAI</span>
	</h2>
	<h4 class="text-" style="margin-top:0px;margin-bottom:8px;margin-left:275px;">
		<span style="display: inline-block;"><?= str_replace(' - ', ' - ', $model['peserta']->periodeKota->nama) ?></span>
	</h4>
	<h4 class="text-" style="margin-top:0px;margin-bottom:251px;margin-left:275px;">
		<span style="display: inline-block;"><?= str_replace(' - ', ' - ', $model['peserta']->periodeKota->alamat) ?></span>
	</h4>
	<h4 class="text-" style="margin-top:0px;margin-bottom:0px;margin-left:230px;">
		<span style="display: inline-block;"><?= date('d-m-Y h:i:s a'); ?></span>
	</h4>
</div>
	

<!-- </div> -->
