<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/peserta/form-pendaftaran-awal.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\FileInputAsset::register($this);

//
$errorMessage = '';
$errorVue = false;
if ($model['peserta_misi']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta_misi'], ['class' => '']);
}
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  box-shadow: 0 0 10px rgba(51, 118, 184, 0.3);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-gray bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
    
    <h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

    <div class="fs-14 m-fs-13 text-gray text-center">
        <span class="">
            <?= $model['misi']->keterangan ?>.
        </span>
        <hr class="border border-top margin-y-15">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="box box-break-sm box-gutter box-equal">
            <?php if ($model['peserta_misi']->catatan_admin) : ?>
            <div class="box-12">
                <?= $form->field($model['peserta_misi'], 'catatan_admin', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta_misi'], 'catatan_admin', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Catatan Admin']); ?>
                    <div class="padding-y-5">
                        <?= $model['peserta_misi']->catatan_admin ?>
                    </div>
                <?= $form->field($model['peserta_misi'], 'catatan_admin')->end(); ?>
            </div>
            <?php endif; ?>
            <div class="box-12">
                <?= $form->field($model['peserta_misi'], 'virtual_poto_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta_misi'], 'virtual_poto_upload', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Upload Bukti Poto']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Foto / screenshot bukti transfer bank. Ukuran maksimal <b>1 MB</b>.</span> -->
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <a href="#" class="input-group-addon btn btn-default square fileinput-exists" data-dismiss="fileinput"><i class="fa fa-close"></i></a>
                        <div class="form-text rounded-xs">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"><a href="<?= $model['peserta_misi']->virtual_poto_download ?>"><?= $model['peserta_misi']->poto ?></a></span>
                        </div>
                        <span class="input-group-addon btn btn-default square btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <?= Html::activeFileInput($model['peserta_misi'], 'virtual_poto_upload'); ?>
                        </span>
                    </div>
                    <?= Html::error($model['peserta_misi'], 'virtual_poto_upload', ['class' => 'form-info fs-15 fw-bold']); ?>
                <?= $form->field($model['peserta_misi'], 'virtual_poto_upload')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['peserta_misi'], 'catatan_peserta', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta_misi'], 'catatan_peserta', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Catatan (opsional)']); ?>
                    <?= Html::activeTextArea($model['peserta_misi'], 'catatan_peserta', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta_misi'], 'catatan_peserta', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta_misi'], 'catatan_peserta')->end(); ?>
            </div>
        </div>

        <hr class="border-light-azure border-top margin-top-50">
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Submit', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>

</div>