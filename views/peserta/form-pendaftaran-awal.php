<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/peserta/form-pendaftaran-awal.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$provincesOriginal = [];
$provincesOriginal = array_map('ucwords', array_map('strtolower', ArrayHelper::map(\technosmart\modules\location\models\Provinces::find()->orderBy('name')->asArray()->all(), 'id', 'name')));

$provinces = [];
foreach ($provincesOriginal as $key => $value) {
    $provinces[] = [
        'value' => $key,
        'text' => $value,
    ];
}
// ddx($provinces);
$this->registerJs(
    'vm.$data.provinces = ' . json_encode($provinces) . ';',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['peserta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta'], ['class' => '']);
}

if (isset($model['peserta_tambahan'])) foreach ($model['peserta_tambahan'] as $key => $pesertaTambahan) {
    if ($pesertaTambahan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pesertaTambahan, ['class' => '']);
        $errorVue = true; 
    }
}
/*if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  box-shadow: 0 0 10px rgba(51, 118, 184, 0.3);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Semakin banyak yang mengajukan kota/kabupatennya untuk diadakan, panitia akan semakin yakin untuk mengadakannya di kota/kabupaten kamu.<br>Jangan lupa untuk ajak teman-teman kelas dan sekolah untuk mengisi form ajukan kota ini.</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="box box-break-sm box-gutter box-equal">
            <!-- box-6 --><div class="box-12">
                <?= $form->field($model['peserta'], 'nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'nama', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Nama Lengkap']); ?>
                    <?= Html::activeTextInput($model['peserta'], 'nama', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'nama', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'nama')->end(); ?>
            </div>
            <!-- box-6 --><div class="box-12">
                <?= $form->field($model['peserta'], 'email', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'email', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Email']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Pastikan email Kamu benar dan bisa dibuka, kami akan kirimkan nomor peserta ke email tersebut. Alamat email yang sama <b>BOLEH</b> digunakan berulang kali.</span> -->
                    <?= Html::activeTextInput($model['peserta'], 'email', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'email', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'email')->end(); ?>
            </div>
            <!-- box-4 --><div class="box-12">
                <?= $form->field($model['peserta'], 'handphone', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'handphone', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Handphone']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- No. HP peserta, akan digunakan untuk menghubungi peserta dalam keadaan mendesak.</span> -->
                    <!-- <div class="form-icon">
                        <span class="icon-prepend padding-right-0" style="line-height: 18px">0 </span>
                    </div> -->
                    <?= Html::activeTextInput($model['peserta'], 'handphone', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'placeholder' => '08xxxxx']); ?>
                    <?= Html::error($model['peserta'], 'handphone', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'handphone')->end(); ?>
            </div>
            <!-- box-6 --><div class="box-12">
                <?= $form->field($model['peserta'], 'id_periode_jenis', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'id_periode_jenis', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Jenis Tryout']); ?>
                    <!-- Pilih jenis tryout SBMPTN yang ingin Kamu ikuti -->
                    <?= Html::error($model['peserta'], 'id_periode_jenis', ['class' => 'form-info']); ?>
                    <?= Html::activeDropDownList($model['peserta'], 'id_periode_jenis', ArrayHelper::map(\app_tryout\models\PeriodeJenis::find()->where(['id_periode' => $idPeriode, 'status' => 'Sedang Aktif'])->indexBy('id')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih jenis tryout', 'class' => 'form-dropdown rounded-xs', 'style' => 'background: #d6ebed;'/*, 'v-model' => 'peserta.id_periode_jenis'*//*, 'v-on:change' => 'changeHarga'*/]); ?>
                <?= $form->field($model['peserta'], 'id_periode_jenis')->end(); ?>
            </div>

            <!-- box-6 --><div class="box-12">
                <?= $form->field($model['peserta'], 'id_request_kota', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'id_request_kota', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Lokasi Tryout']); ?>
                    
                    <select id="peserta-id_provinces" name="Peserta[id_provinces]" class="form-dropdown rounded-xs" style="background: #d6ebed;" aria-invalid="false" v-model="peserta.id_provinces" v-on:change="onProvinceChange">
                        <option value="">Pilih provinsi</option>
                        <option v-for="option in provinces" v-bind:value="option.value">
                            {{ option.text }}
                        </option>
                    </select>

                    <div class="margin-top-15"></div>

                    <select id="peserta-id_request_kota" name="Peserta[id_request_kota]" class="form-dropdown rounded-xs" style="background: #d6ebed;" aria-invalid="false">
                        <option value="">Pilih kota</option>
                        <option v-for="option in regencies" v-bind:value="option.value">
                            {{ option.text }}
                        </option>
                    </select>

                    <?= /*Html::activeDropDownList($model['peserta'], 'id_request_kota',
                        array_map('ucwords', array_map('strtolower', ArrayHelper::map(\technosmart\modules\location\models\Regencies::find()->indexBy('id')->asArray()->all(), 'id', 'name'))),
                        [
                            'prompt' => 'Pilih kota',
                            'class' => 'form-dropdown rounded-xs', 'style' => 'background: #d6ebed;',
                        ]
                    );*/ '' ?>
                    <?= Html::error($model['peserta'], 'id_request_kota', ['class' => 'form-info']); ?>

                    <div class="margin-top-15"></div>
                    <div class="padding-y-15 padding-x-15 margin-bottom-30 border-azure bg-azure">
                        Kami sedang dalam masa pengajuan kota/kabupaten tryout, silahkan pilih kota/kabupaten asal kamu.
                        <br>
                        Semakin banyak yang mengajukan kota/kabupatennya untuk diadakan, panitia akan semakin yakin untuk mengadakannya dikota/kabupaten kamu.
                    </div>
                <?= $form->field($model['peserta'], 'id_request_kota')->end(); ?>
            </div>
            <!-- box-4 --><div class="box-12">
                <?= $form->field($model['peserta'], 'sekolah', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'sekolah', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Asal Sekolah Peserta']); ?>
                    <?= Html::activeTextInput($model['peserta'], 'sekolah', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'placeholder' => 'contoh: SMAN 8 Jakarta']); ?>
                    <?= Html::error($model['peserta'], 'sekolah', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'sekolah')->end(); ?>
            </div>
            <!-- box-12 --><div class="box-12">
                <?= $form->field($model['peserta'], 'alamat', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'alamat', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Alamat Tinggal']); ?>
                    <?= Html::activeTextArea($model['peserta'], 'alamat', ['class' => 'form-textarea rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true]); ?>
                    <?= Html::error($model['peserta'], 'alamat', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'alamat')->end(); ?>
            </div>
            <!-- box-4 --><div class="box-12">
                <?= $form->field($model['peserta'], 'jenis_kelamin', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'jenis_kelamin', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Jenis Kelamin']); ?>
                    <?= Html::activeDropDownList($model['peserta'], 'jenis_kelamin', $model['peserta']->getEnum('jenis_kelamin'), ['prompt' => 'Pilih jenis kelamin', 'class' => 'form-dropdown rounded-xs', 'style' => 'background: #d6ebed;']); ?>
                    <?= Html::error($model['peserta'], 'jenis_kelamin', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'jenis_kelamin')->end(); ?>
            </div>
            <!-- box-12 --><div class="box-12">
                <label class="form-label fw-bold text-uppercase text-gray">Social media / instant messenger <span class="margin-y-5 fw-normal fs-italic">- Isi minimal 2 dari 5.</span></label>
            </div>
            <!-- box-4 --><div class="box-12">
                <?= $form->field($model['peserta'], 'facebook', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-facebook"></i>
                        <?= Html::activeTextInput($model['peserta'], 'facebook', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'placeholder' => 'facebook']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'facebook', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'facebook')->end(); ?>
            </div>
            <!-- box-4 --><div class="box-12">
                <?= $form->field($model['peserta'], 'twitter', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-twitter"></i>
                        <?= Html::activeTextInput($model['peserta'], 'twitter', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'placeholder' => 'twitter']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'twitter', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'twitter')->end(); ?>
            </div>
            <!-- box-4 --><div class="box-12">
                <?= $form->field($model['peserta'], 'instagram', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-instagram"></i>
                        <?= Html::activeTextInput($model['peserta'], 'instagram', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'placeholder' => 'instagram']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'instagram', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'instagram')->end(); ?>
            </div>
            <!-- box-6 --><div class="box-12">
                <?= $form->field($model['peserta'], 'line', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-commenting-o"></i>
                        <?= Html::activeTextInput($model['peserta'], 'line', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'placeholder' => 'line']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'line', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'line')->end(); ?>
            </div>
            <!-- box-6 --><div class="box-12">
                <?= $form->field($model['peserta'], 'whatsapp', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="form-icon">
                        <i class="icon-prepend fa fa-whatsapp"></i>
                        <?= Html::activeTextInput($model['peserta'], 'whatsapp', ['class' => 'form-text rounded-xs', 'style' => 'background: #d6ebed;', 'maxlength' => true, 'placeholder' => 'whatsapp']); ?>
                    </div>
                    <?= Html::error($model['peserta'], 'whatsapp', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'whatsapp')->end(); ?>
            </div>
            <!-- box-12 --><div class="box-12">
                <?= $form->field($model['peserta'], 'id_sumber', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['peserta'], 'id_sumber', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Dari Mana Kamu Mengetahui Tryout Ini ?']); ?>
                    <?= Html::activeDropDownList($model['peserta'], 'id_sumber', ArrayHelper::map(\app_tryout\models\Sumber::find()->indexBy('id')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih sumber', 'class' => 'form-dropdown rounded-xs', 'style' => 'background: #d6ebed;']); ?>
                    <?= Html::error($model['peserta'], 'id_sumber', ['class' => 'form-info']); ?>
                <?= $form->field($model['peserta'], 'id_sumber')->end(); ?>
            </div>
        </div>

        <hr class="border-light-azure border-top margin-top-50">
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Daftar', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>

</div>