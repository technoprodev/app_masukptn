<!-- <div class="margin-y-30 padding-30 shadow" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;"> -->

<style type="text/css">@page { margin: 5px; }</style>

<div class="has-bg-img">
	<div class="bg-img" style="background-repeat: no-repeat; background-image: url('<?= $this->render('img-bg-kartu-ujian'); ?>')"></div>
	<div class="fs-11 text-right fs-italic" style="margin-top:24px;margin-bottom:0px;margin-right:52px;">
		Dicetak tanggal: <?= date('d-m-Y'); ?>
	</div>
	<h6 class="text-right text-dark" style="margin-top:8px;margin-bottom:0px;margin-right:306px;">
		Open Gate <span class="text-red">06.30</span> !
	</h6>
	<div class="text-right text-lightest fs-10" style="margin-top:0px;margin-bottom:0px;margin-right:306px;">
		(waktu setempat)
	</div>
	<h6 class="text-lightest" style="margin-top:6px;margin-bottom:6px;margin-left:96px;">
		<span style="display: inline-block; width:40px;">Nama</span> <span style="display: inline-block;"> : <?= $model['peserta']->nama ?></span>
	</h6>
	<h6 class="text-lightest" style="margin-top:0px;margin-bottom:6px;margin-left:96px;">
		<span style="display: inline-block; width:40px;">Email</span> <span style="display: inline-block;"> : <?= $model['peserta']->email ?></span>
	</h6>
	<h6 class="text-lightest" style="margin-top:0px;margin-bottom:6px;margin-left:96px;">
		<span style="display: inline-block; width:40px;">No HP</span> <span style="display: inline-block;"> : <?= $model['peserta']->handphone ?></span>
	</h6>
	<h6 class="text-lightest" style="margin-top:0px;margin-bottom:6px;margin-left:96px;">
		<span style="display: inline-block; width:100px;">Nomor Peserta</span> <span style="display: inline-block;"> : <?= $model['peserta']->kode ?></span>
	</h6>
	<h5 class="text-lightest" style="margin-top:-10px;margin-bottom:0px;margin-left:346px;">
		<?= $model['peserta']->periodeJenis->nama ?>
	</h5>
	<table class="table table-no-line text-center" style="margin-top:44px;margin-bottom:0px;margin-left:142px;min-width:256px;width:256px;max-width:256px;">
		<tr>
			<td class="text-lightest fs-13 fw-bold" style="padding:0 2px;min-width:128px;width:128px;max-width:128px;"><?= str_replace(' - ', '<br>', $model['peserta']->periodeKota->nama) ?></td>
			<td class="text-lightest fs-13 fw-bold" style="padding:0 2px;min-width:128px;width:128px;max-width:128px;"><?= $model['peserta']->periode_penjualan ?></td>
		</tr>
	</table>
</div>
	

<!-- </div> -->
