<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/dev/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);*/

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
/*$devChildren = [];
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $devChild)
        $devChildren[] = $devChild->attributes;

$this->registerJs(
    'vm.$data.dev.virtual_category = ' . json_encode($model['peserta']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = vm.$data.dev.devChildren.concat(' . json_encode($devChildren) . ');',
    // 'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');',
    3
);*/

//
$errorMessage = '';
$errorVue = false;
if ($model['peserta']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['peserta'], ['class' => '']);
}

/*if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;">

    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Selamat datang :)</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-30"></div>

    <div class="box box-gutter box-break-sm box-space-md">
        <?php if ($model['transaksi']->status_bayar == 'Hanya Daftar') : ?>
            <div class="box-12 padding-15 rounded-sm bg-yellow text-center fs-16">
                <div>
                    Kamu belum memilih kota tempat kamu nanti melaksanakan tryout.
                </div>
                <div class="margin-top-5"></div>
                <div>
                    Klik tombol dibawah untuk memilih
                </div>
                <div class="margin-top-15"></div>
                <div>
                    <?= Html::a('Pilih Kota Tryout', ['pendaftaran-beli'], ['class' => 'button button-md border-lightest bg-lightest text-orange', 'target' => '_blank']) ?>
                    
                </div>
            </div>
        <?php elseif ($model['transaksi']->status_bayar == 'Belum Bayar') : ?>
            <div class="box-12 padding-15 rounded-sm bg-orange text-center fs-16">
                <div>
                    Kamu belum melakukan pembayaran untuk tryout di kota <?= $model['peserta']->periodeKota->nama ?>
                </div>
                <div class="margin-top-5"></div>
                <div>
                    Klik tombol dibawah untuk langsung ke halaman pembayaran
                </div>
                <div class="margin-top-15"></div>
                <div>
                    <a href="https://www.tokopedia.com/events/detail/masukptnid" class="button button-md border-lightest bg-lightest text-orange" target="_blank">Pembayaran</a>
                </div>
            </div>
            <div class="box-12 padding-15 rounded-sm bg-red text-center fs-16">
                <div>
                    Jika sudah melakukan pembayaran, cari Kode Voucher yang berawalan TP yang kamu dapat didalam akun Tokopedia / email kamu
                </div>
                <div class="margin-top-5"></div>
                <div>
                    Masukan kode tersebut <!-- pada kolom dibawah ini --> dengan cara klik tombol dibawah ini
                </div>
                <div class="margin-top-15"></div>
                <div>
                    <?= Html::a('Konfirmasi Pembayaran', ['peserta/konfirmasi'], ['class' => 'button button-md border-lightest bg-lightest text-orange']) ?>
                </div>
            </div>
        <?php elseif ($model['transaksi']->status_bayar == 'Sudah Bayar') : ?>
            <div class="box-12 padding-15 rounded-sm bg-azure text-center fs-16">
                <div>
                    Selamat, kamu sudah terdaftar untuk melaksanakan tryout di <?= $model['peserta']->periodeKota->nama ?>
                </div>
                <div class="margin-top-5"></div>
                <div>
                    Stay Up To Date dengan informasi2 kami di Instagram <a href="https://instagram.com/masukptnid" class="fw-bold">masukptnid</a>
                </div>
                <div class="margin-top-15"></div>
                <div>
                    Dan hubungi kami jika ada pertanyaan di <a href="https://line.me/ti/p/~@masukptnid" class="fw-bold">OFFICIAL LINE</a> kami dengan ID : <a href="https://line.me/ti/p/~@masukptnid" class="fw-bold">@masukptnid</a> (pakai @ dan id ya)
                </div>
            </div>
        <?php elseif ($model['transaksi']->status_bayar == 'Ditolak') : ?>
            <div class="box-12 padding-15 rounded-sm bg-red text-center fs-16">
                <div>
                    Konfirmasi pembayaran anda ditolak dengan alasan: <br>
                    <?= $model['transaksi']->catatan ? $model['transaksi']->catatan : '-' ?>
                </div>
                <div class="margin-top-15"></div>
                <div>
                    Silahkan melakukan konfirmasi ulang di sini : <?= Html::a('Konfirmasi Ulang Pembayaran', ['konfirmasi'], ['class' => 'butbutton button-md border-lightest bg-lightest text-orange']) ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <br>
    <marquee><b>Pendaftaran akan ditutup pada 10 Januari 2020 pada pukul 22.00 WIB. Harap segera melakukan pembayaran dan konfirmasi.</b> : Cara dapat tiket gratis bagi peserta kelas 2 yang tahun lalu ikut MasukPTNid 2019 kemarin akan diumumkan di Instagram @masukptnid</marquee>

    <div class="margin-top-30"></div>
    
    <div class="box box-gutter box-break-sm">
        <div class="box-6 text-gray">
            <div>
                <h6 class="text-azure">Biodata</h6>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Nomor Peserta :</div>
                    <div class="box-8 m-padding-x-0 text-red fw-bold fs-16"><?= $model['peserta']->kode ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Nama :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->nama ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Email :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->email ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Username :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->username ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Handphone :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->handphone ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Jenis Kelamin :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->jenis_kelamin ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Sekolah :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->sekolah ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Domisili :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->id_kota ? $model['peserta']->kota->name : '' ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Alamat :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->alamat ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Facebook :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->facebook ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Twitter :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->twitter ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Instagram :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->instagram ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Line :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->line ?></div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Whatsapp :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= $model['peserta']->whatsapp ?></div>
                </div>
            </div>
        </div>
        <!--<div class="box-6 text-gray">
            <div>
                <h6 class="text-azure">Poin</h6>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Poin Kamu :</div>
                    <div class="box-8 m-padding-x-0 text-dark">0</div>
                </div>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Tambah Poin :</div>
                    <div class="box-8 m-padding-x-0 text-dark"><?= Html::a('Ikuti Misi', ['peserta-misi'], ['class' => 'button button-sm border-azure text-azure']) ?></div>
                </div>
            </div>
            <div class="margin-top-15"></div>
        </div>-->
        <div class="box-6 text-gray">
            <div>
                <h6 class="text-azure">Transaksi</h6>
                <?php if(!$model['transaksi']->kode_referral_agent) : ?>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Total Tagihan :</div>
                    <div class="box-8 m-padding-x-0 text-spring">Rp <?= number_format($model['transaksi']->tagihan, 2) ?></div>
                </div>
                <?php endif; ?>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">Status Pembayaran : </div>
                    <?php
                        $color = 'text-azure';
                        if ($model['transaksi']->status_bayar == 'Hanya Daftar' || $model['transaksi']->status_bayar == 'Belum Bayar' || $model['transaksi']->status_bayar == 'Ditolak') {
                            $color = 'text-red';
                        } elseif ($model['transaksi']->status_bayar == 'Dalam Proses Konfirmasi') {
                            $color = 'text-orange';
                        }
                    ?>
                    <div class="box-8 m-padding-x-0 <?= $color ?>">
                        <i class="fa fa-circle margin-right-2"></i>
                        <?= $model['transaksi']->status_bayar == 'Belum Bayar' ? 'Menunggu Pembayaran' : $model['transaksi']->status_bayar ?><br>
                    </div>
                </div>
                <?php if($model['transaksi']->status_bayar == 'Belum Bayar') : ?>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">
                    </div>
                    <div class="box-8 m-padding-x-0 <?= $color ?>">
                        <?= Html::a('Konfirmasi Pembayaran', ['konfirmasi'], ['class' => 'button button-sm border-azure text-azure']) ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php if($model['transaksi']->status_bayar == 'Ditolak') : ?>
                <div class="box box-break-sm margin-bottom-10">
                    <div class="box-4 padding-x-0 text-gray">
                        Alasan Ditolak :
                    </div>
                    <div class="box-8 m-padding-x-0 <?= $color ?>">
                        <?= $model['transaksi']->catatan ? $model['transaksi']->catatan : '-' ?>
                        <br>
                        <?= Html::a('Konfirmasi Ulang Pembayaran', ['konfirmasi'], ['class' => 'button button-sm border-azure text-azure']) ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="margin-top-15"></div>
        </div>
        <div class="box-6 text-gray">
            <?php if($model['transaksi']->status_bayar != 'Sudah Bayar' && $model['transaksi']->periode->status == 'Aktif') : ?>
                <h5 class="text-azure">Fasilitas Peserta</h5>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Serentak dan <b>REAL TERBESAR</b> 85 Kota
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Penilaian sistem terbaru <b>IRT</b>
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Perankingan nasional <b>puluhan ribu peserta real</b>
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Goodiebag
                </div>

                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Soal 100% baru dan <b>PREDIKTIF</b>
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                   Pembahasan dan kunci jawaban yg <b>akurat</b>
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    <b>Buku Saku</b> Tips & Trikc UTBK SBMPTN
                </div>
                
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Diskon Voucher Menginap <b>Reddoorz</b> 25 %
                    <?= false && Html::a('Lihat Detail', ['detail-popup', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email, 'd' => 'menginap'], ['class' => '', 'modal-md' => '', 'modal-title' => 'Diskon Voucher Menginap']) ?>
                </div>
                <div class="margin-top-5"></div>
                <div>- Akan tersedia 1x24 jam setelah melakukan konfirmasi pembayaran</div>
                <div>- Berlaku sampai : Oktober 2019 dan November 2019</div>
                
                <div class="margin-top-20"></div>

                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Diskon 20% program Intensif Pra-UTBK Liburan oleh <b>SSC</b>
                    <?= false && Html::a('Lihat Detail', ['detail-popup', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email, 'd' => 'ssc'], ['class' => '', 'modal-md' => '', 'modal-title' => 'Diskon Voucher Menginap']) ?>
                </div>
                <div class="margin-top-5"></div>
                <div>- Akan tersedia 1x24 jam setelah melakukan konfirmasi pembayaran</div>
                
                <div class="margin-top-20"></div>
<!--
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Gratis Bimbel Online <b>SSC</b>
                    <?= false && Html::a('Lihat Detail', ['detail-popup', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email, 'd' => 'ssc1'], ['class' => '', 'modal-md' => '', 'modal-title' => 'Diskon Voucher Menginap']) ?>
                </div>
                <div class="margin-top-5"></div>
                <div>- Akan tersedia 1x24 jam setelah melakukan konfirmasi pembayaran</div>-->
                
                <div class="margin-top-20"></div>

                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    E-Certificate seperti UTBK
                    <?= false && Html::a('Lihat Detail', ['detail-popup', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email, 'd' => 'certificate'], ['class' => '', 'modal-md' => '', 'modal-title' => 'E-Certificate']) ?>
                </div>
                <div class="margin-top-5"></div>
                <div>- Akan tersedia setelah ujian selesai dilaksanakan</div>
                
                <div class="margin-top-30"></div>
            <?php elseif($model['transaksi']->status_bayar == 'Sudah Bayar' && $model['transaksi']->periode->status == 'Aktif') : ?>
                <h5 class="text-azure">Fasilitas Peserta</h5>
         <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Serentak dan <b>REAL TERBESAR</b> 85 Kota
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Penilaian sistem terbaru <b>IRT</b>
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Perankingan nasional <b>puluhan ribu peserta real</b>
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Goodiebag
                </div>

                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Soal 100% baru dan <b>PREDIKTIF</b>
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                   Pembahasan dan kunci jawaban yg <b>akurat</b>
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    <b>Buku Saku</b> Tips & Trikc UTBK SBMPTN
                </div>
                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Diskon Voucher Menginap <b>Reddoorz</b> 25 %
                    <?= false && Html::a('Lihat Detail', ['detail-popup', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email, 'd' => 'menginap'], ['class' => '', 'modal-md' => '', 'modal-title' => 'Diskon Voucher Menginap']) ?>
                </div>
                <div class="margin-top-5"></div>
                <div>- Kode: <span class="text-azure">MASUKPTNID2020</span></div>
                <div>- Berlaku sampai : Oktober 2019 dan November 2019</div>
                
                <div class="margin-top-20"></div>

                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Diskon 20% program Intensif Pra-UTBK Liburan oleh <b>SSC</b>
                    <?= false && Html::a('Lihat Detail', ['detail-popup', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email, 'd' => 'ssc'], ['class' => '', 'modal-md' => '', 'modal-title' => 'Diskon Voucher Menginap']) ?>
                </div>
                <!-- <div class="margin-top-5"></div> -->
                <!-- <div>- akan tersedia 1x24 jam setelah melakukan konfirmasi pembayaran</div> -->

              <!--  <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    Gratis Bimbel Online <b>SSC</b>
                    <?= false && Html::a('Lihat Detail', ['detail-popup', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email, 'd' => 'ssc1'], ['class' => '', 'modal-md' => '', 'modal-title' => 'Diskon Voucher Menginap']) ?>
                </div>-->
                <div class="margin-top-5"></div>
                <div>- Notes: <span class="text-azure">10021</span></div>
                <div>- PIN: <span class="text-azure">2121</span></div>
                <div>- Berlaku dari tanggal 14-19 Oktober 2020</div>
                <div>- Klik disini <a href="http://online.sonysugemacollege.com">http://online.sonysugemacollege.com</a></div>
                
                <div class="margin-top-20"></div>
                
                <div class="margin-top-20"></div>

                <div class="text-dark">
                    <i class="fa fa-angle-double-right"></i>
                    E-Certificate
                    <?= false && Html::a('Lihat Detail', ['detail-popup', 'kode' => $model['peserta']->kode, 'email' => $model['peserta']->email, 'd' => 'certificate'], ['class' => '', 'modal-md' => '', 'modal-title' => 'E-Certificate']) ?>
                </div>
                <div class="margin-top-5"></div>
                <div>- Akan tersedia setelah ujian selesai dilaksanakan</div>
                
                <div class="margin-top-30"></div>
            <?php endif; ?>

            <?php if(false) : ?>
                <h5 class="text-azure">Tentang Pengumuman Hasil</h5>

                <div><b>1.</b> Download nilai mu pada tombol "Download Nilai" dibawah</div>
                <div class="margin-top-5"></div>
                <div><b>2.</b> Pengumuman nilai mu berbentuk sertifikat nilai, sama seperti SBMPTN aslinya</div>
                <div class="margin-top-5"></div>
                <div><b>3.</b> Jika ingin mengetahui peringkat nilai mu se-Indonesia melawan semua pejuang SBMPTN 2019 (pengumuman versi baru), masukan nilai mu di aplikasi Pantau Nilai.</div>
                <div class="margin-top-5"></div>
                <div>a. Untuk pengguna android, download aplikasinya di Playstore dengan cari aplikasi Pantau Nilai UTBK, atau klik: <a href="www.bit.ly/unduhpantaunilai">www.bit.ly/unduhpantaunilai</a></div>
                <div class="margin-top-5"></div>
                <div>b. Untuk pengguna iOS, klik: <a href="https://pantaunilaiutbk.id/">https://pantaunilaiutbk.id/</a></div>
                <div class="margin-top-5"></div>
                <div><b>4.</b> Jika ingin melihat tabel peringkat nilai mu melawan peserta tryout MasukPTNid (pengumuman versi lama), tekan tombol "Pengumuman" didalam aplikasi Pantau Nilai</div>
            <?php endif; ?>
        </div>
    </div>

    <div class="margin-bottom-30"></div>

    <h6 class="text-azure margin-0">Daftar Peserta</h6>
    <div class="visible-sm-less">Harap scroll ke kanan untuk download kartu ujian dan sertifikat</div>
    <div class="scroll-x">
    <table class="table margin-0">
        <thead>
            <tr>
                <th class="padding-left-0">Nama</th>
                <th>Kontak</th>
                <th>Jenis</th>
                <th>Lokasi</th>
                <th>E-Tiket</th>
                <th>Nilai</th>
                <!-- <th>E-Certificate</th>
                <th>Nilai</th>
                <th>Pembahasan</th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model['transaksi']->pesertas as $key => $peserta) : ?>
                <tr>
                    <td class="padding-left-0">
                        <div class="fs-14 text-dark"><?= $peserta->nama ?></div>
                        <div>
                            <span class="text-gray">nomor peserta: </span>
                            <span class="text-dark"><?= $peserta->kode ?></span>
                        </div>
                    </td>
                    <td>
                        <div>
                            <i class="fa fa-envelope margin-right-5 text-gray" style="width: 10px;"></i>
                            <span class="text-dark"><?= $peserta->email ?></span>
                        </div>
                        <div>
                            <i class="fa fa-phone margin-right-5 text-gray" style="width: 10px;"></i>
                            <span class="text-dark"><?= $peserta->handphone ?></span>
                        </div>
                    </td>
                    <td>
                        <div class="fs-14 text-dark"><?= $peserta->id_periode_jenis ? $peserta->periodeJenis->nama : 'Belum memilih' ?></div>
                        <?php if(!$model['transaksi']->kode_referral_agent) : ?>
                        <div>
                            <span class="text-gray">Rp </span>
                            <span class="text-dark"><?= number_format($peserta->harga, 2) ?></span>
                        </div>
                        <?php endif; ?>
                    </td>
                    <td>
                        <div>
                            <i class="fa fa-map-marker margin-right-5 text-gray"></i>
                            <span class="text-dark"><?= $peserta->periodeKota ? $peserta->periodeKota->nama : '(kosong)' ?></span>
                        </div>
                        <div>
                            <?= Html::a('Lihat Detail', ['detail-kota', 'id' => $peserta->id_periode_kota], ['modal-md' => '', 'modal-title' => 'Detail Lokasi']) ?>
                        </div>
                    </td>
                    <td>
                        <?php if ($model['transaksi']->periode->status == 'Aktif' && $model['transaksi']->status_bayar == 'Sudah Bayar') : ?>
                            <?= Html::a('Download', ['download-kartu-ujian', 'kode' => $peserta->kode, 'email' => $peserta->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif' && ($model['transaksi']->status_bayar == 'Belum Bayar' || $model['transaksi']->status_bayar == 'Dalam Proses Konfirmasi' || $model['transaksi']->status_bayar == 'Hanya Daftar')) : ?>
                            Belum tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Selesai') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Pending') : ?>
                            Belum tersedia
                        <?php else : ?>
                            Tidak tersedia
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($model['transaksi']->periode->status == 'Aktif' && $model['transaksi']->status_bayar == 'Sudah Bayar') : ?>
                            <?= Html::a('Download', ['download-nilai'], ['class' => 'button button-sm border-azure text-azure']) ?>
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif' && ($model['transaksi']->status_bayar == 'Belum Bayar' || $model['transaksi']->status_bayar == 'Dalam Proses Konfirmasi' || $model['transaksi']->status_bayar == 'Hanya Daftar')) : ?>
                            Belum tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Selesai') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Pending') : ?>
                            Belum tersedia
                        <?php else : ?>
                            Tidak tersedia
                        <?php endif; ?>
                    </td>
                    <!-- <td>
                        <?php if ($model['transaksi']->periode->status == 'Aktif' && $model['transaksi']->status_bayar == 'Sudah Bayar') : ?>
                            <?= Html::a('Download', ['download-sertifikat', 'kode' => $peserta->kode, 'email' => $peserta->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif' && ($model['transaksi']->status_bayar == 'Belum Bayar' || $model['transaksi']->status_bayar == 'Dalam Proses Konfirmasi' || $model['transaksi']->status_bayar == 'Hanya Daftar')) : ?>
                            Belum tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Selesai') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Pending') : ?>
                            Belum tersedia
                        <?php else : ?>
                            Tidak tersedia
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($model['transaksi']->periode->status == 'Aktif' && $model['transaksi']->status_bayar == 'Sudah Bayar') : ?>
                            <?= Html::a('Download Nilai', ['download-nilai-part2', 'kode' => $peserta->kode, 'email' => $peserta->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif' && ($model['transaksi']->status_bayar == 'Belum Bayar' || $model['transaksi']->status_bayar == 'Dalam Proses Konfirmasi' || $model['transaksi']->status_bayar == 'Hanya Daftar')) : ?>
                            Belum tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Selesai') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Pending') : ?>
                            Belum tersedia
                        <?php else : ?>
                            Tidak tersedia
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($model['transaksi']->periode->status == 'Aktif' && $model['transaksi']->status_bayar == 'Sudah Bayar') : ?>
                            <?= Html::a('Lihat Pembahasan', ['pembahasan/index', 'kode' => $peserta->kode, 'email' => $peserta->email], ['class' => 'button button-sm border-azure text-azure']) ?>
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif' && ($model['transaksi']->status_bayar == 'Belum Bayar' || $model['transaksi']->status_bayar == 'Dalam Proses Konfirmasi' || $model['transaksi']->status_bayar == 'Hanya Daftar')) : ?>
                            Belum tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Aktif') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Selesai') : ?>
                            Tidak tersedia
                        <?php elseif ($model['transaksi']->periode->status == 'Pending') : ?>
                            Belum tersedia
                        <?php else : ?>
                            Tidak tersedia
                        <?php endif; ?>
                    </td> -->
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    <div class="visible-sm-less">Harap scroll ke kanan untuk download kartu ujian dan sertifikat<!--  dan mengaktifkan ambassador --></div>

    <div class="margin-top-15"></div>

    <div class="padding-x-15">
        <?php if ($model['transaksi']->status_bayar == 'Belum Bayar' || $model['transaksi']->status_bayar == 'Belum Bayar') : ?>
            <span class="fs-italic"><i class="fa fa-warning margin-right-5"></i> tombol download kartu ujian akan muncul setelah kamu melakukan pembayaran</span>
        <?php elseif ($model['transaksi']->status_bayar == 'Dalam Proses Konfirmasi') : ?>
            <span class="fs-italic"><i class="fa fa-warning margin-right-5"></i> tombol download kartu ujian akan muncul setelah admin melakukan konfirmasi</span>
        <?php endif; ?>
    </div>

    <?php if ($model['transaksi']->status_bayar == 'Hanya Daftar') : ?>
    
    <div class="margin-top-15"></div>

    <?= Html::a('Pilih Kota Tryout', ['pendaftaran-beli'], ['class' => 'button button-md border-orange bg-orange button-block', 'target' => '_blank']) ?>
    
    <?php endif; ?>

    </div>
</div>

<div class="margin-top-30"></div>

<div class="margin-top-50"></div>

</div>