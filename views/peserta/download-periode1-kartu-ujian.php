<!-- <div class="margin-y-30 padding-30 shadow" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;"> -->

<style type="text/css">@page { margin: 30px; }</style>

<table class="table table-no-line">
	<tr>
		<td class="text-center" width="40%">
			<img alt="<?= Yii::$app->params['app.name'] ?> logo" class="text-middle" src="<?= $this->render('img-kartu-ujian'); ?>" style="width: 400px; height: auto;">
		</td>
		<td style="background-image: url('<?= $this->render('img-kartu-ujian-background'); ?>');background-size:auto 100%;background-repeat: no-repeat;background-position: center">
			<table class="table table-no-line fw-bold fs-16">
				<tr>
					<td class="padding-y-5">Nama</td><td class="padding-y-5">: <?= $model['peserta']->nama ?></td>
				</tr>
				<tr>
					<td class="padding-y-5">Email</td><td class="padding-y-5">: <?= $model['peserta']->email ?></td>
				</tr>
				<tr>
					<td class="padding-y-5">Nomor HP</td><td class="padding-y-5">: <?= $model['peserta']->handphone ?></td>
				</tr>
				<tr>
					<td class="padding-y-5">Jenis</td><td class="padding-y-5">: <?= $model['peserta']->periodeJenis->nama ?></td>
				</tr>
				<tr>
					<td class="padding-y-5">Periode Tiket</td><td class="padding-y-5">: <?= $model['peserta']->periode_penjualan ?></td>
				</tr>
				<tr>
					<td class="padding-y-5">Nomor Peserta</td><td class="padding-y-5">: <?= $model['peserta']->kode ?></td>
				</tr>
				<tr>
					<td class="padding-y-5">Lokasi Ujian</td><td class="padding-y-5">: <?= $model['peserta']->periodeKota ? $model['peserta']->periodeKota->nama : '' ?> (<?= $model['peserta']->periodeKota ? $model['peserta']->periodeKota->alamat : '' ?>)</td>
				</tr>
				<tr>
					<td class="padding-y-5">Waktu Tryout</td><td class="padding-y-5">: 13 januari 2019</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<hr class="margin-0 border-top border-azure">

<div style="background-image: url('<?= $this->render('img-footer-complete-download'); ?>');background-size:100% auto;background-repeat: no-repeat;background-position: center bottom;">
	<table class="table table-no-line margin-0">
		<tr>
			<td>
				<div class="margin-bottom-5">1.	E-tiket ini merupakan tiket untuk mengikuti MasukPTNid Tryout Nasional SBMPTN – Test Center 2019.</div>
				<div class="margin-bottom-5">2.	Bawalah e-tiket ini pada hari H tryout.</div>
				<div class="margin-bottom-5">3.	Pembelian tiket yang sah hanya dapat didapat secara online melalui website www.masukptn.id, official line dengan ID @masukptnid, atau partner yang terdapat di website www.masukptn.id</div>
				<div class="margin-bottom-5">4.	Kehilangan dan kerusakan e-tiket bukanlah merupakan tanggung jawab panitia tryout, e-tiket ini dapat diprint kembali oleh peserta dengan mengakses email masing-masing.</div>
				<div class="margin-bottom-5">5.	Panitia tryout berhak melarang masuk peserta yang tidak dapat menunjukkan e-tiket.</div>
				<div class="margin-bottom-5">6.	E-tiket dengan satu nomor perserta hanya bisa digunakan untuk satu orang peserta tryout dan tidak akan bisa digunakan kembali oleh peserta lain.</div>
			</td>
			<td>
				<div class="margin-bottom-5">7.	E-tiket yang telah dibeli tidak dapat ditukar/diuangkan kembali.</div>
				<div class="margin-bottom-5">8.	Harap membawa kartu identitas diri yang sesuai dengan data yang terdapat pada e-tiket ini.</div>
				<div class="margin-bottom-5">9.	E-tiket ini tidak dapat dipindahtangankan.</div>
				<div class="margin-bottom-5">10.	Dilarang membawa obat-obatan terlarang, senjata api, senjata tajam, alkohol, dan barang lain yang dianggap oleh panitia dapat membahayakan peserta lain atau yang dapat mengganggu berjalannya acara ke dalam tempat pelaksanaan tryout.</div>
				<div class="margin-bottom-5">11.	Panitia berhak mengeluarkan pengunjung yang membuat kegaduhan dan mengganggu kenyamanan pengunjung lain atau mengganggu acara.</div>
				<div class="margin-bottom-5">12.	Panitia tidak menyediakan tempat penitipan barang dan tidak bertanggung jawab kepada segala bentuk kehilangan.</div>
				<div class="margin-bottom-5">13.	Panitia berhak tidak memberikan izin kepada peserta untuk masuk kedalam tempat tryout atau suatu ruangan karena sudah dianggap penuh atau terlambat sehingga mengganggu jalannya acara.</div>
			</td>
		</tr>
	</table>

	<p class="padding-x-15">SYARAT DAN KETENTUAN DIATAS TELAH SAYA BACA, PAHAMI, MENGERTI, DAN SAYA MENYETUJUI UNTUK TERIKAT SECARA HUKUM ATAS SAYRAT DAN KETENTUAN DI ATAS</p>
</div>
	

<!-- </div> -->
