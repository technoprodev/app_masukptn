<!-- <div class="margin-y-30 padding-30 shadow" style="max-width: 900px; width: 100%; margin-left: auto; margin-right: auto;"> -->

<style type="text/css">@page { margin: 5px; }</style>

<div class="has-bg-img">
	<div class="bg-img" style="background-repeat: no-repeat; background-image: url('<?= $this->render('img-bg-sertifikat'); ?>')"></div>
	<h1 class="text-center fw-extra-bold fs-60 text-dark" style="margin-top:215px;">
		<?php if (isset($model['peserta'])) {
			echo $model['peserta']->nama;
		} else if (isset($model['volunteer'])) {
			echo $model['volunteer']->nama;
		} else if (isset($model['pic'])) {
			echo $model['pic']->nama;
		} ?>
	</h1>
	<h1 class="text-center fw-extra-bold fs-60" style="margin-top:35px;color:#ff9408;">
		<?php if (isset($model['peserta'])) {
			echo 'PESERTA';
		} else if (isset($model['volunteer'])) {
			echo 'VOLUNTEER';
		} else if (isset($model['pic'])) {
			echo 'PIC';
		} ?>
	</h1>
</div>
	

<!-- </div> -->
