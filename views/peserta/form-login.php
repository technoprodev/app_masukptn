<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
$errorMessage = '';
$errorVue = false;
if ($model['login']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['login'], ['class' => '']);
}

/*if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

    <!-- <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">
            <?php if (false && !$model['login']->isNewRecord) : ?>
                Cek ulang status pembayaran kamu pada formulir di bawah ini
            <?php elseif (false && $model['login']->isNewRecord && $model['login']->hasErrors()): ?>
                Harap perbaiki kesalahan sebelum melakukan pencarian kembali
            <?php else: ?>
                Silahkan masukan username dan password kamu untuk login ke halaman peserta (pembelian tiket, konfirmasi pembayaran, cetak tiket, promo, dll)
            <?php endif; ?>
        </span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div> -->

    <div class="fs-14 m-fs-13 text-gray text-center">
        <span class="">
            Silahkan masukan username dan password kamu untuk login ke halaman peserta (pembelian tiket, konfirmasi pembayaran, cetak tiket, promo, dll)<br>
            Cek <a modal-md="" modal-title="Prosedur Pendaftaran" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-prosedur']) ?>" class="">Prosedur Pendaftaran</a> jika ada yang kurang jelas.
        </span>
        <hr class="border border-top margin-y-15">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-6">
                <?= $form->field($model['login'], 'username', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['login'], 'username', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Username']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Kode pendaftaran yang kamu dapatkan melalui email</span> -->
                    <?= Html::activeTextInput($model['login'], 'username', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['login'], 'username', ['class' => 'form-info']); ?>
                <?= $form->field($model['login'], 'username')->end(); ?>
            </div>
            <div class="box-6">
                <?= $form->field($model['login'], 'password', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['login'], 'password', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Password']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Alamat password yang kamu gunakan untuk mendaftar.</span> -->
                    <?= Html::activePasswordInput($model['login'], 'password', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['login'], 'password', ['class' => 'form-info']); ?>
                <?= $form->field($model['login'], 'password')->end(); ?>
            </div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Masuk/Login', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    <div class="margin-top-30"></div>

    <div class="fs-13 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Belum punya akun ?</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div>

    <div class="margin-top-15"></div>

    <?= Html::a('Daftar Sekarang', ['pendaftaran'], ['class' => 'button button-md border-azure bg-azure button-block']) ?>

    </div>
</div>

<div class="margin-top-50"></div>

</div>