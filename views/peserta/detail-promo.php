<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$content = [
    [//1
        'judul' => 'Spesial ketua OSIS & mantan ketua OSIS,<br>PASTI dapat MINIMAL 5 tiket gratis!!',
        'status' => 'Promo Masih Ada (aktif)',
        'gambar' => Yii::$app->getRequest()->getBaseUrl() . '/img/part-1-2019/promo/promo_1.jpg',
        'link' => 'https://www.instagram.com/p/Bpd-AQbAEWt/?taken-by=masukptnid',
    ],
    [//2
        'judul' => 'Kamu pernah juara Olimpiade? Langsung dapat tiket gratis!',
        'status' => 'Promo Masih Ada (aktif)',
        'gambar' => Yii::$app->getRequest()->getBaseUrl() . '/img/part-1-2019/promo/promo_2.jpg',
        'link' => 'https://www.instagram.com/p/BpbL0gOgTrJ/?taken-by=masukptnid',
    ],
    [//3
        'judul' => 'Promo spesial jika 1 angkatan sekolahmu ikut semua! Harga tiket sangat murah!',
        'status' => 'Promo Masih Ada (aktif)',
        'gambar' => Yii::$app->getRequest()->getBaseUrl() . '/img/part-1-2019/promo/promo_3.jpg',
        'link' => 'http://bit.ly/openschool2019',
    ],
    [//4
        'judul' => 'Jadilah panitia volunteer acara kami!',
        'status' => 'Promo Masih Ada (aktif)',
        'gambar' => Yii::$app->getRequest()->getBaseUrl() . '/img/part-1-2019/promo/promo_4.jpg',
        'link' => 'http://bit.ly/rekruitasimasukptn2019',
    ],
    [//5
        'judul' => 'Jadilah perwakilan kampusmu di acara kami!',
        'status' => 'Promo Masih Ada (aktif)',
        'gambar' => Yii::$app->getRequest()->getBaseUrl() . '/img/part-1-2019/promo/promo_5.jpg',
        'link' => 'http://bit.ly/openmahasiswadaerah2019',
    ],
    [//6
        'judul' => 'Gabung grup Whatsapp kami, dapatkan info dan promo yang hanya ada disana!',
        'status' => 'Promo Masih Ada (aktif)',
        'gambar' => Yii::$app->getRequest()->getBaseUrl() . '/img/part-1-2019/promo/promo_6.jpg',
        'link' => 'http://bit.ly/joingrupmasukptn ',
    ],
    [//7
        'judul' => 'Ajukan kotamu jadi lokasi kami selanjutnya!',
        'status' => 'Promo Masih Ada (aktif)',
        'gambar' => Yii::$app->getRequest()->getBaseUrl() . '/img/part-1-2019/promo/promo_7.jpg',
        'link' => 'https://masukptn.id/peserta/ajukan-kota ',
    ],
    [//8
        'judul' => 'Selesaikan pendaftaranmu di tanggal 24-27 Okt 2018, menangkan pulsa 25.000!',
        'status' => 'Promo Tidak Ada (tidak aktif)',
        'gambar' => '',
        'link' => '',
    ],
    [//9
        'judul' => 'Tebak gambar, menangkan 5 tiket gratis!!',
        'status' => 'Promo Tidak Ada (tidak aktif)',
        'gambar' => '',
        'link' => '',
    ],

];
$index = $promo - 1;
?>
<?php if (!Yii::$app->request->isAjax) : ?>
<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-40 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

    <div class="text-center">
        <h2 class="text-azure"><?= $content[$index]['judul'] ?></h2>
        <!-- <h4 class="text-orange">Status: <?= $content[$index]['status'] ?></h4> -->
        
        <?php if ($content[$index]['gambar']) : ?>
            <div>
                <img src="<?= $content[$index]['gambar'] ?>" width="100%;" class="">
            </div>
        <?php endif; ?>

        <h6 class="text-orange">Link: <a href="<?= $content[$index]['link'] ?>"><?= $content[$index]['link'] ?></a></h6>
        <?php if ($promo == 1) : ?>
            <p>
                Spesial untuk kamu para leader di sekolah seluruh Indonesia!<br>
                Kami menghargai pengabdianmu sebagai Ketua OSIS dan mantan Ketua OSIS<br>
                dengan memberikan minimal 5 tiket gratis untukmu!!<br>
                Tiket gratis dapat kamu bagikan ke teman2 OSIS mu atau teman2mu lainnya.
            </p>

            <p>
                Yang bukan ketua OSIS,<br>
                Kasih tau ketua OSIS mu,<br>
                Minta dia mendaftarkan diri dan minta 1 tiket gratis dari dia ^^
            </p>

            <p>
                Yuk simak caranya PASTI dapat minimal 5 tiket gratis:<br>
            </p>
            <ul class="list-unstyled text-left">
                <li>Caranya:</li>
                <li>1. Chat admin LINE TIKET kami di @tiketmasukptnid (pakai @ dan id)</li>
                <li>2. Ketik chat “SayaOSIS”</li>
                <li>3. Ikuti petunjuk dari admin line tiket kami untuk proses verifikasi</li>
                <li>4. Jika data mu sudah di verifikasi, admin line akan memberikanmu arahan syarat-syarat untuk  mendapatkan minimal 5 tiket gratisnya</li>
                <li>5. Ikuti arahan selanjutnya</li>
            </ul>

            <p>
                *Minimal tiket gratis yang diberikan adalah 5, dan maksimal 10 per orang <br>
                *Tiket gratis yang diperoleh tergantung dari keaktifan ketua OSIS setelah diberikan arahan oleh admin di poin 5 <br>
                *Tiket gratis akan diberikan kepada ketua OSIS MAKSIMAL 2 minggu sebelum acara dilaksanakan <br>
            </p>

            <p>
                Ada pertanyaan?<br>
                Chat LINE kami <a href="https://line.me/R/ti/p/%40masukptnid">@masukptnid</a> (pakai @ dan id)<br>
                <a href="https://line.me/R/ti/p/%40masukptnid">https://line.me/R/ti/p/%40masukptnid</a>
            </p>

            <p>
                Dan LINE <a href="https://line.me/R/ti/p/%40tiketmasukptnid">@tiketmasukptnid</a> (pakai @ dan id)<br>
                Untuk pertanyaan pembayaran/tiket/pindah lokasi<br>
                <a href="https://line.me/R/ti/p/%40tiketmasukptnid">https://line.me/R/ti/p/%40tiketmasukptnid</a>
            </p>

            <p>
                Salam Sukses,<br>
                Salam Tulus,<br>
                Alumni OSIS Indonesia <br>
                AYO Berlayar Ke Kampus Impian!<br>
                <a href="https://www.instagram.com/masukptnid">www.instagram.com/masukptnid</a>
            </p>
        <?php elseif ($promo == 2) : ?>
            <p>
                Spesial untuk kamu yang berprestasi!<br>
                Kami menghargai pencapaian prestasimu dengan memberikan tiket gratis  untukmu!!
            </p>
                
            <ul class="list-unstyled text-left">
                <li>Caranya:</li>
                <li>1. Chat admin LINE TIKET kami di @tiketmasukptnid (pakai @ dan id)</li>
                <li>2. Ketik chat “SayOSN”</li>
                <li>3. Ikuti petunjuk dari admin line tiket kami untuk proses verifikasi</li>
                <li>4. Jika data mu sudah di verifikasi, admin line akan meminta data dirimu untuk memberikan tiket gratis kepadamu dan syarat untuk mendapatkannya</li>
            </ul>

            <p>
                *Berlaku untuk juara olimpiade tingkat kabupaten, kota, provinsi, nasional<br>
                *Tiket gratis akan diberikan paling lambat 2 minggu sebelum acara dilaksanakan<br>
                *Tiket dapat dipindahtangankan jika kamu sudah mendaftar sebelumnya
            </p>

            <p>
                Ada pertanyaan?<br>
                Chat LINE kami <a href="https://line.me/R/ti/p/%40masukptnid">@masukptnid</a> (pakai @ dan id)<br>
                <a href="https://line.me/R/ti/p/%40masukptnid">https://line.me/R/ti/p/%40masukptnid</a>
            </p>

            <p>
                Dan LINE <a href="https://line.me/R/ti/p/%40tiketmasukptnid">@tiketmasukptnid</a> (pakai @ dan id)<br>
                Untuk pertanyaan pembayaran/tiket/pindah lokasi<br>
                <a href="https://line.me/R/ti/p/%40tiketmasukptnid">https://line.me/R/ti/p/%40tiketmasukptnid</a>
            </p>

            <p>
                Salam Sukses,<br>
                Salam Tulus,<br>
                Alumni OSIS Indonesia <br>
                AYO Berlayar Ke Kampus Impian!<br>
                <a href="https://www.instagram.com/masukptnid">www.instagram.com/masukptnid</a>
            </p>
        <?php elseif ($promo == 3) : ?>
            <p>
                Hai para pejuang SBMPTN 2019!!<br>
                Mau acara ini ada di sekolah kamu?<br>
                BISA!
            </p>

            <p>
                Mau dapet harga super spesial untuk SEMUA teman-teman seangkatanmu?<br>
                BISA!
            </p>

            <ul class="list-unstyled text-left">
                <li>Caranya:</li>
                <li>1. Klik link http://bit.ly/openschool2019</li>
                <li>2. Isi formulir dengan sebenar-benarnya</li>
                <li>3. Tunggu dihubungi oleh tim kami jika kamu dan sekolah mu terpilih untuk koordinasi selanjutnya</li>
            </ul>

            <p>
                *Tidak semua pendaftar akan dihubungi oleh tim kami, karena pendaftar sangat banyak<br>
                *Sangat mungkin kamu baru dihubungi tim kami H-7 acara jika panitia merasa sangat memungkinkan untuk mengadakannya disana<br>
                *Jika kamu atau temanmu sudah mendaftar di kota / tempat ujian lain, uang pendaftaran yang sudah dibayarkan dapat kami refund dan pindah lokasi ujian ke sekolahmu
            </p>

            <p>
                Ada pertanyaan?<br>
                Chat LINE kami <a href="https://line.me/R/ti/p/%40masukptnid">@masukptnid</a> (pakai @ dan id)<br>
                <a href="https://line.me/R/ti/p/%40masukptnid">https://line.me/R/ti/p/%40masukptnid</a>
            </p>

            <p>
                Dan LINE <a href="https://line.me/R/ti/p/%40tiketmasukptnid">@tiketmasukptnid</a> (pakai @ dan id)<br>
                Untuk pertanyaan pembayaran/tiket/pindah lokasi<br>
                <a href="https://line.me/R/ti/p/%40tiketmasukptnid">https://line.me/R/ti/p/%40tiketmasukptnid</a>
            </p>

            <p>
                Salam Sukses,<br>
                Salam Tulus,<br>
                Alumni OSIS Indonesia <br>
                AYO Berlayar Ke Kampus Impian!<br>
                <a href="https://www.instagram.com/masukptnid">www.instagram.com/masukptnid</a>
            </p>
        <?php elseif ($promo == 4) : ?>
            <p>
                Kamu sudah kuliah?<br>
                Kamu sudah kerja?<br>
                Kamu adik kelas X dan XI?<br>
                Kamu orang yang ingin punya banyak pengalaman organisasi?
            </p>

            <p>
                Yuk gabung jadi Panitia Volunteer acara kami!<br>
                Kita bantu teman-teman kita yang akan menghadapi SBMPTN 2019 besok!<br>
                Kita fasilitasi mereka sebaik mungkin!
            </p>

            <ul class="list-unstyled text-left">
                <li>Caranya:</li>
                <li>1. Klik link http://bit.ly/rekruitasimasukptn2019</li>
                <li>2. Isi formulir dengan sebenar-benarnya</li>
                <li>3. Tunggu dihubungi oleh tim kami jika kamu terpilih untuk koordinasi waktu, jam, dan tugas yg harus kamu lakukan</li>
            </ul>

            <p>
                *Tidak semua mahasiswa akan dihubungi oleh tim kami, karena pendaftar sangat banyak, dan jumlah posisi sangat terbatas untuk tiap-tiap lokasi ujian<br>
                *Sangat mungkin kamu baru dihubungi tim kami H-1 acara jika orang sebelumnya mengundurkan diri secara mendadak
            </p>

            <p>
                Ada pertanyaan?<br>
                Chat LINE kami <a href="https://line.me/R/ti/p/%40masukptnid">@masukptnid</a> (pakai @ dan id)<br>
                <a href="https://line.me/R/ti/p/%40masukptnid">https://line.me/R/ti/p/%40masukptnid</a>
            </p>

            <p>
                Dan LINE <a href="https://line.me/R/ti/p/%40tiketmasukptnid">@tiketmasukptnid</a> (pakai @ dan id)<br>
                Untuk pertanyaan pembayaran/tiket/pindah lokasi<br>
                <a href="https://line.me/R/ti/p/%40tiketmasukptnid">https://line.me/R/ti/p/%40tiketmasukptnid</a>
            </p>

            <p>
                Salam Sukses,<br>
                Salam Tulus,<br>
                Alumni OSIS Indonesia <br>
                AYO Berlayar Ke Kampus Impian!<br>
                <a href="https://www.instagram.com/masukptnid">www.instagram.com/masukptnid</a>
            </p>
        <?php elseif ($promo == 5) : ?>
            <p>
                Spesial untuk kamu yang sudah duduk di bangku Perguruan Tinggi Negeri. <br>
                Yuk berbagi cerita dan tips n trick ke adik-adik kelas!
            </p>

            <p>
                Kami akan menyediakan waktu dan tempat pada hari H acara untuk kamu melakukan sharing pengalaman kamu untuk masuk ke Perguruan Tinggi Negeri kamu sekarang!<br>
                Menambah pengalaman,<br>
                Menambah kenalan adik-adik kelas,<br>
                Menambah sertifikat keaktifan mu untuk kegiatan luar kampus,<br>
                Sharing is caring! :)
            </p>

            <ul class="list-unstyled text-left">
                <li>Caranya:</li>
                <li>1. Klik link http://bit.ly/openmahasiswadaerah2019</li>
                <li>2. Isi formulir dengan sebenar-benarnya</li>
                <li>3. Tunggu dihubungi oleh tim kami jika kamu terpilih untuk koordinasi waktu, jam, dan peraturannya</li>
            </ul>

            <ul class="list-unstyled text-left">
                <li>*Tidak semua mahasiswa akan dihubungi oleh tim kami, karena pendaftar sangat banyak, dan jumlah posisi sangat terbatas untuk tiap-tiap lokasi ujian</li>
                <li>*Sangat mungkin kamu baru dihubungi tim kami H-1 acara jika orang sebelumnya mengundurkan diri secara mendadak</li>
                <li>*Boleh mengatasnamakan pribadi</li>
                <li>*Boleh mengatasnamakan universitas</li>
                <li>*Boleh mengatasnamakan BEM/organisasi kemahasiswaan lainnya</li>
            </ul>

            <p>
                Ada pertanyaan?<br>
                Chat LINE kami <a href="https://line.me/R/ti/p/%40masukptnid">@masukptnid</a> (pakai @ dan id)<br>
                <a href="https://line.me/R/ti/p/%40masukptnid">https://line.me/R/ti/p/%40masukptnid</a>
            </p>

            <p>
                Dan LINE <a href="https://line.me/R/ti/p/%40tiketmasukptnid">@tiketmasukptnid</a> (pakai @ dan id)<br>
                Untuk pertanyaan pembayaran/tiket/pindah lokasi<br>
                <a href="https://line.me/R/ti/p/%40tiketmasukptnid">https://line.me/R/ti/p/%40tiketmasukptnid</a>
            </p>

            <p>
                Salam Sukses,<br>
                Salam Tulus,<br>
                Alumni OSIS Indonesia <br>
                AYO Berlayar Ke Kampus Impian!<br>
                <a href="https://www.instagram.com/masukptnid">www.instagram.com/masukptnid</a>
            </p>
        <?php elseif ($promo == 6) : ?>
            <p>
                Bosan dengan grup yang sepi dan itu-itu aja orangnya?<br>
                Pengen gabung di grup yang ramai dan kenal dengan teman baru?<br>
                Atau pengen gabung di grup dan bisa kenalan lebih dekat dengan admin kami?
            </p>

            <ul class="list-unstyled text-left">
                <li>Yuk join grup Whatsapp MasukPTNid!</li>
                <li>Caranya:</li>
                <li>1. Klik website http://bit.ly/joingrupmasukptn</li>
                <li>2. Isi formulir nya dengan data kamu sebenar-benarnya</li>
                <li>3. Tunggu dihubungi oleh tim kami untuk dimasukan ke grup</li>
            </ul>

            <ul class="list-unstyled text-left">
                <li>*Jika terdapat data palsu dalam formulir, tidak akan kami proses</li>
                <li>*Jika telah dihubungi oleh tim kami, ikuti petunjuk selanjutnya oleh tim kami untuk proses verifikasi dan peraturan grup</li>
                <li>*Harap bersabar menunggu dihubungi oleh tim kami, karena yang mendaftar sangatlah banyak</li>
            </ul>

            <p>
                Ada pertanyaan?<br>
                Chat LINE kami <a href="https://line.me/R/ti/p/%40masukptnid">@masukptnid</a> (pakai @ dan id)<br>
                <a href="https://line.me/R/ti/p/%40masukptnid">https://line.me/R/ti/p/%40masukptnid</a>
            </p>

            <p>
                Dan LINE <a href="https://line.me/R/ti/p/%40tiketmasukptnid">@tiketmasukptnid</a> (pakai @ dan id)<br>
                Untuk pertanyaan pembayaran/tiket/pindah lokasi<br>
                <a href="https://line.me/R/ti/p/%40tiketmasukptnid">https://line.me/R/ti/p/%40tiketmasukptnid</a>
            </p>

            <p>
                Salam Sukses,<br>
                Salam Tulus,<br>
                Alumni OSIS Indonesia <br>
                AYO Berlayar Ke Kampus Impian!<br>
                <a href="https://www.instagram.com/masukptnid">www.instagram.com/masukptnid</a>
            </p>
        <?php elseif ($promo == 7) : ?>
            <p>
                KAMI ADA DI KOTAMU!!<br>
                Bantu kami menentukan kota mana saja yang harus kami selenggarakan acara Terbaik, Terbesar, Terkeren, Terpercaya, dan Bergengsi ini!
            </p>

            <ul class="list-unstyled text-left">
                <li>Caranya:</li>
                <li>1. Klik website www.masukptn.id </li>
                <li>2. Klik tombol “AJUKAN KOTAMU”</li>
                <li>3. Isi data diri kamu</li>
                <li>4. Pilih provinsi kamu</li>
                <li>5. Pilih kota/kabupaten kamu</li>
                <li>6. Kasih tau teman-teman, dan ajak teman kamu sebanyak-banyak nya untuk memilih provinsi dan kota/kabupaten yang sama denganmu!</li>
            </ul>

            <p>
                Semakin banyak kota/kabupaten yang dipilih.<br>
                Semakin yakin kami panitia untuk menyelenggarakannya disana!
            </p>

            <ul class="list-unstyled text-left">
                <li>*Data diri palsu tidak akan di hitung</li>
                <li>*Kota yang terpilih akan diumumkan di Instagram masukptnid</li>
            </ul>

            <p>
                Ada pertanyaan?<br>
                Chat LINE kami <a href="https://line.me/R/ti/p/%40masukptnid">@masukptnid</a> (pakai @ dan id)<br>
                <a href="https://line.me/R/ti/p/%40masukptnid">https://line.me/R/ti/p/%40masukptnid</a>
            </p>

            <p>
                Dan LINE <a href="https://line.me/R/ti/p/%40tiketmasukptnid">@tiketmasukptnid</a> (pakai @ dan id)<br>
                Untuk pertanyaan pembayaran/tiket/pindah lokasi<br>
                <a href="https://line.me/R/ti/p/%40tiketmasukptnid">https://line.me/R/ti/p/%40tiketmasukptnid</a>
            </p>

            <p>
                Salam Sukses,<br>
                Salam Tulus,<br>
                Alumni OSIS Indonesia <br>
                AYO Berlayar Ke Kampus Impian!<br>
                <a href="https://www.instagram.com/masukptnid">www.instagram.com/masukptnid</a>
            </p>
        <?php elseif ($promo == 8) : ?>
            
        <?php elseif ($promo == 9) : ?>

        <?php endif; ?>
    </div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>

</div>
<?php endif; ?>
