<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>
<?php if (!Yii::$app->request->isAjax) : ?>
<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-40 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

    <?php if ($d == 'magent') : ?>
        <div class="text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/orang.png" style="width: 50px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            1. Daftarkan dirimu sebagai M-Agent MasukPTNid <br><br>
            2. Kamu akan mendapatkan uang tunai tiap 1 peserta yang kamu daftarkan <br><br>
            3. Dan juga tiket gratis jika 5 orang peserta sudah kamu daftarkan <br><br>
            4. Setelah mengisi data diri, kamu akan dihubungi oleh tim kami untuk verifikasi dan perkenalan sistem <br><br>
            <a href="https://bit.ly/magentbatch2" class="button button-md button-block border-azure text-azure margin-top-5" target="_blank">Daftar M-Agent Sekarang</a>
        </div>
    <?php elseif ($d == 'menginap' && $model['peserta']->status_bayar == 'Sudah Bayar' && $model['peserta']->periode->status == 'Aktif') : ?>
        <div class="text-center">
            <img src="https://d1zqphq0azxsrm.cloudfront.net/images/logo-RD-1@2x.png" style="width: 150px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            1. Gunakan kode ini untuk mendapatkan potongan harga 25% pada aplikasi Reddoorz <br><br>
            2. Download aplikasi Reddoorz disini: <br><br>
            <a href="https://play.google.com/store/apps/details?id=com.reddoorz.app" class="button button-md button-block border-azure text-azure margin-top-5">Download</a>
        </div>
    <?php elseif ($d == 'menginap') : ?>
        <div class="text-center">
            <img src="https://d1zqphq0azxsrm.cloudfront.net/images/logo-RD-1@2x.png" style="width: 150px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            Akan tersedia 1x24 jam setelah melakukan konfirmasi pembayaran
        </div>
    <?php elseif ($d == 'jimshoney' && $model['peserta']->status_bayar == 'Sudah Bayar' && $model['peserta']->periode->status == 'Aktif') : ?>
        <div class="text-center">
            <img src="https://www.jimshoneyid.com/wp-content/uploads/2019/03/logo.png" style="width: 180px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            1. Gunakan kode ini untuk mendapatkan potongan harga IDR 50.000,- <br><br>
            1. Minimal pembelian 2 item harga normal, only at <a href="www.jimshoneyofficial.com">www.jimshoneyofficial.com</a> <br><br>
        </div>
    <?php elseif ($d == 'jimshoney') : ?>
        <div class="text-center">
            <img src="https://www.jimshoneyid.com/wp-content/uploads/2019/03/logo.png" style="width: 180px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            Akan tersedia 1x24 jam setelah melakukan konfirmasi pembayaran
        </div>
    <?php elseif ($d == 'ptngo' && $model['peserta']->status_bayar == 'Sudah Bayar' && $model['peserta']->periode->status == 'Aktif') : ?>
        <div class="text-center">
            <img src="https://lh3.googleusercontent.com/NeMMUercMuZ6UqzVQI4wuX-N83Zk179DF57Nqxqj2CPJN51D7R6D0RtpWGmwvC_sjA=s360" style="width: 90px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            1. Gunakan kode ini untuk mendapatkan status premium member aplikasi belajar SBMPTN PTN GO <br> <br>
            2. Download aplikasi PTN GO disini: <br><br>
            <a href="https://play.google.com/store/apps/details?id=sbmptn.scola.id" class="button button-md button-block border-azure text-azure margin-top-5">Download</a>
        </div>
    <?php elseif ($d == 'ptngo') : ?>
        <div class="text-center">
            <img src="https://lh3.googleusercontent.com/NeMMUercMuZ6UqzVQI4wuX-N83Zk179DF57Nqxqj2CPJN51D7R6D0RtpWGmwvC_sjA=s360" style="width: 90px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            Akan tersedia 1x24 jam setelah melakukan konfirmasi pembayaran
        </div>
    <?php elseif ($d == 'pantau-nilai') : ?>
        <div class="text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/orang.png" style="width: 50px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            1. Download via www.bit.ly/pantaunilaiUTBK <br><br>
            2. Daftar dan submit nilai UTBK kamu di sana, dan pantau terus hasilnya. <br><br>
        </div>
    <?php elseif ($d == 'certificate') : ?>
        <div class="text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/orang.png" style="width: 50px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            1. E-Sertifikat keikutsertaan peserta dalam Tryout Nasional SBMPTN MasukPTNid Part.2 di 84 kota se-Indonesia <br><br>
            2. Tombol Sertifikat pada tabel Daftar Tiket Yang Dibeli akan hidup setelah pengumuman hasil +- 10 hari dari hari acara <br><br>
        </div>
    <?php elseif ($d == 'beasiswa-online') : ?>
        <div class="text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/orang.png" style="width: 50px; height: auto;">
        </div>
        <div class="margin-top-10"></div>
        <div class="fs-16" style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            Tombol Kompetisi Beasiswa Online pada tabel <span class="text-azure">Daftar Tiket yang Dibeli</span> akan hidup pada tanggal-tanggal berikut: <br><br>
        </div>
        <div style="max-width: 300px; width: 100%; margin-left: auto; margin-right: auto;">
            <div class="box box-break-sm margin-bottom-20">
                <div class="fw-bold">
                    Gelombang Pertama
                </div>
                <div class="margin-top-5"></div>
                <div>
                    <div>Ujian sudah selesai pada Senin, 25 Februari 2019</div>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-20">
                <div class="fw-bold">
                    Gelombang Kedua
                </div>
                <div class="margin-top-5"></div>
                <div>
                    <div>Ujian sudah selesai pada Senin, 12 Maret 2019</div>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-20">
                <div class="fw-bold">
                    Gelombang Ketiga
                </div>
                <div class="margin-top-5"></div>
                <div>
                    <div>Ujian sudah selesai pada Senin, 25 Maret 2019</div>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-20">
                <div class="fw-bold">
                    Gelombang Keempat
                </div>
                <div class="margin-top-5"></div>
                <div>
                    <div>Ujian sudah selesai pada Senin, 8 April 2019</div>
                </div>
            </div>
            <div class="box box-break-sm margin-bottom-20">
                <div class="fw-bold">
                    Gelombang Kelima
                </div>
                <div class="margin-top-5"></div>
                <div>
                    <div>Sudah dibuka</div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="margin-top-30"></div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>

</div>
<?php endif; ?>
