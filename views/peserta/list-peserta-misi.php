<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/peserta/list-peserta-misi.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-gray bg-lightest rounded-sm" style="max-width: 1200px; width: 100%; margin-left: auto; margin-right: auto;">
    
    <h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

    <div class="fs-14 m-fs-13 text-gray text-center">
        <span class="">
            Ikuti misi !
        </span>
        <hr class="border border-top margin-y-15">
    </div>

    <div class="margin-top-30"></div>

    <table class="datatables-peserta-misi table table-nowrap">
        <thead>
            <tr class="text-dark">
                <th></th>
                <th>Judul</th>
                <th>Status</th>
                <th>Dari Tanggal</th>
                <th>Sampai Tanggal</th>
            </tr>
            <tr class="dt-search">
                <th></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search judul..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search status..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search dari tanggal..."/></th>
                <th><input type="text" class="form-text border-none padding-0" placeholder="search sampai tanggal..."/></th>
            </tr>
        </thead>
    </table>


    </div>
</div>

<div class="margin-top-50"></div>

</div>