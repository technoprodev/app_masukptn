<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/peserta/form-pendaftaran5.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
// technosmart\assets_manager\FileInputAsset::register($this);
// technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$pesertas = [];
if (isset($model['peserta']))
    foreach ($model['peserta'] as $key => $peserta) {
        if (!$peserta->id_periode_jenis) {
            $peserta->id_periode_jenis = '';
        }
        if (!$peserta->id_periode_kota) {
            $peserta->id_periode_kota = '';
        }
        $pesertas[] = $peserta->attributes;
    }

$periodeJenises = [];
if (isset($model['periode_jenis']))
    foreach ($model['periode_jenis'] as $key => $periodeJenis)
        $periodeJenises[] = $periodeJenis->attributes;

$this->registerJs(
    // 'vm.addPeserta();' .
    'vm.$data.transaksi.pesertas = vm.$data.transaksi.pesertas.concat(' . json_encode($pesertas) . ');' .
    'vm.$data.periodeJenises = vm.$data.periodeJenises.concat(' . json_encode($periodeJenises) . ');' .
    '',
    3
);
$disabledPeriodeKota = ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->where(['id_periode' => $idPeriode, 'status' => 'Tidak Aktif'])->orderBy('nama')->asArray()->all(), 'id', 'nama');
foreach ($disabledPeriodeKota as $key => $value) {
    $disabledPeriodeKota[$key] = ['disabled' => true];
}

//
$errorMessage = '';
$errorVue = false;
if ($model['transaksi']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['transaksi'], ['class' => '']);
}

if (isset($model['peserta'])) foreach ($model['peserta'] as $key => $peserta) {
    if ($peserta->hasErrors()) {
        $errorMessage .= Html::errorSummary($peserta, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  box-shadow: 0 0 10px rgba(51, 118, 184, 0.3);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<div class="container padding-y-30">
    <div class="padding-30 shadow border bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

    <!-- <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Lakukan pembelian pada formulir dibawah ini. <br><br> <a modal-md="" modal-title="Prosedur Pendaftaran" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-prosedur']) ?>" class="button button-sm border-azure bg-azure hover-bg-lightest hover-text-azure">Prosedur Pendaftaran</a></span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div> -->

    <div class="fs-14 m-fs-13 text-gray text-center">
        <span class="">
            Pilih jenis tryout (saintek/soshum) dan pilih kota tempat kamu tryout, persediaan sangat terbatas. <br><b>Segera lakukan pembayaran setelah kota berhasil dipilih!</b><br>
            Jika ada yang kurang jelas, silahkan cek <a modal-md="" modal-title="Prosedur Pendaftaran" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-prosedur']) ?>" class="">Prosedur Pendaftaran</a>.
        </span>
        <hr class="border border-top margin-y-15">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
      
        <?php if ($errorMessage) : ?>
            <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <?php if (isset($model['peserta'])) foreach ($model['peserta'] as $key => $value): ?>
            <?php
                $this->registerJs(
                    "vm.changeHarga($key);",
                    3
                );
            ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]nama", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]nama", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]email", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]email", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]handphone", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]handphone", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]id_periode_jenis", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]id_periode_jenis", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= $form->field($model['peserta'][$key], "[$key]id_periode_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= $form->field($model['peserta'][$key], "[$key]id_periode_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]sekolah", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]sekolah", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]id_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]id_kota", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]alamat", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]alamat", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]facebook", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]facebook", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]twitter", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]twitter", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]instagram", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]instagram", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]line", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]line", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>

            <?= false && $form->field($model['peserta'][$key], "[$key]whatsapp", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
            <?= false && $form->field($model['peserta'][$key], "[$key]whatsapp", ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->end(); ?>
        <?php endforeach; ?>

        <template v-if="typeof transaksi.pesertas == 'object'">
            <template v-for="(value, key, index) in transaksi.pesertas">
                <div v-show="!(value.id < 0)">
                    <hr class="border-light-azure border-top margin-top-50">

                    <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-azure text-center" v-if="key!=0">
                      <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
                      Tiket Ke-{{key+1}}
                      <hr class="border-lighter border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 50px;">
                    </div>
                    <div class="margin-top-15"></div>

                    <input type="hidden" v-bind:id="'peserta-' + key + '-id'" v-bind:name="'Peserta[' + key + '][id]'" type="text" v-model="transaksi.pesertas[key].id">

                    <div class="box box-break-sm box-gutter box-equal">
                        <?php if (false) : ?>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-nama'">
                                <label v-bind:for="'peserta-' + key + '-nama'" class="form-label fw-bold text-uppercase text-gray">Nama Lengkap</label>
                                <input v-bind:id="'peserta-' + key + '-nama'" v-bind:name="'Peserta[' + key + '][nama]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].nama">
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-email'">
                                <label v-bind:for="'peserta-' + key + '-email'" class="form-label fw-bold text-uppercase text-gray">Email</label>
                                <input v-bind:id="'peserta-' + key + '-email'" v-bind:name="'Peserta[' + key + '][email]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].email">
                                <div class="form-info"></div>
                                <div class="margin-y-5">gunakan email yang bersangkutan dan harus benar dan aktif, karena UTBK nanti menggunakan email yang bersangkutan.</div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-id_periode_jenis'">
                                <label v-bind:for="'peserta-' + key + '-id_periode_jenis'" class="form-label fw-bold text-uppercase text-gray">Jenis Tryout</label>
                                <select v-bind:id="'peserta-' + key + '-id_periode_jenis'" v-bind:name="'Peserta[' + key + '][id_periode_jenis]'" class="form-dropdown rounded-xs" v-model="transaksi.pesertas[key].id_periode_jenis" v-on:change="changeHarga(key);">
                                    <option value="">Pilih jenis tryout</option>
                                    <?php foreach (ArrayHelper::map(\app_tryout\models\PeriodeJenis::find()->where(['id_periode' => $idPeriode, 'status' => 'Sedang Aktif'])->indexBy('id')->asArray()->all(), 'id', 'nama') as $id => $nama) : ?>
                                        <option value="<?= $id ?>"><?= $nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-id_periode_kota'">
                                <label v-bind:for="'peserta-' + key + '-id_periode_kota'" class="form-label fw-bold text-uppercase text-gray">Lokasi Tryout</label>
                                <select v-bind:id="'peserta-' + key + '-id_periode_kota'" v-bind:name="'Peserta[' + key + '][id_periode_kota]'" class="form-dropdown rounded-xs" v-model="transaksi.pesertas[key].id_periode_kota">
                                    <option value="">Pilih lokasi</option>
                                    <?php  ?>
                                    <?php foreach (str_replace('<br>', ' - ', ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->where(['id_periode' => $idPeriode])->orderBy('nama')->asArray()->all(), 'id', 'nama')) as $id => $nama) : ?>
                                        <option value="<?= $id ?>" <?= isset($disabledPeriodeKota[$id]) ? 'disabled' : '' ?>><?= $nama ?><?= isset($disabledPeriodeKota[$id]) ? ' (kuota telah habis)' : '' ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <?php if (false) : ?>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-handphone'">
                                <label v-bind:for="'peserta-' + key + '-handphone'" class="form-label fw-bold text-uppercase text-gray">Handphone</label>
                                <!-- <div class="form-icon">
                                    <span class="icon-prepend padding-right-0" style="line-height: 18px">0 </span>
                                </div> -->
                                <input v-bind:id="'peserta-' + key + '-handphone'" v-bind:name="'Peserta[' + key + '][handphone]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].handphone" placeholder="08xxxxx">
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-jenis_kelamin'">
                                <label v-bind:for="'peserta-' + key + '-jenis_kelamin'" class="form-label fw-bold text-uppercase text-gray">Jenis Kelamin</label>
                                <select v-bind:id="'peserta-' + key + '-jenis_kelamin'" v-bind:name="'Peserta[' + key + '][jenis_kelamin]'" class="form-dropdown rounded-xs" v-model="transaksi.pesertas[key].jenis_kelamin">
                                    <option value="">Pilih jenis kelamin</option>
                                    <?php foreach (str_replace('<br>', ' - ', (new \app_tryout\models\Peserta)->getEnum('jenis_kelamin')) as $id => $nama) : ?>
                                        <option value="<?= $id ?>"><?= $nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-sekolah'">
                                <label v-bind:for="'peserta-' + key + '-sekolah'" class="form-label fw-bold text-uppercase text-gray">Sekolah</label>
                                <textarea v-bind:id="'peserta-' + key + '-sekolah'" v-bind:name="'Peserta[' + key + '][sekolah]'" class="form-textarea rounded-xs" v-model="transaksi.pesertas[key].sekolah"></textarea>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-alamat'">
                                <label v-bind:for="'peserta-' + key + '-alamat'" class="form-label fw-bold text-uppercase text-gray">Alamat</label>
                                <textarea v-bind:id="'peserta-' + key + '-alamat'" v-bind:name="'Peserta[' + key + '][alamat]'" class="form-textarea rounded-xs" v-model="transaksi.pesertas[key].alamat"></textarea>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <label class="form-label fw-bold text-uppercase text-gray">Social Media / Instant Messenger <span class="margin-y-5 fw-normal fs-italic">- Isi Minimal 2 dari 5.</span></label>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-facebook'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-facebook"></i>
                                    <input v-bind:id="'peserta-' + key + '-facebook'" v-bind:name="'Peserta[' + key + '][facebook]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].facebook" placeholder="facebook">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-twitter'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-twitter"></i>
                                    <input v-bind:id="'peserta-' + key + '-twitter'" v-bind:name="'Peserta[' + key + '][twitter]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].twitter" placeholder="twitter">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-instagram'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-instagram"></i>
                                    <input v-bind:id="'peserta-' + key + '-instagram'" v-bind:name="'Peserta[' + key + '][instagram]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].instagram" placeholder="instagram">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-line'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-commenting-o"></i>
                                    <input v-bind:id="'peserta-' + key + '-line'" v-bind:name="'Peserta[' + key + '][line]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].line" placeholder="line">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <div class="box-12">
                            <div v-bind:class="'form-wrapper field-peserta-' + key + '-whatsapp'">
                                <div class="form-icon">
                                    <i class="icon-prepend fa fa-whatsapp"></i>
                                    <input v-bind:id="'peserta-' + key + '-whatsapp'" v-bind:name="'Peserta[' + key + '][whatsapp]'" class="form-text rounded-xs" type="text" v-model="transaksi.pesertas[key].whatsapp" placeholder="whatsapp">
                                </div>
                                <div class="form-info"></div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>

                    <div class="margin-top-15"></div>
                    
                    <div class="text-center" v-if="key!=0">
                        <a v-on:click="removePeserta(key)" class="button button-sm border-light-red bg-light-red">Hapus Tiket Ke-{{key+1}}</a>
                    </div>
                </div>
            </template>
        </template>

        <?php if (false) : ?>
        <hr class="border-light-azure border-top margin-top-50">

        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-12">
                <?= $form->field($model['transaksi'], 'id_sumber', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['transaksi'], 'id_sumber', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Dari Sumber Mana Kamu Mengetahui Tryout Ini ?']); ?>
                    <?= Html::activeDropDownList($model['transaksi'], 'id_sumber', ArrayHelper::map(\app_tryout\models\Sumber::find()->indexBy('id')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih sumber', 'class' => 'form-dropdown rounded-xs']); ?>
                    <?= Html::error($model['transaksi'], 'id_sumber', ['class' => 'form-info']); ?>
                <?= $form->field($model['transaksi'], 'id_sumber')->end(); ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if (false) : ?>
        <div>
            <h6>Ringkasan</h6>

            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 m-text-left text-gray">Total tiket :</div>
                <div class="box-9 m-padding-x-0 text-dark">{{totalTiket}}</div>
            </div>
            <template v-if="typeof transaksi.pesertas == 'object'">
                <template v-for="(value, key, index) in transaksi.pesertas">
                    <div v-show="!(value.id < 0)">
                        <div class="box box-break-sm margin-bottom-10">
                            <div class="box-3 padding-x-0 m-text-left text-gray">Harga tiket ke-{{key+1}} :</div>
                            <div class="box-9 m-padding-x-0 text-dark">
                                <b>Rp {{value.harga.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</b>
                                <span class="text-grayest margin-left-15">hemat Rp {{(value.harga_asli - value.harga).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</span>
                            </div>
                        </div>
                    </div>
                </template>
            </template>
            <div class="box box-break-sm margin-bottom-10">
                <div class="box-3 padding-x-0 m-text-left text-gray">Total tagihan :</div>
                <div class="box-9 m-padding-x-0 text-spring">
                    <b>Rp {{tagihan.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</b>
                    <span class="text-grayest margin-left-15">hemat <u>Rp {{(tagihanAsli - tagihan).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}}</u></span>
                </div>
            </div>

            <!-- <div class="border-azure bg-lightest padding-15 text-center" v-if="transaksi.pesertas.length <= 8">
                <?php $hargaTiket = (new \yii\db\Query())->select(['harga_2_tiket'])->from('periode_jenis pj')->where('pj.id_periode = ' . \app_tryout\models\Periode::getPeriodeAktif()->id)->andWhere('status = "Sedang Aktif"')->scalar(); ?>
                <b class="fs-14">* Dapatkan harga Rp <?= number_format($hargaTiket, 2) ?> dengan membeli 2 tiket atau lebih</b>
                <br>
                <b class="fs-14">Klik tombol dibawah ini</b>
                <div class="margin-top-10"></div>
                <a v-on:click="addPeserta" class="button border-orange bg-orange margin-left-15">Tambah Tiket</a>
            </div>

            <div class="border-light-azure bg-light-azure padding-15 text-center" v-else>
                <b class="fs-14">* Maksimum pembelian tiket tercapai !</b>
                <br>
                Kamu telah melakukan penghematan maksimum :)
            </div> -->
        </div>
        <?php endif; ?>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Submit Pembelian', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    <hr class="border-light-azure border-top margin-top-50">

    <div class="fs-13 m-fs-13 text-gray text-center bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10">
        Setelah melakukan pemilihan kota, kamu bisa melakukan pembayaran di website Tokopedia atau klik <a href="www.tkp.me/masukptnid">www.tkp.me/masukptnid</a></a> <br>dan lakukan konfirmasi pada tombol dibawah
    </div>

    <div class="margin-top-15"></div>

    <?= Html::a('Konfirmasi Pembayaran', ['konfirmasi'], ['class' => 'button button-md border-orange bg-orange button-block', 'target' => '_blank']) ?>

    </div>
</div>

<div class="margin-top-50"></div>

</div>