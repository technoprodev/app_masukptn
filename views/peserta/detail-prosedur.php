<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

?>
<?php if (!Yii::$app->request->isAjax) : ?>
<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>

<h1 class="text-center fs-40 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">
<?php endif; ?>

    <div class="text-center">
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/prosedur-pendaftaran-v1.png" style="width: 100%; height: auto;">
    </div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>

<div class="margin-top-50"></div>

</div>
<?php endif; ?>
