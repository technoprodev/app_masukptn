<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/dev/form.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);*/

// technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
// technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\FlatpickrAsset::register($this);
// technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
/*$devChildren = [];
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $devChild)
        $devChildren[] = $devChild->attributes;

$this->registerJs(
    'vm.$data.dev.virtual_category = ' . json_encode($model['transaksi']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = vm.$data.dev.devChildren.concat(' . json_encode($devChildren) . ');',
    // 'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');',
    3
);*/

//
$errorMessage = '';
$errorVue = false;
if ($model['transaksi']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['transaksi'], ['class' => '']);
}

/*if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $devChild) {
    if ($devChild->hasErrors()) {
        $errorMessage .= Html::errorSummary($devChild, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/
?>
<style type="text/css">
.form-text:focus,
.form-textarea:focus,
.form-dropdown:focus {
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
</style>

<div class="has-bg-img padding-y-5">

<div class="margin-top-100"></div>


<div class="container padding-y-30">
    <div class="padding-30 shadow border-azure bg-lightest rounded-sm" style="max-width: 600px; width: 100%; margin-left: auto; margin-right: auto;">

    <h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>

    <!-- <div class="fs-16 m-fs-13 margin-x-30 m-margin-x-15 text-gray text-center">
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
        <span class="bg-lightest rounded-md border-light-azure padding-x-20 padding-y-10 inline-block">Isi data-data dibawah untuk melaporkan pembayaranmu</span>
        <hr class="border-azure border-top margin-y-0 margin-x-15 inline-block text-middle hidden-sm-less" style="width: 20px;">
    </div> -->

    <div class="fs-14 m-fs-13 text-gray text-center">
        <span class="">
            Isi data-data dibawah untuk melaporkan pembayaranmu!<br>
            Jika ada yang kurang jelas, silahkan cek <a modal-md="" modal-title="Prosedur Pendaftaran" href="<?= Yii::$app->urlManager->createUrl(['peserta/detail-prosedur']) ?>" class="">Prosedur Pendaftaran</a>.
        </span>
        <hr class="border border-top margin-y-15">
    </div>

    <div class="margin-top-30"></div>

    <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>

        <div class="box box-break-sm box-gutter box-equal">
            <div class="box-12">
                <?= $form->field($model['transaksi'], 'id_periode_metode_pembayaran', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['transaksi'], 'id_periode_metode_pembayaran', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Metode Pembayaran']); ?>
                    <!-- Pilih metode pembayaran yang Kamu gunakan -->
                    <?= Html::activeDropDownList($model['transaksi'], 'id_periode_metode_pembayaran', ArrayHelper::map(\app_tryout\models\PeriodeMetodePembayaran::find()->where(['id_periode' => $idPeriode])->indexBy('id')->asArray()->all(), 'id', 'nama'), ['prompt' => 'Pilih metode pembayaran', 'class' => 'form-dropdown rounded-xs']); ?>
                    <?= Html::error($model['transaksi'], 'id_periode_metode_pembayaran', ['class' => 'form-info']); ?>
                <?= $form->field($model['transaksi'], 'id_periode_metode_pembayaran')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['transaksi'], 'tanggal_pembayaran', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['transaksi'], 'tanggal_pembayaran', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Tanggal Pembayaran']); ?>
                    <?= Html::activeTextInput($model['transaksi'], 'tanggal_pembayaran', ['class' => 'input-flatpickr form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['transaksi'], 'tanggal_pembayaran', ['class' => 'form-info']); ?>
                <?= $form->field($model['transaksi'], 'tanggal_pembayaran')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['transaksi'], 'pembayaran_atas_nama', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['transaksi'], 'pembayaran_atas_nama', ['class' => 'form-label fw-bold text-gray', 'label' =>'Kode Voucher (biasanya diawali dengan huruf TP)']); ?>
                    <!-- Atas Nama <span class="margin-y-5 fw-normal">- Nama pemilik rekening / penyetor yang Kamu gunakan untuk transfer pembayaran.</span> -->
                    <?= Html::activeTextInput($model['transaksi'], 'pembayaran_atas_nama', ['class' => 'form-text rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['transaksi'], 'pembayaran_atas_nama', ['class' => 'form-info']); ?>
                <?= $form->field($model['transaksi'], 'pembayaran_atas_nama')->end(); ?>
            </div>
            <div class="box-12">
                <?= $form->field($model['transaksi'], 'virtual_bukti_pembayaran_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['transaksi'], 'virtual_bukti_pembayaran_upload', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Upload Bukti Pembayaran']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Foto / screenshot bukti transfer bank. Ukuran maksimal <b>1 MB</b>.</span> -->
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <a href="#" class="input-group-addon btn btn-default square fileinput-exists" data-dismiss="fileinput"><i class="fa fa-close"></i></a>
                        <div class="form-text rounded-xs">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"><a href="<?= $model['transaksi']->virtual_bukti_pembayaran_download ?>"><?= $model['transaksi']->bukti_pembayaran ?></a></span>
                        </div>
                        <span class="input-group-addon btn btn-default square btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <?= Html::activeFileInput($model['transaksi'], 'virtual_bukti_pembayaran_upload'); ?>
                        </span>
                    </div>
                    <?= Html::error($model['transaksi'], 'virtual_bukti_pembayaran_upload', ['class' => 'form-info fs-15 fw-bold']); ?>
                <?= $form->field($model['transaksi'], 'virtual_bukti_pembayaran_upload')->end(); ?>
            </div>
            <!-- <div class="box-12"> --><div class="box-12">
                <?= $form->field($model['transaksi'], 'catatan_pembayaran', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <?= Html::activeLabel($model['transaksi'], 'catatan_pembayaran', ['class' => 'form-label fw-bold text-uppercase text-gray', 'label' =>'Informasi Tambahan']); ?>
                     <!-- <span class="margin-y-5 fw-normal">- Jika diperlukan, isi informasi tambahan agar admin dapat melakukan verifikasi dengan benar. -->
                    <?= Html::activeTextArea($model['transaksi'], 'catatan_pembayaran', ['class' => 'form-textarea rounded-xs', 'maxlength' => true]); ?>
                    <?= Html::error($model['transaksi'], 'catatan_pembayaran', ['class' => 'form-info']); ?>
                <?= $form->field($model['transaksi'], 'catatan_pembayaran')->end(); ?>
            </div>
        </div>

        <div class="margin-top-30"></div>
        
        <div class="form-wrapper clearfix">
            <?= Html::submitButton('Konfirmasi', ['class' => 'button button-lg button-block border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>

<div class="margin-top-50"></div>

</div>