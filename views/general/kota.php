<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>
<div class="ff-cabin">
    <div class="container padding-y-15">
        <?php $kotas = \yii\helpers\ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->where(['id_periode' => (\app_tryout\models\Periode::getPeriodeAktif())->id])->orderBy('nama')->indexBy('id')->asArray()->all(), 'id', 'nama'); ?>
        <div class="text-center padding-top-60 m-padding-top-80" id="lokasi-ujian">
            <div class="fs-40 m-fs-30 fw-bold text-orange">Kami Hadir di <?= count($kotas) ?> Kota :</div>
            <div class="fs-20 m-fs-16 fw-bold text-azure">(klik pada lokasi untuk mengetahui alamat lengkapnya)</div>
        </div>
        <div class="margin-top-30 m-margin-top-15"></div>
        <div class="box box-space-md box-gutter box-break-md-6 box-equal fs-18 m-fs-14 text-dark">
            <?php foreach ($kotas as $key => $value) : ?>
                <div class="box-3 padding-x-0">
                    <a href="<?= Yii::$app->urlManager->createUrl(["peserta/detail-kota", 'id' => $key]) ?>" modal-md="" modal-title="Detail Lokasi">
                        <img class="text-middle" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/pin.png" style="width: 10px; height: auto;">
                        <span class="text-middle fw-bold text-wrap stretch-y-sm"><?= str_replace(' - ', '<br>', $value) ?></span>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="margin-y-60 m-margin-y-30">
            <hr class="border-top border-light-azure">
        </div>
    </div>
</div>