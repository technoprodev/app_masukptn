<<?php
use yii\helpers\Url;
use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="ff-cabin">
    <div class="margin-top-20"></div>

    <div class="text-center has-bg-img">
        <div class="margin-x-15">
            <img alt="MasukPTNid logo" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-1-2020/logo-full-min.png" style="width:100%;max-width:400px;height:auto;">
        </div>

        <!-- <div class="margin-top-50"></div>

        <div class="text-center fs-24 text-orange">Pembahasan dapat diakses melalui menu "Pembahasan" dan menu "Login" peserta</div>
        <div class="text-center fs-24 text-azure fw-bold">Hasil kamu sudah dapat dilihat didalam login kamu dengan menekan tombol "Download Nilai"</div> -->

        <!-- <div class="margin-top-50"></div>
        
        <div>
            <div id="demo" class="text-center fs-40 text-azure"></div>
            <script>
                var countDownDate = new Date("<?= (new \yii\db\Query())->select(['waktu_berakhir'])->from('periode_jenis pj')->where('pj.id_periode = ' . \app_tryout\models\Periode::getPeriodeAktif()->id)->andWhere('status = "Sedang Aktif"')->scalar() ?>").getTime();
                var x = setInterval(function() {
                  var now = new Date().getTime();
                  var distance = countDownDate - now;
                  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                  document.getElementById("demo").innerHTML = days + " hari, " + hours + " jam, " + minutes + " menit, " + seconds + " detik ";
                  if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("demo").innerHTML = "";
                  }
                }, 1000);
            </script>
            <div class="text-center fs-24 text-red">Sebelum pendaftaran dan pembayaran ditutup</div>
        </div> -->

        <!-- <div class="margin-top-50"></div>

        <div>
            <a href="<?= Yii::$app->urlManager->createUrl("peserta/pendaftaran") ?>" class="button fs-25 m-fs-20 padding-10 padding-x-30 fw-light border-thin border-lightest bg-orange text-lightest rounded-sm">Daftar Sekarang !</a>
            <span class="margin-right-5"></span>
            <?= Html::a('Konfirmasi Pembayaran', ['peserta/konfirmasi'], ['class' => 'button fs-25 m-fs-20 padding-10 padding-x-30 fw-light border-thin border-lightest bg-azure text-lightest rounded-sm']) ?>
        </div> -->
    </div>

    <div class="margin-top-50"></div>

    <div class="container padding-y-15">
        <!-- <div class="text-center padding-top-60 m-padding-top-80" id="fasilitas-peserta">
            <div class="fs-40 m-fs-30 fw-bold text-orange">Fasilitas Peserta</div>
        </div>
        <div class="margin-top-30 m-margin-top-15"></div>
        <div class="box box-space-md box-gutter box-break-md-6 box-equal fs-18 m-fs-14 text-dark text-center">
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/1 Icon App Pantau Nilai-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Early Access aplikasi Pantau Nilai UTBK pertama di Indonesia</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/2 Icon BukuSaku-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Buku Saku Tips & Trick UTBK</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/3 Icon Soal-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">100% Soal Baru dan sesuai komponen</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/4 Icon MemberPTNgo-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Premium Member Aplikasi Belajar SBMPTN</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/5 Icon KBO-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Gratis ikut kompetisi beasiswa online</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/6 Icon MAgent-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Dapat uang jajan dari program M-Agent</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/7 Icon GoodieBag-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Goodie Bag Menarik</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/8 Icon VoucherDiskon-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Voucher diskon belanja</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/9 Icon PartTime-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Tawaran part-time ketika kuliah</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/10 Icon ESertifikat-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">E-Sertifikat</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/11 Icon vouchermenginap-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Diskon menginap di hotel</h3>
            </div>
            <div class="box-3">
                <div class="padding-y-15 padding-x-15 rounded-sm" style="height: 140px;">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/icon/12 Icon Hadiah-min.png" style="height: 95%; width: auto;">
                </div>
                <div class="margin-top-15"></div>
                <h3 class="text-orange">Beasiswa, hadiah, doorprize, dll</h3>
            </div>
        </div>

        <div class="margin-top-30 m-margin-top-15"></div> -->
        
        <div class="text-center">
            <div class="fs-20 m-fs-16 fw-bold text-azure">Tunggu Apalagi...</div>
            <div class="margin-top-5"></div>
            <a href="<?= Yii::$app->urlManager->createUrl("peserta/pendaftaran") ?>" class="button fs-40 m-fs-20 padding-10 padding-x-30 fw-light border-thin border-lightest bg-orange text-lightest rounded-sm">Daftar Sekarang !</a>
        </div>

        <div class="margin-y-60 m-margin-y-30">
            <hr class="border-top border-light-azure">
        </div>

        <!-- <?php $kotas = \yii\helpers\ArrayHelper::map(\app_tryout\models\PeriodeKota::find()->where(['id_periode' => (\app_tryout\models\Periode::getPeriodeAktif())->id])->orderBy('nama')->indexBy('id')->asArray()->all(), 'id', 'nama'); ?>
        <div class="text-center padding-top-60 m-padding-top-80" id="lokasi-ujian">
            <div class="fs-40 m-fs-30 fw-bold text-orange">Kami Hadir di <?= count($kotas) ?> Kota :</div>
            <div class="fs-20 m-fs-16 fw-bold text-azure">(klik pada lokasi untuk mengetahui alamat lengkapnya)</div>
        </div>
        <div class="margin-top-30 m-margin-top-15"></div>
        <div class="box box-space-md box-gutter box-break-md-6 box-equal fs-18 m-fs-14 text-dark">
            <?php foreach ($kotas as $key => $value) : ?>
                <div class="box-3 padding-x-0">
                    <a href="<?= Yii::$app->urlManager->createUrl(["peserta/detail-kota", 'id' => $key]) ?>" modal-md="" modal-title="Detail Lokasi">
                        <img class="text-middle" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/pin.png" style="width: 10px; height: auto;">
                        <span class="text-middle fw-bold text-wrap stretch-y-sm"><?= str_replace(' - ', '<br>', $value) ?></span>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="margin-y-60 m-margin-y-30">
            <hr class="border-top border-light-azure">
        </div> -->

        <div class="text-center">
            <div class="fs-40 m-fs-30 fw-bold text-orange">8 Alasan Utama Kenapa Kamu</div>
            <div class="fs-40 m-fs-30 fw-bold text-azure">Harus Ikut MasukPTNid 2020</div>
            <div class="fs-29 m-fs-22 text-azure">Tryout Nasional UTBK SBMPTN 2020</div>
        </div>
        <div class="margin-top-30 hidden-md-less"></div>
        <div class="box box-space-md box-gutter box-break-md box-equal">
            <div class="box-3 m-padding-x-50">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/k1-v2.png" style="width: 100%; height: auto;">
            </div>
            <div class="box-3 m-padding-x-50">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/k2-min.png" style="width: 100%; height: auto;">
            </div>
            <div class="box-3 m-padding-x-50">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/k3-min.png" style="width: 100%; height: auto;">
            </div>
            <div class="box-3 m-padding-x-50">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/k4-min.png" style="width: 100%; height: auto;">
            </div>
        </div>
        <div class="margin-top-30 hidden-md-less"></div>
        <div class="box box-space-md box-gutter box-break-md box-equal">
            <div class="box-3 m-padding-x-50">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/k5-min.png" style="width: 100%; height: auto;">
            </div>
            <div class="box-3 m-padding-x-50">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/k6-min.png" style="width: 100%; height: auto;">
            </div>
            <div class="box-3 m-padding-x-50">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/k7-min.png" style="width: 100%; height: auto;">
            </div>
            <div class="box-3 m-padding-x-50">
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/k8-min.png" style="width: 100%; height: auto;">
            </div>
        </div>

        <div class="margin-y-60 m-margin-y-30">
            <hr class="border-top border-light-azure">
        </div>

        <!-- <div class="text-center padding-top-60 m-padding-top-80" id="alur-pendaftaran">
            <div class="fs-40 m-fs-30 fw-bold text-orange">ALUR PENDAFTARAN</div>
        </div>
        <div class="margin-top-30 m-margin-top-15"></div>
        <div class="margin-y-60 m-margin-y-30 text-center">
            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/prosedur-v1-min.png" style="width: 80%; height: auto;">
        </div>

        <div class="margin-y-60 m-margin-y-30">
            <hr class="border-top border-light-azure">
        </div> -->

        <!-- <div class="text-center">
            <div class="fs-40 m-fs-30 fw-bold text-orange">Statistik Jumlah Kota MasukPTN.id Pertahun</div>
        </div>
        <div class="margin-top-30 m-margin-top-15"></div>
        <div class="text-center">
            <img alt="MasukPTNid logo" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/statistik-v2-min.png" style="width:100%;max-width:600px;height:auto;">
        </div>

        <div class="margin-y-60 m-margin-y-30">
            <hr class="border-top border-light-azure">
        </div> -->

        <!-- <div class="text-center" id="faq">
            <div class="fs-40 m-fs-30 fw-bold text-orange">FAQ (Frequently Asked Questions)</div>
        </div>
        <div class="margin-top-30 m-margin-top-15"></div>
        <div class="fs-14 m-fs-13 text-dark margin-x-auto" style="max-width: 600px; width: 100%;">
            <h4>Kumpulan pertanyaan umum seputar Tryout Nasional SBMPTN-Test Center 2019</h4>
            
            <p>Sesungguhnya semua yg ada di Official Instagram dan Line @masukptnid sudah sangat jelas informasinya dan sudah berulang kami beritahukan. Namun ada saja yg masih bertanya-tanya. Kami menganggap itu adalah hal yg wajar, karena jangan sampai kalian “malu bertanya sesat di jalan”. Kami akan coba rangkum semua pertanyaan yg paling sering ditanyakan kepada kami.</p>

            <div class="text-middle fw-bold text-wrap stretch-y-sm">
                <i class="fa fa-question-circle margin-right-5"></i>
                <span class="">Ini acara apa yah ka ?</span>
            </div>
            <div class="margin-top-5"></div>
            <div class="text-middle text-wrap stretch-y-sm">
                <i class="fa fa-play margin-right-5 text-azure"></i>
                <span class="">Ini adalah ujian tulis Tryout Nasional SBMPTN-Test Center MasukPTNid berskala nasional. Tryout offline paper based atau ujian tulis uji coba yang diselenggarakan secara SERENTAK di banyak kota di Indonesia dengan peserta puluhan ribu.</span>
            </div>
            <div class="margin-top-30"></div>

            <div class="text-middle fw-bold text-wrap stretch-y-sm">
                <i class="fa fa-question-circle margin-right-5"></i>
                <span class="">Siapa penyelenggaranya ka?</span>
            </div>
            <div class="margin-top-5"></div>
            <div class="text-middle text-wrap stretch-y-sm">
                <i class="fa fa-play margin-right-5 text-azure"></i>
                <span class="">Alumni OSIS Indonesia (AOI), organisasi jaringan OSIS terbesar di Indonesia dengan anggota siswa dan alumni SMA/SMK. Anggota kami tersebar di banyak kota dan berpusat di Bandung, Jawa Barat.</span>
            </div>
            <div class="margin-top-30"></div>

            <div class="text-middle fw-bold text-wrap stretch-y-sm">
                <i class="fa fa-question-circle margin-right-5"></i>
                <span class="">Acaranya kapan ka?</span>
            </div>
            <div class="margin-top-5"></div>
            <div class="text-middle text-wrap stretch-y-sm">
                <i class="fa fa-play margin-right-5 text-azure"></i>
                <span class="">Kami selalu buat tiap tahun. Ini adalah program rutin dari AOI. Tahun 2017 kami buat di bulan April. Tahun 2018 kami buat di bulan Januari dan bulan April. Tahun 2019 kami sudah buat ditanggal 13 januari 2019 dan akan buat part 2 pada tanggal 21 april 2019.</span>
            </div>
            <div class="margin-top-30"></div>

            <div class="text-middle fw-bold text-wrap stretch-y-sm">
                <i class="fa fa-question-circle margin-right-5"></i>
                <span class="">Apakah diadakah di semua kota yg ada di Indonesia ka? Di kota saya apakah ada?</span>
            </div>
            <div class="margin-top-5"></div>
            <div class="text-middle text-wrap stretch-y-sm">
                <i class="fa fa-play margin-right-5 text-azure"></i>
                <span class="">Saat ini kami baru bisa menjangkau 66 Kota dan Kabupaten. Doakan terus kami agar bisa mewujudkan Tryout SBMPTN Serentak di semua kota yg ada di Indonesia. Aamiin. Daftar 66 Kota dan Kabupaten yg kami adakan bisa dilihat di website www.masukptn.id</span>
            </div>
            <div class="margin-top-30"></div>
        </div>

        <div class="margin-y-60 m-margin-y-30">
            <hr class="border-top border-light-azure">
        </div> -->

        <div class="text-center">
            <div class="fs-40 m-fs-30 fw-bold text-orange">Mari Berkolaborasi !</div>
        </div>
        <div class="margin-top-30 m-margin-top-15"></div>
        <p class="fs-18 text-center">
            "Semua kemungkinan bisa terjadi dengan adanya sebuah perbincangan"
        </p>
        <div class="text-center">
            <a href="mailto:partnershipmasukptn@gmail.com" class="button fs-25 m-fs-20 padding-10 padding-x-30 fw-light border-thin border-lightest bg-azure text-lightest rounded-sm">partnershipmasukptn@gmail.com</a>
        </div>

        <div class="margin-y-60 m-margin-y-30">
            <hr class="border-top border-light-azure">
        </div>
        <!--
        <div class="text-center">
            <div class="fs-40 m-fs-30 fw-bold text-orange">Previous Support By</div>
        </div>
        <div class="margin-top-30 m-margin-top-15"></div>
        <div class="box box-gutter text-center box-equal">
            <div class="box-6">
                <a href="http://pmb.ittelkom-pwt.ac.id/"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/ittp-min.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-6">
                <a href="#"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/edutama-min.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-4">
                <a href="http://sonysugemacollege.com/"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/ssc-min.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-4">
                <a href="https://mamikos.com/"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/mamikos.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-4">
                <a href="https://www.tokopedia.com/events/"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/tokopedia.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-4">
                <a href="https://www.jimshoneyid.com/"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/jimshoney.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-4">
                <a href="https://www.reddoorz.com/"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/reddoorz.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-4">
                <a href="http://anytest.id/"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/anytest.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-3">
                <a href="https://www.instagram.com/sambalebikapuas/"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/sambalebi-min.png" style="width:110%;height:auto;"></a>
            </div>
            <div class="box-3">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/tips-guru-2-min.png" style="width:45%;height:auto;"></a>
            </div>
            <div class="box-3">
                <a href="http://idcloudhost.com"><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/idcloudhost.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-3">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/coco9-min.png" style="width:65%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/hmti-min.png" style="width:130%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/mac-min.png" style="width:130%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/forasta-min.png" style="width:130%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/polibisnis.png" style="width:130%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Pangandaran2.png" style="width:65%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/q9hJ_2ei.png" style="width:65%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Tulungagung SMAN 1 Boyolangu.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Padang SMA Adabiah Padang.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Cianjur.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Bekasi Perguruan Tinggi Bina Insani.png" style="width:70%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Bogor Universitas Pakuan.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Bukittinggi SMAN 1 Bukittinggi.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Ciamis SMAN 1 Ciamis.png" style="width:65%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Cikarang.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Cirebon SMA Mandiri.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Garut Universitas Garut.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Jayapura USTJ Jayapura.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Jember SMAN 1 Jember.png" style="width:50%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Pangandaran.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/wonosobo.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Karawang Univ Buana Perjuangan.png" style="width:70%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Sumedang IKOPIN.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Logo_smk_negeri_1_klaten.png" style="width:50%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/univ balikpapan.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Batu.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Denpasar.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Lhoksumawe.png" style="width:67%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Kuningan Sekolah Tinggi Ilmu Kesehatan.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Bandar Lampung Universitas Muhammadiyah Lampung.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Lhokseumawe - Universitas malikussaleh.gif" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Logo-SMKN_1_Kota_Tangerang.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Majalengka SMAN 2 Majalengka.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/man 2 model medan.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Metro Universitas Muhammadiyah Metro.png" style="width:65%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Ngawi SMAN 2 Ngawi.png" style="width:48%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Pasuruan SMAN 2 Pasuruan.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/smk2pekalongan.png" style="width:65%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Purwakarta POLIBISNIS.png" style="width:95%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Tegal Politeknik Harapan Bersama.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Salatiga STIE Ama Salatiga.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Tangerang STIE Pembangunan.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota SemarangUniversitas 17 Agustus 1945.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/sman 1 bangko.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/sma athirah makassar.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/logo-sma-negeri-bawang.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/LOGO SMK 1 MGL.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/sman 1 majalaya.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/sman 1 lembang.png" style="width:50%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/SMAN 1 Tembilah Kota.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/sman 2 lamongan.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/SMAN 3 Tasikmalaya.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/sman 4 malang.png" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/sman 1 pekanbaru.png" style="width:65%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/sman 1 rangkasbitung.png" style="width:65%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/SMAN 5 Cimahi.png" style="width:65%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/smkn 1 bontang.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/sman 10 aceh.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/SMAN_11_Depok.png" style="width:63%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Madiun SMKN 1 Madiun.png" style="width:45%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Kebumen STIE Putra Bangsa Kebumen.png" style="width:70%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kab. Temanggung SMA 2.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Surabaya Universitas Surabaya.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/smk BISA.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/UNBAJA LOGO HD.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Kediri Universitas Islam Kadiri.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/universitas mulawarman.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Surakarta Universitas Sahid Surakarta.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Bandung 1Universitas Nurtanio (Bandara).png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/untirta cilegon.png" style="width:60%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Manado SMA 1 Manado.png" style="width:50%;height:auto;"></a>
            </div>
            <div class="box-2">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/150px-SMA_Negeri_3_Sidoarjo.gif" style="width:55%;height:auto;"></a>
            </div>
            <div class="box-3">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Kota Tangerang Selatan Institut Teknologi Indonesia.png" style="width:110%;height:auto;"></a>
            </div>
            <div class="box-3">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/kampus/uigm-min.png" style="width:85%;height:auto;"></a>
            </div>
            <div class="box-3">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/support/kampus/Kab. SubangSTIE Sutaadmadja-min.png" style="width:100%;height:auto;"></a>
            </div>
            <div class="box-3">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/Jakarta - IBN.png" style="width:90%;height:auto;"></a>
            </div>
        </div>

        <div class="margin-y-60 m-margin-y-30">
            <hr class="border-top border-light-azure">
        </div>

        <div class="text-center">
            <div class="fs-40 m-fs-30 fw-bold text-orange">Previous Media Partner<br><br></div>
        </div>
        <div class="box box-gutter text-center box-equal">
            <div class="box-12">
                <a href=""><img alt="sponsor masukptn" src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-2-2019/kampus/media part 2-min.png" style="width:90%;height:auto;"></a>
            </div>
        </div>-->

        <div class="margin-top-50"></div>
    </div>
</div>