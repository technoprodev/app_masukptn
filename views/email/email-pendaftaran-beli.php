<div style="box-sizing:border-box;padding:15px!important;color: #595959;background-color: #fafafa">
    <div style="box-sizing:border-box;width:100%;max-width:600px;margin:30px auto!important;background:#fff;border:1px solid #3376b8;border-radius: 8px">
        <div style="box-sizing:border-box;padding:15px!important;text-align:center;">
            <img alt="<?= Yii::$app->params['app.name'] ?> logo" src="https://masukptn.id/img/part-1-2020/logo-full.png" style="width:100%;max-width:300px;height:auto">
        </div>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

        <h2 style="box-sizing:border-box;margin-right:0;margin-left:0;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:5px;font-size:24px;line-height:38px;text-align:center;color:#3376b8;padding:0 30px">Selamat !<br>Kamu Berhasil Terdaftar pada</h2>
        <h1 style="box-sizing:border-box;margin-right:0;margin-left:0;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:5px;margin-bottom:5px;font-size:32px;line-height:38px;text-align:center;color:#3376b8;padding:0 30px">TRYOUT NASIONAL SBMPTN – Test Center MasukPTNid 2020</h1>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

        <div style="box-sizing:border-box;padding:15px!important;">
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;"><b style="box-sizing:border-box;font-weight:bolder;">PERHATIAN !</b> Email ini dikirim secara otomatis, membalas email ini tidak akan mendapatkan respon apapun. Harap menghubungi kami melalui <a href="https://line.me/ti/p/~@masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">OFFICIAL LINE</a> kami dengan ID : <a href="https://line.me/ti/p/~@masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">@masukptnid</a> (pakai @ dan id ya) :)</p>

            <table class="table" style="box-sizing:border-box;width:100%;margin-bottom:15px;border-collapse:collapse;">
                <thead style="box-sizing:border-box;">
                    <tr style="box-sizing:border-box;border-bottom: 1px solid #dbdbdb;">
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Nomor Peserta & Nama Peserta</th>
                        <!-- <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Nama</th> -->
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;min-width:90px!important;word-break:break-all">Kontak</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Jenis</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Lokasi</th>
                    </tr>
                </thead>
                <tbody style="box-sizing:border-box;">
                    <?php foreach ($model['transaksi']->pesertas as $key => $peserta) : ?>
                        <tr style="box-sizing:border-box;border-top: 1px solid #ededed;">
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold text-red fs-18" style="box-sizing:border-box;font-size:18px;line-height:24px;font-weight:700;color:#3376b8;"><?= $peserta->kode ?></div>
                                <div class="fw-bold fs-14" style="box-sizing:border-box;font-size:14px;line-height:20px;font-weight:700;"><?= $peserta->nama ?></div>
                            </td>
                            <!-- <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            </td> -->
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;min-width:90px!important;word-break:break-all">
                                <div style="box-sizing:border-box;"><?= $peserta->email ?></div>
                                <div style="box-sizing:border-box;"><?= $peserta->handphone ?></div>
                            </td>
                            <!-- <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= false && $peserta->periodeJenis->nama ?></div>
                                <div style="box-sizing:border-box;">
                                    <span class="text-gray" style="box-sizing:border-box;color:#878787;">Rp </span>
                                    <span class="text-spring" style="box-sizing:border-box;color:#33b876;"><?= false && number_format($peserta->harga, 2) ?></span>
                                </div>
                            </td> -->
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $peserta->periodeKota->nama ?></div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

            <!-- <h2 style="box-sizing:border-box;margin-right:0;margin-left:0;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:5px;font-size:24px;line-height:38px;text-align:center;color:#3376b8;padding:0 30px">Yang harus dilakukan sekarang :</h2> -->

            <!-- <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Menunggu pengumuman pembayaran di instagram masukptnid</p>
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;"><a href="https://www.instagram.com/masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">https://www.instagram.com/masukptnid</a></p> -->

            <h2 style="box-sizing:border-box;margin-right:0;margin-left:0;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:5px;font-size:24px;line-height:38px;text-align:center;color:#3376b8;padding:0 30px">Yang harus dilakukan sekarang :</h2>
            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;text-align:center;text-decoration:underline;">Melakukan Pembayaran melalui TOKOPEDIA</h4>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Segera lakukan pembayaran melalui website <a href="https://www.tokopedia.com/events/detail/masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">TOKOPEDIA</a>, <span class="fw-bold" style="font-weight:700;">maksimal 1x24 jam</span> melalui link berikut: <a href="https://www.tokopedia.com/events/detail/masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">https://www.tokopedia.com/events/detail/masukptnid</a></p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Dengan membayar melaui <a href="https://www.tokopedia.com/events/detail/masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">TOKOPEDIA</a>, kamu dapat memilih metode pembayaran menggunakan: <span class="fw-bold text-red" style="font-weight:700;color:#3376b8;">Transfer ATM, Internet / Mobile Banking, Indomaret, Alfamart, dll</span></p>

            <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;text-align:center;text-decoration:underline;">Lakukan ini setelah melakukan pembayaran :</h4>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Setelah melakukan pembayaran, kamu <span class="fw-bold" style="font-weight:700;">WAJIB</span> melakukan konfirmasi pembayaran dengan cara login terlebih dahaulu disini:<br> <a href="https://masukptn.id/peserta/login" class="button border-azure bg-azure text-lightest rounded-sm" style="box-sizing:border-box;text-decoration:none;cursor: pointer;color: white;background-color: #3376b8;border: 1px solid #3376b8;border-radius: 8px;display: inline-block;text-align: center;white-space: nowrap;padding: 5px 15px;margin-top:5px;font-size: 18px;">Login Peserta</a><br><br>Lakukan konfirmasi pembayaran <span class="fw-bold" style="font-weight:700;">maksimal 1x24 jam</span> setelah pembayaran</p>

            <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;text-align:center;text-decoration:underline;">Punya Pertanyaan ?</h4>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Pertanyaan lebih lanjut dapat ditanyakan melalui akun LINE:<br><a href="https://line.me/ti/p/~@masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">@masukptnid</a> (pakai @ dan id ya), atau klik <a href="https://line.me/ti/p/~@masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">disini</a></p>

            <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;text-align:center;"><span style="color:#b83333;">Scroll kebawah</span> untuk melihat prosedur pendaftaran dan <br> prosedur pembayaran melalui Tokopedia</h4>
        </div>
        <div style="box-sizing:border-box;padding:15px!important;text-align:center;">
            <img alt="<?= Yii::$app->params['app.name'] ?> logo" src="https://masukptn.id/img/part-1-2020/footer-v2.png" style="width:100%;height:auto">
        </div>
    </div>

    <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">
    
    <div style="box-sizing:border-box;width:100%;max-width:600px;margin:30px auto!important;">
        <div style="box-sizing:border-box;text-align:center;">
            <img alt="<?= Yii::$app->params['app.name'] ?> logo" src="https://masukptn.id/img/prosedur-pendaftaran-v1.png" style="width:100%;height:auto">
        </div>
    </div>
</div>