<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div style="box-sizing:border-box;padding:15px!important;color: #595959;background-color: #fafafa">
    <div style="box-sizing:border-box;width:100%;max-width:600px;margin:30px auto!important;background:#fff;border:1px solid #3376b8;border-radius: 8px">
        <div style="box-sizing:border-box;padding:15px!important;text-align:center;">
            <img alt="<?= Yii::$app->params['app.name'] ?> logo" src="https://masukptn.id/img/part-1-2020/logo-full.png" style="width:100%;max-width:300px;height:auto">
        </div>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

        <h1 style="box-sizing:border-box;margin-right:0;margin-left:0;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:32px;line-height:38px;text-align:center;color:#3376b8;padding:0 30px;text-align:center;">Selamat Bergabung!</h1>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

        <div style="box-sizing:border-box;padding:15px!important;">
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Selamat!<br>Konfirmasi pembayaran kamu telah sukses terverifikasi oleh tim kami.<br>Silahkan download dan print kartu ujian mu pada tombol dibawah:</p>

            <table class="table" style="box-sizing:border-box;width:100%;margin-bottom:15px;border-collapse:collapse;">
                <thead style="box-sizing:border-box;">
                    <tr style="box-sizing:border-box;border-bottom: 1px solid #dbdbdb;">
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Nomor Peserta & Nama Peserta</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Username & Password</th>
                        <!-- <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Nama</th> -->
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Jenis</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Lokasi</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">E-Tiket</th>
                    </tr>
                </thead>
                <tbody style="box-sizing:border-box;">
                    <?php foreach ($model['transaksi']->pesertas as $key => $peserta) : ?>
                        <tr style="box-sizing:border-box;border-top: 1px solid #ededed;">
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold text-red fs-18" style="box-sizing:border-box;font-size:18px;line-height:24px;font-weight:700;color:#3376b8;"><?= $peserta->kode ?></div>
                                <div class="fw-bold fs-14" style="box-sizing:border-box;font-size:14px;line-height:20px;font-weight:700;"><?= $peserta->nama ?></div>
                            </td>
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;line-height:24px;font-weight:700;"><?= $peserta->username ?></div>
                                <div class="" style="box-sizing:border-box;line-height:20px;"><?= $peserta->password ?></div>
                            </td>
                            <!-- <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            </td> -->
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $peserta->periodeJenis->nama ?></div>
                                <div style="box-sizing:border-box;">
                                    <span class="text-gray" style="box-sizing:border-box;color:#878787;">Rp </span>
                                    <span class="text-spring" style="box-sizing:border-box;color:#33b876;"><?= number_format($peserta->harga, 2) ?></span>
                                </div>
                            </td>
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $peserta->periodeKota->nama ?></div>
                            </td>
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <a href="<?= Url::toRoute(['peserta/download-kartu-ujian', 'kode' => $peserta->kode, 'email' => $peserta->email], true); ?>" class="button button-sm border-azure text-azure" style="cursor:pointer;color:#3376b8;border:1px solid #3376b8;padding: 5px 5px;font-size: 13px;display: inline-block;line-height: inherit;text-align: center;white-space: nowrap;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;background-color: transparent;text-decoration: none;">Download</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Harap membawa kartu ujian mu dalam bentuk cetak pada hari H pelaksanaan acara untuk melancarkan proses daftar ulang. (jenis kertas dan ukuran kertas bebas).</p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Anda juga dapat download E-Tiket Peserta di <a href="https://masukptn.id/peserta/login" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">disini</a> atau salin tautan ini https://masukptn.id/peserta/login</p>

            <?php if (isset($model['peserta']->catatan)) : ?>
                <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Catatan tambahan dari Admin:<br>"<?= $model['peserta']->catatan ?>"</p>
            <?php endif; ?>

            <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">
            
            <!--
            
            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;text-align:center;">Gratis Bimbel Online SSC</h4>
            
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Selamat kamu mendapatkan Free Bimbel ONLINE dari Sony Sugema College<br>Berikut link untuk melakukan Bimbel Online <br>http://online.sonysugemacollege.com/ <br>Data Login IPA/Saintek = Notes : <b>10001</b>, PIN : <b>1010</b><br>Data Login IPS/Soshum = Notes : <b>10002</b>, PIN : <b>2020</b></p>
            
            <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">
            
            -->

            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;text-align:center;">Punya Pertanyaan ?</h4>
            
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Pertanyaan lebih lanjut dapat ditanyakan melalui akun LINE:<br><a href="https://line.me/ti/p/~@masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">@masukptnid</a> (pakai @ dan id ya), atau klik <a href="https://line.me/ti/p/~@masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">disini</a></p>
        </div>
        <div style="box-sizing:border-box;padding:15px!important;text-align:center;">
            <img alt="<?= Yii::$app->params['app.name'] ?> logo" src="https://masukptn.id/img/part-1-2020/footer-v2.png" style="width:100%;height:auto">
        </div>
    </div>
</div>