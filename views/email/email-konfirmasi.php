<div style="box-sizing:border-box;padding:15px!important;color: #595959;background-color: #fafafa">
    <div style="box-sizing:border-box;width:100%;max-width:600px;margin:30px auto!important;background:#fff;border:1px solid #3376b8;border-radius: 8px">
        <div style="box-sizing:border-box;padding:15px!important;text-align:center;">
            <img alt="<?= Yii::$app->params['app.name'] ?> logo" src="https://masukptn.id/img/part-1-2020/logo-full.png" style="width:100%;max-width:300px;height:auto">
        </div>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

        <h1 style="box-sizing:border-box;margin-right:0;margin-left:0;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:32px;line-height:38px;text-align:center;color:#3376b8;padding:0 30px;text-align:center;">Konfirmasi Pembayaran Sedang Diproses</h1>

        <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

        <div style="box-sizing:border-box;padding:15px!important;">
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Anda telah melakukan konfirmasi pembayaran untuk tiket :</p>

            <ul class="list-unstyled" style="box-sizing:border-box;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;padding-left:0;list-style-type:none;list-style-position:outside;list-style-image:none;">
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Total tiket :</span>
                    <span class="fw-bold text-dark" style="font-weight:700;color:#363636;"><?= $model['transaksi']->jumlah_tiket ?></span>
                </li>
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Total tagihan :</span>
                    <span class="fw-bold text-spring" style="font-weight:700;color:#33b876;">Rp <?= number_format($model['transaksi']->tagihan, 2) ?></span>
                </li>
                <li style="box-sizing:border-box;">
                    <span class="inline-block" style="display:inline-block;width: 150px;">Status pembayaran :</span>
                    <span class="fw-bold text-orange" style="font-weight:700;color:#b87633;"><?= $model['transaksi']->status_bayar ?></span>
                </li>
            </ul>

            <table class="table" style="box-sizing:border-box;width:100%;margin-bottom:15px;border-collapse:collapse;">
                <thead style="box-sizing:border-box;">
                    <tr style="box-sizing:border-box;border-bottom: 1px solid #dbdbdb;">
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Nomor Peserta & Nama Peserta</th>
                        <!-- <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Nama</th> -->
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;min-width:90px!important;word-break:break-all">Kontak</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Jenis</th>
                        <th style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;font-weight:inherit;text-align:left;vertical-align:bottom;">Lokasi</th>
                    </tr>
                </thead>
                <tbody style="box-sizing:border-box;">
                    <?php foreach ($model['transaksi']->pesertas as $key => $peserta) : ?>
                        <tr style="box-sizing:border-box;border-top: 1px solid #ededed;">
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold text-red fs-18" style="box-sizing:border-box;font-size:18px;line-height:24px;font-weight:700;color:#3376b8;"><?= $peserta->kode ?></div>
                                <div class="fw-bold fs-14" style="box-sizing:border-box;font-size:14px;line-height:20px;font-weight:700;"><?= $peserta->nama ?></div>
                            </td>
                            <!-- <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                            </td> -->
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;min-width:90px!important;word-break:break-all">
                                <div style="box-sizing:border-box;"><?= $peserta->email ?></div>
                                <div style="box-sizing:border-box;"><?= $peserta->handphone ?></div>
                            </td>
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $peserta->periodeJenis->nama ?></div>
                                <div style="box-sizing:border-box;">
                                    <span class="text-gray" style="box-sizing:border-box;color:#878787;">Rp </span>
                                    <span class="text-spring" style="box-sizing:border-box;color:#33b876;"><?= number_format($peserta->harga, 2) ?></span>
                                </div>
                            </td>
                            <td style="box-sizing:border-box;padding-top:10px;padding-bottom:10px;padding-right:5px;padding-left:5px;vertical-align:top;">
                                <div class="fw-bold" style="box-sizing:border-box;font-weight:700;"><?= $peserta->periodeKota->nama ?></div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Proses pengecekan konfirmasi anda, akan segera dilakukan paling lama 2x24 jam setelah anda melakukan permintaan konfirmasi ini.</p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Anda akan segera menerima balasan e-mail baru dari kami ketika pembayaran sukses terverifikasi.</p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Anda juga dapat mengecek status pembayaran di <a href="https://masukptn.id/peserta/login" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">disini</a> atau salin tautan ini https://masukptn.id/peserta/login untuk melihat apakah pembayaran sudah sukses terverifikasi. :)</p>

            <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;text-align:center;">Punya Pertanyaan ?</h4>
            
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Pertanyaan lebih lanjut dapat ditanyakan melalui akun LINE:<br><a href="https://line.me/ti/p/~@masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">@masukptnid</a> (pakai @ dan id ya), atau klik <a href="https://line.me/ti/p/~@masukptnid" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">disini</a></p>

            <hr style="box-sizing: content-box;height:0;margin-top:15px;margin-bottom:15px;border:0;border-top: 1px solid #dbdbdb">

            <h4 style="box-sizing:border-box;max-width:100%;font-weight:700;word-break:normal;word-wrap:break-word;margin-top:15px;margin-bottom:15px;font-size:16px;line-height:22px;text-align:center;">SELAMAT!</h4>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Kamu berkesempatan untuk menjadi M.AGENT dan dapat uang tunai, tiket gratis, dan masih banyak lagi!</p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Sukses dengan M.AGENT Batch-1 masukptnid membuka pendaftaran M.AGENT batch-2, mau?</p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">DAFTAR di :<br><a href="bit.ly/magentbatch2" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">bit.ly/magentbatch2</a></p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">Apa itu M.AGENT?<br>Klik:<br><a href="https://www.instagram.com/p/BuDjEv0n2sc/" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">https://www.instagram.com/p/BuDjEv0n2sc/</a></p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">DAFTAR di :<br><a href="bit.ly/magentbatch2" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">bit.ly/magentbatch2</a></p>

            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;">KUOTA TERBATAS!</p>
        </div>
        <div style="box-sizing:border-box;padding:15px!important;text-align:center;">
            <img alt="<?= Yii::$app->params['app.name'] ?> logo" src="https://masukptn.id/img/part-1-2020/footer-v2.png" style="width:100%;height:auto">
        </div>
    </div>
</div>