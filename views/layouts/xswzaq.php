<?php

use app_masukptn\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
if (Yii::$app->session->hasFlash('info'))
    $this->registerJs(
        'fn.alert("Info", "' . Yii::$app->session->getFlash('info') . '", "info");',
        3
    );
if (Yii::$app->session->hasFlash('warning'))
    $this->registerJs(
        'fn.alert("Warning", "' . Yii::$app->session->getFlash('warning') . '", "warning");',
        3
    );
if (Yii::$app->session->hasFlash('error'))
    $this->registerJs(
        'fn.alert("Error", "' . Yii::$app->session->getFlash('error') . '", "danger");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <meta name="description" content="<?= isset($this->description) ? $this->description : Yii::$app->params['app.description'] ?>">
        <link rel="icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="device-width, height=device-height, initial-scale=1, minimum-scale=1">
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <div class="wrapper">
            <div class="header bg-dark-azure hidden-sm-less">
                <div class="container">
                    <div class="pull-left padding-top-15">
                        <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="a-nocolor">
                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="30px;" class="circle padding-5 margin-right-5 bg-lightest text-middle">
                            <span class="fw-bold fs-18 text-middle"><?= Yii::$app->params['app.name'] ?></span>
                        </a>
                    </div>
                    <?php if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['webmaster'])) : ?>
                        <?php $menu['allMenu'] = [
                            [
                                'label' => '<i class="fa fa-rocket margin-right-5"></i> Dashboard',
                                'url' => ['xswzaq/index'],
                                'visible' => Menu::menuVisible(true, 'xswzaq', 'index'),
                            ],
                            [
                                'label' => '<i class="fa fa-users margin-right-5"></i> Peserta',
                                'url' => ['xswzaq/peserta'],
                                'visible' => Menu::menuVisible(true, 'xswzaq', 'peserta'),
                            ],
                            [
                                'label' => '<i class="fa fa-user margin-right-5"></i> PIC',
                                'url' => ['xswzaq/pic'],
                                'visible' => Menu::menuVisible(true, 'xswzaq', 'pic'),
                            ],
                            [
                                'label' => '<i class="fa fa-user margin-right-5"></i> Referral',
                                'url' => ['xswzaq/referral-agent'],
                                'visible' => Menu::menuVisible(true, 'xswzaq', 'referral-agent'),
                            ],
                            [
                                'label' => '<i class="fa fa-cog margin-right-5"></i> Setting',
                                'url' => ['xswzaq/setting'],
                                'visible' => Menu::menuVisible(true, 'xswzaq', 'setting'),
                            ],
                            [
                                'label' => '<i class="fa fa-user margin-right-5"></i> Logout',
                                'url' => ['xswzaq/logout'],
                                'visible' => Menu::menuVisible(true, 'xswzaq', 'logout'),
                            ],
                        ]; ?>
                    <?php elseif (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->userAdmin->identity->id)['operator'])) : ?>
                        <?php $menu['allMenu'] = [
                            [
                                'label' => '<i class="fa fa-users margin-right-5"></i> Peserta',
                                'url' => ['xswzaq/peserta'],
                                'visible' => Menu::menuVisible(true, 'xswzaq', 'peserta'),
                            ],
                            [
                                'label' => '<i class="fa fa-user margin-right-5"></i> Logout',
                                'url' => ['xswzaq/logout'],
                                'visible' => Menu::menuVisible(true, 'xswzaq', 'logout'),
                            ],
                        ]; ?>
                    <?php endif; ?>
                    <?= MenuWidget::widget([
                        'items' => $menu['allMenu'],
                        'options' => [
                            'class' => 'pull-right menu-x menu-space-y-20 fs-14
                                menu-hover-bg-lighter menu-hover-text-dark-azure menu-active-bg-lightest menu-active-text-dark-azure
                                submenu-text-dark-azure submenu-hover-bg-lighter submenu-hover-text-dark-azure submenu-active-bg-lightest submenu-active-text-dark-azure',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]); ?>
                </div>
            </div>

            <div class="header visible-sm-less">
                <div class="text-center padding-y-10 bg-dark-azure darker-20">
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="a-nocolor">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="30px;" class="circle padding-5 margin-right-5 bg-lightest text-middle">
                        <span class="fw-bold fs-18 text-middle"><?= Yii::$app->params['app.name'] ?></span>
                    </a>
                </div>
                <div class="scroll-x">
                <div class="bg-dark-azure text-center padding-top-15">
                    <?= MenuWidget::widget([
                        'items' => $menu['allMenu'],
                        'options' => [
                            'class' => 'menu-x menu-space-y-10 fs-13 inline-block margin-bottom-min-5
                                menu-active-border-lightest
                                submenu-text-dark-azure submenu-hover-bg-lighter submenu-hover-text-dark-azure submenu-active-bg-lightest submenu-active-text-dark-azure',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]); ?>
                </div></div>
            </div>

            <div class="body">
                <div class="page-wrapper">
                    <?= $content ?>
                </div>
            </div>

            <footer class="footer">
                <div class="bg-darkest">
                    <div class="container padding-y-30 m-text-center">
                        2018 © All Rights Reserved - <a href="" class="a-nocolor">Privacy Policy</a> | <a href="" class="a-nocolor">Terms of Use</a> | <a href="" class="a-nocolor">License</a> | <a href="" class="a-nocolor">Support</a>
                        <div class="pull-right m-pull-none m-margin-top-20 fs-italic"><?= Yii::$app->params['app.name'] ?> - <?= Yii::$app->params['app.description'] ?></div>
                    </div>
                </div>
            </footer>
        </div>

        <?php if (YII_ENV == 'prod') : ?>
            <div class="back-to-top bg-lighter fs-13 border-azure text-center text-azure rounded-xs padding-y-10" style="width:40px; height:40px;">▲</div>
        <?php elseif (YII_ENV == 'dev') : ?>
            <div class="back-to-top bg-lighter fs-13 border-red text-center text-red rounded-xs padding-y-10" style="width:40px; height:40px;">▲</div>
        <?php endif; ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>