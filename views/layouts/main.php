<?php

use app_masukptn\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
if (Yii::$app->session->hasFlash('info'))
    $this->registerJs(
        'fn.alert("Info", "' . Yii::$app->session->getFlash('info') . '", "info");',
        3
    );
if (Yii::$app->session->hasFlash('warning'))
    $this->registerJs(
        'fn.alert("Warning", "' . Yii::$app->session->getFlash('warning') . '", "warning");',
        3
    );
if (Yii::$app->session->hasFlash('error'))
    $this->registerJs(
        'fn.alert("Error", "' . Yii::$app->session->getFlash('error') . '", "danger");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <meta name="description" content="<?= isset($this->description) ? $this->description : Yii::$app->params['app.description'] ?>">
        <link rel="icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="device-width, height=device-height, initial-scale=1, minimum-scale=1">
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
        <style type="text/css">
            .bg-azure {
                background-color: #0FAAD2;
            }
            .text-azure {
                color: #0FAAD2;
            }
            .bg-orange {
                background-color: #FF7708;
            }
            .text-orange {
                color: #FF7708;
            }
            @keyframes blink {
              50% {
                opacity: 0.0;
              }
            }
            @-webkit-keyframes blink {
              50% {
                opacity: 0.0;
              }
            }
            .blink {
              animation: blink 2s step-start 0s infinite;
              -webkit-animation: blink 2s step-start 0s infinite;
            }
            @font-face {
              font-family: 'cabin';
              font-style: normal;
              font-weight: 400;
              src: local('Cabin'), local('Cabin-Regular'), url(font/Cabin-Regular.ttf);
            }
            .ff-cabin {
              font-family: "cabin", Arial, sans-serif;
              font-size: 13px;
            }
            .custom-chat {
                position: fixed;
                right: -10px;
                bottom: 3px;
                z-index: 5;
                cursor: pointer;
                width:180px;
                height:180px;
            }
            @media (max-width: 767px) {
                .custom-chat {
                    width:80px;
                    height:80px;
                }
            }
            .form-label {
                color: #666;
            }
            .form-text,
            .form-textarea,
            .form-dropdown {
                /*background: #d6ebed;*/
                /*background: #e9f3f4;*/
                border: 1px solid #666;
            }
            .form-text:focus,
            .form-textarea:focus,
            .form-dropdown:focus {
                outline: none;
                border-color: #9ecaed;
                -webkit-box-shadow: 0px 0px 10px #9ecaed;
                -moz-box-shadow: 0px 0px 10px #9ecaed;
                box-shadow: 0px 0px 10px #9ecaed;
                -webkit-transition: .5s;
                -moz-transition: .5s;
                -o-transition: .5s;
                -ms-transition: .5s;
                transition: .5s;
            }
            .form-dropdown [disabled] {
                color: #bbb;
            }
            .has-error .form-text:focus,
            .has-error .form-textarea:focus,
            .has-error .form-dropdown:focus {
                -webkit-box-shadow: 0px 0px 10px rgba(242,0,0,0.5);
                -moz-box-shadow: 0px 0px 10px rgba(242,0,0,0.5);
                box-shadow: 0px 0px 10px rgba(242,0,0,0.5);
                border-color: rgba(242,0,0,0.5) !important;
            }
            .menu-x.menu-border > li + li > a {
                border-left: 1px solid #FF7708;
            }
            .menu-x.submenu-border > li li + li > a {
                border-top: 1px solid #FF7708;
            }
            .menu-space-x-10 > li > a {
              padding-right: 10px;
              padding-left: 10px;
            }
        </style>
    </head>

    <body>
        <?php $this->beginBody() ?>
        
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <div class="wrapper has-bg-img">
            <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/bg.png');"></div>
            <div class="bg-img" style="background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-1-2020/bg-awan.png');background-size: 100% auto; background-repeat: no-repeat; background-position: center top"></div>

            <div class="hidden-sm-less">
            <div class="header text-uppercase fs-16 text-orange fixed-on-scroll" data-technoart-addition="bg-lightest">
                <div class="container">
                    <div class="pull-left padding-top-5">
                        <a href="<?= Yii::$app->urlManager->createUrl('site/index') ?>" class="a-nocolor">
                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-min.png" width="50px;" class="">
                            <!-- circle padding-5 margin-right-5 bg-lightest text-middle -->
                            <!-- <span class="fw-bold fs-18 text-middle"><?= Yii::$app->params['app.name'] ?></span> -->
                        </a>
                    </div>
                    <?php if (Yii::$app->controller->id == 'pic') : ?>
                        <?php if (Yii::$app->userPic->isGuest) : ?>
                            <?php $menu['allMenu'] = [
                                [
                                    'label' => 'Login PIC',
                                    'url' => ['pic/login'],
                                    'visible' => Menu::menuVisible(true, 'pic', 'login'),
                                ],
                            ]; ?>
                        <?php else : ?>
                            <?php $menu['allMenu'] = [
                                [
                                    'label' => 'Halaman PIC',
                                    'url' => ['pic/dashboard'],
                                    'visible' => Menu::menuVisible(true, 'pic', 'dashboard'),
                                ],
                                [
                                    'label' => 'Logout',
                                    'url' => ['pic/logout'],
                                    'visible' => Menu::menuVisible(true, 'pic', 'logout'),
                                ],
                            ]; ?>
                        <?php endif; ?>
                    <?php elseif (Yii::$app->controller->id == 'referral') : ?>
                        <?php if (Yii::$app->userPic->isGuest) : ?>
                            <?php $menu['allMenu'] = [
                                [
                                    'label' => 'Login Referral',
                                    'url' => ['referral/login'],
                                    'visible' => Menu::menuVisible(true, 'referral', 'login'),
                                ],
                            ]; ?>
                        <?php else : ?>
                            <?php $menu['allMenu'] = [
                                [
                                    'label' => 'Halaman Referral',
                                    'url' => ['referral/dashboard'],
                                    'visible' => Menu::menuVisible(true, 'referral', 'dashboard'),
                                ],
                                [
                                    'label' => 'Logout',
                                    'url' => ['referral/logout'],
                                    'visible' => Menu::menuVisible(true, 'referral', 'logout'),
                                ],
                            ]; ?>
                        <?php endif; ?>
                    <?php else : ?>
                        <?php if (Yii::$app->user->isGuest) : ?>
                            <?php $menu['allMenu'] = [
                                [
                                    'label' => 'Pendaftaran',
                                    'url' => ['peserta/pendaftaran'],
                                    'visible' => Menu::menuVisible(true, 'peserta', 'pendaftaran'),
                                ],
                                [
                                    'label' => 'Lokasi Ujian',
                                    'url' => ['general/kota'],
                                    'visible' => Menu::menuVisible(true, 'general', 'kota'),
                                ],
                                [
                                    'label' => 'Login',
                                    'url' => ['peserta/login'],
                                    'visible' => Menu::menuVisible(true, 'peserta', 'login'),
                                ],
                            ]; ?>
                        <?php else : ?>
                            <?php $menu['allMenu'] = [
                                [
                                    'label' => 'Halaman Peserta',
                                    'url' => ['peserta/dashboard'],
                                    'visible' => Menu::menuVisible(true, 'peserta', 'dashboard'),
                                ],
                                [
                                    'label' => 'Pembahasan',
                                    'url' => ['pembahasan/index'],
                                    'visible' => Menu::menuVisible(true, 'pembahasan', 'index'),
                                ],
                                [
                                    'label' => 'Pengumuman',
                                    'url' => ['pengumuman/index'],
                                    'visible' => Menu::menuVisible(true, 'pengumuman', 'index'),
                                ],
                                [
                                    'label' => 'Lokasi Ujian',
                                    'url' => ['general/kota'],
                                    'visible' => Menu::menuVisible(true, 'general', 'kota'),
                                ],
                                [
                                    'label' => 'Logout',
                                    'url' => ['peserta/logout'],
                                    'visible' => Menu::menuVisible(true, 'peserta', 'logout'),
                                ],
                            ]; ?>
                        <?php endif; ?>
                    <?php endif; ?>


                    <?= MenuWidget::widget([
                        'items' => $menu['allMenu'],
                        'options' => [
                            'class' => 'pull-right menu-x menu-space-y-20 menu-space-x-10 fs-14
                                menu-active-bg-lightest menu-active-text-azure',
                        ],
                        'activateItems' => true,
                        'openParents' => true,
                        'parentsCssClass' => 'has-submenu',
                        'encodeLabels' => false,
                        'labelTemplate' => '<a>{label}</a>',
                        'hideEmptyItems' => true,
                    ]); ?>

                    <?php if (false) : ?>
                        <?php if ($this->context->id == 'pic') : ?>
                            <?php $menu['pic'] = [
                                [
                                    'label' => isset(Yii::$app->params['logoutPic']) && Yii::$app->params['logoutPic'] ? 'Halaman PIC' : 'Login PIC',
                                    'url' => isset(Yii::$app->params['logoutPic']) && Yii::$app->params['logoutPic'] ? ['pic/index', 'kode' => Yii::$app->params['kodePic'], 'username' => Yii::$app->params['usernamePic']] : ['pic/index'],
                                    'visible' => Menu::menuVisible(true, 'pic', 'index'),
                                ],
                                [
                                    'label' => 'Logout',
                                    'url' => ['site/index'],
                                    'visible' => Menu::menuVisible(isset(Yii::$app->params['logoutPic']) && Yii::$app->params['logoutPic'] ? true : false, 'site', 'index'),
                                ],
                            ]; ?>
                            <?= MenuWidget::widget([
                                'items' => $menu['pic'],
                                'options' => [
                                    'class' => 'pull-right menu-x menu-space-y-20 menu-space-x-10 fs-14
                                        menu-active-bg-lightest menu-active-text-azure',
                                ],
                                'activateItems' => true,
                                'openParents' => true,
                                'parentsCssClass' => 'has-submenu',
                                'encodeLabels' => false,
                                'labelTemplate' => '<a>{label}</a>',
                                'hideEmptyItems' => true,
                            ]); ?>
                        <?php else : ?>
                            <?php $menu['header'] = [
                                /*[
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Pendaftaran belum dibuka',
                                    'icon' => null,
                                    'url' => null,
                                    'url_controller' => 'site',
                                    'url_action' => 'index',
                                    'param_key_1' => null,
                                    'param_value_1' => null,
                                    'param_key_2' => null,
                                    'param_value_2' => null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],*/
                                /*[
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Lokasi <span class="hidden-sm-less">Ujian</span>',
                                    'icon' => null,
                                    'url' => 'https://masukptn.id/#lokasi-ujian',
                                    'url_controller' => null,
                                    'url_action' => null,
                                    'param_key_1' => null,
                                    'param_value_1' => null,
                                    'param_key_2' => null,
                                    'param_value_2' => null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],
                                [
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Fasilitas <span class="hidden-sm-less">Peserta</span>',
                                    'icon' => null,
                                    'url' => 'https://masukptn.id/#fasilitas-peserta',
                                    'url_controller' => null,
                                    'url_action' => null,
                                    'param_key_1' => null,
                                    'param_value_1' => null,
                                    'param_key_2' => null,
                                    'param_value_2' => null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],
                                [
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Cara Daftar',
                                    'icon' => null,
                                    'url' => 'https://masukptn.id/#alur-pendaftaran',
                                    'url_controller' => null,
                                    'url_action' => null,
                                    'param_key_1' => null,
                                    'param_value_1' => null,
                                    'param_key_2' => null,
                                    'param_value_2' => null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],
                                [
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Alumni',
                                    'icon' => null,
                                    'url' => null,
                                    'url_controller' => 'alumni',
                                    'url_action' => 'index',
                                    'param_key_1' => null,
                                    'param_value_1' => null,
                                    'param_key_2' => null,
                                    'param_value_2' => null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],
                                [
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Pertanyaan?',
                                    'icon' => null,
                                    'url' => null,
                                    'url_controller' => 'faq',
                                    'url_action' => 'index',
                                    'param_key_1' => null,
                                    'param_value_1' => null,
                                    'param_key_2' => null,
                                    'param_value_2' => null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],*/
                                /*[
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Konfirmasi <span class="hidden-sm-less">Pembayaran</span>',
                                    'icon' => null,
                                    'url' => null,
                                    'url_controller' => 'peserta',
                                    'url_action' => 'konfirmasi',
                                    'param_key_1' => null,
                                    'param_value_1' => null,
                                    'param_key_2' => null,
                                    'param_value_2' => null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],
                                [
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Pendaftaran',
                                    'icon' => null,
                                    'url' => null,
                                    'url_controller' => 'peserta',
                                    'url_action' => 'pendaftaran',
                                    'param_key_1' => null,
                                    'param_value_1' => null,
                                    'param_key_2' => null,
                                    'param_value_2' => null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],*/
                                /*[
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Pembahasan',
                                    'icon' => null,
                                    'url' => null,
                                    'url_controller' => 'pembahasan',
                                    'url_action' => 'index',
                                    'param_key_1' => 'kode',
                                    'param_value_1' => isset(Yii::$app->params['kode']) ? Yii::$app->params['kode'] : null,
                                    'param_key_2' => 'email',
                                    'param_value_2' => isset(Yii::$app->params['email']) ? Yii::$app->params['email'] : null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],*/
                                /*[
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => 'Pengumuman',
                                    'icon' => null,
                                    'url' => null,
                                    'url_controller' => 'pengumuman',
                                    'url_action' => 'index',
                                    'param_key_1' => 'kode',
                                    'param_value_1' => isset(Yii::$app->params['kode']) ? Yii::$app->params['kode'] : null,
                                    'param_key_2' => 'email',
                                    'param_value_2' => isset(Yii::$app->params['email']) ? Yii::$app->params['email'] : null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],*/
                                /*[
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => '1',
                                    'title' => isset(Yii::$app->params['logout']) && Yii::$app->params['logout'] ? 'Halaman Peserta' : 'Login',
                                    'icon' => null,
                                    'url' => null,
                                    'url_controller' => 'peserta',
                                    'url_action' => 'login',
                                    'param_key_1' => 'kode',
                                    'param_value_1' => isset(Yii::$app->params['kode']) ? Yii::$app->params['kode'] : null,
                                    'param_key_2' => 'email',
                                    'param_value_2' => isset(Yii::$app->params['email']) ? Yii::$app->params['email'] : null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],
                                [
                                    'code' => 'peserta',
                                    'id' => '0',
                                    'parent' => null,
                                    'order' => '0',
                                    'enable' => isset(Yii::$app->params['logout']) && Yii::$app->params['logout'] ? '1' : '0',
                                    'title' => 'Logout',
                                    'icon' => null,
                                    'url' => null,
                                    'url_controller' => 'site',
                                    'url_action' => 'index',
                                    'param_key_1' => null,
                                    'param_value_1' => null,
                                    'param_key_2' => null,
                                    'param_value_2' => null,
                                    'param_key_3' => null,
                                    'param_value_3' => null,
                                ],*/
                            ]; ?>
                            <?= MenuWidget::widget([
                                'items' => Menu::menuTree($menu['header'], null, true),
                                'options' => [
                                    'class' => 'pull-right menu-x menu-space-y-20 menu-space-x-10 fs-14
                                        menu-active-bg-lightest menu-active-text-azure',
                                ],
                                'activateItems' => true,
                                'openParents' => true,
                                'parentsCssClass' => 'has-submenu',
                                'encodeLabels' => false,
                                'labelTemplate' => '<a>{label}</a>',
                                'hideEmptyItems' => true,
                            ]); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            </div>

            <div class="visible-sm-less">
            <div class="header fixed-on-scroll">
                <div class="text-center padding-y-10 bg-azure darker-20">
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="a-nocolor">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-min.png" width="30px;" class="circle padding-5 margin-right-5 bg-lightest text-middle">
                        <span class="fw-bold fs-18 text-middle"><?= Yii::$app->params['app.name'] ?></span>
                    </a>
                </div>
                <div class="bg-azure text-center">
                    <?= MenuWidget::widget([
                            'items' => $menu['allMenu'],
                            'options' => [
                                'class' => 'menu-x menu-space-y-10 menu-space-x-10 fs-13 inline-block margin-bottom-min-5
                                    menu-active-bg-lightest menu-active-text-orange',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]); ?>
                </div>
            </div>
            </div>

            <div class="body">
                <div class="page-wrapper has-bg-img">
                    <?= $content ?>
                    <footer class="footer text-lightest text-center border-darkest border-bottom">
                        <div>
                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/part-1-2020/footer-v2.png" style="width:100%;height:auto;" class="margin-0">
                        </div>
                        <div  class="padding-y-20" style="background-color:#009ed6;">
                            <!-- <table class="table table-noline margin-y-0 margin-x-auto" style="width:100%;max-width:1200px;">
                                <tr>
                                    <td>
                                        <a href="https://www.facebook.com/masukptnid">
                                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/footer1.png" style="width:100%;height:auto;">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="https://www.twitter.com/masukptnid">
                                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/footer2.png" style="width:100%;height:auto;">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="https://www.instagram.com/masukptnid">
                                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/footer3.png" style="width:100%;height:auto;">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="https://line.me/ti/p/~@masukptnid">
                                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/footer4.png" style="width:100%;height:auto;">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="https://www.masukptn.id">
                                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/footer5.png" style="width:100%;height:auto;">
                                        </a>
                                    </td>
                                </tr>
                            </table> -->
                            <div class="text-center fs-16">
                                Support by <a href="https://idcloudhost.com" class="text-lightest fw-bold">Web Hosting Indonesia</a>  - IDcloudhost
                            </div>
                        </div>
                    </footer>
                </div>
            </div>

        </div>

        <?php if (YII_ENV == 'prod') : ?>
            <div class="custom-chat"><a href="https://line.me/ti/p/~@masukptnid"><img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/chat-v1-min.png" style="width:100%;height:auto;"></a></div>
        <?php elseif (YII_ENV == 'dev') : ?>
            <div class="custom-chat"><a href="https://line.me/ti/p/~@masukptnid"><img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/chat-v1-min.png" style="width:100%;height:auto;"></a></div>
        <?php endif; ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>