<div class="ff-cabin">
    <div class="container padding-y-15">
        <!-- <div class="text-center" id="faq">
            <div class="fs-40 m-fs-30 fw-bold text-orange">FAQ</div>
        </div> -->
        <h1 class="text-center fs-50 m-fs-30 text-orange fw-bold text-wrap text-uppercase" style="color: #FF7708;"><?= $title; ?></h1>
        <div class="margin-top-30 m-margin-top-15"></div>
        <div class="fs-14 m-fs-13 text-dark margin-x-auto" style="max-width: 600px; width: 100%;">
            <h4>Kumpulan pertanyaan umum seputar Tryout Nasional SBMPTN-Test Center 2019</h4>
            
            <p>Sesungguhnya semua yg ada di Official Instagram dan Line @masukptnid sudah sangat jelas informasinya dan sudah berulang kami beritahukan. Namun ada saja yg masih bertanya-tanya. Kami menganggap itu adalah hal yg wajar, karena jangan sampai kalian “malu bertanya sesat di jalan”. Kami akan coba rangkum semua pertanyaan yg paling sering ditanyakan kepada kami.</p>

            <div class="text-middle fw-bold text-wrap stretch-y-sm">
                <i class="fa fa-question-circle margin-right-5"></i>
                <span class="">Ini acara apa yah ka ?</span>
            </div>
            <div class="margin-top-5"></div>
            <div class="text-middle text-wrap stretch-y-sm">
                <i class="fa fa-play margin-right-5 text-azure"></i>
                <span class="">Ini adalah ujian tulis Tryout Nasional SBMPTN-Test Center MasukPTNid berskala nasional. Tryout offline paper based atau ujian tulis uji coba yang diselenggarakan secara SERENTAK di banyak kota di Indonesia dengan peserta puluhan ribu.</span>
            </div>
            <div class="margin-top-30"></div>

            <div class="text-middle fw-bold text-wrap stretch-y-sm">
                <i class="fa fa-question-circle margin-right-5"></i>
                <span class="">Siapa penyelenggaranya ka?</span>
            </div>
            <div class="margin-top-5"></div>
            <div class="text-middle text-wrap stretch-y-sm">
                <i class="fa fa-play margin-right-5 text-azure"></i>
                <span class="">Alumni OSIS Indonesia (AOI), organisasi jaringan OSIS terbesar di Indonesia dengan anggota siswa dan alumni SMA/SMK. Anggota kami tersebar di banyak kota dan berpusat di Bandung, Jawa Barat.</span>
            </div>
            <div class="margin-top-30"></div>

            <div class="text-middle fw-bold text-wrap stretch-y-sm">
                <i class="fa fa-question-circle margin-right-5"></i>
                <span class="">Acaranya kapan ka?</span>
            </div>
            <div class="margin-top-5"></div>
            <div class="text-middle text-wrap stretch-y-sm">
                <i class="fa fa-play margin-right-5 text-azure"></i>
                <span class="">Kami selalu buat tiap tahun. Ini adalah program rutin dari AOI. Tahun 2017 kami buat di bulan April. Tahun 2018 kami buat di bulan Januari dan bulan April. Tahun 2019 kami sudah buat ditanggal 13 januari 2019 dan akan buat part 2 pada tanggal 21 april 2019.</span>
            </div>
            <div class="margin-top-30"></div>

            <div class="text-middle fw-bold text-wrap stretch-y-sm">
                <i class="fa fa-question-circle margin-right-5"></i>
                <span class="">Apakah diadakah di semua kota yg ada di Indonesia ka? Di kota saya apakah ada?</span>
            </div>
            <div class="margin-top-5"></div>
            <div class="text-middle text-wrap stretch-y-sm">
                <i class="fa fa-play margin-right-5 text-azure"></i>
                <span class="">Saat ini kami baru bisa menjangkau 66 Kota dan Kabupaten. Doakan terus kami agar bisa mewujudkan Tryout SBMPTN Serentak di semua kota yg ada di Indonesia. Aamiin. Daftar 66 Kota dan Kabupaten yg kami adakan bisa dilihat di website www.masukptn.id</span>
            </div>
            <div class="margin-top-30"></div>
        </div>

        <div class="margin-top-50"></div>
    </div>
</div>